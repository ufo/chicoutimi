Name:           pam_mount
Version:        0.49
Release:        3%{?dist}
Summary:        A PAM module that can mount volumes for a user session

Group:          System Environment/Base
# The library and binaries are LGPLv2+, the scripts are GPLv2+, except for convert_pam_mount_conf.pl
# convert_pam_mount_conf.pl is licensed under the same license as pam_mount itself. 
# pmt-fd0ssh is licensed under LGPLv2 or LGPLv3
License:        LGPLv2+ and GPLv2+ and (LGPLv2 or LGPLv3)
URL:            http://pam-mount.sourceforge.net/
Source0:        http://downloads.sourceforge.net/pam-mount/%{name}-%{version}.tar.lzma
#Source1:        http://downloads.sourceforge.net/pam-mount/%{name}-%{version}.tar.lzma.asc
Source2:        passwdcrypt
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

Patch0:         pam_mount.passwd.patch
 
BuildRequires:  glib2-devel, pam-devel, openssl-devel
BuildRequires:  libxml2-devel
BuildRequires:  libHX-devel >= 1.25
BuildRequires:  lzma
Requires:       pam
Requires:       psmisc
Requires:       libHX >= 1.25
Requires(post):  perl(XML::Writer)


%description
This module is aimed at environments with central file servers that a
user wishes to mount on login and unmount on logout, such as
(semi-)diskless stations where many users can logon.

The module also supports mounting local filesystems of any kind the
normal mount utility supports, with extra code to make sure certain
volumes are set up properly because often they need more than just a
mount call, such as encrypted volumes. This includes SMB/CIFS, NCP,
davfs2, FUSE, losetup crypto, dm-crypt/cryptsetup and truecrypt.

If you intend to use pam_mount to protect volumes on your computer
using an encrypted filesystem system, please know that there are many
other issues you need to consider in order to protect your data.  For
example, you probably want to disable or encrypt your swap partition.
Don't assume a system is secure without carefully considering
potential threats.


%prep
%setup -q -T -c
lzma -c -d %SOURCE0 | tar -xvvf - -C ..
%patch0 -p1

%build
%configure --with-slibdir=/%{_lib}
make %{?_smp_mflags} V=verbose


%install
rm -rf $RPM_BUILD_ROOT

make install DESTDIR=$RPM_BUILD_ROOT V=verbose

install -p scripts/convert_pam_mount_conf.pl $RPM_BUILD_ROOT/%{_bindir}
rm -f $RPM_BUILD_ROOT/%{_lib}/security/*.{a,la}

mkdir -p $RPM_BUILD_ROOT/%{_bindir}
cp %{SOURCE2} $RPM_BUILD_ROOT/%{_bindir}/passwdcrypt

%post
if [ -e %{_sysconfdir}/security/pam_mount.conf ]; then
    convert_pam_mount_conf.pl <%{_sysconfdir}/security/pam_mount.conf \
                              >%{_sysconfdir}/security/pam_mount.conf.xml;
fi;


%clean
rm -rf $RPM_BUILD_ROOT


%files
%defattr(-,root,root,-)
%doc doc/bugs.txt doc/changelog.txt doc/faq.txt doc/options.txt doc/pam_mount.txt doc/todo.txt
%doc config/pam_mount.conf.xml
%doc LICENSE.GPL2 LICENSE.GPL3
%doc LICENSE.LGPL2 LICENSE.LGPL3
%config(noreplace) %{_sysconfdir}/security/pam_mount.conf.xml
/%{_lib}/security/pam_mount.so
%{_sbindir}/pmvarrun
%{_bindir}/autoehd
%{_bindir}/convert_pam_mount_conf.pl
%{_bindir}/mkehd
%{_bindir}/mount_ehd
# passwdehd is not ported to xml config and uses an insecure tempfile
%exclude %{_bindir}/passwdehd
%{_bindir}/pmt-fd0ssh
%{_bindir}/pmt-ofl
/sbin/mount.crypt
/sbin/mount.encfs13
/sbin/umount.crypt
%{_bindir}/passwdcrypt
%{_mandir}/man?/*
%{_localstatedir}/run/pam_mount


%changelog
* Tue Nov 18 2008 Till Maas <opensource@till.name> - 0.49-2
- Remove passwdehd (does not work and uses an insecure tempfile)
- Red Hat Bug #472109

* Wed Oct 08 2008 Till Maas <opensource@till.name> - 0.49-1
- Update to new release

* Sat Sep 13 2008 Till Maas <opensource@till.name> - 0.48-2
- Update libHX dependencies
- remove lsof dependency
- Add patch to fix convert_pam_mount_conf.pl by Stephen P. Schaefer
  (Red Hat Bug 462155)

* Thu Sep 11 2008 Till Maas <opensource@till.name> - 0.48-1
- Update to new version

* Fri Sep 05 2008 Till Maas <opensource@till.name> - 0.47-1
- Update to new version that includes a security fix:
  https://sourceforge.net/project/shownotes.php?release_id=624240
- Add lzma BR and unpack source manually
- Update libHX requirements
- add new binary

* Mon Jun 23 2008 Till Maas <opensource@till.name> - 0.41-2
- Add patch to fix <or> handling in config file, reference:
  Red Hat Bugzilla #448485 comment 9
  http://sourceforge.net/tracker/index.php?func=detail&aid=1974442&group_id=41452&atid=430593
  comment from 2008-06-19 10:29

* Tue Jun 17 2008 Till Maas <opensource till name> - 0.41-1
- Update to new version

* Wed Jun 11 2008 Till Maas <opensource till name> - 0.40-1
- Update to new version
- set make variable V for full compiler commandline

* Mon May 05 2008 Till Maas <opensource till name> - 0.35-1
- Update to new version
- Use $RPM_BUILD_ROOT instead of %%{buildroot}
- Update description
- create and own %%{_localstatedir}/run/pam_mount

* Sun Feb 24 2008 Till Maas <opensource till name> - 0.33-1
- update to new version

* Wed Feb 20 2008 Fedora Release Engineering <rel-eng@fedoraproject.org> - 0.32-3
- Autorebuild for GCC 4.3

* Mon Jan 07 2008 Till Maas <opensource till name> - 0.32-2
- fix config conversion scriptlet

* Mon Jan 07 2008 Till Maas <opensource till name> - 0.32-1
- update to new version
- add default/example config to %%doc

* Wed Dec 05 2007 Release Engineering <rel-eng at fedoraproject dot org> - 0.29-2
 - Rebuild for deps

* Wed Oct 10 2007 Till Maas <opensource till name> - 0.29-1
- bump to new version
- remove uneeded patches
- add config file conversion script and convert config in %%post

* Thu Aug 23 2007 Till Maas <opensource till name> - 0.18-2
- add README.Fedora
- add patch to wait for applications that do not process pam properly
- update license tag

* Sun Dec 10 2006 Till Maas <opensource till name> - 0.18-1
- Version bump
- removed Patch0
- adding LICENSE.GPL2 to %%doc with LICENSE.*
- removed ChangeLog (is empty)
- removed NEWS (nothing interesting)
- removed INSTALL
- removed BR: zlib-devel, already in openssl-devel
- added -p to install to preserve timestamp

* Thu Sep 07 2006 Michael J. Knox <michael[AT]knox.net.nz> - 0.17-5
- Upstream update
- Build on x86_64

* Mon Aug 27 2006 Michael J. Knox <michael[AT]knox.net.nz> - 0.13.0-9
- Rebuild for FC6

* Wed Jul 05 2006 Michael J. Knox <michael[AT]knox.net.nz> - 0.13.0-8
- don't add a local loop.h for rawhide

* Wed Jul 05 2006 Michael J. Knox <michael[AT]knox.net.nz> - 0.13.0-4
- added patch for bz #188284
- spec tidy, removed unused buildreq on kernel-devel

* Wed Apr 05 2006 W. Michael Petullo <mike[@]flyn.org> - 0.13.0-3
   - Ship LICENSE.LGPL2.

* Sun Apr 02 2006 W. Michael Petullo <mike[@]flyn.org> - 0.13.0-2
   - Don't use --owner=root or --group=root on install.

* Sun Apr 02 2006 W. Michael Petullo <mike[@]flyn.org> - 0.13.0-1
   - Updated to pam_mount 0.13.0.
   - Added patch to allow to build with local loop.h (see RH BZ 174190.)
   - Ensure module installed in RPM_BUILD_ROOT when building package.

* Sun Jan 01 2006 W. Michael Petullo <mike[@]flyn.org> - 0.11.0-1
   - Updated to pam_mount 0.11.0.
   - Mike Petullo is no longer the up-stream maintainer.
   - Change URL.
   - Change Source.

* Thu Jun 09 2005 W. Michael Petullo <mike[@]flyn.org> - 0.9.25-4
   - Bump release for devel.

* Thu Jun 09 2005 W. Michael Petullo <mike[@]flyn.org> - 0.9.25-3
   - Bump release for FC-4.

* Thu Jun 09 2005 W. Michael Petullo <mike[@]flyn.org> - 0.9.25-2
   - Bump release for FC-3.

* Thu Jun 09 2005 W. Michael Petullo <mike[@]flyn.org> - 0.9.25-1
   - Updated to pam_mount 0.9.25.

* Thu Jun 09 2005 W. Michael Petullo <mike[@]flyn.org> - 0.9.25-1
   - Updated to pam_mount 0.9.25.

* Sat May 14 2005 W. Michael Petullo <mike[@]flyn.org> - 0.9.24-1
   - Updated to pam_mount 0.9.24.

* Wed May 04 2005 W. Michael Petullo <mike[@]flyn.org> - 0.9.23-1
   - Updated to pam_mount 0.9.23.

   - Remove fdr from version.

   - Get rid of rel variable.

   - %%{PACKAGE_VERSION} to %%{name}-%%{version}.

* Thu Feb 10 2005 W. Michael Petullo <mike[@]flyn.org> - 0.9.22-0.fdr.1
   - Updated to pam_mount 0.9.22.

   - Should now build properly on x86-64.

* Sun Dec 12 2004 W. Michael Petullo <mike[@]flyn.org> - 0.9.21-0.fdr.1
   - Updated to pam_mount 0.9.21.

* Fri Jul 23 2004 W. Michael Petullo <mike[@]flyn.org> - 0.9.20-0.fdr.1
   - Updated to pam_mount 0.9.20.

* Sun Jun 27 2004 W. Michael Petullo <mike[@]flyn.org> - 0.9.19-0.fdr.1
   - Updated to pam_mount 0.9.19.

   - Moved policy sources to /etc/selinux.

* Sun Apr 25 2004 W. Michael Petullo <mike[@]flyn.org> - 0.9.18-0.fdr.1
   - Updated to pam_mount 0.9.18.

   - Added mount.crypt and umount/crypt.

   - Added pmvarrun.

* Wed Apr 21 2004 W. Michael Petullo <mike[@]flyn.org> - 0.9.17-0.fdr.1
   - Updated to pam_mount 0.9.17.

   - Added pam_mount_macros.te.

* Tue Mar 23 2004 W. Michael Petullo <mike[@]flyn.org> - 0.9.16-0.fdr.1
   - Updated to pam_mount 0.9.16.

   - Ensure pam_mount.conf etc. has safe permissions (install vs. cp).

   - Don't compress documentation files.

   - Don't set distribution in .spec.

   - Remove uneeded prefix definition.

   - Fix buildroot.

* Wed Mar 10 2004 W. Michael Petullo <mike[@]flyn.org> - 0.9.15-0.fdr.1
   - Updated to pam_mount 0.9.15.

   - Added zlib-devel to BuildRequires.

* Tue Feb 10 2004 W. Michael Petullo <mike[@]flyn.org> - 0.9.14-0.fdr.1
   - Updated to pam_mount 0.9.14.

   - Added pam_mount_auth.so and pam_mount_session.so to package.

* Sat Jan 25 2004 W. Michael Petullo <mike[@]flyn.org> - 0.9.13-0.fdr.1
   - Updated to pam_mount 0.9.13.

* Sat Jan 24 2004 W. Michael Petullo <mike[@]flyn.org> - 0.9.12-0.fdr.2
   - RPM specification work.

* Fri Jan 23 2004 W. Michael Petullo <mike[@]flyn.org> - 0.9.12-0.fdr.1
   - Updated to pam_mount 0.9.12.
