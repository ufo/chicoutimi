#!/usr/bin/env python

# TODO: Add code to enable discovering of the path
#       in order to find the root of the svn

import tempfile

CODA_SRV="CHANGEME.agorabox.org"
CODA_REALM="agorabox.org"
CODA_REPLICATED_GROUP="CHANGEME.agorabox.org"
DEF_RIGHT="rwlkid"
CODA_ADM_USER="codaroot"
CODA_ADM_PASS="CHANGEME"
AUTH2_ADM_PROG="./aup"
KADMIN_USER="codaroot"
KADMIN_PASS=""
KADMIN_KEYTAB=tempfile.mkstemp()[1]

BIND_USER="root"


KRBKDC="CHANGEME.agorabox.org"
""" Key tab file to use no default """
KEYTAB_FILE="./automated.keytab"
""" Principal to used to act """
BIND_PRINCIPAL="automated_creator/gest"
""" Path to the kinit """
KINIT_PROG="/usr/kerberos/bin/kinit"
""" Path to the kadmin programme """
KADMIN_PROG="/usr/kerberos/sbin/kadmin"
KADMIN_LOCAL_PROG="/usr/kerberos/sbin/kadmin.local"


""" LDAP creation conf """
TEMPLATE_FILE="../ldap/30agoraxpiir.ldif"
PROG_CREATION_LDAP="../ldap/create_ldap_user"
PROG_DELETION_LDAP="../ldap/delete_ldap_user"
