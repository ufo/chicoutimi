#!/usr/bin/env python
# -*- coding: utf-8 -*-


from os import access,W_OK,F_OK,remove,mkdir,fdopen,write,walk,chmod,chdir,rmdir
from os.path import exists,join,curdir,abspath

from commands import getstatusoutput
from socket import getfqdn
from tempfile import mkstemp

import sys




if len(sys.argv) != 3:
    print """ 2 arguments sont necessaires pour executez le script 
./install.py -y <mot de passe admin>"""
    exit(1)

if sys.argv[1] != "-y":
    print """ Attention ce script supprime toutes configuration de coda existante !! 
    ajoutez l'option -y en premier arguments afin de proceder a la configuration.
    A vos risques et périls """

    exit(1)


_coda_adm_password=sys.argv[2]

CODACONF_ISSCM="Y"
CODACONF_SCMHOST="elko.agorabox.org"
# Limite à 8 caractere
CODACONF_TK={"update":"""todigest""",
            "auth2":"""todigest""",
            "volutil":"""todigest"""} 




CODACONF_DIRECTORY="/etc/coda"
CODACONF_CONFDIR="/vice"
CODACONF_FILE="server.conf"
CODACONF_SERVER=join(CODACONF_DIRECTORY,CODACONF_FILE)
CODACONF_VALUES={"vicedir":"/vice",
        "numservers":"1",
        "rvmtruncate":"5",
        "trace":"100",
        "allow_sha":"1",
        "kerberos5service":"coda/%s",
        "kerberos5keytab":"/etc/coda/coda.keytab"
        }

CODACONF_MAXGROUPID="2130706432"
CODACONF_SCM_ID=1
CODACONF_ROOT_ID=132
CODACONF_ROOT_NAME="codaroot"
CODACONF_PDBTOOL="/usr/sbin/pdbtool"
CODACONF_INITPW="/usr/sbin/initpw"
CODACONF_RDSINIT="/usr/sbin/rdsinit"
CODACONF_RVMUTL="/usr/sbin/rvmutl"
CODACONF_CREATEVOL_REP="./createvol_rep_no_backup"

# DATA_SIZE=805306368
# LOG_FILE=31457280
CODACONF_RVM={  "DATA_FILE"     : join(CODACONF_CONFDIR,"DATA"),
                "DATA_SIZE"     : 2147483648,
                "LOG_FILE"      : join(CODACONF_CONFDIR,"LOG"),
                "LOG_SIZE"      : 209715200, # In bytes
                "STATICSIZE"    : 1048576,  # Reserve 1mb for a static allocation area
                "RVMRESERVE"    : 65536,    # Reserver 64K for RVM overhead(need pagesize in bytes)
                "RVMSTART"      : 0x50000000,
                "NLISTS"        : 80,
                "CHUNK"         : 64
            }      
CODACONF_RVM["HEAPSIZE"] = CODACONF_RVM["DATA_SIZE"] - CODACONF_RVM["STATICSIZE"] - CODACONF_RVM["RVMRESERVE"]
CODACONF_RVM["PARMS"] = "%d 0x%x %d %d %d %d"%(CODACONF_RVM["DATA_SIZE"],CODACONF_RVM["RVMSTART"],
                                            CODACONF_RVM["HEAPSIZE"],CODACONF_RVM["STATICSIZE"],
                                            CODACONF_RVM["NLISTS"],CODACONF_RVM["CHUNK"]) 
CODACONF_VALUES_RVM={"rvm_log" : CODACONF_RVM["LOG_FILE"],
                    "rvm_data" : CODACONF_RVM["DATA_FILE"],
                    "rvm_data_length" : CODACONF_RVM["DATA_SIZE"] }

CODACONF_SRVDIR={"DATA_PART" : "/vicepa",
                "TREE" : 3, #Index for tree width and depth see below
                "FTREE" :    ["ftree width=64,depth=3", # 0 Hardcoded values do not edit
                            "ftree width=32,depth=4",   # 1
                            "ftree width=128,depth=3",  # 2
                            "ftree width=256,depth=3"], # 3
                }
        

            

CODACONF_SETUP_CLIENT=["/usr/sbin/vice-setup-rvm","/usr/sbin/vice-setup-srvdir"]
CODACONF_SETUP_SERVER=["/usr/sbin/vice-setup-scm","/usr/sbin/vice-setup-user"].append(CODACONF_SETUP_CLIENT)


CODACONF_VICEDIR=["auth2","backup","db","misc","vol","vol/remote","srv"]

CODACONF_FILES="""auth2.pw
auth2.tk
auth2.tk.BAK
auth2.lock
dumplist
files
prot_users.cdb
servers
scm
update.tk
volutil.tk
VLDB
VRDB
VRList
maxgroupid""" #a ajouter dans /db/files



INITD_DIRECTORY="/etc/init.d"
CODACONF_SERVICE="codasrv"
CODACONF_EDIT="/usr/sbin/codaconfedit"

CODACONF_UPDATEFETCH="/usr/sbin/updatefetch"
CODACONF_FETCH_FILE=["servers","auth2.pw","auth2.tk","auth2.lock","prot_users.cdb","volutil.tk","files"]



def AddToCodaConf(key,value=""):
    o,r = getstatusoutput("%s %s %s %s"%(CODACONF_EDIT,CODACONF_FILE,key,value))
    return r

def RemoveAndCreate(filename):
#    try : 
#        remove(file)
#    except OSError:
#        pass

    file(filename,"w+")
    filename.close()

def WriteInto(filename,line="",suppr=0):
    #if 0!=suppr:
    #    RemoveAndCreate(filename)

    FILE=""
    if exists(filename):
        if access(filename,W_OK):
            if 0==suppr:
                FILE=file(filename,"a")
            else:
                FILE=file(filename,"w")
        else:
            raise Exception("Impossible d'ecrire dans le fichier %s"%(filename,))
    else:
        FILE=file(filename,"w")

    FILE.write(line.strip()+"\n")
    FILE.close()
        

def InTempFileWrite(str):
    # Don't forget to remove the file once you finished with it
    tmp_fd, tmp_name=mkstemp()
    f=fdopen(tmp_fd,"w+")
    f.write(str)
    f.close()
    return tmp_name

def ClientFetch(host,remote,local):
    """ We must change directory before doing the updateclnt """
    curd = abspath(curdir)
    chdir(join(CODACONF_VALUES["vicedir"],"db"))
    status,output=getstatusoutput("%s -h %s -r %s -l %s"%(CODACONF_UPDATEFETCH,host,remote,local))
    chdir(curd)
    if status!=0:
        chdir(curd)
        raise Exception("%s Failed for %s. check %s configuration"%(CODACONF_UPDATEFETCH,local,host)) 
    
    
        

def vice_setup_scm():
    # Write first maximum replicated volume 
    print "Vice-setup-scm"
    WriteInto(join(CODACONF_VALUES["vicedir"],"db","maxgroupid"),CODACONF_MAXGROUPID)
    WriteInto(join(CODACONF_VALUES["vicedir"],"db","servers"),"%s\t\t%d"%(hostfqdn,CODACONF_SCM_ID))
    WriteInto(join(CODACONF_VALUES["vicedir"],"db","auth2.lock"),"") 
    #WriteInto(join(CODACONF_VALUES["vicedir"],"db","scm"),hostname)

    AddToCodaConf("kerberos5keytab"=CODACONF_VALUES['kerberos5keytab'])
    AddToCodaConf("rvm_log",CODACONF_RVM["LOG_FILE"])

def vice_setup_user():
    # Reinitialisation du fichier prot_users.cdb
    WriteInto(join(CODACONF_VALUES["vicedir"],"db","prot_users.cdb"),"",1)
    
    chmod("/vice/db/prot_users.cdb",600)

    tmp_name=InTempFileWrite("""nui System 1
nui %s %d
ng System:Administrators %d
ng System:AnyUser 1
"""%(CODACONF_ROOT_NAME, CODACONF_ROOT_ID, CODACONF_ROOT_ID))

    status,output=getstatusoutput("%s source %s"%(CODACONF_PDBTOOL,tmp_name))
    remove(tmp_name)
    
    tmp_name=InTempFileWrite("""%s\t%s\tadmin user"""%(CODACONF_ROOT_ID,_coda_adm_password))
    status,output=getstatusoutput("""%s -k "drseuss " < %s > %s"""%(CODACONF_INITPW,tmp_name,join(CODACONF_VALUES["vicedir"],"db","auth2.pw")))
    remove(tmp_name)


def vice_setup_rvm():
    tmp_name=InTempFileWrite("""i %s %d\nq\n"""%(CODACONF_RVM["LOG_FILE"],CODACONF_RVM["LOG_SIZE"]))
    status,output = getstatusoutput("""cat %s | %s >/dev/null"""%(tmp_name,CODACONF_RVMUTL))
    remove(tmp_name)

    status,output= getstatusoutput("""%s -f %s %s %s"""%(CODACONF_RDSINIT,CODACONF_RVM["LOG_FILE"],CODACONF_RVM["DATA_FILE"],CODACONF_RVM["PARMS"]))
    AddToCodaConf("rvm_log",CODACONF_RVM["LOG_FILE"])
    AddToCodaConf("rvm_data",CODACONF_RVM["DATA_FILE"])
    AddToCodaConf("rvm_data_length",CODACONF_RVM["DATA_SIZE"])


def vice_setup_srvdir():
    for root,dirs,files in walk(CODACONF_SRVDIR["DATA_PART"],False):
        for filer in files:
            remove(join(root,filer))

        rmdir(root)

    status,output = getstatusoutput("mkdir %s"%(CODACONF_SRVDIR["DATA_PART"],))
    status,output = getstatusoutput("touch %s/FTREEDB"%(CODACONF_SRVDIR["DATA_PART"],))
    WriteInto(join(CODACONF_VALUES["vicedir"],"db","vicetab"),"%s     %s  %s"%(hostfqdn,CODACONF_SRVDIR["DATA_PART"],CODACONF_SRVDIR["FTREE"][CODACONF_SRVDIR["TREE"]]))
        
    

# 1 recuperer le domaine du serveur
hostname,domainname=getfqdn().split(".",1)
hostfqdn=getfqdn()

# 3 tester le fichier de conf
if exists(CODACONF_SERVER):
    if not access(CODACONF_SERVER,W_OK|F_OK):
        print "Impossible d'acceder au fichier %s veuillez verifier vos droit"%(CODACONF_SERVER,)
        exit(2)
else:
    if not access(CODACONF_DIRECTORY,W_OK):
        print "Impossible d'écrire dans le repertoire %s veuillez vérifier vos droit"
        exit(2)


#codaconf edit vicedir numservers
for k,v in CODACONF_VALUES.items():
    AddToCodaConf(k,v)
    
#create dir
status,output=getstatusoutput("rm -rf %s"%(CODACONF_VALUES["vicedir"],))
mkdir(CODACONF_VALUES["vicedir"])

for v in CODACONF_VICEDIR:
    mkdir(join(CODACONF_VALUES["vicedir"],v))

#creation fichier /vice/hostname avec le hostname de l'hote.
hostname_file=join(CODACONF_VALUES["vicedir"],"hostname")
WriteInto(hostname_file,hostfqdn,1)

db_dir=join(CODACONF_VALUES["vicedir"],"db")

if CODACONF_ISSCM == "Y":
#SI l'hote est un SCM
    #ecriture du fqdn du scm dans /vice/db/scm
    WriteInto(join(db_dir,"scm"),hostfqdn,1)
    #ecriture des tokens dans les fichier $(token).tk, on les efface et on les reecrit
    for k,v in CODACONF_TK.items():
        WriteInto(join(db_dir,k+".tk"),CODACONF_TK[k],1)

    #ecriture de CODACONF_FILES dans db/files
    WriteInto(join(db_dir,"files"),CODACONF_FILES,1)
    #Sur une ligne ecrire "db" dans db/files.export
    WriteInto(join(db_dir,"files.export"),"db",1)
    #pareil pour toute les fichiers dans db/files 
    for a in open(join(db_dir,"files")).readlines():
        WriteInto(join(db_dir,"files.export"),"db/%s"%(a,))
    
    
    print "vice_setup_scm()"
    vice_setup_scm()
    print "vice_setup_user()"
    vice_setup_user()
    print "vice_setup_rvm()"
    vice_setup_rvm()
    print "vice_setup_srvdir()"
    vice_setup_srvdir()

    #lancement des services
    status,output=getstatusoutput("/usr/sbin/auth2")
    status,output=getstatusoutput("/usr/sbin/updatesrv")
    #status,output=getstatusoutput("/usr/sbin/updateclnt -h %s"%(hostname,))
    status,output=getstatusoutput("/usr/sbin/startserver")
    
    #creation du volume racine
    status,output=getstatusoutput("%s / %s%s"%(CODACONF_CREATEVOL_REP,hostname,CODACONF_SRVDIR["DATA_PART"]))

    # Ajout des services au demarrage du systeme
    
    status,output=getstatusoutput("chkconfig codasrv on")
    status,output=getstatusoutput("chkconfig auth2 on")
    status,output=getstatusoutput("chkconfig update on")
    # quittage
    exit(0)
else:
#SINON
    #ecriture du update token dans /vice/db/update.tk
    WriteInto(join(CODACONF_VALUES["vicedir"],"db","update.tk"),CODACONF_TK["update"],1)
    chmod(join(CODACONF_VALUES["vicedir"],"db","update.tk"),600)
    #lancement de updatefetch du fichier servers, return status pour savoir si erreur
    #status,output=getstatusoutput("%s -h %s -r db/servers -l servers"%(CODACONF_UPDATEFETCH,CODACONF_SCMHOST))
    for filer in CODACONF_FETCH_FILE:
        ClientFetch(CODACONF_SCMHOST,"db/%s"%(filer),filer)

    #pius sur le reste de FETCH_FILE
    #lancement de test des retours:
    print "vice_setup_rvm()"
    vice_setup_rvm()
    print "vice_setup_srvdir()"
    vice_setup_srvdir()

    status,output=getstatusoutput("/usr/sbin/auth2 -chk")
    #  status,output=getstatusoutput("/usr/sbin/updatesrv")
    status,output=getstatusoutput("/usr/sbin/updateclnt -h %s"%(CODACONF_SCMHOST,))
    #modification des script d'initd. updateclnt -h scm
    #                                               auth2 -chk
    #quittage
    status,output=getstatusoutput("chkconfig codasrv on")
    status,output=getstatusoutput("chkconfig auth2 on")
    status,output=getstatusoutput("chkconfig update on")

    print """ Veuillez a ce que le fichier /vice/db/servers presentent bien tout les serveurs.
De meme veuillez "ajoutez" le contenu du fichier /vice/db/vicetab de cette machine a celui du scm."""
    print """ Attendez que les le fichier local /vice/db/vicetab correpondent a celui qui existe sur le scm
        puis lancer service codasrv start """
    print """ Rien a faire pour l'instant machine non scm """
    exit(0)
#FINITO 





#echo  "Now installing things specific to non-SCM machines..."
#
#if ! ( /usr/sbin/vice-setup-rvm && /usr/sbin/vice-setup-srvdir ) ;  then
#    echo "Your coda server is not completely setup.  You will need"
#    echo "set it up by hand or fix the problems and rerun $0."
#    exit 1
#fi
#
#scm=`cat ${vicedir}/db/scm`
#echo "You have set up " `cat ${vicedir}/hostname`
#echo "Your SCM is $scm"
#echo "Other config files will be fetched from the SCM by updateclnt."
#echo
#echo "To finish your server setup you should"
#echo
#echo " start updateclnt as:  updateclnt -h $scm "
#echo " start the auth2 server as: auth2 -chk"
#echo
#echo "After that, there is still some configuration needed on the SCM before"
#echo "this server can be started."
#echo
#echo "An entry for this host is needed in /vice/db/servers"
#echo "Then all servers need to be shut down and restarted, as they need to"
#echo "know about the new server."
#echo "After all that it _should_ be ok to start the new server and create"
#echo "your first replicated volume."
##echo " On the SCM ($scm) run: new_server $hn"
##echo
##echon "Now you can "
##echo
##if [ $numservers = 1 ]; then
##    echo " start the fileserver: startserver &"
##else
##    echo " start your fileservers as:
##    n=1
##    while [ $n -le $numservers ]; do
##	echo "    startserver -n $n &"
##	n=`expr $n + 1`
##    done
##fi
