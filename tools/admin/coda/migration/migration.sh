#!/bin/sh
# mise a jour de zenity afin que les fenetre s'ouvre par défaut en premier plan


sed -i '/focus_on_map/s/False/True/' /usr/share/zenity/zenity.glade

username=$(awk '$1 ~ /default_user/ {print $2}' /etc/slim.conf)
coda_cache="/home/$username/.cache/coda.token"
#coda_cache="/home/piir/.coda/coda.token"
backup=$(mktemp)
# recupération du marinersocket.
source /etc/coda/venus.conf
# ché po ça sert
source /etc/sysconfig/ufo/coda

echo $username
echo $backup
# Verif synchro
verif_synchro () {
	response=""
    file=$(mktemp)
    while [ "$response" == "" ]; do
        echo "echo -en \"set:volstate\\n\" | nc -U $marinersocket >$file &"
        echo -en "set:volstate\n" | nc -U $marinersocket >$file &
        sleep 1
        kill %1
        response=$(grep "$username@agorabox.org reachable 0" $file)
    done

    rm $file
    echo "synchro ok"
}

ls /coda/$CODAREALM/users/$username &>/dev/null

verif_synchro


clog -fromfile $coda_cache
# ToDo: check tickets is still valide
# ToDo: Check We're able to join coda

cd /coda/$CODAREALM/users/$username/
# 1. Tar du dossier coda.
tar cjf $backup *
cd -
# 2. Exctinction du client coda.
/etc/init.d/coda-client stop
# 3. Remplacement du fichier krb5.conf par le nouveau
sed -n '/^### krb5.conf/,/^###/p' $0 | sed '1d;$d' > /etc/krb5.conf
# 4. Modification du /etc/coda/realms
sed -n '/^### coda.realms/,$p' $0 | sed '1d' > /etc/coda/realms
# 5. Suppression de tout les fichiers coda /var/lib/coda/*
rm -rf /var/lib/coda/*
# 6. venus-setup
venus-setup alpha.agorabox.org 
/etc/init.d/coda-client start
# 7. Récupération du mot de passe
ret=1
while [ $ret -ne 0 ]; do
    mdp=$(zenity --entry --hide-text --title="Connexion au nouveau domaine" --text="Veuillez saisire votre mot de passe" --width=300 --height=80)
    uid=$(awk -F: '$1 ~ /'$username'/ {print $3}' /etc/passwd)
    cache_filename="/tmp/krb5cc_$uid"
    
    echo "$mdp" | kinit -c $cache_filename $username
    ret="$?"
    if [ $ret -ne 0 ]; then 
        zenity --warning --title="Erreur de mot de passe" --text="Le mot de passe saisi n'est pas le bon, veuillez ré-essayer"
    fi

done

chown $username.$username $cache_filename

# 8. connexion a coda via kclog
echo "sudo -u $username kclog $username"
sudo -u $username kclog $username
echo "clog -fromfile $coda_cache"
clog -fromfile $coda_cache
# 9. copie des donnes dans el nouveau volume
cd /coda/alpha.agorabox.org/alpha/$username/ 
tar xjf $backup 

cd -
# message tout c'est bien passer
zenity --info --title="Terminer" --text="L\'opération s\'est terminée avec succès"
#zenity --question --title="Annulation" --text="Etes-vous sur de vouloir annuler l'opération? Clickez sur Annuler pour quitter, ou bien, Valider pour ré-essayer"

rm $backup

# Desktop link
unlink "/home/$username/Bureau/Mes fichiers synchronisés"
ln -s /coda/alpha.agorabox.org/alpha/$username  "/home/$username/Bureau/Mes fichiers synchronisés"

# /etc/sysconfig/ufo
sed -n '/^### coda/,/^###/p' $0 | sed '1d;$d' > /etc/sysconfig/coda/ufo


exit 2

### coda
CODAREALM="alpha.agorabox.org"
KRB5REALM="ALPHA.AGORABOX.ORG"
CODASRV="coda.alpha.agorabox.org"
CODAFOLDER="Mes fichiers synchronisés"

### krb5.conf
[logging]
 default = FILE:/var/log/krb5libs.log
 kdc = FILE:/var/log/krb5kdc.log
 admin_server = FILE:/var/log/kadmind.log

[kdcdefaults]
 kdc_ports = 88,750
 kdc_tcp_ports = 88

[libdefaults]
 default_realm = ALPHA.AGORABOX.ORG
 dns_lookup_realm = false
 dns_lookup_kdc = false
 ticket_lifetime = 24h
 default_tkt_enctypes = des3-hmac-sha1 des-cbc-crc
 default_tgs_enctypes = des3-hmac-sha1 des-cbc-crc
 ldap_servers = ldap://ldap.agorabox.org ldap://ldap2.agorabox.org

[realms]
ALPHA.AGORABOX.ORG = {
  kdc = kerberos.alpha.agorabox.org
  admin_server = kerberos.alpha.agorabox.org
}
BETA.AGORABOX.ORG = {
  kdc = kerberos.agorabox.org
  admin_server = kerberos.agorabox.org
}

[domain_realm]
 .alpha.agorabox.org = ALPHA.AGORABOX.ORG
 alpha.agorabox.org = ALPHA.AGORABOX.ORG
 .beta.agorabox.org = BETA.AGORABOX.ORG
 beta.agorabox.org = BETA.AGORABOX.ORG

### coda.realms
alpha.agorabox.org coda.alpha.agorabox.org
