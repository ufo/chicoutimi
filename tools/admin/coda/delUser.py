#!/usr/bin/env python
# -*- coding: utf-8 -*-

from optparse import OptionParser
import sys
import os
from classes import *
from user import *

usage = "%prog <username> <password> [-g g1[,g2[,..]] [-l] [-v]"
description = "Implements the full user deletion process. It deletes users for coda,auth2,kerberos and optionnaly ldap(not yet implemented).It has to be launched on the computer hosting kerberos and coda"
version="%prog 0.1"


""" Debut du programme """
parser = OptionParser(usage=usage, description=description, version=version)
parser.add_option("-l", "--ldap", dest="ldap", default=False,
                  help="Delete an user from ldap")
parser.add_option("-v", "--verbose", dest="verbose", default=False,
                  help="write log to standart input")

(options, args) = parser.parse_args()

if len(args) < 1:
        parser.error("Vous devez fournir au moins un username.")


username = sys.argv[1]

volutil=Volutil()
aup=Aup()
krb=Kerberos()

## 7. Création de l'utilisateur dans la base de auth2.
# 1. Création de l'utilisateur au niveau de coda
aup.deleteUser(username)
volutil.deleteUsersVolume(username)
krb.deleteUser(username)

if not options.ldap:
    print "finito"
else:
    print "ldap deletion"
    os.system("%s %s"%(PROG_DELETION_LDAP,username))
