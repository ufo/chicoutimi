#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
from automatedconf import *
from helper import Ssh
from commands import getstatusoutput

import time



class Volume:
    def __init__(self,line):
        tab=line.strip().split()
        self.volumeName=tab[0][1:]
        if tab[0][0] == "W" and tab[0][-2:] == ".0":
                self.name=tab[0][1:-2]
        else:
            self.name=tab[0]
        self.volumeId=tab[1][1:]
        self.partition=tab[3]

class Volutil:
    def __init__(self,server=CODA_SRV,bindUser=BIND_USER,realm=CODA_REALM,replicas=CODA_REPLICATED_GROUP):
        self.listVol={}
        self.server=server
        self.realm=realm
        self.replicas=replicas
        self.bindUser=bindUser

        self.ssh=Ssh(server,bindUser)

        ligne=self.ssh.exe("volutil getvolumelist 2> /dev/null","-t")

        for li in ligne.readlines():
            if li.strip() == "GetVolumeList finished successfully":
                return
            obj=Volume(li)
            self.listVol[obj.name]=obj

    def deleteUsersVolume(self,user):
        try:
            #time.sleep(1)
            print("cfs rmmount /coda/%s/users/%s"%(self.realm,user))
            os.popen("cfs rmmount /coda/%s/users/%s"%(self.realm,user))
        except KeyError:
            print("ERROR: RMMOUNT:No volume named %s exists in volume list"%(user))
            #time.sleep(1)
        try:
            print("purgevol_rep --kill %s"%(user))
            self.ssh.exe("purgevol_rep --kill %s"%(user),"-t")
        except KeyError:
            print("ERROR: No volume named %s exists in volume list"%(user))
            #time.sleep(1)
        try:
            print("volutil purge %s %s"%(self.listVol[user].volumeId,self.listVol[user].volumeName))
            self.ssh.exe("volutil purge %s %s"%(self.listVol[user].volumeId,self.listVol[user].volumeName),"-t")
        except KeyError:
            print("ERROR: No volume named %s exists in volume list"%(user))


    def deleteVolulme(self,volume):
        try:
            #time.sleep(1)
            print("cfs rmmount /coda/%s/users/%s"%(self.realm,volume.volumeName))
            os.popen("cfs rmmount /coda/%s/users/%s"%(self.realm,volume.volumeName))
            #time.sleep(1)
            print "purgevol_rep --kill %s"%(self.volume.volumename)
            self.ssh.exe("purgevol_rep --kill %s"%(self.volume.name),"-t")
            #time.sleep(1)
            self.ssh.exe("volutil purge %s %s"%(volume.volumeId,volume.name),"-t")
        except KeyError:
            print("ERROR: No volume named %s exists in volume list"%(user))

    def createVolume(self,volume):
        self.ssh.exe("""createvol_rep %s %s/vicepa"""%(volume,self.replicas),"-t","echo -e \\\"n\\n\\\"")
        time.sleep(1)
        print("cfs mkmount /coda/%s/users/%s %s"%(self.realm,volume,volume)) 
        os.popen("cfs mkmount /coda/%s/users/%s %s"%(self.realm,volume,volume))
        time.sleep(1)
        print("cfs setacl /coda/%s/users/%s %s %s"%(self.realm,volume,volume,DEF_RIGHT))
        status,output= getstatusoutput("cfs setacl /coda/%s/users/%s %s %s"%(self.realm,volume,volume,DEF_RIGHT))
        print("cfs setacl -negative /coda/%s/users/%s System:AnyUser %s"%(self.realm,volume,DEF_RIGHT))
        status,output= getstatusoutput("cfs setacl -negative /coda/%s/users/%s System:AnyUser %s"%(self.realm,volume,DEF_RIGHT))
        if status==255:
            raise Exception("Le dossier n'est surement pas créer. executez ./delUser volume")     
