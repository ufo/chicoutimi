#!/bin/sh
# Description : aims at restoring a coda backup
#               It's designed to be executed on the coda servers.
#
#
# ToDo: implements the

# restauration d'un dump avec ses fichier incremental, avec chioix du level.
# ou bien d'un dump archivers avec tout ou partie de ses incremental.



REMOTE_PATH="/tmp/backup"
VERBOSE=""
RESTORE_TYPE=""
TMP_FOLDER="/tmp"

INC_LEVEL=""


WORK_FOLDER=""
TMP_DEST="/tmp/restore"

TMP_RESTORE="restore"

PREFIX=""
CODA_HOST="elko.agorabox.org"
BACKUP_PATH="/coda/agorabox.org/backup"

VOLUME_NAME=""
BIND_USER="restore/$(hostname)"

usage (){
cat <<EOF
usage: $(basename $0) [--verbose] [-q|--quiet] [-v|--volume <volume_or_archive_name>] [-d|--destination <backup_dest>] [-o|--owner <owner>][-h|--host <coda_host>] [-t|--topath <coda_path>] [-l|--level <incremental_level]  [-b|--bind <user>]
    
       Perform a full or a partial restore of a backed up coda volume.
        If no information about coda is given, the restored data won't be suppressed at the end of the script nor will it be copied on a coda tree.
        Script should have the coda admin right on the BACKUP_PATH (-t option)
 
        General Options:
        -v|--volume <volume>: Name of the volume to restore. If the name exist as a dump name, we use it, if not we restore the last dup that match this name.
        -d|--destination <backup_dest> : Where to unpack the volume. Default: $TMP_DEST
        -l|--level <inc_level> : if possible restore backup a a partial level, automatically restore with all incremental dumps.
        -q|--quiet : suppress all outputs, instead log errors in syslog facility.level=cron.warn .
        --verbose : Set the verbose mode.

        Coda Options :
        -o|--owner <user> : username to which we give read access. If none is specified Systeme:Anyuser will have read access.
        -t|--topath <coda_path> : Path of the volume where to copies restored data. Path must exists prior to the restoration. Default: $BACKUP_PATH
        -h|--host <coda_host> : hostname of the server on which we restore backups. Default: $CODA_HOST

        Kerberos Options :
        -b|--bind <user> : Bind username for the connection to the scm. Default: restore/$(hostname)
EOF
}

# Gère les erreurs lors du scripts. Avec le message et tout et tout
error (){ # $1 = code de retour d'une commande $2 = etape de la commande

    if [ $1 -ne 0 ]; then
        case $2 in
            "ls")
                echo "Aucun fichier ne correspond a $VOLUME_NAME"
                ;;
            "dump_level")
                echo "dump level must be between 0 and 9, leave empty for an automatic restore."
                ;;
            "test_lvl")
                echo "Il n'existe pas d'archive correspondante à $WORK_FOLDER/$PREFIX-$INC_LEVEL.gz .Veuillez changez le niveau -l"
                ;;
            "create_workdir")
                echo "Le dossier $WORK_FOLDER existe déja."
                ;;
            "merge")
                echo "erreur lors du merge"
                ;;
            "copy_inc")
                echo "Erreur lors de la copie"
                ;;
            "read_packed")
                echo "erreur lors de la lecture de l'archive"
                ;;
            "untar_full")
                echo "Impossible de détarrer l'archive"
                ;;
            "codadump2tar")
                echo "Impossible de dumper l'archive."
                ;;
        esac 
    
        echo 
        clearance
        echo "Command exits with error message :"

        exit 1
    fi
}

debug (){
        if [ "$VERBOSE" == "v" ] && [ "$QUIET" != "q" ]; then
            echo $1
        fi
}


while [ $# -gt 0 ]; do
   case $1 in
        -v | --volume)
            # Volume name
            shift
            VOLUME_NAME=$1
            ;;
        -d | --destination)
            # where to unpack
            shift
            TMP_DEST=$1
            ;;
        -t | --topath)
            #keytab
            shift
            CODA_PATH=$1
            ;;
        -u | --user)
            #owner user
            shift
            CODA_USER=$1
            ;;
        -l | --level)
            #inc restore level
            shift
            INC_LEVEL=$1
            ;;
        --verbose)
            # verbose mode
            VERBOSE="v"
            ;; 
        -q | --quiet)
            # no output
            QUIET="q"
            ;;
        * )
            echo 
            echo "Erreur : Parametre $1 non reconnu"
            echo
            usage 
            exit 1
            ;;
  
     esac
     shift
done


# Fonction find_backup
# @name=find the last archive corresponding to this name
# Retrouve automatiquement la derniere backup a utiliser.
find_backup (){

    ls -1 ${VOLUME_NAME}* 1>/dev/null 2>/dev/null
    error $? "ls"
    # On sessaye de recupere le chemin absolu du fichier
    list_archive=$(ls -1 ${VOLUME_NAME}* | xargs -n1 readlink -f | sort)
    last_backup=$(tail -n1 <<EOF
$list_archive
EOF
)

    debug "last_backup=$last_backup"
    if [ "$last_backup" == "$(echo $last_backup | grep '.*-[0-9].gz')" ]; then
        RESTORE_TYPE="P"
        PREFIX="$(basename $last_backup | sed 's/-[0-9].gz//')"
    else
        RESTORE_TYPE="F"
        PREFIX="$(basename $last_backup | sed 's/.tar.gz//')"
    fi
    VOLUME_NAME="$last_backup"
    debug "VOLNAME=$last_backup"
    debug "RESTORE_TYPE=$RESTORE_TYPE"
    debug "PREFIX=$PREFIX"
}


# Remplit le WORK_FOLDER avec les archives qui vont bien.
# Gere les 
handle_packed () {
    # Full unpack it before.

    echo "last_inc=\$(gzip -dc $last_backup | tar -t | sort | tail -n1 | sed 's/.*-\([0-9]\).gz/\1/')"
    last_inc=$(gzip -dc $last_backup | tar -t | sort | tail -n1 | sed 's/.*-\([0-9]\).gz/\1/')
    error $? "read_packed"

    if [ $last_inc -lt ${INC_LEVEL:-0} ] || [ ${INC_LEVEL:-0} -lt 0 ]; then
        error 1 "test_lvl"
    elif [ "$INC_LEVEL" == "" ]; then
        INC_LEVEL=$last_inc
    fi
    
    debug "restore_full"
    debug "last_inc=$last_inc"
    debug "INC_LEVEL_FINALE=$INC_LEVEL" 
    debug "VOLUME_NAME=$VOLUME_NAME"

    echo "tar xzf $VOLUME_NAME $PREFIX-0.gz -C $WORK_FOLDER $PREFIX-$INC_LEVEL.gz"
    tar xzf $VOLUME_NAME -C $WORK_FOLDER $PREFIX-0.gz $PREFIX-$INC_LEVEL.gz 
    error $? "untar_full"
    
    
}

handle_incs() {
	if [ ! $INC_LEVEL == "" ] && [ ! -f $REMOTE_PATH/$PREFIX-$INC_LEVEL.gz ]; then
	    error 1 "test_lvl"
	elif [ "$INC_LEVEL" == "" ]; then
	    #automatically get last backups
	    INC_LEVEL=$(ls -1 $REMOTE_PATH/$PREFIX-* | sort | tail -n1 | sed 's/.*-\([0-9]\).gz$/\1/')
	fi

    debug "cp $REMOTE_PATH/$PREFIX-[0$INC_LEVEL].gz $WORK_FOLDER/"
    cp $REMOTE_PATH/$PREFIX-[0$INC_LEVEL].gz $WORK_FOLDER/
    error $? "copy_inc"

}


# Fonction restore.
# Restaure un dump incremental dans le dossier TMP_RESTORE
restore_partial (){
    debug "inc_level=$INC_LEVEL"

    # On emrge les deux dumps
    debug "merge $WORK_FOLDER/${TMP_RESTORE} $WORK_FOLDER/$PREFIX-0 $WORK_FOLDER/$PREFIX-$INC_LEVEL"
    merge $WORK_FOLDER/${TMP_RESTORE} $WORK_FOLDER/$PREFIX-0 $WORK_FOLDER/$PREFIX-$INC_LEVEL
    error $? "merge"

    debug "rm -f $WORK_FOLDER/${PREFIX}-[0${INC_LEVEL}]"
    rm -f $WORK_FOLDER/${PREFIX}-[0${INC_LEVEL}]

    debug "codadump2tar -f $WORK_FOLDER/${TMP_INC_PREFIX}${INC_LEVEL} -o $WORK_FOLDER/${TMP_INC_PREFIX}FULL"
    codadump2tar -f $WORK_FOLDER/${TMP_RESTORE} -o $WORK_FOLDER/${TMP_RESTORE}.tar
    error $? "codadump2tar"

    debug "rm -f $WORK_FOLDER/$TMP_RESTORE"
    rm -f $WORK_FOLDER/$TMP_RESTORE

    debug "tar xf $WORK_FOLDER/$TMP_RESTORE.tar -C $TMP_RESTORE"
    tar xf $WORK_FOLDER/$TMP_RESTORE.tar -C $TMP_RESTORE

    debug "rm -f $WORK_FOLDER/$TMP_RESTORE"
    rm -f $WORK_FOLDER/$TMP_RESTORE
}


# Clear all temp folders and files
clearance (){

    rm -rf $WORK_FOLDER
    rm -f $errfile

}




###### Debut #######
errfile=$(mktemp)
exec 2<>$errfile

# Determine si oui ou non le nom de volume est un chemin relatif ou absolu ou bien juste un nom de volume.
if [ "${VOLUME_NAME##*/}" == "${VOLUME_NAME}" ]; then
     VOLUME_NAME=$REMOTE_PATH/$VOLUME_NAME 
fi

debug "volume_name=$VOLUME_NAME"
find_backup $VOLUME_NAME
# Maintenant on sais quelle backup on utilise et quel type de backup est-ce
debug "restore_type=$RESTORE_TYPE"

WORK_FOLDER="${TMP_FOLDER}/$(basename $PREFIX)"

if [ ! -d $WORK_FOLDER ]; then
    debug "mkdir $WORK_FOLDER"
    mkdir $WORK_FOLDER
else
    # On genere une erreur afin d'éviter de supprimer une restoration précédante.
    error 1 "create_workdir"
fi

if [ "$RESTORE_TYPE" == "F" ]; then
    handle_packed
else
    handle_incs
fi

debug "gzip -d $WORK_FOLDER/*"
gzip -d $WORK_FOLDER/*
restore_partial  
clearance

