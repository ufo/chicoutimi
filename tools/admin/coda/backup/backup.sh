#!/bin/sh
# Description : This script aims at backup a coda volume.
#               It's designed to be executed on the coda servers.
#               In order to performs its task the servers must have acces to the backup server, whitout password.
#               Kerberos authentication will be used, A keytab must be present on the Coda server.
#
#
# ToDo: Handles multiples CODA_PATH

TMP_PATH=/tmp
REMOTE_PATH=/backup
LOCAL_PATH=/backup
LOG_FILE="$LOCAL_PATH/coda_dump_log"

CODA_PATH=/coda/users
BACKUP_HOST=""

# Ces options servent à la connexion du scp via kerberos.
KEYTAB_FILE="/etc/coda/backup.keytab"
BIND_USER="backup"


# ARGUMENTS
VERBOSE=""

usage (){
cat <<EOF
usage: $(basename $0) [--verbose] [-v|--volume <volume_name>] [-h|--host <backup_coordinator>] [-u|--user <bind_user>]
[-k|--keytab <keytab_file>] [-q|--quiet]
    
      Perform a full or incremental dumps of a coda volume, and optionaly "scp" it
      to the backup coordinator.
        
        -v|--volume <volume>: Name of the volume to backup.
                              If unspecified, it tries to backup all directories found in CODA_PATH=$CODA_PATH.
        -h|--host <backup_coordinator> : Host to which we sends backup
        -u|--user <bind_user> : Username to bind to while conenction to the backup coordinator
        -k|--keytab <keytab_file> : keytab file used while retrieving kerberos ticket. default /etc/krb5.keytab
        -q|--quiet : suppress all outputs, instead log errors in syslog facility.level=cron.warn .
EOF

}

debug (){
        if [ "$VERBOSE" == "v" ] && [ "$QUIET" != "q" ]; then
            echo $1
        fi
}


function error (){ # $1 = code de retour d'une commande $2 = etape de la commande

    return_code=0

    if [ $1 -ne 0 ]; then
        return_code=1
        echo "Error : aborting. Please refer to /vice/srv/SrvLog"
        case $2 in
            "info")
                echo "volume name may be wrong, check it"
                ;;
            "lock")
                echo "Volume may be lock try to exec"
                echo "volutil unlock $volid"
                ;;
            "envoi")
                echo "Impossible d'envoyer le fichier"
                ;;
            "getvolumelist")
                echo "Impossible de récupérer la listes des volumes."
        esac 
    
        echo 
        echo "Command exits with error message :"
        if [ "$QUIET" == "q" ] ;then
             logger -t $0 -p cron.warn "stage=$2 volume=$VOLUME_NAME ${BACKUP_NAME:+backup_name=}$BACKUP_NAME"
        fi

        cat $errfile 
        volutil unlock $volid &>/dev/null
        #rm $tmpfile
        #rm $errfile
        if [ "$BACKUP_HOST" != "" ]; then
            next
        else
            exit 1
        fi 
    fi
    
    # On efface errfile afin d'avoir juste la sortie correspondante
    # a la prochaine commande
    # pas sur que ça soit utile
    cat /dev/null > $errfile
    return $return_code
}


doTheWork (){

    volume_name=$1

    # On récupère l'id du volume que l'on veut backuper
    debug "\$(volutil info $volume_name 1>$tmpfile)"
    volutil info $volume_name 1>$tmpfile #2>/dev/null
    error $? "info" ; if [ ! $? ]; then return ;fi
    
  
    debug "volid=\$(sed -ne 's/^id = \([0-9a-f]\+\).*/\1/p' <$tmpfile)"
    volid=$(sed -ne 's/^id = \([0-9a-f]\+\).*/\1/p' <$tmpfile)
  
    last_backup=$(grep "$volid" $LOG_FILE | tail -n 1)
    DUMP_LVL=$(cut -d"-" -f4 <<-EOF
		$last_backup
		EOF
	) 
    DATE_BACKUP=$(cut -d'-' -f3 <<-EOF
		$last_backup
		EOF
	)
  
    # On recupère ou on crée DATE_BACKUP et DUMP_LVL, suivant qu'une backup existe déja ou non
    if [ "$last_backup" == "" ] || [ ${DUMP_LVL:-0} -eq 9 ]
    then # means we have to create a full backup 
        # We have already made a cycle for this volume delete any entries 
        if [ ${DUMP_LVL:-0} -eq 9 ]; then
            sed -i "/$volume_name-[^-]\+-$DATE_BACKUP/d" $LOG_FILE
        fi
        DUMP_LVL=""
        DUMP="F"
        DATE_BACKUP=$(date '+%s')
    else
        DUMP_LVL=$(($DUMP_LVL+1))
        DUMP="I"
    fi
  
    BACKUP_NAME=$volume_name-$volid-$DATE_BACKUP-${DUMP_LVL:-0}
    # Fix in order to avoid problem with root volume - named "/".
    BACKUP_NAME=${BACKUP_NAME/\//rootvolume}
  
    debug "volid=$volid"
    debug "last_backup=$last_backup"
    debug "volume_name=$volume_name"
    debug "DUMP=$DUMP ${DUMP_LVL:+DUMP_LVL=}$DUMP_LVL"
    debug "DATE_BACKUP=$DATE_BACKUP"
    debug "BACKUP_NAME=$BACKUP_NAME"
  
    debug "volutil lock $volid"
    volutil lock $volid >/dev/null
    error $? "lock"; if [ ! $? ]; then return;fi
  
    debug "volutil backup $volid  1>$tmpfile"
    volutil backup $volid  1>$tmpfile
    error $? "backup"; if [ ! $? ]; then return;fi
  
    debug "backup_id=\$(sed -ne 's/^Backup (id = \\([0-9a-f]\+\\).*/\1/p' <$tmpfile)"
    backup_id=$(sed -ne 's/^Backup (id = \([0-9a-f]\+\).*/\1/p' <$tmpfile)
    debug "backup_id=$backup_id"
  
    debug "volutil dump ${DUMP_LVL:+-i }$DUMP_LVL $backup_id $TMP_PATH/$BACKUP_NAME 1>/dev/null"
    volutil dump ${DUMP_LVL:+-i }$DUMP_LVL $backup_id $TMP_PATH/$BACKUP_NAME 1>/dev/null
    error $? "dump" ; if [ ! $? ]; then return;fi
  
    debug "volutil ancient $backup_id 1>/dev/null" 
    volutil ancient $backup_id 1>/dev/null
    
  
    backup_path=$TMP_PATH/$BACKUP_NAME
    debug "gzip -9 $backup_path"
    gzip -9 $backup_path
    debug " mv $backup_path.gz $LOCAL_PATH/"
    mv $backup_path.gz $LOCAL_PATH/
  
    # Le dump et le tar ne servent plus
    # rm -f $backup_path{,.tar} 2>/dev/null
    debug "Volume_backup_id = $backup_id"
    
    echo "$BACKUP_NAME" >> $LOG_FILE

}


while [ $# -gt 0 ]; do
   case $1 in
        -v | --volume)
            # Volume name
            shift
            VOLUME_NAME=$1
            ;;
        -h | --host)
            # Host to transmit the data to
            shift
            BACKUP_HOST=$1
            ;;
        -k | --keytab)
            #keytab
            shift
            KEYTAB_FILE=$1
            ;;
        -u | --user)
            #bind user
            shift
            BIND_USER=$1
            ;;
        --verbose)
            # verbose mode
            VERBOSE="v"
            ;; 
        -q | --quiet)
            # no output
            QUIET="q"
            ;; 
        * )
            echo 
            echo "Erreur : Parametre $1 non reconnu"
            echo
            usage 
            exit 1
  
     esac
     shift
done



if [ ! -x "$LOG_FILE" ];then touch $LOG_FILE; fi 
tmpfile=$(mktemp)
errfile=$(mktemp)

## Avant tout on s'assure que le ficheir de log est trier
# On redirige par défaut la sortie d'erreur vers le fichier errfile.
exec 2<>$errfile

if [ $VOLUME_NAME == ""]; then

    # Si aucun Voluname n'est spécifie on boucle
    debug "volumeList=\$(volutil getvolumelist 2>/dev/null | grep '^W' | cut -f1 -d'.')"
    volumeList=$(volutil getvolumelist 2>/dev/null)
    error $? "getvolumelist"
    
    volumeList=`cat <<-EOF | sed -n 's/^W\([^.]\+\).*/\1/p'
		$volumeList 
		EOF
   ` 

    cat <<-EOF | { while read user; do doTheWork $user; echo $user; done }
		$volumeList
		EOF

else
    doTheWork $VOLUME_NAME
fi


# Nettoyage des ficheirs temporaires en cas d'envoie par scp
if [ "$BACKUP_HOST" == "" ]; then
    # rien a faire
    echo "i" >/dev/null  
else
    debug "backup_host=$BACKUP_HOST"
    # Authenticate to kerberos.
    export KRB5CCNAME=$tmpfile
    debug "KRB5CCNAME=$KRB5CCNAME kinit -kt $KEYTAB_FILE $BIND_USER"
    kinit -kt $KEYTAB_FILE $BIND_USER
    # Doing parralelize sends.
    debug "scp $LOCAL_PATH/*.gz $BIND_USER@$BACKUP_HOST:$REMOTE_PATH 1>/dev/null"
    scp $LOCAL_PATH/*.gz $BIND_USER@$BACKUP_HOST:$REMOTE_PATH 1>/dev/null
    error $? "envoi"; if [ ! $? ]; then return;fi
    kdestroy -c $tmpfile
    unset KRB5CCNAME
    # if no error happens remove the compressed files.
    rm -rf $LOCAL_PATH/*.gz
fi
    

exec 2>/dev/null
# Remove unused files.
rm $tmpfile
rm $errfile
exit 0
