#!/bin/sh
# Description : This script suppress old backup that are older than 20 days. 
#               It also packs backup that are complete.(One full 9 backup)
#               
#               

REMOTE_PATH=/tmp/backup
MAX_DAYS=21 # add plus one to be sure.


cd $REMOTE_PATH
now=$(date '+%s')

echo "MAX DATE = $(date --date="$MAX_DAYS days ago")"
dumps_file=$(mktemp)
packed_file=$(mktemp)
find . -name '*-[0-9].gz' | sort | sed 's/^\.\///' | sed 's/-[0-9].gz//' | uniq >$dumps_file
find . -regextype posix-egrep -regex '.*[0-9]{4,}.tar.gz' | sort | sed 's/^\.\///' >$packed_file



#first treat the packed list
while read packed; do
    #extract timestamp
    timestamp=$(cut -d"-" -f 3 <<EOF
$packed 
EOF
)
    timestamp=`echo $timestamp | sed 's/.tar.gz//'`
    if [ $timestamp -lt $(date --date="$MAX_DAYS days ago" '+%s') ]; then
        rm -f $packed
        
    fi
done <$packed_file


# then the incs dump
while read dump; do
    date_backup=$(cut -d'-' -f3 <<EOF
$dump
EOF
)
    last_backup=$(find . -name '*-[0-9].gz' | sort | sed 's/^\.\///' | grep $dump | tail -n1)
    dump_level=$(cut -d"-" -f4 <<EOF
$last_backup
EOF
) 
    dump_level=$(echo $dump_level |  sed 's/.gz//')
    if [ $dump_level -eq 9 ] && [ $(stat -c '%X' $last_backup) -lt $(date --date='2 days ago' '+%s') ]; then
        # pack all incs and full backups
        tar cvzf $dump.tar.gz $dump-*
        rm -f $dump-*
    fi
        
done <$dumps_file

cd -
