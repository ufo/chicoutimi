#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys,random,os,re,time
from automatedconf import *
from helper import Ssh

a="""USER vienin
  *  id: 151
  *  belongs to no groups
  *  cps: [ 151 ]
  *  owns no groups
"""
b="""USER codaroot
  *  id: 147
  *  belongs to groups: [ -1 ]
  *  cps: [ -1 147 ]
  *  owns groups: [ -1 ]"""


"""
    Fonction genpwd :
        Genere un mot de passe aléatoire constitué de tout les types de caractère possible.
"""
def genpwd():
    char="abcdefghilmnopqrstuvwxzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!;.,?*_-()#$"
    pwlen=12
    b=0
    str=""
    while b<pwlen:
        str+=char[random.randint(0,len(char)-1)]
        b+=1
    
    return str


class User(object):
    def __init__(self,lines=""):
        if lines=="":
            pass

        else:
            regexp=re.compile("(-?\d+)") 
            """ here we suppose that we have a good formed line """
            list = lines.split("\n")

            # First line contained the suer name
            self.username=list[0].strip().split(" ")[1]
            self.id=list[1].strip().split(" ")[2]
            self.groups=regexp.findall(list[2])
            self.cps=regexp.findall(list[3])
            self.ownedGroups=regexp.findall(list[4])


    def attrToList(self,attr):
        str = ""
        at=self.__getattribute__(attr)
        if len(at) == 0:
            str="no groups"
        else:
            str = "[ "
            for item in at:
                str+=item+" "

            str+="]"

        return str


    def __repr__(self):
        return """USER %s
  *  id: %s
  *  belongs to %s
  *  cps: %s
  *  owns %s"""%(self.username,self.id,
            self.attrToList("groups"),
            self.attrToList("cps"),
            self.attrToList("ownedGroups"))

    def __str__(self):
        return """USER %s
  *  id: %s
  *  belongs to %s
  *  cps: %s
  *  owns %s"""%(self.username,self.id,
            self.attrToList("groups"),
            self.attrToList("cps"),
            self.attrToList("ownedGroups"))


c="""GROUP System:AnyUser OWNED BY System
  *  id: -2
  *  owner id: 1
  *  belongs to no groups
  *  cps: [ -2 ]
  *  has members: [ 1 ]"""
d="""GROUP System:Administrators OWNED BY codaroot
  *  id: -1
  *  owner id: 147
  *  belongs to no groups
  *  cps: [ -1 ]
  *  has members: [ 147 ]"""

class Group(object):
    def __init__(self,lines=""):
        if lines=="":
            pass

        else:
            regexp=re.compile("(-?\d+)") 
            """ here we suppose that we have a good formed line """
            list = lines.split("\n")

            # First line contained the group name
            self.groupname=list[0].strip().split(" ")[1]
            self.id=list[1].strip().split(" ")[2]
            self.owner=list[2].strip().split(" ")[3]
            self.groups=regexp.findall(list[3])
            self.cps=regexp.findall(list[4])
            self.members=regexp.findall(list[5])


    def attrToList(self,attr):
        str = ""
        at=self.__getattribute__(attr)
        if len(at) == 0:
            str="no groups"
        else:
            str = "[ "
            for item in at:
                str+=item+" "

            str+="]"

        return str


    def __repr__(self):
        return """GROUP %s
  *  id: %s
  *  owner id: %s
  *  belongs to %s
  *  cps: %s
  *  has members: %s"""%(self.groupname,self.id,
            self.owner,
            self.attrToList("groups"),
            self.attrToList("cps"),
            self.attrToList("members"))

    def __str__(self):
        return """GROUP %s
  *  id: %s
  *  owner id: %s
  *  belongs to %s
  *  cps: %s
  *  has members: %s"""%(self.groupname,self.id,
            self.owner,
            self.attrToList("groups"),
            self.attrToList("cps"),
            self.attrToList("members"))

class Aup:
    def __init__(self,server=CODA_SRV,bindUser=BIND_USER):
        self.server=server
        self.bindUser=bindUser
        self.ssh=Ssh(server,bindUser)
        
    def deleteUser(self,user):
        print "Suppression de la base de auth2"
        os.popen("""cat <<EOF | %s -h %s du
        codaroot
        %s
        %s
        EOF"""%(AUTH2_ADM_PROG,CODA_SRV,CODA_ADM_PASS,user))

        self.ssh.exe("pdbtool d " + user,"-t")
        self.ssh.exe("pdbtool rg g_%s %s"%(user,user),"-t")
        self.ssh.exe("pdbtool rg g_%s"%(user),"-t")
        os.popen("""cat <<EOF | %s -h %s nu
        codaroot
        %s
        %s
        EOF"""%(AUTH2_ADM_PROG,CODA_SRV,CODA_ADM_PASS,user))

    def createUser(self,user):
        print "creation de l'utilisateur dans la base de coda"

        self.ssh.exe("pdbtool nu %s"%(user),"-t")
        password=genpwd()
        print """cat <<EOF | %s -h %s nu
codaroot
%s
%s
%s
%s
EOF"""%(AUTH2_ADM_PROG,CODA_SRV,CODA_ADM_PASS,user,password,user)


        os.popen("""cat <<EOF | %s -h %s nu
codaroot
%s
%s
%s
%s
EOF"""%(AUTH2_ADM_PROG,CODA_SRV,CODA_ADM_PASS,user,password,user))

        # 8. Attribution d'un mot de passe aléatoire.
        #   Celui ci ne sera jamais utilisé car l'authentification 
        #   est géréé par kerberos.
#        print "attribution du mot de passe aléatoire"
#        os.popen("""cat <<EOF | %s -h %s cp 
#codaroot
#%s
#%s
#%s
#EOF"""%(AUTH2_ADM_PROG,CODA_SRV,CODA_ADM_PASS,user,genpwd()))


class Kerberos:
    def __init__(self,bindPrincipal=BIND_PRINCIPAL,keytab=KEYTAB_FILE,server=KRBKDC,bindUser=BIND_USER):
        self.bindPrincipal=bindPrincipal
        self.keytab=keytab
        self.ssh=Ssh(server,bindUser)
        
    def deleteUser(self,username):
        print "KERBEROS: Suppression utilisateur %s"%(username,)
#       print("""echo -e \"delprinc %s\\nyes\" | %s -p %s -k -t %s"""%(username,KADMIN_PROG,self.bindPrincipal,self.keytab))
#       os.popen("""echo -e \"delprinc %s\\nyes\" | %s -p %s -k -t %s"""%(username,KADMIN_PROG,self.bindPrincipal,self.keytab))
        #print("""echo -e \"delprinc %s\\nyes\" | %s """%(username,KADMIN_LOCAL_PROG))
        self.ssh.exe(KADMIN_LOCAL_PROG,stdin="""echo -e \\\"delprinc %s\\nyes\\\""""%(username),opt="-t")
        


    def createUser(self,username,password):
        print "KERBEROS: création utilisateur %s "%(username,)
#       print("""echo -e \"addprinc %s\\n%s\\n%s\\n\" | %s -p %s -k -t %s"""%(username,password,password,KADMIN_PROG,self.bindPrincipal,self.keytab))
#       os.popen("""echo -e \"addprinc %s\\n%s\\n%s\\n\" | %s -p %s -k -t %s"""%(username,password,password,KADMIN_PROG,self.bindPrincipal,self.keytab))
        #print("""echo -e \"addprinc %s\\n%s\\n%s\\n\" | %s """%(username,password,password,KADMIN_LOCAL_PROG))
        self.ssh.exe(KADMIN_LOCAL_PROG,stdin="""echo -e \\\"addprinc %s\\n%s\\n%s\\n\\\""""%(username,password,password),opt="-t")

    def DisableUser(self,username):
        print("KERBEROS: Disabling user %s "%(username,))
        print("Not yet impemented")
#       os.popen(""" echo -e \"modprinc -allow_tix %s\n\" | %s -p %s -k -t %s"""%(username,KADMIN_PROG,self.bindPrincipal,self.keytab))

    def EnableUser(self,username):
        print("KERBEROS: Enabling user %s "%(username,))
        print("Not yet impemented")
#       os.popen(""" echo -e \"modprinc +allow_tix %s\n\" | %s -p %s -k -t %s"""%(username,KADMIN_PROG,self.bindPrincipal,self.keytab))
