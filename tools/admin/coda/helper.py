#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
class Ssh:
    def __init__(self,server,sudoUser=None):
        self.server=server
        self.sudoUser=sudoUser
        if sudoUser:
                self.strsudo="sudo -u %s"%(self.sudoUser)
        else:
                self.strsudo=""


    def exe(self,str,opt="",stdin=None):
        pipe=""
        if stdin:
            pipe="%s | "%(stdin)
        

        str="ssh %s %s \"%s %s %s\" >/dev/null"%(opt,self.server,pipe,"sudo -u %s"%(self.sudoUser),str)
        print str
        return os.popen(str)
        #return true
