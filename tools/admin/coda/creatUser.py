#!/usr/bin/env python
# -*- coding: utf-8 -*-

""" Script de création d'utilisateurs de la solution handy
    Ce script devra êter éxécuter sur le serveur héberrgeant coda, kerberos, auth2
    voir à le scindé en plusieur partie.
    La génération aléatoire parer peut probable
        ### Partie Coda
    1. Création de l'utilisateur au niveau de coda
    2. Création d'un groupe pour l'utilisateur
    3. Ajout de groupe supplémentaire à l'utilisateur (Opt.)
    4. Création du volume.
    5. Montage du volume.
    6. Attribution des droits, sur le volume
    7. Création de l'utilisateur dans la base de auth2.
    8. Attribution d'un mot de passe aléatoire.
        Celui ci ne sera jamais utilisé car l'authentification 
        est géréé par kerberos.

        ### Partie kerberos
    9. Création de l'utilisateur dans les base kerberos.
    
        ### Partie LDAP  (Opt.)
    10. Création d'un compte posix dans ldap.
    11. Attribution d'un mot de passe.
    12. Attribution des groupes supplémentaire.
"""


from automatedconf import *
from optparse import OptionParser
import sys,random,os
from classes import *
from user import *
import getpass

import time



"""
    Fonction genpwd :
        Genere un mot de passe aléatoire constitué de tout les types de caractère possible.
"""
def genpwd():
    char="abcdefghilmnopqrstuvwxzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!;.,?*_-()#$"
    pwlen=12
    b=0
    str=""
    while b<pwlen:
        str+=char[random.randint(0,len(char)-1)]
        b+=1
    
    return str

"""
    Fonction sshIt:
        Prend en argument une commande a lancer sur un serveur passer en argument
        et quitte en renvoyant le code de retour de la commande
"""
def sshIt(str,srv):
    print "ssh %s %s "%(srv,str)
    str = "ssh %s %s "%(srv,str)
    return os.popen(str)


def creation(username,password):
        global PROG_CREATION_LDAP,TEMPLATE_FILE
        #CODA_ADM_PASS = getpass.getpass("Entrez le mot de passe coda: ")
        #KADMIN_USER=getpass._raw_input("Entrez l'utilisateur kadmin: ")
        #KADMIN_PASS = getpass.getpass("Entrez le mot de passe Kadmin: ")
        # Créations des utilisateurs
        aup=Aup()
        aup.createUser(username)


        # 3. Ajout de groupe supplémentaire à l'utilisateur (Opt.)
        #if options.groups :
        #    list = options.groups.split(',') 
        #
        #    for group in list:
        #        sshIt("-t sudo -u %s pdbtool ag %s %s"%(CODA_ADM_USER,group,username),CODA_SRV)

        # Creation des volumes
        volutil=Volutil()
        volutil.createVolume(username)

        krb=Kerberos()
        krb.createUser(username,password)



        if (options.ldap!=False):
            print "création d'un compte ldap"
            print "%s %s %s %s"%(PROG_CREATION_LDAP,username,password,TEMPLATE_FILE)
            os.system("%s %s %s %s"%(PROG_CREATION_LDAP,username,password,TEMPLATE_FILE))
        else:
            print "finito"



usage = "%prog <username> <password> [-g g1[,g2[,..]] [-l] [-v]"
description = "Implements the full user creating process. It creates users for coda,auth2,kerberos and optionnaly ldap(not yet implemented).It has to be launched on the computer hosting kerberos and coda"
version="%prog 0.1"

parser = OptionParser(usage=usage, description=description, version=version)
parser.add_option("-g", "--groups", dest="groups",default="",
                  help="Additional groups membership, gids comma separated list")
parser.add_option("-l", "--ldap", dest="ldap", default=False,
                  help="Create an ldap user")
parser.add_option("-v", "--verbose", dest="verbose", default=False,
                  help="write log to standart input")
parser.add_option("-f","--file",dest="file",default="",
                  help="Open a file a read datas in it. File should containe one a line an username and his password one per line") 

(options, args) = parser.parse_args()


if len(args) < 2 and file=="" :
        print args
        print options
        parser.error("ligne de commande mal formatée. Veuillez rentrer un utilisateur et sont mot de passe ou bien -f et un fichier à utilisé")


if options.file != "":
    for line in open(options.file,"r").readlines():
        username,password = line.strip().split()
        creation(username,password)
else:
        username = sys.argv[1]
        password = sys.argv[2]
        creation(username,password)


os.remove(KADMIN_KEYTAB)

