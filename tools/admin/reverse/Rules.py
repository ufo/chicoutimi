#!/usr/bin/env python
# -*- coding: utf-8 -*-

import random
import os


class Rules():
    """ Rules in  chain list (iptables -L chain to see all rules in a Chain)"""

    def __init__(self,IP,PORT,proto,prefix):
        self.proto=proto
        self.ip=IP
        self.port=PORT
        self.table="KNOCKING/%s"%(proto,)
        self.prefix=prefix

    def iptFormat(self):
        return "-p %s --destination-port %s --source %s -j %s"%(self.proto,self.port,self.ip,self.prefix)


    def deleteEntry(self):
        os.system("iptables -D %s %s"%(self.table,self.iptFormat()))
        os.system("iptables -F %s"%(self.prefix,))
        os.system("iptables -X %s"%(self.prefix,))
        

    def insertEntry(self):
        os.system("iptables -N %s"%(self.prefix))
        os.system("iptables -A %s -j ULOG --ulog-cprange 1 --ulog-prefix %s"%(self.prefix,self.prefix))
        os.system("iptables -A %s -j DROP"%(self.prefix))
        os.system("iptables -I %s 1 %s"%(self.table,self.iptFormat()))

    def match(self,string):
        pass
        




def createKnockingTable():
    os.system("iptables -N KNOCKING/tcp")
    os.system("iptables -I INPUT -p tcp --destination-port 60000:65535 -j KNOCKING/tcp")
    os.system("iptables -N KNOCKING/udp")
    os.system("iptables -I INPUT -p udp --destination-port 60000:65535 -j KNOCKING/udp")

def deleteKnockingTable():
    os.system("iptables -D INPUT -p tcp --destination-port 60000:65535 -j KNOCKING/tcp")
    os.system("iptables -D INPUT -p udp --destination-port 60000:65535 -j KNOCKING/udp")
    os.system("iptables -F KNOCKING/tcp")
    os.system("iptables -F KNOCKING/udp")
    os.system("iptables -X KNOCKING/tcp")
    os.system("iptables -X KNOCKING/udp")

#createKnockingTable()

#a=Rules("127.0.0.1","61000","tcp")
#b=Rules("192.168.1.7","62000","udp")


#print("#### insert A #####")
#a.insertEntry()
#print("#### insert B #####")
#b.insertEntry()
#print("#### delete A #####")
#a.deleteEntry()
#print("#### delete B #####")
#b.deleteEntry()


