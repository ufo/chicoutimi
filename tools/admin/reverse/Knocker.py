#!/usr/bin/env python
# -*- coding: utf-8 -*-


PORT_MIN=60000
PORT_MAX=65535

SLOT_MIN=59990
SLOT_MAX=59999
NUM_SLOT=10
LIST_PROTO=['tcp','udp']
# number of knock to achive the sequence
NB_KNOCK=5

import Rules
import random

class Knocker():
    _listPrefix=[] 

    @staticmethod
    def generatePrefix():
        tmp=""
        str="azertyuiopqsdfghjklmwxcvbnAZERTYUIOPQSDFGHJKLMWXCVBN"
        while tmp=="" or "%s"%(tmp) in Knocker._listPrefix:
            tmp=""
            for i in range(0,15):
                tmp+=str[random.randrange(0,len(str))]
            
        Knocker._listPrefix.append("%s"%(tmp))
        return "%s"%(tmp) 

    @staticmethod
    def removePrefix(prefix):
        if prefix in Knocker._listPrefix:
            Knocker._listPrefix.remove(prefix)

    def __init__(self,clientIP):
        self.ip=clientIP
        self.prefix=Knocker.generatePrefix()
        self.generateSequence()
        pass
    
    def getPrefix(self):
        return self.prefix
    
    def enableKnocker(self):    
        self.sequence[NB_KNOCK-1].insertEntry()

    def stopKnocker(self):
        self.sequence.pop().deleteEntry()

    def matchedPacket(self):
        self.sequence.pop().deleteEntry()
        try:
            self.sequence[len(self.sequence)-1].insertEntry()
        except IndexError:
            print("Knocker.py::MatchedPAcket knocker %s Done !! let's open the Gate "%(self.ip))
            self.status = "Done"
            Knocker.removePrefix(self.prefix)

    def generateSequence(self):
        self.sequence=[ Rules.Rules(self.ip,random.randrange(PORT_MIN,PORT_MAX+1),LIST_PROTO[random.randrange(0,2)],self.prefix) for x in range(0,5) ]

#
#    _listForwardedPort={}
#    @staticmethod
#    def getForwardedPort():
#        if len([x for x in _listForwardedPort.iteritems() if x != "Unused"])<10:
#            for k,v in _listForwardedPort.iteritems():
#                if v == "Unused":
#                    _listForwardedPort[k]="Used"
#                    return v
#        else 
#            raise TooMuchConnexion
#
#    @staticmethod
#    def delForwardedPort(port):
#        _listForwardedPort[port]="Unused"
#
#    @staticmethod
#    def initialiseList():
#        for a in range(SLOT_MIN,SLOT_MAX):
#            Knocker._listForwardedPort[a]="Unused"
