#!/usr/bin/env python
# -*- coding: utf-8 -*-

import SimpleXMLRPCServer


MAX_SLOTS = 10
ERR_TOO_MUCH_CONNECTION = 1


class Connection:
    def __init__(self):
        self.port = 0
        self.status = 'unset'

    def getUnusedPort(self):
        
class QueueServer:
    usedSlot = 0
    
    def registerConnection(self):
        """ Le port retourné ne pourra jamais etre inférieur à 1024
            car les utlisateurs ne sont pas root donc ne peuvent ouvrir
            de serveur sur les port inférieurs."""
        if self.usedSlot < 10:
            self.usedSlot += 1
            return self.getUnusedPort()
        else:
            return ERR_TOO_MUCH_CONNECTION
             
    def getUnusedPort(self):
        return 6666

queue_server = QueueServer()
server = SimpleXMLRPCServer.SimpleXMLRPCServer(("localhost",8888))
server.register_instance(queue_server)

#Go into the main listener loop
print "Listening on port 8888"
server.serve_forever()
