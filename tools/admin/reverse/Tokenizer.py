#!/usr/bin/env python
# -*- coding: utf-8 -*-
import select,re,time

NAMED_PIPE="/var/log/ulogd/named.fifo"


def ListenAndThrow(callback):
    print("Tokenizer.py::ListenAndThrow started")
    tokenizer=re.compile('[a-zA-Z]{15}')
    p=select.poll()
    fifo=open(NAMED_PIPE,"r")
    p.register(fifo,select.POLLIN)

    while 1:
        event=p.poll()
        strfull=""
        for line in fifo.readline():
            #print line
            if line!='\n':
                strfull+=line
            else:
                print("ListenAndThrow: %s"%(strfull))
                matched=tokenizer.search(strfull).group()
                print("ListenAndThrow: matched pattern %s"%(matched))
                callback(matched)

    p.unregister(fifo.filneno())
    fifo.close()
    
#ListenAndThrow()
