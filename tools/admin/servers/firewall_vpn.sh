#!/bin/sh
# Attention aucune detection d'erreur n'est faites.

IP_REGEX='\([0-9]\+\.\)\{3\}[0-9]\+'
# Interface réseau pour les différent sous réseau
IF_INET=eth0
IF_VMS=virbr0

# Récupère les adresse IPS assigné pour chaque interface
# en francais adr en anglais addr
IP_INET=$(ifconfig $IF_INET | sed -n '2s/.*inet ad\{1,2\}r:\('$IP_REGEX'\).*Bcast.*/\1/p')
IP_VMS=$(ifconfig $IF_VMS | sed -n '2s/.*inet ad\{1,2\}r:\('$IP_REGEX'\).*Bcast.*/\1/p')


# On pourrait être sur une version anglaise donc Masque ou Mask
net_vms_mask=$(ifconfig $IF_VMS | sed -n '2s/.*Mas\(k\|que\):\('$IP_REGEX'\).*/\2/p')

prefix_net_vms=$(ipcalc -p $IP_VMS $net_vms_mask)
net_vms=$(ipcalc -n $IP_VMS $net_vms_mask)


# retour d'ipcalc ELEMENT=<valeur>, donc on vire le égal ainsi que tout ce qui précéde
NET_VMS_CIDR="${net_vms##*=}/${prefix_net_vms##*=}"
NET_INET_CIDR="0.0.0.0/0"

# Liste des services disponible et devant être redirigés sur les VMs
SERVICES_INET_VMS=$(mktemp)
# Accessible sur l'hote local depuis internet
SERVICES_INET_LOCAL=$(mktemp)
# Accessible seulement pour les VMS sur l'hote (reseau privé des VMS) 
SERVICES_VMS_LOCAL=$(mktemp)
# Access des VMS vers l'internet
SERVICES_VMS_INET=$(mktemp)

# Port Protocole
cat <<EOF >$SERVICES_INET_LOCAL
22 tcp
EOF

# PortLocal Protocole Destination PortDestination
cat <<EOF >$SERVICES_INET_VMS
139 udp coda.alpha.agorabox.org 139
22501 tcp vpn.agorabox.org 22
1194 tcp vpn.agorabox.org 1194
EOF


# Port Protocole
cat <<EOF >$SERVICES_VMS_LOCAL
53 udp
53 tcp
EOF

# Port Protocole
cat <<EOF >$SERVICES_VMS_INET
1194 tcp
139 tcp
EOF

#Apperement les modules utiles a iptables sont insérer autoamatiquement
#Activate forwarding
echo 1 > /proc/sys/net/ipv4/ip_forward

# Effacement de toutes les regles et mise en place des politque par défaut
# Pour la machine host on authorise tout en sortit
iptables -t filter -F
iptables -t filter -X
iptables -P INPUT DROP
iptables -P OUTPUT ACCEPT
iptables -P FORWARD DROP

## Initialisation de la table NAT
# Tout le filtrage sur le NAT se fait via la règle FORWARD de la 
# table INPUT
iptables -t nat -F
iptables -t nat -X
iptables -t nat -P PREROUTING  ACCEPT
iptables -t nat -P POSTROUTING ACCEPT
iptables -t nat -P OUTPUT      ACCEPT

## Initialisation de la table MANGLE
iptables -t mangle -F
iptables -t mangle -X
iptables -t mangle -P PREROUTING  ACCEPT
iptables -t mangle -P INPUT       ACCEPT
iptables -t mangle -P OUTPUT      ACCEPT
iptables -t mangle -P FORWARD     ACCEPT
iptables -t mangle -P POSTROUTING ACCEPT

## Authorisation de toutes les connexions locales
iptables -A INPUT -i lo -j ACCEPT

## On authorise par défaut toutes les réponses aux connexions établie par l'hôte
iptables -A INPUT -m state --state ESTABLISHED,RELATED -j ACCEPT
## On authorise les requète icmp sur l'hôte
# Todo: filtrer un peu poru ne pas repondre a plus de N echo par seconde.
iptables -A INPUT -p icmp -j ACCEPT

while read port proto; do
    iptables -A INPUT -m state --state NEW -m $proto -p $proto --dport $port -j ACCEPT
done<$SERVICES_INET_LOCAL

while read port proto; do
    iptables -A INPUT -i $IF_VMS -p $proto -m $proto --dport $port -j ACCEPT
done<$SERVICES_VMS_LOCAL

# Activation du masquerade 
# remplacement de l'adresses sources pour les paquets sortant de réseau des VMs en direction d'internet
# par l'adresse de l'hôte.
# les adresse local ne sont pas routables sur internet n'importe qu'elle routeur les refuseraient
iptables -t nat -A POSTROUTING -o $IF_INET -s $NET_VMS_CIDR ! -d $NET_VMS_CIDR -j MASQUERADE

# Il n'existe pas de réelle réseau privé entre les VMS tout les paquets qui
# en sortent passe par l'interface $IF_VMS de l'hôte donc passeront sur la table forward
# On authorise les VMs à communiquer entre elle sans restrictions
iptables -A FORWARD -i $IF_VMS -o $IF_VMS -s $NET_VMS_CIDR -d $NET_VMS_CIDR -j ACCEPT

# Accepte le transit du traffic déja inité 
# Dans ls deux sens.
iptables -A FORWARD ! -s $NET_VMS_CIDR -i $IF_INET -d $NET_VMS_CIDR -o $IF_VMS -m state --state ESTABLISHED,RELATED -j ACCEPT
iptables -A FORWARD -s $NET_VMS_CIDR -i $IF_VMS ! -d $NET_VMS_CIDR -o $IF_INET -m state --state ESTABLISHED,RELATED -j ACCEPT

while read port proto; do
    iptables -A FORWARD -d $NET_INET_CIDR -i $IF_VMS -o $IF_INET -m state --state NEW -j ACCEPT
done<$SERVICES_VMS_INET

while read ps proto dst pd;
do
        host_ip=$(python -c 'import socket; print socket.gethostbyname("'$dst'")')
        iptables -t nat -A PREROUTING -i $IF_INET '!' -s $NET_VMS_CIDR -d $IP_INET -m $proto -p $proto --dport $ps -m state '!' --state INVALID,UNTRACKED -j DNAT  --to-destination $host_ip:$pd
        iptables -A FORWARD -d $host_ip -i $IF_INET -o $IF_VMS -m state --state NEW -m $proto -p $proto --dport $pd -j ACCEPT
done<$SERVICES_INET_VMS

rm $SERVICES_INET_VMS
rm $SERVICES_VMS_LOCAL
rm $SERVICES_INET_LOCAL
rm $SERVICES_VMS_INET
