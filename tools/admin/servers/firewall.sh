#!/bin/sh
# Attention aucune detection d'erreur n'est faites.


[ "$1" = "-v" ] && VERBOSE=1

function debug () {

	if [ "$VERBOSE" = "1" ]; then
		echo $1
	else 
		$1
	fi
}

IP_REGEX='\([0-9]\+\.\)\{3\}[0-9]\+'
HOSTNAME=$(hostname -s)
CONF_FILE="$(hostname -s)_firewall.conf"

# Récupère les adresse IPS assigne à l'interface passe en parametre
# en francais adr en anglais addr
function getIfIP () {
    #echo getIfIP
    ifconfig $1 | sed -n '2s/.*inet ad\{1,2\}r:\('$IP_REGEX'\).*[BP].*/\1/p'
}

function getIfMask () {
    #echo getIfMask
    ifconfig $1 | sed -n '2s/.*Mas\(k\|que\):\('$IP_REGEX'\).*/\2/p'
}

# Retourne l'adresse du reseau  sur lequelle est connecté une interface
# sous la forme net/bit
# params : 1 interface
function getNetCIDR () {
    #echo getNetCIDR
# On pourrait être sur une version anglaise donc Masque ou Mask
    ip=$(getIfIP $1)
    net_mask=$(getIfMask $1)
    prefix_net=$(ipcalc -p $ip $net_mask)
    net=$(ipcalc -n $ip $net_mask)
    # retour d'ipcalc ELEMENT=<valeur>, donc on vire le égal ainsi que tout ce qui précéde
    echo "${net##*=}/${prefix_net##*=}"
}

function getFileConfFromFile (){
    #echo $getConfFromFile
    tofile=$(mktemp)
    sed -e "/# $1/,/^##/!d" -e "/^#.*$/d" $CONF_FILE > $tofile
    echo $tofile
}

function getConfFromFile (){
    sed -e "/# $1/,/^##/!d" -e "/^#.*$/d" $CONF_FILE
}


# Interface réseau pour les différent sous réseau
IF_INET=$(getConfFromFile IF_INET)
IF_VMS=$(getConfFromFile IF_VMS)
IF_LAN=$(getConfFromFile IF_LAN)
IF_VPN_1=$(getConfFromFile IF_VPN_1)

IP_INET=$(getIfIP $IF_INET)
IP_VMS=$(getIfIP $IF_VMS)
IP_VPN_1=$(getIfIP $IF_VPN_1)

NET_VMS_CIDR=$(getNetCIDR $IF_VMS)
NET_VPN_1_CIDR=$(getNetCIDR $IF_VPN_1)
NET_LAN_CIDR="192.168.1.0/24"
NET_VMS_GLOBAL_CIDR="192.168.122.0/23"
NET_INET_CIDR="0.0.0.0/0"

# Liste des services disponible et devant être redirigés sur les VMs
SERVICES_INET_VMS=$(getFileConfFromFile SERVICES_INET_VMS)
# Accessible sur l'hote local depuis internet
SERVICES_INET_LOCAL=$(getFileConfFromFile SERVICES_INET_LOCAL)
# Accessible seulement pour les VMS sur l'hote (reseau privé des VMS) 
SERVICES_VMS_LOCAL=$(getFileConfFromFile SERVICES_VMS_LOCAL)
# Access des VMS vers l'internet
SERVICES_VMS_INET=$(getFileConfFromFile SERVICES_VMS_INET)
# Acces du VPN au VMS
# Utilisation futur peut-etre
#SERVICES_VPN_VMS=$(getFileConfFromFile SERVICES_VPN_VMS)

# Utile pour debug
# echo IF_INET $IF_INET IP_INET $IP_INET NET_INET_CIDR $NET_INET_CIDR 
# echo IF_VMS $IF_VMS IP_VMS $IP_VMS NET_VMS_CIDR $NET_VMS_CIDR
# echo NET_LAN_CIDR $NET_LAN_CIDR 
# echo IF_VPN_1 $IF_VPN_1 IP_VPN_1 $IP_VPN_1 NET_VPN_1_CIDR $NET_VPN_1_CIDR
# echo NET_VMS_GLOBAL_CIDR $NET_VMS_GLOBAL_CIDR

#Apparement les modules utiles a iptable sont insérer autoamatiquement
#Activate forwarding
echo 1 > /proc/sys/net/ipv4/ip_forward

# Effacement de toutes les règles et mise en place des politques par défaut
# Pour la machine host on authorise tout en sortie
debug "iptables -t filter -F"
debug "iptables -t filter -X"
debug "iptables -P INPUT DROP"
debug "iptables -P OUTPUT ACCEPT"
debug "iptables -P FORWARD DROP"

## Initialisation de la table NAT
# Tout le filtrage sur le NAT se fait via la règle FORWARD de la 
# table INPUT, comme ça le traffic est déja rerouté - tables CHAINE PREROUTING
debug "iptables -t nat -F"
debug "iptables -t nat -X"
debug "iptables -t nat -P PREROUTING  ACCEPT"
debug "iptables -t nat -P POSTROUTING ACCEPT"
debug "iptables -t nat -P OUTPUT      ACCEPT"

## Initialisation de la table MANGLE
debug "iptables -t mangle -F"
debug "iptables -t mangle -X"
debug "iptables -t mangle -P PREROUTING  ACCEPT"
debug "iptables -t mangle -P INPUT       ACCEPT"
debug "iptables -t mangle -P OUTPUT      ACCEPT"
debug "iptables -t mangle -P FORWARD     ACCEPT"
debug "iptables -t mangle -P POSTROUTING ACCEPT"

## Authorisation de toutes les connexions sur le loopback
debug "iptables -A INPUT -i lo -j ACCEPT"

## On authorise par défaut toutes les réponses aux connexions établie par l'hôte
debug "iptables -A INPUT -m state --state ESTABLISHED,RELATED -j ACCEPT"
## On authorise les requète icmp sur l'hôte
# Todo: Mettre en place un chaine afin de gérer les floog icmp
debug "iptables -A INPUT -p icmp -j ACCEPT"

# Authorisation des services accessible depuis le net pour l'hote
while read port proto; do
    debug "iptables -A INPUT -m state --state NEW -m $proto -p $proto --dport $port -j ACCEPT"
done<$SERVICES_INET_LOCAL

# Authorisation des traffics depuis les VMS vers l'hote locales
while read port proto; do
    debug "iptables -A INPUT -i $IF_VMS -p $proto -m $proto --dport $port -j ACCEPT"
done<$SERVICES_VMS_LOCAL

# Activation du masquerade 
# remplacement de l'adresses sources pour les paquets sortant de réseau des VMs en direction d'internet
# par l'adresse de l'hôte.
# les adresse local ne sont pas routables sur internet n'importe qu'elle routeur les refuseraient
debug "iptables -t nat -A POSTROUTING -o $IF_INET -s $NET_VMS_CIDR ! -d $NET_VMS_CIDR -j MASQUERADE"

# Il n'existe pas de réelle réseau privé entre les VMS tout les paquets qui
# en sortent passe par l'interface $IF_VMS de l'hôte donc, passeront sur la table forward.
## NEW SECTION
# On authorise sur le forward tout les paquets établis ou relié
# Les créations de traffic seront 
debug "iptables -A FORWARD -m state --state ESTABLISHED,RELATED -j ACCEPT"

## ATTENTION ORDRE IMPORTANT
# Tout d'abord on filtre le trafic arrivant aux vms local avec l'etat NEW
# SI le traffic n'est pas destiné à une vm locales on l'accepte
# Creation d'une table pour filtrer les demandes de service depuis un reseau de vms distantes
debug "iptables -N F_G_VMS"
# Pour saute dans cette table c'est spécifique à l'hote 
debug "iptables -A FORWARD -d $NET_VMS_CIDR -s $NET_VMS_GLOBAL_CIDR -m state --state NEW -j F_G_VMS"
# Ajout des regles;
debug "iptables -A F_G_VMS -p icmp -j ACCEPT"
debug "iptables -A F_G_VMS -j LOG --log-prefix \"traf_inter_vms\""
# On droppe tout les paquets non accepté
debug "iptables -A F_G_VMS -j DROP"
# Authorise les VMS à demander n'importe quelle services aux autres vms
debug "iptables -A FORWARD -d $NET_VMS_GLOBAL_CIDR -s $NET_VMS_CIDR -m state --state NEW -j ACCEPT"
## ORDRE IMPORTANT

# Creation d'une règle pour filtrer les traffic provenant du lan
debug "iptables -A FORWARD -s $NET_LAN_CIDR -d $NET_VMS_GLOBAL_CIDR -m state --state NEW -j ACCEPT"
## END_NEW


# Création en masses des règles de traffic entre les vms et le net
while read port proto; do
    debug "iptables -A FORWARD -d $NET_INET_CIDR -i $IF_VMS -o $IF_INET -m $proto -p $proto --dport $port -m state --state NEW -j ACCEPT"
done<$SERVICES_VMS_INET

while read ps proto dst pd;
do
        host_ip=$(python -c 'import socket; print socket.gethostbyname("'$dst'")')
        debug "iptables -t nat -A PREROUTING -i $IF_INET ! -s $NET_VMS_CIDR -d $IP_INET -m $proto -p $proto --dport $ps -m state ! --state INVALID,UNTRACKED -j DNAT  --to-destination $host_ip:$pd"
        debug "iptables -A FORWARD -d $host_ip -i $IF_INET -o $IF_VMS -m state --state NEW -m $proto -p $proto --dport $pd -j ACCEPT"
done<$SERVICES_INET_VMS

# Utile pour debug
#debug "iptables -A FORWARD -j LOG --log-prefix \"FORWARD_DROP\""

rm $SERVICES_INET_VMS
rm $SERVICES_VMS_LOCAL
rm $SERVICES_INET_LOCAL
rm $SERVICES_VMS_INET
#rm $SERVICES_VPN_VMS
