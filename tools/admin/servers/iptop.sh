#!/bin/sh


temp_file=$(mktemp)

trap "rm $temp_file" EXIT

while true; do
	echo " ============================ FILTER TABLE ==============================" > $temp_file
	iptables -L -v >> $temp_file 
	echo >>$temp_file
	echo >>$temp_file
	echo " ============================== NAT TABLE ===============================" >> $temp_file
	iptables -L -v -t nat >> $temp_file
	clear
    cat $temp_file

	sleep 1
done
