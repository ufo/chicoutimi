Connexion internet limitée.

Le mode survivor, permet de se connecter au réseau agorabox même au travers
d'une connection limitée.
La technique utilisé est celle du tunnel VPN.


1. Condition d'établissement.
En premier lieu, une routine devra déterminer, si oui ou non une
connection internet existe, et si elle n'est pas limitée.
On entend par limitée, une connexion, ou les ports nécessaire à l'accès aux
services sont fermés, et ou seule les connection Web - flux http et surtout
https - sont authorisés, au travers d'un proxy.

2. Etablissment.
Dans le cas d'une connexion limitée, on établit un tunnel VPN au travers
du proxy. Ceci crée une interface réseau virtuel par laquelle les flux
d'authentification et du système de fichier réseau passeront.
Le point d'entrée de base du réseau est l'adresse:
    - gates.agorabox.org sur le port 443.


3. Conditions légales.
Attention toutefois, la plupart des chartes d'utilisations interdisent ce
procédé. Il faut voir à proposer une notice, afin de ne pas passer outre les
conditions d'utilisations du réseau hôtes automatiquement, et ainsi engager la
responsabilité de l'utilisateur.

 
II. Techniques.
1. Lien Virtuel
Lors de l'établisemment de la connection VPN, une interface virtuel est créer
sur chacun des hotes, et utilise le protocole PPtP - protocle agissant sur la
couche 3(réseau) du modèle OSI.

Le Lien PPtP possède une adresse ip pour chaque bout. Il existe donc un réseau
IP, spécifique au lien VPN. Ce réseau fonctionne de manière standard, il est
régit par les mème règles que tout autre réseau IP.


1. Encapsulation.
Le réseau est virtuel car en fait tout les paquets, sont transmis au proxy, a
l'interieur de paquet HTTP. Le proxy se charge ensuite de le transmettre au
serveur VPN pensant que ce sont des paquets valide du connection Web sécurisée
par SSL.


2. Services Proposés.
Les services proposer sur le VPN seront uniquement les services proposés en
marge de la clé. Il n'est donc pas utile de rediriger tout le traffic réseau
sur le VPN, seulement les flux coda, kerberos.
Pour ceci la technique adoptée et de "pousser" sur la configuration client
les couples FQDN/IP des hotes assurant ces services.
Le fichier modifié est /etc/resolv.conf



-- config client

dev tun0
remote gates.agorabox.org 443
ca /etc/openvpn/keys/ca.crt
cert /etc/openvpn/keys/testvpn.crt
key /etc/openvpn/keys/testvpn.key

tls-client
proto tcp-client
http-proxy 192.168.1.4 3128
float
verb 3
#Maquillage du Navigateur.
http-proxy-option AGENT Mozilla/5.0 (X11; U; Linux x86_64; fr; rv:1.9.0.4) Gecko/2008111217 Fedora/3.0.4-1.fc10 Firefox/3.0.4
http-proxy-retry

pull


-- config serveur


dev tun
server 10.8.0.0 255.255.255.0
topology p2p
mode server
proto tcp-server
tls-server
proto tcp-server
float
verb 10
ca /etc/openvpn/keys/ca.crt 
cert /etc/openvpn/keys/openvpn.crt
key /etc/openvpn/keys/openvpn.key
dh /etc/openvpn/keys/dh2048.pem
crl-verify /etc/openvpn/keys/crl.pem
client-to-client
port 443
port-share kickstart.agorabox.org 444
