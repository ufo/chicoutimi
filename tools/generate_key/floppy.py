from genutils import *

def bind_floppy_grub(vfatmountpoint, mountpoint):
    floppy = mount(os.path.join(vfatmountpoint, ".data", ".VirtualBox", "Images", "UFO-VirtualBox-boot.img"), name="floppy", options="loop")
    mount(os.path.join(floppy, "boot", "grub"), name="bindgrub", mountpoint=os.path.join(mountpoint, "boot", "grub"), options="bind")

def unbind_floppy_grub():
    umount("bindgrub")
    umount("floppy")
