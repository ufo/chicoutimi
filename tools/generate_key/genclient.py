#!/usr/bin/python
# -*- coding: utf-8 -*-

import Pyro.core
import time
import gencommon
import sys
from optparse import OptionParser

usage = """%prog launch [ dev0 [ dev1 ... ] | loop maxusb | tasks | state | usb | list | clean | reload | graphical"""
description = "Generates either an Agorabox prototype, either an Agorabox USB key (serie)"
version="%prog 0.1"

parser = OptionParser(usage=usage, description=description, version=version)
parser.add_option("--host", dest="host", default="localhost",
                  help="specify host")
parser.add_option("-p", "--port", dest="port", default=7766,
                  help="specify port")
parser.add_option("-n", "--name", dest="name", default="",
                  help="specify remote object name")
parser.add_option("-r", "--repeat", dest="repeat",
                  help="repeat the command for ever")
parser.add_option("-g", "--graphical", dest="graphical",
                  help="show graphical progress bars")
# parser.add_option("", "--max-usb", dest="maxusb",
#                   help="specify the number of USB keys to generate")
(options, args) = parser.parse_args()

if args:
    command = args[0]
    args = args[1:]
else:
    parser.error("No command was specified")

if options.name: options.name = "-" + options.name
server = Pyro.core.getProxyForURI("PYROLOC://%s:%d/keygen%s" % (options.host, options.port, options.name))
try:
    server.ping()
except Pyro.errors.ProtocolError, e:
    print "Could not connect to host. Make sure the server is running."
    sys.exit(1)
        
def process_launch():
    if not args:
        parser.error("You must specify the prototype to use")
    keys = []
    if len(args) > 1:
        for dev in args[1:]:
            keys.append({ "dev" : dev })
    server.launch(protoname = args[0], keys = keys)

def process_loop():
    if not args:
        parser.error("You must specify the number of USB keys to generate and the prototype to use")
    server.set_max_usb(int(args[0]))
    server.loop(protoname = args[1])

def process_tasks():
    tasks = server.get_running_tasks()
    if tasks:
        print len(tasks), "tasks:"
        for task in tasks:
            print "Free space", task.get_free_space()
    else:
        print "No running task"

def process_state():
    state = server.get_state()
    print "Server's state :", [ "initializing", "Ready", "Generating" ][state - 1]

def process_usb():
    controllers = server.detect_usb_controllers()
    print len(controllers), "USB controllers:"
    for usb in controllers:
        print usb

    usb = server.detect_plugged_usb()
    print len(usb), "USB keys detected:"
    print " ".join([ x["dev"] for x in usb ])

def process_caps():
    print server.get_capabilities()

def process_report():
    print server.get_report()

def process_graphical():
    from PyQt4 import QtCore, QtGui

    class LabelLog:
        def __init__(self, task, parent):
            self.dev = task.dev
            self.parent = parent

        def show_log(self):
            logwin = QtGui.QDialog(self.parent)
            layout = QtGui.QVBoxLayout()
            layout.addWidget(QtGui.QPlainTextEdit(QtCore.QString(server.get_log(self.dev))))
            logwin.setLayout(layout)
            logwin.setMinimumSize(800, 600)
            #QtGui.QPlainTextDocumentLayout(QtGui.QTextDocument()))
            logwin.show()
            

    class MainWindow(QtGui.QMainWindow):
        def __init__(self):
            QtGui.QMainWindow.__init__(self)
            self.central = QtGui.QWidget(self)
            self.setWindowTitle(QtCore.QString("UFO-key-generator"))
            self.resize(500, 300)
            self.progressLayout = QtGui.QVBoxLayout()
            self.central.setLayout(self.progressLayout)
            self.setCentralWidget(self.central)

            self.last_state = server.get_state()
            self.statusBar().showMessage("Etat du serveur: " + [ "initializing", "Ready", "Generating" ][ self.last_state - 1])
            self.bars = None
            self.devs = None
            self.buttons = None
            
        def addProgress(self, task):
            if self.devs:
                hline = QtGui.QFrame(self)
                hline.setFrameShape(QtGui.QFrame.HLine)
                hline.setFrameShadow(QtGui.QFrame.Sunken)
                self.progressLayout.addWidget(hline)
            else:
                self.devs = {}
                self.bars = {}
                self.buttons = {}

            progressEntry = QtGui.QHBoxLayout()
            labelEntry = QtGui.QHBoxLayout()
            logButton = QtGui.QPushButton(QtGui.QIcon("/usr/share/pixmaps/gnome-news.png"), "", self)
            self.buttons[logButton] = LabelLog(task, self)
            self.connect(logButton, QtCore.SIGNAL("clicked()"), self.buttons[logButton].show_log)
            logButton.setFlat(True)
            
            label = QtGui.QLabel(task.dev)
            labelEntry.addWidget(label, 0, QtCore.Qt.AlignRight)
            labelEntry.addWidget(logButton, 0, QtCore.Qt.AlignRight)
            progressEntry.addLayout(labelEntry)
            progressLayout = QtGui.QHBoxLayout()
            rootProgressBar = QtGui.QProgressBar()
            fatProgressBar = QtGui.QProgressBar()
            fatProgressBar.setMaximumWidth(150)
            rootProgressBar.setMinimumWidth(400)
            self.devs[task.dev] = label
            self.bars[task.dev + "1"] = fatProgressBar
            self.bars[task.dev + "3"] = rootProgressBar
            progressLayout.addWidget(rootProgressBar, 0, QtCore.Qt.AlignRight)
            progressLayout.addWidget(fatProgressBar, 0, QtCore.Qt.AlignRight)
            progressEntry.addLayout(progressLayout)

            self.progressLayout.addLayout(progressEntry)

        def updateProgress(self, task):
            free = task.get_free_space()
            # fat partiton at sd*1 is finished when 43%
            if free.has_key(task.dev + "1"):
                real_percent = min(100, int(free[task.dev + "1"][:len(free[task.dev + "1"]) - 1]) * (100./43.))
                self.bars[task.dev + "1"].setValue(real_percent)
            else:
                self.bars[task.dev + "1"].setValue(0)
            # fat partiton at sd*3 is finished when 74%
            if free.has_key(task.dev + "3"):
                real_percent = min(100, int(free[task.dev + "3"][:len(free[task.dev + "3"]) - 1]) * (100./74.))
                self.bars[task.dev + "3"].setValue(real_percent)
            else:
                self.bars[task.dev + "3"].setValue(0)

        def resetProgress(self, task):
            self.bars[task.dev + "3"].setValue(0)
            self.bars[task.dev + "1"].setValue(0)

    app=QtGui.QApplication(sys.argv)
    main=MainWindow()
    main.show()

    cpt = 0
    while True:
        if cpt == 10:
            cpt = 0
            tasks = server.get_running_tasks()
            report = server.get_report()
            if tasks:
                for task in tasks:
                    if main.devs and task.dev in main.devs:
                        main.updateProgress(task)
                    else:
                        main.addProgress(task)

            # Bouhhhh
            if main.devs:
                for dev in main.devs.keys():
                    found = False
                    if tasks:
                        for task in tasks:
                            if task.dev == dev:
                                found = True
                        if not found:
                            class Holder: pass
                            t = Holder()
                            t.dev = dev
                            main.resetProgress(t)
                            if not report:
                                label = QtCore.QString("<font color=white>" + key + "</font>")
                                main.devs[key].setText(label)
            if report:
                for key in report.keys():
                    if not main.devs or not key in main.devs:
                        class Holder: pass
                        t = Holder()
                        t.dev = key
                        main.addProgress(t)
                    if int(report[key]) != 0:
                        label = QtCore.QString("<font color=red>" + key + "</font>")
                    else:
                        label = QtCore.QString("<font color=blue>" + key + "</font>")
                    if main.devs:
                        main.devs[key].setText(label + " : return "  + QtCore.QString(str(report[key])))
            else:
                if main.devs:
                    for key in main.devs.keys():
                        main.devs[key].setText(QtCore.QString(key))
        
            s = server.get_state()
            if main.last_state != s:
                main.last_state = s
                main.statusBar().showMessage("Etat du serveur: " + [ "initializing", "Ready", "Generating" ][ s - 1])

        app.processEvents()
        time.sleep(0.1)
        cpt += 1

def process_reload():
    print "Reloading configuration"
    server.reload()

def process_clean():
    print "Cleaning everything"
    server.cleanup()

def process_list():
    print "Available prototypes:"
    for proto in server.list_prototypes():
        print proto["title"] + ":"
        print "  name =", proto["name"]
        print "  root =", proto["path"]
        print "  args =", proto["args"]

def process_command(command):
    func = globals().get("process_" + command, None)
    if func:
        func()
    else:
        parser.error("Unknown command : " + command)

if __name__ == "__main__":
    process_command(command)
