#!/usr/bin/python
# -*- coding: utf-8 -*-

import Pyro.core
from daemonizer import Daemonizer
import glob
import logging
import struct
import socket
import threading
import exceptions
import subprocess
import time
import sys
import os, os.path as path
import gencommon
from optparse import OptionParser
from ConfigParser import ConfigParser

if len(sys.argv) > 1 and path.basename(sys.argv[1]) == "python":
    exec_path = path.dirname(path.abspath(sys.argv[1]))
else:
    exec_path = path.dirname(path.abspath(sys.argv[0]))

os.chdir(exec_path)

INITIALIZING = 1
READY = 2
GENERATING = 3

usage = """%prog hostname"""
description = "Launch the key generator daemon"
version="%prog 0.1"
parser = OptionParser(usage=usage, description=description, version=version)
parser.add_option("-l", "--log", dest="logfile",
                  help="write logs to FILE", metavar="FILE")
parser.add_option("-p", "--pidfile", dest="pidfile", default="", metavar="FILE",
                  help="write the PID to FILE") 
parser.add_option("-n", "--name", dest="name", default="",
                  help="specify remote object name")
parser.add_option("-m", "--model", dest="model", default="all",
                  help="generates on USB keys of the specified model", metavar="MODEL")
parser.add_option("-d", "--daemon", dest="daemon", default=False, action="store_true",
                  help="daemonize the server")
parser.add_option("-r", "--reports", dest="reports", default="reports",
                  help="specify the folder to put the reports into")
parser.add_option("-t", "--prototypes", dest="protopath", default="",
                  help="specify the folder that contains the prototypes")
parser.add_option("-a", "--arguments", default="",
                  help="the arguments that are passed to the generation program")
parser.add_option("-c", "--config", dest="configfile", default="/etc/ufokeygen.conf",
                  help="the configuration file to use")
parser.add_option("--test", dest="testing", default=False, action="store_true",
                  help="do not actually perform generation process")
# parser.add_option("-e", "--end", dest="ending_stage",
#                   help="ends the script after the specified stage", metavar="STAGE")
(options, args) = parser.parse_args()

if not options.model:
    parser.error("You must specify a USB key model")

if options.logfile:
    logging.basicConfig(level=logging.DEBUG, filename=options.logfile)
else:
    logging.basicConfig(level=logging.DEBUG)

if os.getuid() != 0:
    logging.critical("You must be root to run this script")
    sys.exit(1)

if not os.path.isabs(options.reports):
    options.reports = path.join(exec_path, options.reports)
if not os.path.isdir(options.reports):
    os.mkdir(options.reports)
logging.info("Writing reports to " + options.reports)

class KeyGenerator(Pyro.core.ObjBase):
    def __init__(self):
        Pyro.core.ObjBase.__init__(self)
        self.state = INITIALIZING
        self.controllers = self.detect_usb_controllers()
        self.max_usbs = len(self.controllers)
        self.keys = []
        self.tasks = []
        self.ip = socket.gethostbyname(socket.gethostname())
        self.hostname = socket.gethostname()
        self.state = READY
        self.stop = False
        self.report = None
        self.verbosity = "debug"
        self.args = []
        self.prototypes = {}
        self.load_config_file()

    def load_config_file(self):
        self.prototypes = {}
        self.cf = ConfigParser()
        try:
            self.cf.read(options.configfile)
        except:
            logging.error("Failed to load config file. Exiting...")
            sys.exit(1)
        self.protopath = self.cf.get("global", "protopath")
        for section in [ x for x in self.cf.sections() if x != "global" ]:
            name, proto = self.load_proto(self.cf, section)
            self.prototypes[name] = proto

    def load_proto(self, cf, section):
        logging.debug("Loading prototype " + section)
        proto = {}
        proto["title"] = cf.get(section, "title")
        proto["path"] = cf.get(section, "path")
        proto["args"] = cf.get(section, "args")
        proto["name"] = section
        if cf.has_option(section, "generator"):
            proto["generator"] = cf.get(section, "generator")
        else:
            providedgen = path.join(proto["path"], "generator", "launch.py")
            if path.exists(providedgen):
                proto["generator"] = providedgen
            else:
                proto["generator"] = path.join(exec_path, "launch.py")
        return section, proto

    def detect_usb_controllers(self):
        controllers_ = glob.glob("/sys/bus/pci/drivers/ehci_hcd/00*")
        controllers = []
        for controller in controllers_:
            usbs = glob.glob(controller + "/usb[0-9]*")
            for usb in usbs:
                slots = int(open(usb + "/maxchild").read())
                controllers.append({ "slots" : slots })
        return controllers

    def detect_plugged_usb(self):
        if options.model == "all":
            model = ""
        else:
            model = options.model
        output = subprocess.Popen([ "devkit-disks", "--enumerate-device-files" ],
                                  stdout=subprocess.PIPE).communicate()[0]
        lines = output.split("\n")
        i, j = 0, 0
        keys = []
        while i < len(lines) and len(lines[i]):
            if not lines[i].startswith("/dev/disk/") and \
               not lines[i][-1].isdigit():
               j = i + 1
               while j < len(lines):
                   if not os.path.islink(lines[j]):
                       break
                   j += 1
               if "by-id/usb-" + model in " ".join(lines[i:j]):
                   keys.append({ "dev" : lines [i] })
               i = j - 1               
            i += 1
        return keys
        
    def ping(self):
        return "pong"

    def set_max_usb(self, n):
        self.max_usbs = n
        
    def get_max_usb(self):
        return self.max_usbs

    def stop(self):
        self.stop = True

    def detect_plugging(self, old):
        new = set([ x["dev"] for x in self.detect_plugged_usb() ])
        plugged = new - old
        unplugged = old - new
        if len(plugged):
            for key in plugged:
                logging.debug(key + " was plugged")
        if len(unplugged):
            for key in unplugged:
                logging.debug(key + " was unplugged")
        return new
    
    def loop(self, prototype):
        while True:
            logging.info("Generation loop")
            usbs = set([ x["dev"] for x in self.detect_plugged_usb() ])
            logging.debug("Found " + str(len(usbs)) + " plugged keys")
            logging.debug("Waiting for " + str(self.max_usbs) + " USB keys")
            while not self.stop and len(usbs) < self.max_usbs:
                usbs = self.detect_plugging(usbs)
                time.sleep(3)
            logging.info("Launching generations")
            self.report = None
            tasks = self.launch(protoname = prototype)
            logging.info("Waiting for completion")
            while not self.stop and self.state != READY:
                time.sleep(3)
            self.report = {}
            for task in tasks:
                self.report.update(task.report())
            logging.info("Waiting for unplugging")
            while not self.stop and len(self.detect_plugged_usb()):
                usbs = self.detect_plugging(usbs)
                time.sleep(3)

        self.stop = False

    def get_launcher_args(self, proto):
        return self.args + proto["args"].split(" ") + \
               [ "--verbose=" + self.verbosity, "-s",
                 proto["path"],
                 proto["name"], "foo" ]

    def launch(self, protoname, keys={}):
        if self.state != READY:
            raise exceptions.Exception("Server is not ready")
        if not self.prototypes.has_key(protoname):
            raise exceptions.Exception("Unknown prototype")
        prototype = self.prototypes[protoname]
        if not path.exists(prototype["generator"]):
            raise exceptions.Exception("Invalid generator " + prototype["generator"])
        self.state = GENERATING
        tasks = []
        self.tasks = []
        if keys:
            self.keys = keys
        else:
            self.keys = self.detect_plugged_usb()
        for key in self.keys:
            task = gencommon.KeygenTask(key["dev"], self,
                                        prototype = prototype,
                                        args = self.get_launcher_args(prototype),
                                        logfile=time.strftime(path.join(options.reports, "%d-%m-%Y-%H:%M-" + os.path.basename(key["dev"]) + ".log")),
                                        testing = options.testing)
            tasks.append(task)
            self.tasks.append(task)
            task.start()
        return tasks

    def notify_finished(self, task):
        self.tasks.remove(task)
        if not self.tasks:
            self.state = READY

    def get_state(self):
        return self.state

    def get_log(self, dev):
        for task in self.tasks:
            if task.dev == dev:
                return task.get_output()
        return u"Log non trouvé"

    def get_running_tasks(self):
        return [ task for task in self.tasks if task.thread.is_alive() ]

    def get_report(self):
        if self.report:
            return self.report
        else:
            return None

    def look_for_prototypes(self):
        protos = glob.glob("proto-*")
        return protos

    def list_prototypes(self):
        return self.prototypes.values()

    def get_capabilities(self):
        return self.keys

    def get_host_name(self):
        return self.hostname

    def reload(self):
        self.load_config_file()

    def cleanup(self):
        for report in glob.glob(path.join(options.reports, "*.log")):
            os.unlink(report)

class KeygenDaemon(Pyro.core.Daemon, Daemonizer):
    def __init__(self):
        Pyro.core.Daemon.__init__(self)
        Daemonizer.__init__(self)

    def main_loop(self):
        self.requestLoop()
    
Pyro.core.initServer()
daemon = KeygenDaemon()
keygen = KeyGenerator()

if options.name: options.name = "-" + options.name
uri = daemon.connect(keygen, "keygen" + options.name)

print "The daemon runs on port:", daemon.port
print "The object's uri is:", uri

if options.pidfile:
    open(options.pidfile, "w").write(str(os.getpid()))
    
if options.daemon:
    daemon.daemon_start()
else:
    daemon.requestLoop()

