import os, os.path as path
import sys
from makeparts import *
from packages import *
from genutils import *
import logging
import tempfile
import augeas
import prototype
from generator import *
from serie import mountfat
from common import cleanyumcache, redirecthost, restorehost, disableselinux

usedm = False

@stage
def install_spin_kickstart():
    """
    Install spin_kickstart
    """
    if run_command("rpm -qi spin-kickstarts", output=True)[0]:
        run_command("yum -y install spin-kickstarts")

@stage
def createlauncher():
    """
    Creating launcher
    """
    import urllib
    import tarfile
    tmpdir = tempfile.mkdtemp(prefix="livecd-launcher")
    latest_version = urllib.urlopen("http://elko.agorabox.org/launcher/latest").read().strip()
    url = "http://elko.agorabox.org/launcher/launcher-" + latest_version + ".tar.bz2"
    logging.debug("Downloading " + url)
    filename = urllib.urlretrieve(url)[0]
    logging.debug("Downloaded as " + filename)
    tgz = tarfile.open(filename)
    tgz.extractall(tmpdir)
    logging.debug("Extracting launcher to " + tmpdir)
    tgz.close()
    
    conffile = os.path.join(tmpdir, ".data", "settings", "settings.conf")
    logging.debug("Updating " + conffile)
    cp = ConfigParser()
    cp.read([conffile])
    cp.set("launcher", "LIVECD", "1")
    cp.set("vm", "BOOTISO", ".VirtualBox/Isos/UFO.iso")
    cp.write(open(conffile, "w"))

    vdi = os.path.join(tmpdir, ".data", ".VirtualBox", "HardDisks", "bootdisk.vdi")
    if not os.path.exists(os.path.dirname(vdi)):
        os.makedirs(os.path.dirname(vdi))
    shutil.copyfile("bootdisk.vdi", vdi)

    run_command("cp ../../vlaunch/setup/settings.conf.live.macos" + " " + os.path.join(tmpdir, "Mac-Intel", "UFO.app", "Contents", "Resources", "settings.conf"))
    run_command("cp -R " + os.path.join(tmpdir, ".data") + " " + os.path.join(tmpdir, "Mac-Intel", "UFO.app", "Contents", "Resources"))
    # Use makedmg and Apple's version of mkfs.hfsplus
    # http://www.opensource.apple.com/darwinsource/tarballs/apsl/diskdev_cmds-332.11.9.tar.gz
    # patch with diskdev_cmds-332.11.9.patch
    run_command("./makedmg UFO-Creator.dmg 'UFO Creator' 128 " + os.path.join(tmpdir, "Mac-Intel"))
    # import zipfile
    # zip = zipfile.ZipFile("UFO-Creator.zip", "w")
    # zip.write(os.path.join(tmpdir, "Mac-Intel"))
    # Seigneur, pardonne moi pour ce que je fais
    run_command("cd " + os.path.join(tmpdir) + " && zip -r UFO-Creator-for-Windows.zip * && mv UFO-Creator.zip " + os.getcwd())

    tar = tarfile.open("UFO-Creator-for-Linux.tar.bz2", "w:bz2")
    cwd = os.getcwd()
    os.chdir(tmpdir) # path.join(tmpdir, "Linux"))
    import tarfile
    for name in [ "Linux", ".data" ]:
        tar.add(name)
    tar.close()
    os.chdir(cwd)

@stage
def genfstab(mountpoint):
    """
    Generating fstab
    """
    generate_fstab(None, mountpoint, utiluuid=None, rootuuid=None)

@stage
def geninitrd(mountpoint):
    """
    Generating initrd
    """
    oldgrub = open(os.path.join(mountpoint, "boot", "grub", "grub.conf")).read()

    latest_vbox_kernel = get_kernels(mountpoint, "vbox")[0]
    dracut(mountpoint, format_kernel_name(latest_vbox_kernel), modules = [ "base", "plymouth", "ufo", "vbox", "dmsquash-live" ])

    latest_regular_kernel = get_kernels(mountpoint)[0]
    dracut(mountpoint, format_kernel_name(latest_regular_kernel), modules = [ "base", "plymouth", "udev-rules", "kernel-modules", "dmsquash-live" ])

    open(os.path.join(mountpoint, "boot", "grub", "grub.conf"), 'w').write(oldgrub)

@stage
def installpkgs(mountpoint):
    """
    Installing required packages
    """
    install_yum_package(mountpoint, "syslinux ufo-liveusb-creator xfwm4")

@stage
def livecd(mountpoint, exec_path):
    """
    Creating LiveCD ISO
    """
    opts = ""
    if not options.compress:
        opts = " --skip-compression "
    env = os.environ.copy()
    env["PYTHONPATH"] = path.join(exec_path, "livecd")
    run_command("livecd/tools/livecd-creator --verbose --debug " + \
                "--config=ufo-livecd-desktop.ks " + \
                "--image=" + mountpoint + " --fslabel=UFO" + opts, env=env)

@stage
def unmount(mountpoint):
    """
    Unmounting everything
    """
    if not options.noumount:
        umountAll()
        os.system("umount " + mountpoint + " &")

class LiveCDGenerator(Generator):
    stages = [ redirecthost,
               mountfat,
               cleanyumcache,
               install_spin_kickstart,
               createlauncher,
               genfstab,
               geninitrd,
               installpkgs,
               disableselinux,
               restorehost,
               livecd,
               unmount ]
               
    short_option = "-d"
    long_option = "--livecd"
    help = "generates a LiveCD from prototype"

    def __init__(self, options, args, parser):
        Generator.__init__(self, options)
        if len(args) != 4:
            parser.error("Wrong number of parameters...")
        self.base, self.core, self.proto, self.outputdir = [ makeabs(x) for x in args ]

        if not options.mountpoint:
            if usedm:
                self.mountpoint, loop = create_snapshot(target, 1024, 12289725, "proto-sparse", keep=options.keep)
            else:
                self.mountpoint = mount_aufs( [ self.find_sparse(self.proto),
                                                self.find_sparse(self.core),
                                                self.base ],
                                              self.outputdir, options.keep)
        else:
            self.mountpoint = options.mountpoint

        self.root = self.mountpoint
