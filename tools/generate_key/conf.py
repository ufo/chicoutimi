# Cela represente et l'ordre dans lequel seront partition les 
# partition et le nom donne au partoche
#
# variable de make_security_links
#noncrypted=$1
#crypted=$2

# build_suid_files_list
# partition racine


# Dans makeparts.py
# besoin de toutes les partitions dans l'ordres
# et leur taille approx


# dans formatparts besoin des numero de partoche ainsi que de

import os
import commands

cfg = [ { "name"  : "UFO",
          "size"  : 750,
          "type"  : "c",
          "fs"    : "vfat" },

        { "name"  : "home",
          "label" : "ufohome",
          "size"  : 0,
          "type"  : "83",
          "fs"    : "crypt",
          "uuid"  : "fedcba98-7654-3210-fedc-ba9876543210" },

        { "name"  : "root",
          "label" : "ufosys",
          "size"  : 4500,
          "type"  : "83",
          "fs"    : "ext3",
          "uuid"  : "01234567-89ab-cdef-0123-456789abcdef"
        }, ]

def getPart(name):
    for part in cfg:
        if part["name"] == name:
            return part

def getNumPart(name):
    return cfg.index(getPart(name)) + 1

def getDevPart(dev, name):
    return dev + str(getNumPart(name))

def getDevUUID(dev):
    return commands.getoutput("/sbin/blkid -c /dev/null " + dev + " | sed -n 's/.*UUID=\"\\([^\"]*\\).*/\\1/p'")

def getUUIDFromName(dev, name):
    part = getPart(name)
    if part:
        return part.get("uuid", getDevUUID(getDevPart(dev, name)))
