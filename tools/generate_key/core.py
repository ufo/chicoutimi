#!/usr/bin/python
# -*- coding: utf-8 -*-

import os, os.path as path
import sys
from makeparts import *
from packages import *
from genutils import *
import logging
from generator import *
from common import cleanyumcache

excludes = [ "agorabox-conf-[a-zA-Z]*" ]
usedm = False

@stage
def rebuildrpmdb(mountpoint):
    """
    Rebuild the rpm database
    """
    rebuild_rpmdb(mountpoint)

@stage
def someupdates(mountpoint):
    """
    Some updates we want
    """
    install_updates(mountpoint, excludes=["kernel", "kernel-PAE"])

@stage
def prerequisites(mountpoint, fedora_name, fedora_version, arch):
    """
    Installing prerequisite packages
    """
    install_yum_package(mountpoint, "yum-priorities")
    install_yum_package(mountpoint, "yum-presto")
    install_rpm_package(mountpoint, "http://downloads.agorabox.org/yum/" + fedora_name + "/" + arch + "/agorabox-yum-repository-1.0-1.fc" + str(fedora_version) + ".noarch.rpm")
    if options.testing:
        install_rpm_package(mountpoint, "http://downloads.agorabox.org/yum/" + fedora_name + "/" + arch + "/agorabox-yum-repository-testing-1.0-1.ufo" + str(fedora_version)[-1] + ".noarch.rpm")


@stage
def rpmfusion(mountpoint, fedora_name):
    """
    Installing rpmfusion
    """
    if fedora_name == "rawhide":
        install_rpm_package(mountpoint, "http://download1.rpmfusion.org/free/fedora/rpmfusion-free-release-rawhide.noarch.rpm")
        install_rpm_package(mountpoint, "http://download1.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-rawhide.noarch.rpm")
    else:
        install_rpm_package(mountpoint, "http://download1.rpmfusion.org/free/fedora/rpmfusion-free-release-stable.noarch.rpm")
        install_rpm_package(mountpoint, "http://download1.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-stable.noarch.rpm")

    run_command("sed -i s/\#baseurl/baseurl/ " + os.path.join(mountpoint, "etc", "yum.repos.d", "rpmfusion-free-updates.repo"))
    run_command("sed -i s/mirrorlist/\#mirrorlist/ " + os.path.join(mountpoint, "etc", "yum.repos.d", "rpmfusion-free-updates.repo"))

    run_command("sed -i s/\#baseurl/baseurl/ " + os.path.join(mountpoint, "etc", "yum.repos.d", "rpmfusion-nonfree-updates.repo"))
    run_command("sed -i s/mirrorlist/\#mirrorlist/ " + os.path.join(mountpoint, "etc", "yum.repos.d", "rpmfusion-nonfree-updates.repo"))

    run_command("sed -i s/\#baseurl/baseurl/ " + os.path.join(mountpoint, "etc", "yum.repos.d", "rpmfusion-free.repo"))
    run_command("sed -i s/mirrorlist/\#mirrorlist/ " + os.path.join(mountpoint, "etc", "yum.repos.d", "rpmfusion-free.repo"))

    run_command("sed -i s/\#baseurl/baseurl/ " + os.path.join(mountpoint, "etc", "yum.repos.d", "rpmfusion-nonfree.repo"))
    run_command("sed -i s/mirrorlist/\#mirrorlist/ " + os.path.join(mountpoint, "etc", "yum.repos.d", "rpmfusion-nonfree.repo"))

@stage
def kernel(mountpoint):
    """
    Installing Handy kernel
    """
    update_rpm(mountpoint, "mkinitrd")
    update_rpm(mountpoint, "kernel")

@stage
def removefedoralogos(mountpoint):
    """
    Removing fedora logos
    """
    run_command("rpm --nodeps -e fedora-logos", chroot=mountpoint)
    install_yum_package(mountpoint, "agorabox-ui")

@stage
def updates(mountpoint):
    """
    Installing updates
    """
    install_updates(mountpoint, excludes=excludes)

@stage
def applications(mountpoint):
    """
    Installing Yum group
    """
    install_yum_group(mountpoint, "UFO", excludes=["openldap-clients", "gstreamer-plugins-*"] + excludes)

@stage
def unwanted(mountpoint):
    """
    Removing unwanted packages
    """
    remove_unwanted_packages(mountpoint, "UFO")

@stage
def thirdparty(mountpoint):
    """
    Third party packages
    """
    install_rpm_package(mountpoint, "http://linuxdownload.adobe.com/adobe-release/adobe-release-i386-1.0-1.noarch.rpm")
    install_yum_package(mountpoint, "flash-plugin")
    # install_yum_package(mountpoint, "skype")

@stage
def uselessservices(mountpoint):
    """
    Desactivating useless services
    """
    activate_services(mountpoint, ["bluetooth", "setroubleshoot", "auditd", "idmapd", "atd", "rpcidmapd"
                                   "sendmail", "kerneloops", "ip6tables", "iptables", "netfs", "rpcbind",
                                   "nfslock", "ntpd", "ntpdate", "isdn", "mdmonitor", "pcscd", "irqbalance",
                                   "sshd", "exim" ], "off")

@stage
def ntp(mountpoint):
    """
    Configuring NTP
    """
    aug = augeas.Augeas(root=mountpoint)
    aug.set("/files/etc/ntp.conf/server[1]", "ntp.lip6.fr")
    aug.save()

@stage
def removexorg(mountpoint):
    """
    Remove xorg.conf
    """
    xorg = os.path.join(mountpoint, 'etc', 'X11', 'xorg.conf')
    if os.path.exists(xorg):
        os.unlink(xorg)

class CoreGenerator(Generator):
    stages = [ rebuildrpmdb,
               someupdates,
               prerequisites,
               rpmfusion,
               kernel,
               cleanyumcache,
               removefedoralogos,
               updates,
               applications,
               unwanted,
               thirdparty,
               uselessservices,
               ntp,
               removexorg ]

    short_option = "-c"
    long_option = "--core"
    help = "creates an UFO core"
                           
    def __init__(self, options, args, parser):
        Generator.__init__(self, options)
        if len(args) != 2:
            parser.error("Wrong number of parameters...")
        base, outputdir = args
        if not os.path.isabs(base):
            base = os.path.join(self.exec_path, base)
        if not os.path.exists(base):
            parser.error("The file %s does not exist" % (base, ))
        self.base = base
        self.mountpoint = options.mountpoint
        self.arch = options.arch
        self.outputdir = outputdir

        if not options.mountpoint:
            if usedm:
                mountpoint, loop = create_snapshot(dev, 2048, 12289725, "core-sparse")
            else:
                self.mountpoint = mount_aufs([ base ],
                                             outputdir,
                                             keep = options.keep)
        else:
            mountpoint = options.mountpoint

