#!/usr/bin/python
# -*- coding: utf-8 -*-

import os, os.path as path
import sys
import stat
from optparse import OptionParser
import conf as cf
from makeparts import *
from packages import *
from genutils import *
import logging
import time
import commands
import glob
import crypt
import tempfile
import shutil

usage = """%prog [-p base_image coredir outputdir device group]
                 [-s protodir target device]
                 [-c base]
                 [-d base core proto outputdir ]
                 [-t base core vdifile target]"""

description = "Generates either an Agorabox prototype, either an Agorabox USB key (serie)"
version="%prog 0.1"

parser = OptionParser(usage=usage, description=description, version=version)
parser.add_option("-z", "--compress", dest="compress", default="",
                  help="use root filesystem compression", metavar="PATH")
parser.add_option("--compressroot", dest="compressroot", default=False,
                  action="store_true", help="use btrfs filesystem compression")
parser.add_option("--bootpart", dest="bootpart", default=False,
                  action="store_true", help="use /boot partition")
parser.add_option("-g", "--start", dest="starting_stage",
                  help="starts the script at the specified stage", metavar="STAGE")
parser.add_option("-e", "--end", dest="ending_stage",
                  help="ends the script after the specified stage", metavar="STAGE")
parser.add_option("-y", "--cleanyumcache", dest="cleanyumcache", default=True,
                  action="store_true", help="cleans yum's cache")
parser.add_option("-o", "--only", dest="only", default="",
                  help="only execute the specified stages", metavar="FILE")
parser.add_option("-i", "--skip", dest="skip",
                  help="skips stages (separated by colons)", metavar="STAGE")
parser.add_option("-v", "--verbose", dest="loglevel", default="info",
                  help="set verbosity (can be notset, debug, info, warning, error, fatal, or critical)")
parser.add_option("-u", "--noumount", dest="noumount", default=False,
                  action="store_true", help="do not umount")
parser.add_option("-l", "--log", dest="logfile",
                  help="write logs to FILE", metavar="FILE")
parser.add_option("-m", "--mountpoint", dest="mountpoint", default="",
                  help="use mountpoint", metavar="DIR")
parser.add_option("-n", "--nogrubinstall", dest="nogrubinstall", default=False,
                  action="store_true", help="do not install grub")
parser.add_option("-k", "--keep", dest="keep", default=False,
                  action="store_true", help="keep files")
parser.add_option("-f", "--fedora", dest="fedora", default=12,
                  help="Fedora version", metavar="VERSION")
parser.add_option("", "--filelist", dest="filelist",
                  help="Generates a file list for a prototype, check against it for a serie", metavar="FILE")
parser.add_option("--arch", dest="arch", default="i386",
                  help="Specify architecture")
parser.add_option("--testing", dest="testing", default=False,
                  action="store_true", help="Enables testing repositories")
parser.add_option("--lang", dest="lang", default="auto",
                  help="Set language", metavar="LANG")
parser.add_option("--redirect", dest="redirect", default="",
                  help="Redirect host")
parser.add_option("--root-password", dest="rootpassword", default="",
                  help="Set root password", metavar="PASSWORD")
parser.add_option("--no-signals", dest="nosignals", default=False,
                  action="store_true", help="Do not use signals")
parser.add_option("--step", dest="step", default=False,
                  action="store_true", help="Pause at each step")
parser.add_option("--defaultuser", dest="defaultuser", default=False,
                  action="store_true", help="Set up a default user")

from generator import Generator
from core import CoreGenerator
from prototype import PrototypeGenerator
from livecd import LiveCDGenerator
from penitentiary import PenitentiaryGenerator
from serie import SerieGenerator

Generator.register(CoreGenerator, parser)
Generator.register(PrototypeGenerator, parser)
Generator.register(LiveCDGenerator, parser)
Generator.register(PenitentiaryGenerator, parser)
Generator.register(SerieGenerator, parser)

(options, args) = parser.parse_args()

format = "%(asctime)s %(levelname)s %(message)s"
levels = [logging.getLevelName(x) for x in (0, 10, 20, 30, 40, 50)]
loglevel = options.loglevel.upper()
try:
    level = levels.index(loglevel) * 10
except ValueError:
    parser.error("Invalid log level " + loglevel)

if options.logfile:
    logging.basicConfig(format=format, level=level, filename=options.logfile)
else:
    logging.basicConfig(format=format, level=level)

if os.getuid() != 0:
    logging.critical("You must be root to run this script")
    sys.exit(-1)

if not reach_downloads_agorabox():
    logging.critical("Cannot reach downloads.agorabox.org")
    sys.exit(-1)

if options.only or options.starting_stage:
    options.keep = True

if options.compressroot:
    options.bootpart = True
    cf.getPart("root")["fs"] = "btrfs"
    cf.getPart("root")["options"] = ["compress"]
    cf.getPart("root")["size"] = 2200

if options.compress:
    cf.cfg.append({ "name"  : "compressed",
                    "size"  : 1536,
                    "type"  : "83",
                    "fs"    : "btrfs" })
    cf.getPart("root")["size"] -= cf.cfg[-1]["size"]

if options.bootpart:
    cf.cfg.insert(2, { "name"  : "boot",
                       "label" : "boot",
                       "size"  : 100,
                       "type"  : "83",
                       "fs"    : "ext3",
                       "uuid"  : "00112233-4455-6677-8899-aabbccddeeff" })

for genclass in Generator.generators:
    if getattr(options, genclass.long_option[2:]):
        generator = genclass(options, args, parser)
        sys.exit(generator.run())
        
parser.print_usage()
