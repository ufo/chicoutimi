import inspect
import os.path as path
import sys
import traceback
import logging
import signal
from genutils import umountAll

stages = []

def stage(f):
    stages.append(f)
    if not hasattr(f, "mandatory"):
        f.mandatory = False
    return f

def canfail(f):
    f.canfail = True
    return f

def mandatory(f):
    f.mandatory = True
    return f

def cleanup(f):
    f.cleanup = True
    return f

class Generator:
    stages = [ ]
    generators = [ ]
    
    def __init__(self, options):
        self.options = options
        self.current_stage = 1
        self.exec_path = path.dirname(sys.argv[0])
        if options.fedora == "rawhide":
            self.fedora_version = 13
            self.fedora_name = "rawhide"
        else:
            self.fedora_version = options.fedora
            self.fedora_name = str(self.fedora_version)
            
        if not options.nosignals:
            signal.signal(signal.SIGINT, self.interrupt)

    @staticmethod
    def register(self, parser):
        parser.add_option(self.short_option,
                          self.long_option,
                          dest=self.long_option[2:],
                          default=False,
                          action="store_true",
                          help=self.help)
        Generator.generators.append(self)

    def run_stage(self, stage, quiet=False, cleaning=False):
        if self.options.step:
            print "Next step:", stage.func_name, "- Press enter to continue."
            raw_input()
        stage.func_globals["options"] = self.options
        stage.func_globals["cleaning"] = cleaning
        newvars = {}
        try:
            kw = self.get_args(stage)
            msg = "(Stage %d) %s" % (self.current_stage, stage.func_name)
            doc = stage.__doc__
            if doc: msg += " - " + doc.strip()
            self.current_stage += 1
            if not quiet:
                logging.info(msg)
            newvars = stage(**kw)
        except Exception, e:
            logging.critical("Exception during the execution of the stage")
            info = sys.exc_info()
            logging.critical("Unexpected error: " + str(info[1]))
            logging.critical("".join(traceback.format_tb(info[2])))
            if not getattr(stage, "canfail", False):
                self.cleanup()
                sys.exit(1)
        if newvars:
            for name, value in newvars.items():
                setattr(self, name, value)

    def cleanup(self):
        logging.debug("Cleaning up")
        cleanstages = [ stage for stage in self.stages if getattr(stage, "cleanup", False) == True ]
        for stage in cleanstages:
            logging.debug("Clean stage: " + stage.func_name)
            self.run_stage(stage, cleaning=True)
        logging.shutdown()
        if not self.options.noumount:
            umountAll()
    
    def run(self):
        if self.options.only:
            only = self.options.only.split(",")
            self.stages = [ stage for stage in self.stages if stage.mandatory or ((not stage.mandatory) and stage.func_name in only) ]
        if self.options.skip:
            skip = self.options.skip.split(",")
            self.stages = [ stage for stage in self.stages if stage.mandatory or (not stage.func_name in skip) ]
        if self.options.starting_stage:
            i = 0
            while len(self.stages) - i:
                print len(self.stages), i
                if getattr(self.stages[i], "mandatory", False):
                    i += 1
                    continue
                if self.stages[i].func_name != self.options.starting_stage:
                    del self.stages[i]
                else:
                    break
        if self.options.ending_stage:
            i = len(self.stages)
            while len(self.stages) - i:
                if self.stages[i].mandatory:
                    i -= 1
                    continue
                if self.stages[i].func_name != self.options.ending_stage:
                    del self.stages[i]
                else:
                    break
                    
        for stage in [ stage for stage in self.stages if getattr(stage, "cleanup", False) == False ]:
            self.run_stage(stage)

        self.cleanup()
        
        return 0

    def get_args(self, stage):
        args = inspect.getargs(stage.__code__)
        dargs = { }
        for arg in args.args:
            try:
                dargs[arg] = getattr(self, arg)
            except:
                raise Exception("Stage %s requires argument %s" % (stage.func_name, arg))
        return dargs

    def find_sparse(self, p):
        return path.join(p, "aufs-sparse")

    def interrupt(self, signum, frame):
        print "Interrupted. Cleaning up"
        logging.shutdown()
        umountAll()
        sys.exit(1)
