# rpm --queryformat=%{VERSION} -q -p  vlaunch-r958.rpm

rev=`cat ../../vlaunch/vlaunch.spec | grep Version:`
rev=${rev#* }
fedora_version=`cat ../../Makefile.mk | grep ^FEDORA_VERSION= | head -n 1`
fedora_version=${fedora_version#*=}
read rev <<< "$rev"; echo $rev
# rev=`python -c "import pysvn; print pysvn.Client().info('../../vlaunch')['revision'].number"`
echo "Latest local version: $rev"

webrev=`wget -O - http://elko.agorabox.org/launcher/latest 2> /dev/null` 
echo "Latest version on downloads.agorabox.org: $webrev"

update_dir="`pwd`/update-$rev"
rm -rf $update_dir
mkdir -p $update_dir/var/cache/yum
mkdir -p $update_dir/etc/yum.repos.d
cat > $update_dir/etc/yum.conf <<EOF
[main]
cachedir=/var/cache/yum
keepcache=0
debuglevel=2
reposdir=$update_dir/etc/yum.repos.d
EOF
cat > $update_dir/etc/yum.repos.d/agorabox.repo <<EOF
[agorabox]
name=The Agorabox repository
baseurl=http://kickstart.alpha.agorabox.org/yum/$fedora_version/i386
gpgcheck=1
priority=1
gpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-agorabox
EOF

sed -i 's/downloads.agorabox.org/kickstart.alpha.agorabox.org/' $update_dir/etc/yum.conf
url=`yumdownloader -c $update_dir/etc/yum.conf --urls vlaunch`
echo $url
filename="vlaunch-r$rev.rpm"
if [ -f $filename ]
then
    rm -rf $filename
fi
echo "Downloading $url"
wget -O $filename $url

#yum --installroot=$update_dir -c /tmp/yum.conf install vlaunch

echo "Installing vlaunch rpm"
echo rpm --nodeps --root=$update_dir -ivf $filename 2> /dev/null
rpm --nodeps --root=$update_dir -ivf $filename 2> /dev/null

echo "Removing vdis, vmdks, and floppy disk"
find $update_dir -name "*.vdi" -exec rm -f {} \;
find $update_dir -name "*.img" -exec rm -f {} \;
find $update_dir -name "*.vmdk" -exec rm -f {} \;
rm -rf $update_dir/media/UFO/.data/settings

echo "Compressing to launcher-$rev.tar.bz2"
# files=`cd $update_dir/media/UFO && python -c "import glob; print '\"' + '\" \"'.join(glob.glob('*') + glob.glob('.*')) + '\"'" && cd ../../..`
# files=`cd $update_dir/media/UFO && python -c "import glob; files = [ x.replace(' ', '\\\\ ') for x in glob.glob('*') + glob.glob('.*') ]; print ' '.join(files)" && cd ../../..`
# echo tar -cjf ../../../launcher-$rev.tar.bz2 $files
# cd $update_dir/media/UFO && tar -cjf ../../../launcher-$rev.tar.bz2 $files && cd ../../..

cd $update_dir/media/UFO && find . -mindepth 1 -not -path "./.data*" | sed 's/^.\///' > .data/launcher.filelist && python <<EOF
import glob
import tarfile

tar = tarfile.open('../../../launcher-$rev.tar.bz2', 'w:bz2')
map(tar.add, glob.glob('*') + glob.glob('.*'))
tar.close()
EOF

cd ../../..
echo "Uploading to downloads.agorabox.org"
echo -n $rev > $update_dir/latest
[ ! -z "$TESTING" ] && subpath="testing/"
[ -z "$NOUPLOAD" ] && scp $update_dir/latest launcher-$rev.tar.bz2 bob@elko.agorabox.org:/var/www/html/launcher/$subpath

echo "Update done."
