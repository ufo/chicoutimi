import augeas

def insert_kernel(mountpoint, kernel, version, args)
    if args.has_key("root"):
        args["root"] = aug.get('/files/etc/grub.conf/title[1]/kernel/root')

    aug.set('/files/etc/grub.conf/title[1]', 'UFO (' + version + ')')
    aug.set('/files/etc/grub.conf/title[1]/kernel', '/boot/vmlinuz-' + version)
    aug.set('/files/etc/grub.conf/title[1]/kernel/root', args["root"])
    aug.set("/files/etc/grub.conf/title[1]/kernel/LANG",
            aug.get('/files/etc/sysconfig/i18n/LANG'))
    aug.set("/files/etc/grub.conf/title[1]/kernel/SYSFONT",
            aug.get('/files/etc/sysconfig/i18n/SYSFONT'))
    aug.set("/files/etc/grub.conf/title[1]/kernel/KEYBOARDTYPE",
            aug.get('/files/etc/sysconfig/keyboard/KEYBOARDTYPE'))
    aug.set("/files/etc/grub.conf/title[1]/kernel/KEYTABLE",
            aug.get('/files/etc/sysconfig/keyboard/KEYTABLE'))
    aug.set("/files/etc/grub.conf/title[1]/kernel/KEYTABLE",
            aug.get('/files/etc/sysconfig/keyboard/KEYTABLE'))
    aug.save()

