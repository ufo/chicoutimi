import augeas

class Grub:
    def __init__(self, grub_root, root="/", kernel_args=[], boot_part=False):
        self.root = root
        self.aug = augeas.Augeas(root)
        self.kernel_args = kernel_args
        self.grub_root = grub_root
        self.boot_part = boot_part
        if boot_part:
            self.boot_prefix = ""
        else:
            self.boot_prefix = "/boot"

    def set_defaults(self):
        pass
        """
        splashimage = self.aug.get('/files/etc/grub.conf/splashimage')
        splashimage = splashimage[splashimage.find(')') + 1:]
        self.aug.set('/files/etc/grub.conf/splashimage', self.grub_root + splashimage)
        """

    def insert_kernel(self, version, release, arch, args, suffix="", i18n=True):
        index = len(self.aug.match("/files/etc/grub.conf/*/kernel")) + 1
        prefix = '/files/etc/grub.conf/title[%d]' % (index,)

        args = self.kernel_args + args
        if i18n:
            i18n = [ ("LANG", self.aug.get('/files/etc/sysconfig/i18n/LANG')),
                     ("SYSFONT", self.aug.get('/files/etc/sysconfig/i18n/SYSFONT')) ]
            keyboard = [ ("KEYBOARDTYPE", self.aug.get('/files/etc/sysconfig/keyboard/KEYBOARDTYPE')),
                         ("KEYTABLE", self.aug.get('/files/etc/sysconfig/keyboard/KEYTABLE')) ]
            args = i18n + keyboard + args

        root = None
        for arg in args:
            if type(arg) == tuple and arg[0] == "root":
                root = arg
                break
        if not root:
            args.append(("root", self.aug.get(prefix + '/kernel/root')))

        fullversion = version + '-' + release + "." + arch + suffix
        self.aug.set(prefix, 'UFO (' + fullversion + ')')
        self.aug.set(prefix + '/root', self.grub_root)
        self.aug.set(prefix + '/kernel', self.boot_prefix + '/vmlinuz-' + fullversion)

        last = "root"
        for name in args:
            if type(name) == tuple:
                name, value = name
                self.aug.set(prefix + '/kernel/' + name, value)
            else:
                self.aug.insert(prefix + '/kernel/' + last, name, False)
            last = name

        self.aug.set(prefix + "/initrd", self.boot_prefix + '/initramfs-' + fullversion + ".img")

    def remove_all_entries(self):
        while self.aug.remove('/files/etc/grub.conf/title[1]'):
            pass

    def save(self):
        self.aug.save()

    def set(self, name, value):
        self.aug.set("/files/etc/grub.conf/" + name, value)

