import os, os.path as path
from generator import *
from genutils import *
import augeas
import socket

@stage
def filelist(mountpoint):
    """
    Collecting file list
    """
    if options.filelist:
        options.filelist = makeabs(options.filelist)
        cwd = os.getcwd()
        os.chdir(mountpoint)
        run_command("find . > ../%s", (options.filelist,))

@stage
def cleanyumcache(mountpoint):
    """
    Cleaning yum cache
    """
    if options.cleanyumcache:
        run_command("yum clean all", chroot=mountpoint)

@stage
def redirecthost(mountpoint):
    if not options.redirect: return
    host, newhost = options.redirect.split(",")
    newip = socket.gethostbyaddr(newhost)[2][0]
    aug = augeas.Augeas(mountpoint)
    cns = aug.match("/files/etc/hosts/*/canonical")
    for cn in cns:
        if aug.get(cn) == host:
            aug.set(path.join(path.dirname(cn), "ipaddr"), newip)
            aug.save()
            return
    index = len(cns) + 1
    aug.set("/files/etc/hosts/%d/ipaddr" % (index,), newip)
    aug.set("/files/etc/hosts/%d/canonical" % (index,), host)
    aug.save()

@stage
def restorehost(mountpoint):
    if not options.redirect: return
    host, newhost = options.redirect.split(",")
    newip = socket.gethostbyaddr(newhost)[2][0]
    aug = augeas.Augeas(mountpoint)
    cns = aug.match("/files/etc/hosts/*/canonical")
    for cn in cns:
        if aug.get(cn) == host:
            aug.remove(path.dirname(cn))
            aug.save()
            return


@stage 
def disableselinux(mountpoint):
    """
    Disabling SELinux
    """
    aug = augeas.Augeas(root=mountpoint)
    aug.set("/files/etc/sysconfig/selinux/SELINUX", "disabled")
    aug.save()
