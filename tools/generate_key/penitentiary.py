#!/usr/bin/python
# -*- coding: utf-8 -*-

import os, os.path as path
import sys
from makeparts import *
from packages import *
from genutils import *
import logging
import crypt
from generator import *

@stage
def mountparts():
    """
    Mounting partitions
    """

    if not options.mountpoint:
        if usedm:
            mountpoint, loop = create_snapshot(dev, 2048, 12289725, "penitentiary-sparse", keep=starting_stage != "")
        else:
            mountpoint = mount_union([ core, base ], "penitentiary-unionfs", "penitentiary-sparse", options.keep)
    else:
        mountpoint = options.mountpoint
    
    # vdiloop = commands.getoutput("losetup -f").strip()
    # run_command("losetup -f -o 1085770240 penitentiary.vdi")
    # run_command("mkfs.ext3 " + vdiloop)
    # run_command("losetup -d " + vdiloop)
    
    vdimountpoint = mount(vdi, "penitentiary", options=["loop","offset=1085770240"])
    
    return { "mountpoint" : mountpoint,
             "vdimountpoint" : vdimountpoint }

@stage
def genfstab(mountpoint):
    """
    Generating fstab
    """
    generate_fstab("/dev/loop0", mountpoint,
                   rootuuid = "2a2c8037-5b75-4e0b-b0ef-4168fae79140",
                   utiluuid = "")
    
    # if stage("Installing grub", "installgrub"):
    #     loop = commands.getoutput("losetup -f").strip()
    #     run_command("losetup -f -o 33280 penitentiary.vdi")
    #     install_grub(loop, mountpoint="/")
    #     open("/boot/grub/device.map", "a").write("(hd5)\t" + loop)
    #     install_grub(loop, mountpoint="/", recheck=False)

    
    if options.cleanyumcache:
        run_command("yum clean all", chroot=mountpoint)

@stage
def penitentiarysetup(mountpoint, target):
    """
    Installing penitenciary-setup
    """
    install_yum_package(mountpoint, "kernel-vbox kernel-vbox-devel initscripts penitentiary-setup-" + target)

@stage
def adminaccount(mountpoint):
    """
    Create administrator account
    """
    run_command('ls -s /bin/bash /bin/hidden-bash', chroot=mountpoint)
        
    admin_login  = 'admin'
    admin_passwd = 'demodemo'
		
    crypted_passwd = crypt.crypt(admin_passwd, 'wt')
    run_command('/usr/sbin/groupadd --gid 501 ' + admin_login, chroot=mountpoint)
    run_command('/usr/sbin/useradd --shell /bin/hidden-bash --uid 501 --gid 501 -p "' + crypted_passwd + '" ' + admin_login, chroot=mountpoint)

@stage
def services(mountpoint):
    """
    Restore some services
    """
    activate_services(mountpoint, ["rsyslog"], "on")
    activate_services(mountpoint, ["penitentiaryd"], "on")

@stage
def uselessservices(mountpoint):
    """
    Desactivating useless services
    """
    activate_services(mountpoint, [ "dkms_autoinstaller" ], "off")

@stage
def copyfilesystem(mountpoint, vdimountpoint):
    """
    Copying to VDI
    """
    run_command("rm -Rf " + os.path.join(vdimountpoint, "*"))
    run_command("cp -p -R -P " + os.path.join(mountpoint, "*") + " " + vdimountpoint)

@stage
def authsingleuser(mountpoint):
    """
    Enable authentication for Single-User mode
    """
    open(os.path.join(mountpoint, "etc", "inittab"), "a").write("~~:S:wait:/sbin/sulogin\n")

@stage
def disableinteractive(mountpoint):
    """
    Disable interactive hotkey startup at boot
    """
    import augeas
    aug = augeas.Augeas(root=mountpoint)
    aug.set("/files/etc/sysconfig/init/PROMPT", "no")
    aug.save()

@stage
@cleanup
def cleanup():
    """
    Cleaning up
    """
    clean_penitentiary()

class PenitentiaryGenerator(Generator):
    stages = [ mountparts,
               genfstab,
               penitentiarysetup,
               adminaccount,
               services,
               uselessservices,
               copyfilesystem,
               authsingleuser,
               disableinteractive,
               cleanup ]
               
    short_option = "-t"
    long_option = "--penitentiary"
    help = "generates a penitentiary as VDI file"
               
    def __init__(self, options, args, parser):
        Generator.__init__(self, options, args)
        if len(args) != 4:
            parser.error("Wrong number of parameters...")
        self.base, self.core, self.vdi, self.target = args
        self.base = makeabs(self.base)
        self.core = makeabs(self.core)
