 #!/usr/bin/python
# -*- coding: utf-8 -*-

import os, os.path as path
import sys
import stat
from optparse import OptionParser
import conf as cf
from makeparts import *
from packages import *
from genutils import *
from floppy import *
import logging
import time
import commands
import glob
import crypt
import tempfile
import shutil
from generator import *
from common import filelist, cleanyumcache
from grub import Grub

@stage
def repart(dev):
    """
    Repartitionning the key
    """
    repartition(dev)

@stage
@mandatory
def mountroot(dev):
    """
    Mount partitions
    """
    mountpoint = mount(dev, "root")
    if options.bootpart or options.compressroot:
        mount(dev, "boot", os.path.join(mountpoint, "boot"))
    vfatmountpoint = mount(dev, "UFO",
                           os.path.join(mountpoint, "media", "UFO"),
                           options="utf8=1")
    return { "mountpoint" : mountpoint,
             "vfatmountpoint" : vfatmountpoint }

@stage
def mountfat(proto, root):
    """
    Binds the fat folder to /media/UFO
    """
    if not os.path.ismount(os.path.join(root, "media", "UFO")):
        mount(os.path.join(proto, "fat"),
              name="bindmedia",
              mountpoint=os.path.join(root, "media", "UFO"),
              options="bind")

@stage
def copyfilesystem(mountpoint, root):
    """
    Copying filesystem
    """
    copy_filesystem(mountpoint, root)

@stage
def installgrub(dev, mountpoint, vfatmountpoint):
    """
    Installing grub
    """
    rootuuid = cf.getUUIDFromName(dev, "root")
    if options.compress:
        compruuid = cf.getUUIDFromName(dev, "compressed")
    if options.bootpart:
        rootpart = "boot"
    else:
        rootpart = "root"
    grub_dev = '(hd0,%d)' % (cf.getNumPart(rootpart) - 1,)

    # Real boot grub.conf
    grub = Grub(grub_dev, mountpoint, boot_part=options.bootpart)
    grub.remove_all_entries()
    grub.set_defaults()
    kernels = get_kernels(mountpoint)
    kernargs = [ ( "root", "UUID=" + rootuuid ),
                 ( "overlay", "tmpfs" ),
                 "udev", "ro", "rhgb", "quiet", "5" ]
    if options.compressroot:
        kernargs.insert(1, ("rootflags", "compress"))
        kernargs.insert(2, ("rootfstype", "btrfs"))
    for kernel in kernels:
        grub.insert_kernel(kernel.version, kernel.release, kernel.arch,
                           kernargs)
    kernels = get_kernels(mountpoint, "PAE")
    for kernel in kernels:
        grub.insert_kernel(kernel.version, kernel.release, kernel.arch,
                           kernargs, suffix=".PAE")
    grub.save()

    # grub.conf for virtualization
    bind_floppy_grub(vfatmountpoint, mountpoint)
    grub = Grub(grub_dev, mountpoint, boot_part=options.bootpart)
    grub.remove_all_entries()
    grub.set_defaults()
    kernels = get_kernels(mountpoint, "vbox")
    kernargs = [ ( "root", "UUID=" + rootuuid) ]
    if options.compressroot:
        kernargs.insert(1, ("rootflags", "compress"))
    for kernel in kernels:
        grub.insert_kernel(kernel.version, kernel.release, kernel.arch,
                           kernargs, suffix=".vbox", i18n=False)
    grub.save()
    unbind_floppy_grub()

@stage
def agoraboxconsts(dev, mountpoint, target):
    """
    Generating Agorabox constants
    """
    generate_agorabox_consts(dev, mountpoint, target)

@stage
def usbmodel(dev, mountpoint):
    """
    Getting USB key model
    """
    set_key_model(dev, mountpoint)

@stage
def copybtrfs(mountpoint, dev):
    """
    Setting up compressed partition
    """
    if not options.compress:
        return
    logging.debug("Copying compressed partition")
    btrfs = os.path.join(mountpoint, "..", "btrfs")

    loop = commands.getoutput("losetup -f").strip()
    run_command("losetup -f " + compressed)
    mount(loop, "loopcompress", mountpoint=btrfs) # , options="compress")

    install_yum_package(mountpoint, "yum-compressed")
    compress = os.path.join(mountpoint, "compress")
    if not os.path.exists(compress):
        os.makedirs(compress)
    comprmountpoint = mount(cf.getDevPart(dev, "compressed"), "btrfs", os.path.join(mountpoint, "compress"), options="compress")
    copy_filesystem(comprmountpoint, btrfs)

    umount("loopcompress")
    umount("compress")

    # run_command("dd if=" + compressed + " of=" + cf.getDevPart(dev, "compressed"))
    # Does not work. Can't change UUID ???
    # import augeas
    # aug = augeas.Augeas(root=mountpoint)
    # uuid = aug.get('/files/etc/sysconfig/ufo/uuids/compressed')
    # run_command("tune2fs -U " + uuid + " " + cf.getDevPart(dev, "compressed"))

@stage
def hideparts(dev, mountpoint):
    """
    Hiding home partition from nautilus
    """
    size = cf.getPart("home")["size"]
    homedev = cf.getDevPart(dev, "home")
    size2 = int(open("/sys/block/%s/%s/size" % (os.path.basename(dev),
                                                os.path.basename(homedev))).read()) * 512 / 1000 / 1000 / 1000.
    s = "%.1f GB Filesystem\n" % (size2,)
    volume_exceptions = os.path.join(mountpoint, "usr", "share", "nautilus", "volumes.exceptions")
    if os.path.exists(os.path.dirname(volume_exceptions)):
        open(volume_exceptions, "w").write(s.replace('.', ','))

@stage
def mbr(dev, mountpoint):
    """
    Writing Master Boot Record
    """
    write_mbr(dev, mountpoint)

@stage
@cleanup
@mandatory
def unmountall(mountpoint):
    """
    Unmount everything
    """
    umountAll()
    os.system("umount " + mountpoint + " &")

@stage
def hidefiles(dev, exec_path):
    """
    Hiding some files on FAT
    """
    shutil.copy(path.join(exec_path, "mtools.conf"), "/etc/mtools.conf")
    letter = cf.getDevPart(dev, "UFO")[-2]
    files = [ "UFO.ico", ".background", "autorun.inf", ".autorun",
              "Windows/*.dll", "Windows/bin", "Windows/bin64", "Windows/Microsoft.VC90.CRT",
              ".data", ".DS_Store", "ufo.app", ".VolumeIcon.icns", ".*" ]
    for f in files:
        run_command("mattrib +h %s:%s" % (letter, f), critical=True)

class SerieGenerator(Generator):
    stages = [ repart,
               mountroot,
               mountfat,
               copyfilesystem,
               installgrub,
               agoraboxconsts,
               usbmodel,
               filelist,
               copybtrfs,
               hideparts,
               mbr,
               unmountall,
               hidefiles ]

    short_option = "-s"
    long_option = "--serie"
    help = "generates a key from prototype"
                       
    def __init__(self, options, args, parser):
        Generator.__init__(self, options)
        if len(args) != 3:
            parser.error("Wrong number of parameters...")
        proto, target, dev = args
        self.proto = makeabs(proto)
        if not os.path.exists("/usr/bin/mattrib"):
            print "You need the mtools package to generate a serie"
            sys.exit(1)
        if not os.path.exists(proto):
            parser.error("The directory %s does not exist" % (proto, ))
        self.target = target
        self.compressed = options.compress
        self.compressroot = options.compressroot
        self.dev = dev
        self.root = path.join(proto, "aufs")
        if not path.exists(self.root):
            self.root = os.path.join(proto, "root")
        self.options = options
        self.mountpoint = options.mountpoint

if __name__ == "__main__":
    seriegen = SerieGenerator()
    seriegen.run(options=None)
