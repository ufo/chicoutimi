import os
import commands
from genutils import run_command
import logging
import yum

def get_mounted_path(mountpoint, program):
    return os.path.join(mountpoint, program)

def get_yum_path(mountpoint):
    return get_mounted_path(mountpoint, 'usr/bin/yum')

def get_rpm_path(mountpoint):
    return get_mounted_path(mountpoint, 'bin/rpm')

def get_rpmdb_path(mountpoint):
    return get_mounted_path(mountpoint, 'usr/bin/rpmdb')

def install_yum_package(mountpoint, pkg, excludes=[], critical=True):
    exclude_arg = ""
    if excludes:
       exclude_arg = "--exclude=" + " --exclude=".join(excludes)
    # run_command("%s --nogpgcheck -y --installroot=%s install %s" % (get_yum_path(mountpoint), mountpoint, pkg))
    run_command("/usr/bin/yum -y " + exclude_arg + " install %s" % (pkg,), chroot=mountpoint, critical=critical)

def remove_yum_package(mountpoint, pkg, critical=True):
    # run_command("%s --nogpgcheck -y --installroot=%s remove %s" % (get_yum_path(mountpoint), mountpoint, pkg))
    run_command("/usr/bin/yum -y remove %s" % (pkg,), chroot=mountpoint, critical=critical)

def install_yum_group(mountpoint, group, excludes=[], critical=True):
    exclude_arg = ""
    if excludes:
        exclude_arg = "--exclude=" + " --exclude=".join(excludes)
    # run_command("%s --nogpgcheck -y --installroot=%s groupinstall %s" % (get_yum_path(mountpoint), mountpoint, group))
    run_command("/usr/bin/yum " + exclude_arg + " -y groupinstall %s" % (group,), chroot=mountpoint, critical=critical)
    
def install_rpm_package(mountpoint, pkg, critical=True):
    # run_command("%s -ivf --root=%s %s" % (get_rpm_path(mountpoint), mountpoint, pkg))
    run_command("/bin/rpm -ivf %s" % (pkg,), chroot=mountpoint, critical=critical)

def install_updates(mountpoint, excludes=[], critical=True):
    exclude_arg = ""
    if excludes:
       exclude_arg = "--exclude=" + " --exclude=".join(excludes)
    # run_command("%s --nogpgcheck -y --installroot=%s update" % (get_yum_path(mountpoint), mountpoint))
    run_command("/usr/bin/yum " + exclude_arg + " -y update", chroot=mountpoint, critical=critical)

def rebuild_rpmdb(mountpoint):
    # run_command("rm -rf %s" % (os.path.join(mountpoint, 'var/lib/rpm/__db*'),))
    run_command("rm -rf /var/lib/rpm/__db*", chroot=mountpoint)
    run_command("/usr/bin/rpmdb --rebuilddb", chroot=mountpoint)

def remove_unwanted_packages(mountpoint, group):
    """
    group += " Unwanted"
    import yum
    base = yum.YumBase()   
    base.doConfigSetup(root=mountpoint)
    base.doRepoSetup()
    groups = base.doGroupSetup()
    for grp in groups.groups:
        if grp.name == group:
            base.close()
            base.closeRpmDB()
            remove_yum_package(mountpoint, " ".join(grp.packages))
            return
    print "Error: Didn't find the group", group
    base.close()
    base.closeRpmDB()
    """

    open(os.path.join(mountpoint, "list_unwanted.py"), "w").write("""
def remove_unwanted_packages(mountpoint, group):
    group += " Unwanted"
    import yum
    import os 
    base = yum.YumBase()   
    base.doConfigSetup(root=mountpoint)
    base.doRepoSetup()
    groups = base.doGroupSetup()
    for grp in groups.groups:   
        if grp.name == group:   
            base.close()
            base.closeRpmDB()
            os.system("yum -y remove " + " ".join(grp.packages))
            return
    print "Error: Didn't find the group", group
    base.close()
    base.closeRpmDB()

remove_unwanted_packages("/", "%s")
""" % (group, ))
    run_command("python /list_unwanted.py", chroot=mountpoint)
    run_command("rm -f /list_unwanted.py", chroot=mountpoint) 

def update_rpm(mountpoint, pkg, critical=True):
    if type(pkg) == list:
        pkg = " ".join(pkg)
    run_command("yum -y install " + pkg, chroot=mountpoint, critical=critical)

def get_kernels(mountpoint = "/", variant=""):
    if variant:
        kernel = "kernel-" + variant
    else:
        kernel = "kernel"
    yumbase = yum.YumBase()
    yumbase.doConfigSetup(root=mountpoint)
    pkglist = yumbase.doPackageLists('installed')
    exactmatch, matched, unmatched = yum.packages.parsePackages(pkglist.installed, [ kernel ])
    exactmatch.sort(key=lambda x: x.version)
    yumbase.close()
    yumbase.closeRpmDB()
    return exactmatch

def format_kernel_name(kernel):
    index = kernel.name.find("-")
    if index != -1:
        suffix = "." + kernel.name[index + 1:]
    else:
        suffix = ""
    return kernel.version + '-' + \
           kernel.release + "." + \
           kernel.arch + suffix
