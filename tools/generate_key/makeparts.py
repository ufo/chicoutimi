#!/usr/bin/env python

# Auteurs:  Marc SCHLINGER
# Date:     15/08/2008
# 

import re,os,sys
import math
import conf as cf
import shutil
import string
import augeas
import logging
import commands
from genutils import *

def generate_sfdisk(dev):
    """
    Genere le partionnement des cles Agorabox
    la premiere partition correspond au home de l'utilisateur
    sa taille est determinee en fonction de la place restante
    laisse par les partitions de taille fixe.
    """

    LINES_SFDISK = ""
    SF_CYL = "/sbin/sfdisk -l -uC"
    SF_BUB = "/sbin/sfdisk -l"
    SF_SEC = "/sbin/sfdisk -s"

    # Regex
    numCylindresMatch = re.compile(r"" + dev + " ?: (\d+) cylind")
    tailleBlockMatch = re.compile(r"(blocs de|blocks of) (\d+) (bytes|octets)")

    # Recuperation du nombre de cylindres
    fullOutput = ""
    for line in os.popen(SF_CYL+" " + dev).readlines():
        fullOutput += line

    numCylindres=int(numCylindresMatch.search(fullOutput).groups()[0])

    # Recuperation de la taille d'un block
    fullFile = ""
    for line in os.popen(SF_BUB + " " + dev).readlines():
        fullFile += line

    tailleBlocks = int(tailleBlockMatch.search(fullFile).groups()[1])

    # Recuperation du nombre de block
    numBlocks = int(os.popen(SF_SEC + " " + dev).readline())

    tailleTotal = tailleBlocks * numBlocks

    # taille approximative d'un cylindre en Mega
    approxTailleCylindre = float(tailleTotal / numCylindres) / 1024 / 1024

    # #######################################################################
    # #######################################################################
    # #######################################################################

    # La taille fixe du partitionnement est calculee en additionnant
    # toute les tailles jusqu'a la partition etendue (compris)

    partFixe = 0
    for part in cf.cfg:
        partFixe+=part["size"]
        if part["type"]=="5":
            break

    # approximation du nombre de cylindre a reserver pour la partie fixe du partitionement
    nbCylPartFix = int(partFixe/approxTailleCylindre) 

    # On en deduit le nombre de cylindre pour les partoches
    nbCylHome = numCylindres - nbCylPartFix

    # nbCylRoot=int(4000/approxTailleCylindre)
    # nbCylSwap=int(512/approxTailleCylindre)
    # nbCyleSys=int(35/approxTailleCylindre)

    # Generation des entrees attendues par sfdisk
    for part in cf.cfg:
        size=""
        type=""
        opt=""
    
        size = "%s" % (int(part["size"] / approxTailleCylindre),)

        # partition swap 
        if part["type"] == "82":
            type = "S"

        # partition extended
        elif part["type"] == "5":
            type = "E"
            size = ""
    
        type = part["type"]

        # La derniere partition ne se voit pas affecter de taille afin qu'elle occupe tout l'espace restant
        if len(cf.cfg)-1 == cf.cfg.index(part):
            size = ""
    
        # partition home taille calculee
        if part["name"] == "home":  
            size = "%d" % (nbCylHome,)
            part["size"] = int(size) * approxTailleCylindre

        # La partition root doit etre bootable
        if part["name"] == "root":
            opt = ",*"

        LINES_SFDISK += ",%s,%s%s\n" % (size, type, opt)

    return LINES_SFDISK

def format_partitions(dev):
    for part in cf.cfg:
        format_cmd = ""
        format_opt = ""
        name_opt = ""
        partnum = cf.getNumPart(part["name"])  # +1 car le tableau commence a 0 
        cmd = ""
        if part.has_key("label"):
            label = part["label"]
        else:
            label = part["name"]
                                                                        
        if part["fs"] in ("ext3", "ext4", "vfat", "ubifs", "btrfs"):
            format_cmd = "/sbin/mkfs.%s"%(part["fs"],)
            name_opt = ""
            uuid_opt = ""

            if part["fs"] in ("ext3", "ext4", "btrfs"):
                if part.has_key("label"):
                    label = part["label"]
                else:
                    label = part["name"]
                name_opt = "-L %s"%(label,)

                # It seems like we can't specify UUID for a brfs partition
                if part.has_key("uuid") and part["fs"] != "btrfs":
                    uuid_opt += "-U " + part["uuid"]
            
            if part["fs"] == "vfat":
                name_opt = " -n %s"%(label,)

            cmd = "%s %s %s %s %s%s" % (format_cmd, format_opt, uuid_opt, name_opt, dev, partnum)
            logging.info("%s will be formatted using %s" % (part["name"], cmd))

            run_command(cmd)

            if part["fs"] == "btrfs":
                part["uuid"] = get_uuid(dev + str(partnum))

        elif part["fs"] == "crypt":
            run_command("echo -e 'YES\\nufo\\nufo' | cryptsetup luksFormat %s%s" % (dev, partnum))
            part["uuid"] = get_uuid(dev + str(partnum))

def repartition(dev):
    partitions = generate_sfdisk(dev)
    run_command("umount " + dev + "*", logging.info)
    run_command("echo \"%s\" | /sbin/sfdisk %s " % (partitions, dev))
    run_command("umount " + dev + "*", logging.info)
    format_partitions(dev)

def get_uuid(part):
    return run_command("blkid -o value -s UUID -c /dev/null %s" % (part,), output=True)[1].strip()

def write_mbr(dev, mountpoint, recheck=False):
    open(os.path.join(mountpoint, 'boot/grub/device.map'), 'w').write("(hd0)	" + dev)
    cmd = "/sbin/grub-install"
    if recheck:
        cmd += " --recheck"
    cmd += " --no-floppy --root-directory=%s %s" % (mountpoint, dev)
    # cmd = "echo -e 'root (hd0,2)\\nsetup (hd0)' | grub --batch"
    run_command(cmd, critical=True)
    # run_command(cmd, critical=True, chroot=mountpoint)
    open(os.path.join(mountpoint, 'boot/grub/device.map'), 'w').write("(hd0)	/dev/sda")

def update_grub_conf(mountpoint):
    newest = []
    kernels = commands.getoutput("chroot " + mountpoint + " rpm -qa | grep kernel-2").split("\n")
    for k in kernels:
        if map(int, k[7:k.find('-', 7)].split('.')) > map(int, newest):
           newkernver = k[7:]
           newest = k[7:k.find('-', 7)].split('.')
    logging.debug("Most recent kernel " + newkernver)

def generate_fstab(dev, mountpoint, rootuuid="", utiluuid="", bootuuid="", vboxsf=False):
    if rootuuid == "":
        rootuuid = cf.getUUIDFromName(dev, "root")
    if utiluuid == "":
        utiluuid = cf.getUUIDFromName(dev, "UFO")
    if bootuuid == "":
        bootuuid = cf.getUUIDFromName(dev, "boot")
    fstab = """# /etc/fstab
#
# Accessible filesystems, by reference, are maintained under '/dev/disk'
# See man pages fstab(5), findfs(8), mount(8) and/or vol_id(8) for more info
#
"""

    if rootuuid:
        fstab += "UUID=" + rootuuid + "\t/\t\t" + cf.getPart("root")["fs"] + "\tnoatime,defaults\t1 1\n"

    if bootuuid:
        fstab += "UUID=" + bootuuid + "\t/boot\t\t" + cf.getPart("boot")["fs"] + "\tnoatime,defaults\t1 1\n"
    
    if utiluuid:
        fstab += "UUID=" + utiluuid + "          /media/UFO              vfat    ro,umask=0077,shortname=winnt 0 0\n"
        
    fstab += """tmpfs                   /dev/shm                tmpfs   defaults,mode=777        0 0
devpts                  /dev/pts                devpts  gid=5,mode=620  0 0
sysfs                   /sys                    sysfs   defaults        0 0
proc                    /proc                   proc    defaults        0 0
"""

    open(os.path.join(mountpoint, "etc/fstab"), "w").write(fstab)

def remove_from_fstab(mountpoint, to_remove):
    aug = augeas.Augeas(root=mountpoint)
    matches = aug.match("/files/etc/fstab/*/file")
    for match in matches:
        if aug.get(match) in to_remove:
            aug.remove(os.path.dirname(match))
    aug.save()

