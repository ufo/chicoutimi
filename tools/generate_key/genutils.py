import os
import sys
import conf as cf
import logging
import commands
import subprocess
import random as rnd
import shutil
from ConfigParser import ConfigParser, SafeConfigParser

def makeabs(path):
    if not os.path.isabs(path):
        return os.path.join(os.path.dirname(os.path.abspath(sys.argv[0])), path)
    return path
                
def fail(msg = ""):
    if msg: logging.critical(msg)
    raise Exception("The generation has failed because of a critical error")

def bind_chroot(mountpoint):
    mount('/proc', 'proc', mountpoint=os.path.join(mountpoint, 'proc'), options='bind', log=False)
    mount('/dev', 'dev', mountpoint=os.path.join(mountpoint, 'dev'), options='bind', log=False)
    mount('/sys', 'sys', mountpoint=os.path.join(mountpoint, 'sys'), options='bind', log=False)
    if not os.path.exists(os.path.join(mountpoint, 'etc')):
        os.mkdir(os.path.join(mountpoint, 'etc'))
    open(os.path.join(mountpoint, 'etc', 'resolv.conf'), 'w').write(open('/etc/resolv.conf').read())

def unbind_chroot(mountpoint):
    umount('proc', log=False)
    umount('dev', log=False)
    umount('sys', log=False)

def run_command(cmd, func=logging.error, chroot='', output=False, env = None, cwd = None, critical=False, log=True):
    if chroot:
        bind_chroot(chroot)
        cmd = "chroot " + chroot + " " + cmd
    if log: logging.debug(cmd)
    # status, output = commands.getstatusoutput(cmd)
    # returncode = subprocess.call(cmd.split(" "))
    if output:
        proc = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, env = env, cwd = None)
        output = proc.communicate()[0]
    else:
        proc = subprocess.Popen(cmd, shell=True, env = env, cwd = None)
        proc.wait()
    # output, errors = proc.communicate()
    # if output:
    #     logging.debug(output + '\n' + errors)
    if log and proc.returncode:
        func("The command %s returned : %d\n" % (cmd, proc.returncode))
        if critical:
            fail("The critical command " + cmd + " failed")
    # if proc.returncode:
    #     func("The command %s returned : %d\nOutput :\n%s\n%s" % (cmd, proc.returncode, output, errors))
    if chroot:
        unbind_chroot(chroot)
    if output:
        return proc.returncode, output
    else:
        return proc.returncode

def reach_downloads_agorabox():
    return not run_command("/usr/bin/gethostip downloads.agorabox.org")

def copy_filesystem(mountpoint, serie):
    if os.path.isdir(mountpoint):
        run_command("cp -p -R -P %s %s" % (os.path.join(serie, '*'), mountpoint), critical=True)
    else:
        # Copying cpio backup in the right place (mounted root partitions)
        # As I have no idea what cpio does, it something messes with the filesystem
        run_command("cd %s && cpio -cid < %s" % (mountpoint, serie))

def generate_agorabox_consts(dev, mountpoint, target=None):
    folder = os.path.join(mountpoint, "etc/sysconfig/ufo")
    try: os.makedirs(folder)
    except: pass
    # Put partitions uuid in agorabox system configuration
    f = open(os.path.join(folder, "uuids"), "w")
    for part in cf.cfg:
        f.write("%s=%s\n" % (part["name"], cf.getUUIDFromName(dev, part["name"])))
    cp = SafeConfigParser()
    settings = os.path.join(mountpoint, "media", "UFO", ".data", "settings", "settings.conf")
    cp.read(settings)
    if not cp.has_section("rawdisk"):
        cp.add_section("rawdisk")
    cp.set("rawdisk", "FATUUID", cf.getUUIDFromName(dev, "UFO"))
    if target:
        if not cp.has_section("launcher"):
            cp.add_section("launcher")
        cp.set("launcher", "updateurl", "http://downloads.agorabox.org/launcher/" + target)
    cp.set("rawdisk", "parts", ",".join(map(str, range(2, len(cf.cfg) + 1))))
    cp.write(open(settings, "w"))

mount_dir = '/mnt'
mounts = []

def genMountPoint():
    r = rnd.randint(10000, 99999)
    chemin = os.path.join(mount_dir, "genkey-%d/" % (r,))

    while os.path.exists(chemin):
        r = rnd.randint(10000, 99999)
        chemin = os.path.join(mount_dir, "genkey-%d" % (r,))

    os.mkdir(chemin)
    return chemin

def mount(dev, name, mountpoint="", mountexec="mount", part="", fstype="", options = ['defaults'], log=True):
    """
    Monte une partition et renvoie le point de montage
    Si la partition est deja montee mount renvoie le point de montage
    """
    infos = cf.getPart(name)
    if not fstype:
        try: fstype = "-t " + infos["fs"]
        except: pass
    else:
        fstype = "-t " + fstype
    
    if not part:
        try: part = cf.getDevPart(dev, name)
        except: part = dev
    
    if not mountpoint:
        mnt_path = genMountPoint()
        mountpoint = os.path.join(mnt_path, name)

    if not os.path.ismount(mountpoint):
        if type(options) == str:
            opts = "-o " + options
        else:
            opts = "-o " + ','.join(options)
        if infos:
            opts += "," + ",".join(infos.get("options", []))
        cmd = "%s %s %s %s %s" % (mountexec, opts, fstype, part, mountpoint)
        if log:
            logging.debug("Mounting: " + cmd)
        try: os.makedirs(mountpoint)
        except: pass
        run_command(cmd, log=log)
        mounts.append((name, mountpoint))
    
    return mountpoint

def findmount(name):
    for i, mount in enumerate(mounts):
         if mount[0] == name: return i
    return -1

def umount(name, log=True):
    ind = findmount(name)
    if ind != -1:
        run_command("umount %s" % (mounts[ind][1],), log=log)
        del mounts[ind]

def dummyUmount(name):
    ind = findmount(name)
    run_command("umount %s" % (mounts[ind][1],))
    del mounts[ind]
    
def umountAll():
    global mounts
    rmounts = mounts[:]
    rmounts.reverse()
    for name, mountpoint in rmounts:
        if run_command("umount " + mountpoint):
            run_command("lsof " + mountpoint)
    mounts=[]
        
def clearPath():
    if len(mounts):
        umountAll()

def generate_cpio(mountpoint, target):
    if not os.path.isabs(target):
        target = os.path.join(os.getcwd(), target)
    run_command("cd %s && find . | cpio -co > %s" % (mountpoint, target))

def activate_services(mountpoint, services, state="on"):
    for service in services:
        run_command("/sbin/chkconfig " + service + " " + state, chroot=mountpoint)
   
def create_snapshot(dev, size, origsize, name, keep=False):
    if not keep:
        os.system("rm -rf " + name)
    fd = os.open(name, os.O_WRONLY | os.O_CREAT)
    os.lseek(fd, size * 1024 * 1024, 0)
    os.write(fd, '\x00')
    os.close(fd)

    loop = commands.getoutput("losetup -f").strip()
    run_command("losetup -f " + name)

    size = int(commands.getoutput("df " + dev + " | tail -n 1").split()[1]) * 1024 / 512
    run_command('dmsetup create ' + name + ' --table "0 %s snapshot %s %s p 8"' % (origsize, dev, loop))

    return mount("/dev/mapper/" + name, "root", part="/dev/mapper/" + name), loop

def copy_devs(mountpoint):
    pwd = os.getcwd()
    if not os.path.exists("dev-vbox.cpio"):
        run_command("wget http://kickstart.agorabox.org/private/virtualization/dev-vbox.cpio")
    run_command("rm -rf " + os.path.join(mountpoint, "dev", "*") + " && cd " + os.path.join(mountpoint, "dev") + " && cpio -id < " + os.path.join(pwd, "dev-vbox.cpio") + " && cd " + pwd)

def set_key_model(dev, mountpoint):
    vendor = open(os.path.join("/", "sys", "block", os.path.basename(dev), "device", "vendor")).read().strip()
    model = open(os.path.join("/", "sys", "block", os.path.basename(dev), "device", "model")).read().strip()
    cp = ConfigParser()
    conf = os.path.join(mountpoint, "media", "UFO", ".data", "settings", "settings.conf")
    cp.read(conf)
    if not cp.has_section("rawdisk"):
        cp.add_section("rawdisk")
    cp.set("rawdisk", "model", vendor + " " + model + " USB Device")
    cp.write(open(conf, "w"))

def mount_union(dirs, dest, tmp, keep = False):
    tmp = os.path.abspath(tmp)
    if not keep:
        logging.debug("Cleaning " + tmp)
        run_command("rm -rf " + tmp)
        os.makedirs(tmp)
    dest = os.path.abspath(dest)
    mount("none", "unionfs", mountpoint=dest, fstype="unionfs",
          options="dirs=" + tmp + "=rw:" + "=ro:".join(dirs) + "=ro")
    return dest

def mount_aufs(dirs, outputdir, keep = False):
    tmp = os.path.join(outputdir, "aufs-sparse")
    mountpoint = os.path.join(outputdir, "aufs")
    tmp = os.path.abspath(tmp)
    if not keep:
        logging.debug("Cleaning " + tmp)
        run_command("rm -rf " + tmp)
        os.makedirs(tmp)
    dest = os.path.abspath(mountpoint)
    mount("none", "aufs", mountpoint=mountpoint, fstype="aufs",
          options="xino=/dev/shm/aufs.xino,br=" + tmp + ":" + "=ro+wh:".join(dirs))
    return mountpoint

def compress_pkg(pkg, dst, mountpoint):
    space_used = int(commands.getoutput("df | grep " + mountpoint).split("\n")[0].split()[2]) / 1024
    err, output = run_command("rpm -ql " + pkg, output=True, chroot=mountpoint)
    files = output.split("\n")
    sum = 0
    for file in files:
        if not file: continue
        cp = dst + '/' + file
        src = mountpoint + '/' + file
        # print "Copying", file, "to", cp
        if not os.path.exists(os.path.dirname(cp)):
            os.makedirs(os.path.dirname(cp))
        if os.path.isfile(src):
            size = os.stat(src).st_size
            sum += size
            shutil.move(src, cp)
    new_space_used = int(commands.getoutput("df | grep " + mountpoint).split("\n")[0].split()[2]) / 1024
    return sum, new_space_used - space_used

def dracut(mountpoint, kernel, modules=""):
    if modules:
        modules = "--modules '" + " ".join(modules) + "'"
    run_command("/sbin/dracut --force %s /boot/initramfs-%s.img %s" % (modules, kernel, kernel),
                chroot=mountpoint, critical=True)

