# -*- coding: utf-8 -*-

import Pyro.core
import logging
import threading
import subprocess
import time
import signal
import os, os.path as path
import sys

testing = False

if len(sys.argv) > 2 and path.basename(sys.argv[1]) == "python":
    exec_path = path.dirname(path.abspath(sys.argv[1]))
else:
    exec_path = path.dirname(path.abspath(sys.argv[0]))

class KeygenThread(threading.Thread):
    def __init__(self, dev, task, logfile, testing):
        threading.Thread.__init__(self)
        self.dev = dev
        self.task = task
        self.process = None
        self.logfile = logfile
        self.testing = testing
    
    def run(self):
        start = time.time()
        logging.debug("Running generation on " + self.dev)
        if testing:
            time.sleep(5)
            self.task.stdout = self.task.stderr = ""
            self.task.returncode = 123
        else:
            if self.testing:
                generator = "false"
            else:
                generator = self.task.prototype["generator"]
            cmd = [ generator ] + self.task.args + [ self.dev ]
            # if skip: cmd.insert(1, "--skip=" + ",".join(skip)
            # if only: cmd.insert(1, "--only=" + ",".join(only)
            if self.logfile:
                if self.logfile:
                    cmd = cmd[0:1] + [ "--log", self.logfile ] + cmd[1:]
                stdout = subprocess.PIPE
                stderr = subprocess.PIPE
                # stdout = os.open(self.logfile, os.O_APPEND | os.O_CREAT)
                # stderr = stdout
            else:
                stdout = subprocess.PIPE
                stderr = subprocess.PIPE

            self.process = subprocess.Popen(cmd,
                                            stdout=stdout,
                                            stderr=stderr,
                                            cwd=exec_path)
            self.task.stdout, self.task.stderr = self.process.communicate()
            if self.logfile:
                open(self.logfile, "a").write("Command line: " + " ".join(cmd) + "\n" + \
                                              "Returned error:\n" + self.task.stderr)
            self.task.returncode = self.process.returncode

        end = time.time()
        self.task.time = end - start
        self.task.keygen.notify_finished(self.task)

class KeygenTask(Pyro.core.ObjBase):
    def __init__(self, dev, keygen, prototype, args, logfile=None, testing=False):
        Pyro.core.ObjBase.__init__(self)
        self.dev = dev
        self.keygen = keygen
        self.pid = -1
        self.time = 0
        self.logfile = logfile
        self.prototype = prototype
        self.args = args
        self.testing = testing

    def start(self):
        self.thread = KeygenThread(self.dev, self, logfile=self.logfile, testing=self.testing)
        self.thread.start()

    def interrupt(self):
        os.kill(self.thread.process.pid, signal.SIGINT)

    def __getstate__(self):
        return { "dev" : self.dev }

    def get_free_space(self):
        parts = {}
        process = subprocess.Popen([ "df" ], stdout=subprocess.PIPE, shell=True)
        lines = process.communicate()[0].split("\n")[1:]
        if not process.returncode:
            for line in lines:
                if line:
                    try:
                        dev, blocks, used, free, capacity, mountpoint = line.split()
                        if dev.startswith(self.dev):
                            parts[dev] = capacity
                    except:
                        pass
        return parts
            
    def report(self):
        report = "Task '%s' report:" % (str(self),)
        report += "Took %d seconds (%d minutes)" % (int(self.time), int(self.time / 60))
        report += "Return code: %d" % (self.returncode,)
        logging.info(report)
        return { self.dev : self.returncode}

    def get_output(self):
        return open(self.logfile).read()

