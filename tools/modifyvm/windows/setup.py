from distutils.core import setup
import py2exe
import sys

setup(zipfile = None,
      options = {'py2exe': {'bundle_files': 1}},
      windows = [{'script': "modifyvm.py"}],
)
