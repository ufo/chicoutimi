#!/bin/sh

# Attend un argument pour le root de la cle
#
if [ $# -lt 2 ]
    echo "usage : $0 <part> <ficher de sauvegarde>


find $1 -perm +4000 2> /dev/null > $2
