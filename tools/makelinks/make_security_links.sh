#!/bin/sh

if $# < 3 then
	echo "usage : make_security_links <noncrypted_part> <crypted_part> <file_1> <file_2> ...
fi

noncrypted=$1
crypted=$2

for file in $*
do
	# Make a symbolic link for each files in .cfg given in parameter
	while read ligne
	do
       	# Save files for instance
        # Commented 'coz no use :p
        # install -D $ligne /root/backup/$ligne

        # Build complete path if don't exist
        noncrypted_dir="$noncrypted`dirname $ligne`"
        crypted_dir="$crypted`dirname $ligne`"
        [ -d $noncrypted_dir ] || mkdir -p $noncrypted_dir
        [ -d $crypted_dir ] || mkdir -p $crypted_dir

        # Copy the real file in both partitions,
        # then create a symbolic link on the non crypted one
        echo "cp --preserve $ligne $noncrypted$ligne && rm -f $ligne"
        echo "cp --preserve $ligne $crypted$ligne && rm -f $ligne"
        cp --preserve $ligne $noncrypted$ligne && rm -f $ligne
        cp --preserve $ligne $crypted$ligne && rm -f $ligne
        echo "ln -s $noncrypted$ligne $ligne"
        ln -s $noncrypted$ligne $ligne
	done < $file
done
