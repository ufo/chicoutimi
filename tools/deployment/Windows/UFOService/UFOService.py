import win32serviceutil
import win32service
import win32event
import win32pipe
import win32file
import pywintypes
import winerror
import os, sys
import commands
import subprocess
import win32process
import win32con
import time
import win32gui
import logging
from ConfigParser import ConfigParser

vboxpath = "C:\\Program Files\\Sun\\xVM VirtualBox"
pipeName = "\\\\.\\pipe\\UFOPipe"
winpath = "C:\\Windows\\system32"
servicepath = "C:\\Program Files\\U.F.O for Windows Hosts"
settings = os.path.join(servicepath, "settings", "settings.conf")

class UFOService(win32serviceutil.ServiceFramework):
    _svc_name_ = "UFOService"
    _svc_display_name_ = "U.F.O Windows Service"
    def __init__(self, args):
        win32serviceutil.ServiceFramework.__init__(self, args)
        # Create an event which we will use to wait on.
        # The "service stop" request will set this event.
        self.hWaitStop = win32event.CreateEvent(None, 0, 0, None)
        # We need to use overlapped IO for this, so we dont block when
        # waiting for a client to connect.  This is the only effective way
        # to handle either a client connection, or a service stop request.
        self.overlapped = pywintypes.OVERLAPPED()
        # And create an event to be used in the OVERLAPPED object.
        self.overlapped.hEvent = win32event.CreateEvent(None,0,0,None)

        format = "%(asctime)s %(levelname)s %(message)s"
        logfile = os.path.join(servicepath, "service.log")
        logging.basicConfig(format=format, level=logging.DEBUG, filename=logfile)

    def SvcStop(self):
        # Before we do anything, tell the SCM we are starting the stop process.
        self.ReportServiceStatus(win32service.SERVICE_STOP_PENDING)
        # And set my event.
        win32event.SetEvent(self.hWaitStop)
        logging.shutdown()

    def SvcDoRun(self):
        # We create our named pipe.
        openMode = win32pipe.PIPE_ACCESS_DUPLEX | win32file.FILE_FLAG_OVERLAPPED
        pipeMode = win32pipe.PIPE_TYPE_MESSAGE
        
        # When running as a service, we must use special security for the pipe
        sa = pywintypes.SECURITY_ATTRIBUTES()
        # Say we do have a DACL, and it is empty
        # (ie, allow full access!)
        sa.SetSecurityDescriptorDacl ( 1, None, 0 )

        logging.debug("Creating named pipe")
        
        pipeHandle = win32pipe.CreateNamedPipe(pipeName,
            openMode,
            pipeMode,
            win32pipe.PIPE_UNLIMITED_INSTANCES,
            0, 0, 6000, # default buffers, and 6 second timeout.
            sa)

        while 1:
            logging.debug("Entering main loop")            
            try:
                hr = win32pipe.ConnectNamedPipe(pipeHandle, self.overlapped)
            except: # error, details:
                # logging.debug("Error connecting pipe! " + str(details))
                logging.debug("Error connecting pipe!")
                pipeHandle.Close()
                break
                   
            if hr==winerror.ERROR_PIPE_CONNECTED:
                # Client is fast, and already connected - signal event
                win32event.SetEvent(self.overlapped.hEvent)
            # Wait for either a connection, or a service stop request.
            timeout = win32event.INFINITE
            waitHandles = self.hWaitStop, self.overlapped.hEvent
            rc = win32event.WaitForMultipleObjects(waitHandles, 0, timeout)
            if rc==win32event.WAIT_OBJECT_0:
                # Stop event
                break
            else:
                try:
                    logging.debug("Waiting for command")
                    hr, data = win32file.ReadFile(pipeHandle, 256)
                    logging.debug("Got " + data)
                    cmds = data.split("|")
                    cmd = cmds.pop(0)
                    logging.debug("Command " + cmd)
                    
                    if cmd == "startufo":
                        si = win32process.STARTUPINFO()
                        si.lpDesktop = "WinSta0\\Default"
                        si.dwFlags = win32process.STARTF_USESHOWWINDOW
                        si.wShowWindow = win32con.SW_NORMAL
                        logging.debug(os.getcwd())
                        cmdline = os.path.join(servicepath, "ufo.exe")
                        logging.debug(cmdline)
                        handle = win32process.CreateProcess(None,
                            cmdline, None, None, False, 0, None, servicepath, si)
                        hprocess = handle[0]
                        ret = win32con.STILL_ACTIVE
                        while ret == win32con.STILL_ACTIVE:
                            msg = win32gui.PeekMessage(None, 0, 0, win32con.PM_REMOVE)[0]
                            while msg:
                                win32gui.TranslateMessage(msg)
                                win32gui.DispatchMessage(msg)
                                msg = win32gui.PeekMessage(None, 0, 0, win32con.PM_REMOVE)
                            time.sleep(1)
                            ret = win32process.GetExitCodeProcess(hprocess)
                            
                        logging.debug("Return code : " + str(ret))
                        win32file.WriteFile(pipeHandle, str(ret))
                        win32pipe.DisconnectNamedPipe(pipeHandle)

                except:
                    logging.debug("Exception : " + sys.exc_info()[1].message)
                    
if __name__=='__main__':
    win32serviceutil.HandleCommandLine(UFOService)
