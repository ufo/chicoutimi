from distutils.core import setup
import py2exe
import sys

cmdline_style='pywin32'

# If run without args, build executables, in quiet mode.
if len(sys.argv) == 1:
    sys.argv.append("py2exe")
    sys.argv.append("-q")

class Target:
    def __init__(self, **kw):
        self.__dict__.update(kw)
        self.version = "1.0.0"
        self.company_name = "Agorabox"
        self.copyright = "Copyright 2009"
        self.name = "U.F.O Window Service"

ufoservice = Target(
    description = "U.F.O Windows Service",
    modules = ["UFOService"]
    )

setup(
    options = {"py2exe": {"typelibs":
                          # typelib for WMI
                          [('{565783C6-CB41-11D1-8B02-00600806D9B6}', 0, 1, 2)],
                          # create a compressed zip archive
                          "compressed": 1,
                          "optimize": 2,
                          "bundle_files": 1}},
    zipfile = None,
    service = [ufoservice],
    )