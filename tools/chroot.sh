#!/bin/sh

mountpoint=$1
mount -o bind /proc $mountpoint/proc
mount -o bind /dev $mountpoint/dev
mount -o bind /dev $mountpoint/sys
shift
chroot $mountpoint $*
umount $mountpoint/proc
umount $mountpoint/dev
umount $mountpoint/sys
