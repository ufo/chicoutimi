#!/usr/bin/python

import os, sys
import commands
import tempfile
from optparse import OptionParser

usage = "%prog patch src dest"

parser = OptionParser(usage=usage)

(options, args) = parser.parse_args()

if len(args) != 3:
    parser.error("Wrong number of arguments")

patch, orig, mod = args

if not os.path.exists(patch):
    parser.error("File " + patch + " does not exist...")

if not (os.path.exists(orig) and os.path.isdir(orig)):
    parser.error("Folder " + orig + " must exist...")

if not (os.path.exists(mod) and os.path.isdir(mod)):
    parser.error("Folder " + mod + " must exist...")

len1 = len(orig.split('/'))
len2 = len(mod.split('/'))
level = 1

newdiff = ""
lines = open(patch).readlines()
temp = tempfile.mktemp()
os.system('mkdir ' + temp)
for n, line in enumerate(lines):
    if line.startswith('diff '):
        print lines[n + 1], lines[n + 2]
        if len(lines) > n + 2 and (lines[n + 1].startswith('--- ') or lines[n + 1].startswith('*** ')) \
           and (lines[n + 2].startswith('+++ ') or lines[n + 2].startswith('--- ')):
            f_orig, f_mod = line.split(' ')[-2:]
            f_orig = f_orig.strip()
            f_mod = f_mod.strip()
            if f_orig:
                f_orig = lines[n + 1].split()[1]
                f_mod = lines[n + 2].split()[1]
            print "f_orig", f_orig, "f_mod", f_mod
            rel_orig = '/'.join(f_orig.split('/')[level:])
            rel_mod = '/'.join(f_mod.split('/')[level:])
            # print f_orig, f_mod, rel_orig, rel_mod
            if os.path.exists(os.path.join(orig, rel_orig)):
                os.system('install -D ' + os.path.join(orig, rel_orig) + ' ' + os.path.join(temp, 'a', rel_orig))
            if os.path.exists(os.path.join(mod, rel_mod)):
                os.system('install -D ' + os.path.join(mod, rel_mod) + ' ' + os.path.join(temp, 'b', rel_mod))

cur = os.getcwd()
os.chdir(temp)
output = commands.getoutput('diff -u -r -N a b') + '\n'
# print temp
# os.system('rm -rf ' + temp)
os.chdir(cur)
open(patch, 'w').write(output)

