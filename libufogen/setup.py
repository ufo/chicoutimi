#!/usr/bin/env python2

DESCRIPTION = """\
libufogen is a library to generate UFO keys
"""

import os, sys
from distutils.core import setup, Extension

setup(name = "libufogen",
      version = "0.1",
      license = "GPLv2",
      description = "UFO keys generation library",
      long_description = DESCRIPTION,
      author = "Sylvain Baubeau",
      author_email = "sylvain.baubeau@agorabox.org",
      maintainer = "Sylvain Baubeau",
      maintainer_email = "sylvain.baubeau@agorabox.org",
      url = "http://www.agorabox.org",
      packages=['libufogen'],
      package_dir = {'libufogen': 'src'})

PATHHELP = """\

See http://www.python.org/doc/current/inst/search-path.html for detailed
information on various ways to set the search path.
One easy way is to set the environment variable PYTHONPATH.
"""
if "install" in sys.argv:
    print PATHHELP
