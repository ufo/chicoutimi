Name:           pam_vbox
Version:        0.1
Release:        1%{?dist}
Summary:        Authenticates from the host

Group:          System/Tools
License:        GPLv2
URL:            http://www.agorabox.org
Source0:        %{name}-%{version}.tar.gz
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  VirtualBox-OSE-guest-devel
Requires:       VirtualBox-OSE-guest

%define _moduledir /%{_lib}/security

%description
Authenticates from the host


%prep
%setup -q


%build
make %{?_smp_mflags}


%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT


%clean
rm -rf $RPM_BUILD_ROOT


%files
%defattr(-,root,root,-)
%doc
%{_moduledir}/pam_vbox.so


%changelog
* Tue Aug 11 2009 Sylvain Baubeau <sylvain.baubeau@agorabox.org>
Initial release
