/*
 * PAM module to authenticate from the host, through a guest property
 */

#include <stdio.h>
#include <unistd.h>
#include <ctype.h>
#include <stdarg.h>
#include <stdlib.h>
#include <string.h>
#include <syslog.h>
#include <sys/types.h>
#include <netdb.h>
#include <sys/types.h>
#include <fcntl.h>
#include <errno.h>

#include <VBox/err.h>
#include <VBox/VBoxGuest.h>
#include <iprt/initterm.h>

#include <security/pam_modules.h>
#include <security/_pam_macros.h>
#include <security/pam_ext.h>

/* defines for (internal) return values */
#define _PAM_VBOX_SYSERR  -1
#define _PAM_VBOX_OK       0
#define _PAM_VBOX_UNREACH -2

#define STATUS_GP_PATTERN  "/UFO/Credentials/Status"
#define AUTHTOK_GP_PATTERN "/UFO/Credentials/AuthTok"

#define SUCCESS_STATUS_GP_VALUE   "OK"
#define FAILURE_STATUS_GP_VALUE   "FAILED"
#define RETRIEVED_STATUS_GP_VALUE "PASSWORD_RETRIEVED"
#define NOPASS_GP_STATUS_VALUE    "NO_PASSWORD"


static void _pam_log(int err, const char *format, ...)
{
    va_list args;

    va_start(args, format);
    openlog("pam_vbox", LOG_PID, LOG_AUTHPRIV);
    vsyslog(err, format, args);
    va_end(args);
    closelog();
}

PAM_EXTERN
int pam_sm_authenticate(pam_handle_t * pamh, int flags,
                        int argc, const char **argv)
{
    uint32_t u32ClientId;
    int rc = VINF_SUCCESS;
    int retval;
    char *password;
    char *status;

    if (!RT_SUCCESS(RTR3Init()) || !RT_SUCCESS(VbglR3Init()))
    {
        _pam_log(0, "Could not contact the host system");
        return PAM_AUTHINFO_UNAVAIL;
    }

    /*
     * Connect to the service
     */
    rc = VbglR3GuestPropConnect(&u32ClientId);
    if (!RT_SUCCESS(rc))
    {
        _pam_log(0, "Failed to connect to the guest property service");
        return PAM_AUTHINFO_UNAVAIL;
    }

    rc = VbglR3GuestPropReadValueAlloc(u32ClientId,
                                       STATUS_GP_PATTERN,
                                       &status);

    if (RT_SUCCESS(rc))
    {
        if (strcmp(status, RETRIEVED_STATUS_GP_VALUE) == 0)
        {
            VbglR3GuestPropWriteValue(u32ClientId,
                                      STATUS_GP_PATTERN,
                                      FAILURE_STATUS_GP_VALUE);

            _pam_log(0, "Wrong password, switch to manual request\n");
            VbglR3GuestPropDisconnect(u32ClientId);
            return PAM_AUTHINFO_UNAVAIL;
        }
        else if ((strcmp(status, FAILURE_STATUS_GP_VALUE) == 0) ||
                 (strcmp(status, NOPASS_GP_STATUS_VALUE)  == 0) ||
                 (strcmp(status, SUCCESS_STATUS_GP_VALUE) == 0))
        {
            _pam_log(0, "Password has been retrieved\n");
            VbglR3GuestPropDisconnect(u32ClientId);
            return PAM_AUTHINFO_UNAVAIL;
        }
    }

    /*
     * Read password from host
     */
    rc = VbglR3GuestPropReadValueAlloc(u32ClientId,
                                       AUTHTOK_GP_PATTERN,
                                       &password);

    if (!RT_SUCCESS(rc))
    {
        _pam_log(0, "Could not read guest property\n");
        VbglR3GuestPropWriteValue(u32ClientId,
                                  STATUS_GP_PATTERN,
                                  NOPASS_GP_STATUS_VALUE);
        VbglR3GuestPropDisconnect(u32ClientId);
        return PAM_AUTHINFO_UNAVAIL;
    }

    VbglR3GuestPropWriteValue(u32ClientId,
                              STATUS_GP_PATTERN,
                              RETRIEVED_STATUS_GP_VALUE);

    /*
     * Clear guest property
     */
    VbglR3GuestPropWriteValue(u32ClientId,
                              AUTHTOK_GP_PATTERN,
                              NULL);
    /*
     * Set password for following modules
     */
	retval = pam_set_item(pamh, PAM_AUTHTOK, password);
    if (retval != PAM_SUCCESS)
    {
        _pam_log(0, "Failed to set password");
        VbglR3GuestPropDisconnect(u32ClientId);
        return PAM_AUTHINFO_UNAVAIL;
    }

    VbglR3GuestPropReadValueFree(password);
    VbglR3GuestPropDisconnect(u32ClientId);
    return PAM_SUCCESS;
}

PAM_EXTERN
int pam_sm_acct_mgmt(pam_handle_t *pamh, int flags, int argc, const
		     char **argv)
{
    return PAM_IGNORE;
}

PAM_EXTERN
int pam_sm_setcred(pam_handle_t * pamh, int flags,
                   int argc, const char **argv)
{
    return PAM_IGNORE;
}

PAM_EXTERN
int pam_sm_open_session(pam_handle_t * pamh, int flags,
                        int argc ,const char **argv)
{
	int retval;
	int rc = VINF_SUCCESS;
	uint32_t u32ClientId;
    char *status;

    if (!RT_SUCCESS(RTR3Init()) || !RT_SUCCESS(VbglR3Init()))
    {
        _pam_log(0, "Could not contact the host system");
        return PAM_IGNORE;
    }

    /*
     * Connect to the service
     */
    rc = VbglR3GuestPropConnect(&u32ClientId);
    if (!RT_SUCCESS(rc))
    {
        _pam_log(0, "Failed to connect to the guest property service");
        return PAM_IGNORE;
    }

    rc = VbglR3GuestPropReadValueAlloc(u32ClientId,
                                       STATUS_GP_PATTERN,
                                       &status);

    if (RT_SUCCESS(rc))
    {
        if (strcmp(status, RETRIEVED_STATUS_GP_VALUE) == 0)
        {
            _pam_log(0, "Given vbox password is ok");
            VbglR3GuestPropWriteValue(u32ClientId,
                                      STATUS_GP_PATTERN,
                                      SUCCESS_STATUS_GP_VALUE);
        }
        else
            _pam_log(0, "Password has been given manually");

        VbglR3GuestPropReadValueFree(status);
    }

    VbglR3GuestPropDisconnect(u32ClientId);
    return PAM_IGNORE;
}

PAM_EXTERN
int pam_sm_close_session(pam_handle_t * pamh, int flags, int argc ,
			 const char **argv)
{
    return PAM_IGNORE;
}

PAM_EXTERN
int pam_sm_chauthtok(pam_handle_t *pamh, int flags, int argc,
		     const char **argv)
{
    return PAM_IGNORE;
}
