%{!?python_sitelib: %define python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib()")}

Name:           ufo-liveusb-creator
Version:        1.0.0
Release:        2%{?dist}
Summary:        A liveusb creator and boot-window for UFO

Group:          Applications/System
License:        GPLv2
URL:            http://ufo.agorabox.fr/
Source0:        http://ufo.agorabox.fr/ufo-liveusb-creator/%{name}-%{version}.tar.gz
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildArch:      noarch

BuildRequires:  python-devel, python-setuptools, PyQt4-devel, desktop-file-utils gettext
Requires:       PyQt4, python-augeas
Requires:       wget mtools libufogen python-ldap

BuildRequires:  chkconfig

%description
A liveusb creator for UFO usb system

%prep
%setup -q

%build
%{__python} setup.py build

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

%{__python} setup.py install -O1 --skip-build --root %{buildroot}

mkdir -p %{buildroot}%{_sbindir}
mv %{buildroot}%{_bindir}/%{name} %{buildroot}%{_sbindir}/%{name}
ln -s consolehelper %{buildroot}%{_bindir}/%{name}

install -D -p -m 644 %{name}.pam %{buildroot}/etc/pam.d/%{name}

install -D -p -m 644 %{name}.console %{buildroot}/etc/security/console.apps/%{name}

desktop-file-install --vendor="agorabox"                        \
                     --dir=%{buildroot}%{_datadir}/applications \
                     %{buildroot}/%{_datadir}/applications/ufo-liveusb-creator.desktop

mv %{buildroot}/%{_datadir}/applications/agorabox-ufo-liveusb-creator.desktop \
   %{buildroot}/%{_datadir}/applications/ufo-liveusb-creator.desktop

%clean
rm -rf %{buildroot}

%post


%preun


%files
%defattr(-,root,root,-)
%doc README.txt LICENSE.txt
%{python_sitelib}/*
%{_bindir}/%{name}
%{_sbindir}/%{name}
%{_sysconfdir}/pam.d/%{name}
%{_sysconfdir}/security/console.apps/%{name}
%{_datadir}/applications/ufo-liveusb-creator.desktop
%{_datadir}/pixmaps/ufousb.png
%{_datadir}/locale/fr/LC_MESSAGES/ufo-liveusb-creator.mo

%changelog
* Tue Aug 12 2009 Antonin Fouques <antonin.fouques@agorabox.org> 1.0.0
- Initial release, adapted from package liveusb-creator-3.9
