===================
ufo-liveusb-creator
===================

A tool for easily installing UFO operating systems on to USB
flash drives.

License
-------

The liveusb-creator is licensed under the GPLv2.

This tool is distributed with the following open source software::

   Python
   http://python.org

   PyQt4
   http://wiki.python.org/moin/PyQt4

   SYSLINUX
   http://syslinux.zytor.com/
   Copyright 1994-2008 H. Peter Anvin - All Rights Reserved
   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, Inc., 53 Temple Place Ste 330,
   Boston MA 02111-1307, USA; either version 2 of the License, or
   (at your option) any later version; incorporated herein by reference.
