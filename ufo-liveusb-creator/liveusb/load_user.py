# -*- coding: utf-8 -*-
#
# Copyright © 2010  Agorabox. All rights reserved.
#
# This copyrighted material is made available to anyone wishing to use, modify,
# copy, or redistribute it subject to the terms and conditions of the GNU
# General Public License v.2.  This program is distributed in the hope that it
# will be useful, but WITHOUT ANY WARRANTY expressed or implied, including the
# implied warranties of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.  You should have
# received a copy of the GNU General Public License along with this program; if
# not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
# Floor, Boston, MA 02110-1301, USA. Any Agorabox trademarks that are
# incorporated in the source code or documentation are not subject to the GNU
# General Public License and may only be used or replicated with the express
# permission of Agorabox.
#
# Author(s): Antonin Fouques <antonin.fouques@agorabox.org>

import os, os.path as path
import ldap
import libuser
import augeas
import string
import commands

from ConfigParser import ConfigParser

class LoadUser():

    def apply(self, username, password):

        os.system("umount /var/tsumufs/*")

        aug = augeas.Augeas()
        self.search_base = aug.get("/files/etc/sysconfig/ufo/ldap/BASE")
        self.ldap_host = aug.get("/files/etc/sysconfig/ufo/ldap/SERVER")
        self.realm = aug.get("/files/etc/sysconfig/ufo/kerberos5/REALM")

        if not self.test_connection():
            return 1

        username = string.strip(str(username))
        for char in [ '*', '?', '+' ]:
            username = username.replace(char, '')
        password = str(password)

        if username == "" or password == "":
            os.system(_("zenity --error --text='You need to fill the U.F.O. Login and Password fields.'"))
            return 1

        ldapuser = self.findUser(username)
        if not ldapuser:
            return 1

        uid = ldapuser["uidNumber"][0]
        gid = ldapuser["gidNumber"][0]
        fullName = ldapuser["firstName"][0] + " " + ldapuser["lastName"][0]

        self.root = "/"
        self.admin = libuser.admin()

        homedir = "/home/%s" % username
        user = self.create_user(username, uid, fullName, gid, password, homedir)

        if os.path.exists("/etc/tsumufs/tsumufs.conf"):
            cf = ConfigParser()
            cf.read([ "/etc/tsumufs/tsumufs.conf" ])
            section = cf.sections()[0]
            ip = cf.get(section, "ip")
            export = cf.get(section, "export")
            path = cf.get(section, "fsmountpoint")
            if not os.path.isabs(path):
                path = os.path.join(os.path.expanduser("~" + username), path)
            if not os.path.exists("/var/tsumufs"):
                os.makedirs(path)
            os.chown(path, int(uid), int(gid))
            options = cf.get(section, "options")
            fstype = cf.get(section, "type")
            open("/etc/fstab", "a").write("%s:%s\t%s\t%s\t%s\t0\t0\n" % (ip, export, path, fstype, options))
            cf = ConfigParser()
            cf.read([ "/etc/idmapd.conf" ])
            if not cf.has_section("Mapping"):
                cf.add_section("Mapping")
            cf.set("Mapping", "nobody-user", username)
            cf.set("Mapping", "nobody-group", username)
            cf.write(open("/etc/idmapd.conf", "w"))

        os.system("sed -i 's/default_user        [0-9a-zA-Z_]*$/default_user        " + username + "/' " + \
                  os.path.join(self.root, "etc", "slim.conf"))
        self.add_to_sudoers(username)
        open(os.path.join(self.root, "etc", "sysconfig", "ufo", "mainuser"), "w").write(username)
        os.system('chown -R ' + username + ':' + username + ' ' + homedir)

        print _("Download of the synchronised paramaters")
        if os.path.exists("/etc/tsumufs/tsumufs.conf"):
            cf = ConfigParser()
            cf.read([ "/etc/tsumufs/tsumufs.conf" ])
            section = cf.sections()[0]
            path = cf.get(section, "fsmountpoint")
            status, output = commands.getstatusoutput("su -c '/usr/bin/ufo-restore-sync-conf " +
                                                      username +
                                                      " " +
                                                      password +
                                                      " " +
                                                      path +
                                                      " " +
                                                      homedir +
                                                      "' " +
                                                      username +
                                                      " 2>/dev/null")
        if not os.path.exists("/var/tsumufs/overlay"):
            os.makedirs("/var/tsumufs/overlay")
        os.system('chown -R ' + username + ':' + username + ' /var/tsumufs/overlay')

        print _("Thank you for waiting (and watching debug)")
        return 0

    def test_connection(self):
        try:
            l = ldap.initialize("ldap://" + self.ldap_host)
            users = l.search_s(self.search_base, ldap.SCOPE_SUBTREE,'(uid=*)')
            return True
        except:
            os.system(_("zenity --error --text='No connection found. Please check that your computer") + \
                                      _(" is connected to the Internet'"))
            return False

    def findUser(self, username):
        l = ldap.initialize("ldap://" + self.ldap_host)
        users = l.search_s(self.search_base, ldap.SCOPE_SUBTREE,'(uid=' + username + ')')
        print _("Found users"), users
        if not users:
            os.system(_("zenity --error --text='The user %s does not exists. Pleas check ") + \
                                     _("your Login and Password'") % username)
            return None
        return users[0][1]

    def create_user(self, username, uid, fullName, gid, password, mount_point):

        # If we get to this point, all the input seems to be valid.
        # Let's add the user.
        try: os.system("userdel -r %s" % username)
        except: pass
        userEnt = self.admin.initUser(username)

        userEnt.set(libuser.UIDNUMBER, uid)
        userEnt.set(libuser.GECOS, fullName)
        uidNumber = userEnt.get(libuser.UIDNUMBER)[0]

        groupEnt = self.admin.initGroup(username)
        groupEnt.set(libuser.GIDNUMBER, gid)
        gidNumber = groupEnt.get(libuser.GIDNUMBER)[0]
        userEnt.set(libuser.GIDNUMBER, gid)
        self.admin.addUser(userEnt, True)
        try: self.admin.addGroup(groupEnt)
        except: pass

        self.admin.setpassUser(userEnt, password, 0)
        rootEnt = self.admin.lookupUserByName('root')
        self.admin.setpassUser(rootEnt, password, 0)

        open(path.join(self.root, "etc", "sysconfig", "ufo", "user"), "w").write(username)

        return userEnt

    def add_to_sudoers(self, username):
        cmd = "/usr/sbin/usermod -a -G ufo " + username
        os.system(cmd)
