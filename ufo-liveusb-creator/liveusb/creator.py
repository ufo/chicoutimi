# -*- coding: utf-8 -*-
#
# Copyright © 2009-2010  Agorabox. All rights reserved.
# Copyright © 2008-2009  Red Hat, Inc. All rights reserved.
# Copyright © 2008-2009  Luke Macken <lmacken@redhat.com>
#
# This copyrighted material is made available to anyone wishing to use, modify,
# copy, or redistribute it subject to the terms and conditions of the GNU
# General Public License v.2.  This program is distributed in the hope that it
# will be useful, but WITHOUT ANY WARRANTY expressed or implied, including the
# implied warranties of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.  You should have
# received a copy of the GNU General Public License along with this program; if
# not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
# Floor, Boston, MA 02110-1301, USA. Any Red Hat or Agorabox trademarks that are
# incorporated in the source code or documentation are not subject to the GNU
# General Public License and may only be used or replicated with the express
# permission of Red Hat, Inc or Agorabox.
#
# Author(s): Luke Macken <lmacken@redhat.com>
# Adapted-by: Antonin Fouques <antonin.fouques@agorabox.org>

class LiveUSBCreator(object):

    def detect_removable_drives(self):
        """ Detect all removable USB storage devices using HAL via D-Bus
            The devices must be larger than the UFO-key img """
        import dbus, os
        self.drives = []
        devices = []
        try:
            try: self.imgSize = os.path.getsize("/mnt/live/LiveOS/squashfs.img")
            except:
                try: self.imgSize = os.path.getsize("/mnt/live/LiveOS/ext3fs.img")
                except:
                    if not os.path.exists(os.path.join("/mnt","iso")):
                        os.makedirs(os.path.join("/mnt","iso"))
                    os.system("mount -o loop "+os.path.join("/dev","sr0")
                                                +" "+os.path.join("/mnt","iso"))
                    self.imgSize = os.path.getsize("/mnt/iso/LiveOS/squashfs.img")
        except:
            print _("None of squashfs.img or ext3fs.img found. Are you using the UFO-LiveCD ??")
            #return
            self.imgSize=0

        self.bus = dbus.SystemBus()
        hal_obj = self.bus.get_object("org.freedesktop.Hal",
                                      "/org/freedesktop/Hal/Manager")
        self.hal = dbus.Interface(hal_obj, "org.freedesktop.Hal.Manager")

        devices = self.hal.FindDeviceByCapability("storage")
        for device in devices:
            dev = self._get_device(device)
            self._add_device(dev)

        if not len(self.drives):
            pass

    def _get_device(self, udi):
        """ Return a dbus Interface to a specific HAL device UDI """
        import dbus
        dev_obj = self.bus.get_object("org.freedesktop.Hal", udi)
        return dbus.Interface(dev_obj, "org.freedesktop.Hal.Device")

    def _add_device(self, dev, parent=None):
        try:
            device = str(dev.GetProperty('block.device'))
            size = str(dev.GetProperty('storage.removable.media_size'))
            info = str(dev.GetProperty('storage.model'))
            if ("/dev/sd" in device) and (int(size)>self.imgSize):
                self.drives.append((device,size,info))
        except : pass
