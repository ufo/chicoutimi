# -*- coding: utf-8 -*-
#
# Copyright © 2009-2010  Agorabox. All rights reserved.
# Copyright © 2008-2009  Red Hat, Inc. All rights reserved.
# Copyright © 2008-2009  Luke Macken <lmacken@redhat.com>
# Copyright © 2008  Kushal Das <kushal@fedoraproject.org>
#
# This copyrighted material is made available to anyone wishing to use, modify,
# copy, or redistribute it subject to the terms and conditions of the GNU
# General Public License v.2.  This program is distributed in the hope that it
# will be useful, but WITHOUT ANY WARRANTY expressed or implied, including the
# implied warranties of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.  You should have
# received a copy of the GNU General Public License along with this program; if
# not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
# Floor, Boston, MA 02110-1301, USA. Any Red Hat or Agorabox trademarks that are
# incorporated in the source code or documentation are not subject to the GNU
# General Public License and may only be used or replicated with the express
# permission of Red Hat, Inc or Agorabox.
#
# Author(s): Luke Macken <lmacken@redhat.com>
#            Kushal Das <kushal@fedoraproject.org>
# Adapted-by: Antonin Fouques <antonin.fouques@agorabox.org>

"""
A graphical interface for the LiveUSBCreator
"""

import os
import time
import statvfs

from PyQt4 import QtCore, QtGui

from liveusb.__init__ import *

try:
    import dbus.mainloop.qt
    dbus.mainloop.qt.DBusQtMainLoop(set_as_default = True)
except:
    pass

class LiveUSBApp(QtGui.QApplication):
    """ Main application class """
    def __init__(self, frontend = None):
        ok = False
        self.frontend = frontend
        essai = 0
        while not ok:
            try:
                QtGui.QApplication.__init__(self, [])
                ok = True
            except:
                essai += 1
                if essai == 100:
                    if self.frontend != None and self.frontend != True: self.frontend.stop()
                    return 1
                pass
        if frontend != None:
            self.mainWindow = LiveUSBBootDialog(self)
        else :
            self.mainWindow = LiveUSBDialog(self)
        self.mainWindow.show()
        try:
            self.exec_()
        finally:
            if self.frontend != None and self.frontend != True: self.frontend.stop()
            return 0

    def show_creator(self):
        self.mainWindow.close()
        self.mainWindow = LiveUSBDialog(self)
        self.mainWindow.show()

    def show_boot(self):
        try: del self.lockWindow
        except:
            self.mainWindow.close()
            self.mainWindow = LiveUSBBootDialog(self)
            self.mainWindow.show()

    def show_lock(self):
        try:
            self.lockWindow.activateWindow()
            self.lockWindow.raise_()
        except:
            self.lockWindow = LiveUSBLockDialog(self)
            self.lockWindow.show()

    def event(self, event):
        if isinstance(event, ProgressEvent):
            try: self.mainWindow.set_progress(event.mode, event.valueInit, event.valueMax, event.timeMax, event.size, event.filename)
            except: pass
        else:
            return False
        return True

class LiveUSBDialog(QtGui.QDialog, LiveUSBInterface):
    """ Our main dialog class """

    def __init__(self,parent):
        self.parent = parent
        self.frontend = self.parent.frontend
        QtGui.QDialog.__init__(self)
        LiveUSBInterface.__init__(self)
        self.setupUi(self)
        self.write(QtGui.QApplication.translate("Dialog", _("Select the creation options and click on the button below"), None, QtGui.QApplication.UnicodeUTF8), clear = True)
        self.startButton.setText(QtGui.QApplication.translate("Dialog", _(u"Launch the creation"), None, QtGui.QApplication.UnicodeUTF8))
        self.working = False
        self.live = LiveUSBCreator()
        self.populate_devices()
        self.liveThread = LiveUSBThread(self)
        self.progressTimer = QtCore.QTimer()
        self.connect_slots()
        self.dev = ""

    def populate_devices(self, *args, **kw):
        if self.working:
            return
        self.listCles.clear()
        try:
            self.live.detect_removable_drives()
            for dev in self.live.drives:
                self.listCles.addItem(dev[0] + u" (" + str(round(int(dev[1])*9.31318e-10,1)) + u" Go) : " + dev[2])
            self.toggle_startButton()
            if self.live.imgSize==0:
                self.write(QtGui.QApplication.translate("Dialog", _("None squashfs or ext3fs found. Are you using the UFO LiveCD ??"), None, QtGui.QApplication.UnicodeUTF8), clear = True)
                self.toggle_startButton(value = False)
        except Exception, e:
            print _("Error : ") + str(e)
            self.toggle_startButton()

    def connect_slots(self):
        self.connect(self.startButton, QtCore.SIGNAL("clicked()"), self.begin)       #Bouton Start
        self.connect(self.pushQuit, QtCore.SIGNAL("clicked()"), self.quit_creator)   #Bouton Quitter
        self.connect(self.liveThread, QtCore.SIGNAL("status(PyQt_PyObject, bool)"), self.write)
        self.connect(self.liveThread, QtCore.SIGNAL("enable_widget(bool)"), self.enable_widgets)
        self.connect(self.refreshListCles, QtCore.SIGNAL("clicked()"),               #Bouton refresh liste clés USB
                         self.populate_devices)
        self.connect(self.progressTimer, QtCore.SIGNAL("timeout()"), self.run_progress)

    def begin(self):
        """ Begin the liveusb creation process.

        This method is called when the startbutton is clicked.
        """
        self.write(QtGui.QApplication.translate("Dialog", _("Creation operation launched"), None, QtGui.QApplication.UnicodeUTF8), clear = True)
        self.dev = str(self.listCles.currentText()).split()[0]
        try : self.liveThread.start()
        except Exception, e:
            print _("Error : ") + str(e)
            self.toggle_startButton()

    def enable_widgets(self, enable):
        self.startButton.setEnabled(enable)
        self.listCles.setEnabled(enable)
        self.refreshListCles.setEnabled(enable)
        self.pushQuit.setEnabled(enable)
        if not enable: self.startButton.setText(QtGui.QApplication.translate("Dialog", _("Creation in progress"), None, QtGui.QApplication.UnicodeUTF8))
        else : self.startButton.setText(QtGui.QApplication.translate("Dialog", _(u"Launch the creation"), None, QtGui.QApplication.UnicodeUTF8))
        self.working = not enable


    def get_dev(self):
        return self.dev

    def progress_global(self, value):
        self.progressBarTotal.setValue(value)
        self.update()

    def progress_current_stage(self, value):
        self.progressBar_current.setValue(value)
        self.update()

    def write(self, text, undo = False, clear = False):
        """ Change text on the  "champ-text"."""
        if clear: self.champText.clear()
        if undo : self.champText.undo()
        self.champText.append(text)
        self.update()

    def toggle_startButton(self, value = None):
        """ Change the state of the startButton"""
        if (self.listCles.count() == 0 or self.working or value == False):
            self.startButton.setEnabled(False)
        else: self.startButton.setEnabled(True)

    def quit_live(self):
        QtCore.QThread.terminate(self.liveThread)

    def quit_creator(self):
        """
        Send the quit signal when pushing the "Quitter" Button
        """
        if self.frontend != None:
            self.parent.show_boot()
        else: quit()

    def set_progress(self, mode = 0, valueInit = 0, valueMax = 100, timeMax = 0, size = 0, filename = ""):
        self.progressMode = mode
        self.progressValueInit = valueInit
        self.progressValueMax = valueMax
        self.progressTimeMax = timeMax
        self.progressSize = size
        self.progressFile = filename
        if   self.progressMode == 0:         #Global progress (one time)
            self.progress_global(int(self.progressValueInit))
        elif self.progressMode == 1:         #Stage progress  (one time)
            self.progressTimer.stop()
            self.progress_current_stage(int(self.progressValueInit))
        elif self.progressMode == 2:         #Copie Pogress (timer)
            self.progressTimer.stop()
            self.progressSize *= 1048576
            self.progressSizeCopie = 1
            self.progressValue = self.progressValueInit
            self.progressMountpoint = self.progressFile
            self.progressTimeA = time.time()
            self.progressLastSize = 0
            self.progressSizeInit = os.statvfs(self.progressMountpoint)[statvfs.F_BFREE] * os.statvfs(self.progressMountpoint)[statvfs.F_BSIZE] + os.statvfs(self.progressMountpoint + "/media/UFO/")[statvfs.F_BFREE] * os.statvfs(self.progressMountpoint + "/media/UFO/")[statvfs.F_BSIZE]
            self.write(QtGui.QApplication.translate("Dialog", _("Copied : 0 Mo / "), None, QtGui.QApplication.UnicodeUTF8) + str(int(self.progressSize*9.5367e-7)) + u" Mo")
            if self.working: self.progress_current_stage(int(self.progressValueInit))
            self.progressTimer.start(1000)
        elif self.progressMode == 3:         #Timout Pogress (timer)
            self.progressTimer.stop()
            self.write(QtGui.QApplication.translate("Dialog", _("Estimated time"), None, QtGui.QApplication.UnicodeUTF8) + str(self.progressTimeMax) + u"s")
            if self.working: self.progress_current_stage(int(self.progressValueInit))
            self.progressTimeToNext = 0
            self.progressValue = self.progressValueInit
            self.progressTimer.start(1000)

    def run_progress(self):
        if self.working:
            if self.progressMode == 2:      #Copie progress (timer)
                if self.progressSizeCopie > self.progressSize: self.progressTimer.stop()
                timeB = time.time()
                sizeDiff = self.progressSizeCopie - self.progressLastSize
                timeDiff = timeB - self.progressTimeA
                if timeDiff == 0: timeDiff = 1
                speed = str(int((sizeDiff / timeDiff) * 9.7656e-4))
                if self.progressSize == 0 : self.progressSize = 1
                if self.progressSizeCopie == 0 : self.progressSizeCopie = 1
                self.progressValue = (float(self.progressValueMax - self.progressValueInit) / (float(self.progressSize) / float(self.progressSizeCopie))) + self.progressValueInit
                if self.progressValue < 100: self.progress_current_stage(int(self.progressValue))
                self.write(QtGui.QApplication.translate("Dialog", _("Copied : "), None, QtGui.QApplication.UnicodeUTF8) + str(int(self.progressSizeCopie * 9.5367e-7)) + u" Mo / " + str(int(self.progressSize * 9.5367e-7)) + _(" Mo. speed : ") + speed + " kbps.", undo = True)
                self.update()
                self.progressTimeA = timeB
                self.progressLastSize = self.progressSizeCopie
                self.progressSizeCopie = self.progressSizeInit - (os.statvfs(self.progressMountpoint)[statvfs.F_BFREE] * os.statvfs(self.progressMountpoint)[statvfs.F_BSIZE]) - os.statvfs(self.progressMountpoint + "/media/UFO/")[statvfs.F_BFREE] * os.statvfs(self.progressMountpoint + "/media/UFO/")[statvfs.F_BSIZE]
            elif self.progressMode == 3:  #Timout Pogress (timer)
                if (self.progressTimeToNext > self.progressTimeMax): self.progressTimer.stop()
                self.progressValue = ((float(self.progressValueMax - self.progressValueInit) / float(self.progressTimeMax)) * self.progressTimeToNext) + self.progressValueInit
                self.progressTimeToNext += 1
                self.progress_current_stage(int(self.progressValue))
                self.update()

class LiveUSBLockDialog(QtGui.QDialog, LiveUSBLockInterface):
    """ The dialog to enter the "ids" """

    def __init__(self,parent):
        self.parent = parent
        self.frontend = self.parent.frontend
        QtGui.QDialog.__init__(self)
        LiveUSBLockInterface.__init__(self)
        self.setupUi(self)
        self.lineId.setEnabled(True)
        self.lineId.setFocus()
        self.linePass.setEnabled(True)
        self.boutons.setEnabled(True)

    def accept(self):
        print _("button ok pushed")
        try:
            from liveusb.load_user import LoadUser
            self.load = LoadUser()
            self.lineId.setEnabled (False)
            self.linePass.setEnabled (False)
            self.boutons.setEnabled (False)
            os.system(_("zenity --info --text='We are downloading your informations online. This operation may take from some seconds to some minutes' &"))
            if self.load.apply(self.lineId.text(), self.linePass.text()) == 0:
                os.system("killall slim")
                sys.exit()
            else:
                self.lineId.setEnabled (True)
                self.lineId.setFocus()
                self.linePass.setEnabled (True)
                self.boutons.setEnabled (True)
        except:
            self.lineId.setEnabled (True)
            self.lineId.setFocus()
            self.linePass.setEnabled (True)
            self.boutons.setEnabled (True)
            os.system(_("zenity --error --text='Error during the download of your informations.'"))

    def reject(self):
        """
        reLance la fenêtre de boot
        """
        self.parent.show_boot()

class LiveUSBBootDialog(QtGui.QDialog, LiveUSBBootInterface):
    """ Our first dialog class when the system boot """

    def __init__(self, parent):
        self.parent = parent
        self.frontend = self.parent.frontend
        QtGui.QDialog.__init__(self)
        LiveUSBBootInterface.__init__(self)
        self.setupUi(self)
        self.live = LiveUSBCreator()
        self.toggle_creation()
        self.connect_slots()

    def toggle_creation(self):
        try:
            self.live.detect_removable_drives()
            if len(self.live.drives) < 1: self.launchCreation.setEnabled(False)
            else: self.labelPlug.hide()
        except Exception, e: print _("Error : ") + str(e)

    def connect_slots(self):
        self.connect(self.launchTest, QtCore.SIGNAL("clicked()"), self.launch_test)           #Bouton Lancer UFO
        self.connect(self.launchCreation, QtCore.SIGNAL("clicked()"), self.launch_creation)   #Bouton Lancer Création
        self.connect(self.launchLock, QtCore.SIGNAL("clicked()"), self.launch_lock)           #Bouton Lancer Lock
        self.connect(self.pushQuit, QtCore.SIGNAL("clicked()"), self.quit_boot)               #Bouton Quitter

    def launch_test(self):
        """
        Quit the interface and run the UFO Desktop
        """
        if self.frontend != None and self.frontend != True: self.frontend.stop()
        quit()

    def launch_creation(self):
        """
        Lance la fenêtre de création de clé USB
        """
        self.parent.show_creator()

    def launch_lock(self):
        """
        Lance la fenêtre de création de clé USB
        """
        self.parent.show_lock()

    def quit_boot(self):
        """
        Send the quit signal when pushing the "Quitter" Button
        """
        os.system("shutdown -h now")
        if self.frontend != True: self.frontend.stop()

class ProgressEvent(QtCore.QEvent):
    """ A event that monitors the progress of Live USB creation.
    """

    def __init__(self, mode = 0, valueInit = 0, valueMax = 100, timeMax = 0, size = 0, filename = ""):
        super(ProgressEvent, self).__init__(QtCore.QEvent.None)
        self.mode = mode
        self.valueInit = valueInit
        self.valueMax = valueMax
        self.timeMax = timeMax
        self.size = size
        self.filename = filename

class LiveUSBThread(QtCore.QThread):

    def __init__(self, parent):
        QtCore.QThread.__init__(self, parent)
        self.parent = parent

    def run(self):
        import creation.launch as creation
        dev = self.parent.get_dev()
        self.send_begin()

        creation.main(self, dev)

        self.send_progress(100)
        self.send_progress(100, mode = "stage")
        self.send_finished()

    def write(self, text, undo=False):
        self.emit(QtCore.SIGNAL("status(PyQt_PyObject, bool)"), text, undo)

    def send_begin(self):
        self.emit(QtCore.SIGNAL("enable_widget(bool)"),False)

    def send_finished(self):
        self.parent.working = False
        self.emit(QtCore.SIGNAL("enable_widget(bool)"), True)
        self.parent.update()

    def send_progress(self, valueInit = 0, valueMax = 100, timeMax = 0, mode = "normal", file = "", size = 0):
        if mode == "normal":
            self.parent.parent.postEvent(self.parent.parent, ProgressEvent(0, valueInit))
        elif  mode == "stage":
            self.parent.parent.postEvent(self.parent.parent, ProgressEvent(1, valueInit))
        elif mode == "copie" and file != "aucun" and size != 0:
            self.parent.parent.postEvent(self.parent.parent, ProgressEvent(2, valueInit, valueMax, timeMax, int(size * 9.5367e-7), filename = file))
        elif mode == "timeout" and timeMax != 0:
            self.parent.parent.postEvent(self.parent.parent, ProgressEvent(3, timeMax = timeMax))

    def terminate(self):
        self.send_progress(100)
        self.send_finished()
        self.parent.quit_live()
