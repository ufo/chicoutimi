# -*- coding: utf-8 -*-
#
# Copyright © 2009-2010  Agorabox. All rights reserved.
# Copyright © 2008  Red Hat, Inc. All rights reserved.
#
# This copyrighted material is made available to anyone wishing to use, modify,
# copy, or redistribute it subject to the terms and conditions of the GNU
# General Public License v.2.  This program is distributed in the hope that it
# will be useful, but WITHOUT ANY WARRANTY expressed or implied, including the
# implied warranties of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.  You should have
# received a copy of the GNU General Public License along with this program; if
# not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
# Floor, Boston, MA 02110-1301, USA. Any Red Hat or Agorabox trademarks that are
# incorporated in the source code or documentation are not subject to the GNU
# General Public License and may only be used or replicated with the express
# permission of Red Hat, Inc or Agorabox.
#
# Author(s): Luke Macken <lmacken@redhat.com>
# Adapted-by: Antonin Fouques <antonin.fouques@agorabox.org>

import os
import sys

if os.getuid() != 0:
    print _("You must run this application as root")
    sys.exit(1)
from liveusb.creator import LiveUSBCreator as LiveUSBCreator
from liveusb.dialog import Ui_Dialog as LiveUSBInterface
from liveusb.dialog_boot import Ui_Dialogboot as LiveUSBBootInterface
from liveusb.dialog_lock import Ui_DialogLock as LiveUSBLockInterface

__all__ = ("LiveUSBCreator", "LiveUSBInterface", "LiveUSBBootInterface", "LiveUSBLockInterface")
