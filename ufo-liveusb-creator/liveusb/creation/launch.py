#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Copyright © 2009-2010  Agorabox. All rights reserved.
#
# This copyrighted material is made available to anyone wishing to use, modify,
# copy, or redistribute it subject to the terms and conditions of the GNU
# General Public License v.2.  This program is distributed in the hope that it
# will be useful, but WITHOUT ANY WARRANTY expressed or implied, including the
# implied warranties of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.  You should have
# received a copy of the GNU General Public License along with this program; if
# not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
# Floor, Boston, MA 02110-1301, USA. Any Agorabox trademarks that are
# incorporated in the source code or documentation are not subject to the GNU
# General Public License and may only be used or replicated with the express
# permission of Agorabox.
#
# Author(s): Antonin Fouques <antonin.fouques@agorabox.org>

import os
import sys
from optparse import OptionParser
from libufogen.genutils import run_command
import logging
from libufogen.generator import *
from libufogen.serie import *

@stage
def rmlivecdconf(mountpoint):
    """
    Optimize the UFOkey for a normal use
    """
    run_command("yum -y remove ufo-liveusb-creator libufogen", chroot=mountpoint)

class LiveSerieGenerator(SerieGenerator):
    stages+=[rmlivecdconf]

    def __init__(self, options, args, parser, parent):
        self.parent = parent
        SerieGenerator.__init__(self, options, args, parser)

    def run_stage(self, stage, quiet=False, cleaning=False):
        try:
            doc = stage.__doc__
            if doc:
                msg = "(Etape %d/%d) " % (self.current_stage, len(self.stages))
                msg += doc.strip()
                self.parent.write(msg)
            self.parent.send_progress(self.current_stage*100/(len(self.stages)-1))
            if stage.func_name=="repart":
                self.parent.send_progress(timeMax=60,mode="timeout")
            if stage.func_name=="mountroot":
                self.parent.send_progress(valueInit=0,mode="stage")
            if stage.func_name=="copyfilesystem":
                listefs=run_command("df",output=True)[1].split("\n")
                for fs in listefs:
                    if "/mnt/ext3fs/root" in fs: details = fs.split(" ")
                b=[]
                for a in details:
                    if a!='' and a!='\t': b+=[a]
                self.parent.send_progress(valueInit=0, valueMax=100, timeMax=0, mode="copie", file=self.mountpoint, size=int(b[2])*1024)
            if stage.func_name=="installgrub":
                self.parent.send_progress(timeMax=30,mode="timeout")
            if stage.func_name=="agoraboxconsts":
                self.parent.send_progress(valueInit=0,mode="stage")
        except: pass
        SerieGenerator.run_stage(self, stage, quiet, cleaning)


def main(parenttmp, dev):


    if os.getuid() != 0:
        logging.critical(_("You must be root to run this script"))
        return(-1)

    # On cherche l'ext3fs.img
    # /mnt/live/LiveOS/squashfs.img -> /mnt/squashfs
    if os.path.isfile(os.path.join("/mnt","live","LiveOS","squashfs.img")):
        try: os.makedirs(os.path.join("/mnt","squashfs"))
        except: pass
        run_command("mount -o loop "+os.path.join("/mnt","live","LiveOS","squashfs.img")
                                +" "+os.path.join("/mnt","squashfs"))
    # /mnt/live/LiveOS/ext3fs.img
    elif os.path.isfile(os.path.join("/mnt","live","LiveOS","ext3fs.img")):
        ext3fsimg = os.path.join("/mnt","live","LiveOS","ext3fs.img")
    # /dev/sr0 -> /mnt/iso
    # /mnt/iso/LiveOS/squashfs.img -> /mnt/squashfs.img
    elif os.path.exists(os.path.join("/dev","sr0")):
        try: os.makedirs(os.path.join("/mnt","iso"))
        except: pass
        run_command("mount -o loop "+os.path.join("/dev","sr0")
                                +" "+os.path.join("/mnt","iso"))
        if os.path.isfile(os.path.join("/mnt","iso","LiveOS","squashfs.img")):
            try: os.makedirs(os.path.join("/mnt","squashfs"))
            except: pass
            run_command("mount -o loop "+os.path.join("/mnt","iso","LiveOS","squashfs.img")
                                    +" "+os.path.join("/mnt","squashfs"))
    else: return(_("None of squashfs.img or ext3fs.img found. Are you using the UFO-LiveCD ??"))

    # /mnt/squashfs/LiveOS/ext3fs.img
    if os.path.isfile(os.path.join("/mnt","squashfs","LiveOS","ext3fs.img")):
        try: os.makedirs(os.path.join("/mnt","ext3fs","root"))
        except: pass
        ext3fsimg = os.path.join("/mnt","squashfs","LiveOS","ext3fs.img")

    # ext3fs.img -> /mnt/ext3fs/root
    run_command("mount -o loop "+ext3fsimg+" "+os.path.join("/mnt","ext3fs","root"))
    proto = os.path.join("/mnt","ext3fs")

    if not os.path.isfile(os.path.join("/etc","sysconfig","ufo","flavour")):
        return(_("I cannot smell the flavour of this UFO distribution"))
    flavour = open(os.path.join("/etc","sysconfig","ufo","flavour"),"r")
    target = flavour.read().split("\n")[0]
    flavour.close()

    usage = """%prog -s <device>"""

    description = _("Generates Agorabox USB key (serie) from UFO LiveCD")
    version="%prog 0.1"

    parser = OptionParser(usage=usage, description=description, version=version)
    parser.add_option("-z", "--compress", dest="compress", default="",
                      help="use root filesystem compression", metavar="PATH")
    parser.add_option("-g", "--start", dest="starting_stage",
                      help="starts the script at the specified stage", metavar="STAGE")
    parser.add_option("-e", "--end", dest="ending_stage",
                      help="ends the script after the specified stage", metavar="STAGE")
    parser.add_option("-y", "--cleanyumcache", dest="cleanyumcache", default=True,
                      action="store_true", help="cleans yum's cache")
    parser.add_option("-m", "--mountpoint", dest="mountpoint", default="",
                      help="use mountpoint", metavar="DIR")
    parser.add_option("-f", "--fedora", dest="fedora", default=12,
                      help="Fedora version", metavar="VERSION")
    parser.add_option("", "--filelist", dest="filelist",
                      help="Generates a file list for a prototype, check against it for a serie", metavar="FILE")
    parser.add_option("-o", "--only", dest="only", default="",
                      help="only execute the specified stages", metavar="FILE")
    parser.add_option("-i", "--skip", dest="skip",
                      help="skips stages (separated by colons)", metavar="STAGE")
    parser.add_option("-v", "--verbose", dest="loglevel", default="info",
                      help="set verbosity (can be notset, debug, info, warning, error, fatal, or critical)")
    parser.add_option("-u", "--noumount", dest="noumount", default=False,
                      action="store_true", help="do not umount")
    parser.add_option("-l", "--log", dest="logfile",
                      help="write logs to FILE", metavar="FILE")
    parser.add_option("--redirect", dest="redirect", default="",
                      help="Redirect host")
    parser.add_option("--no-signals", dest="nosignals", default=False,
                      action="store_true", help="Do not use signals")
    parser.add_option("--step", dest="step", default=False,
                      action="store_true", help="Pause at each step")

    Generator.register(LiveSerieGenerator, parser)

    args = ['-s',proto,target,dev,'--skip=mountfat,volumeimage,hidefiles','--verbose=debug','--log=/var/log/ufo-liveusb-creator.log','--no-signals']
    (options, args) = parser.parse_args(args)

    format = "%(asctime)s %(levelname)s %(message)s"
    levels = [logging.getLevelName(x) for x in (0, 10, 20, 30, 40, 50)]
    loglevel = options.loglevel.upper()
    try:
        level = levels.index(loglevel) * 10
    except ValueError:
        parser.error(_("Invalid log level ") + loglevel)

    if options.logfile:
        logging.basicConfig(format=format, level=level, filename=options.logfile)
    else:
        logging.basicConfig(format=format, level=level)

    for genclass in Generator.generators:
        if getattr(options, genclass.long_option[2:]):
            generator = genclass(options, args, parser, parenttmp)
            return(generator.run())


#print main(None, "/dev/sdX")
