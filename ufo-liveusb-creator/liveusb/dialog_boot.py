# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'data/ufo-liveusb-boot.ui'
#
# Created: Wed Feb 24 18:56:16 2010
#      by: PyQt4 UI code generator 4.6.2
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

class Ui_Dialogboot(object):
    def setupUi(self, Dialogboot):
        Dialogboot.setObjectName("Dialogboot")
        Dialogboot.resize(580, 280)
        Dialogboot.setMinimumSize(QtCore.QSize(580, 280))
        Dialogboot.setMaximumSize(QtCore.QSize(580, 280))
        font = QtGui.QFont()
        font.setPointSize(8)
        Dialogboot.setFont(font)
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(":/ufo.ico"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        Dialogboot.setWindowIcon(icon)
        self.Header = QtGui.QLabel(Dialogboot)
        self.Header.setGeometry(QtCore.QRect(0, 0, 580, 116))
        self.Header.setPixmap(QtGui.QPixmap(":/liveusb-header-boot.png"))
        self.Header.setObjectName("Header")
        self.pushQuit = QtGui.QPushButton(Dialogboot)
        self.pushQuit.setGeometry(QtCore.QRect(250, 250, 85, 27))
        icon1 = QtGui.QIcon()
        icon1.addPixmap(QtGui.QPixmap(":/icone-infos.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.pushQuit.setIcon(icon1)
        self.pushQuit.setFlat(True)
        self.pushQuit.setObjectName("pushQuit")
        self.launchTest = QtGui.QPushButton(Dialogboot)
        self.launchTest.setGeometry(QtCore.QRect(180, 120, 160, 100))
        icon2 = QtGui.QIcon()
        icon2.addPixmap(QtGui.QPixmap(":/launch-test.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.launchTest.setIcon(icon2)
        self.launchTest.setIconSize(QtCore.QSize(150, 100))
        self.launchTest.setFlat(True)
        self.launchTest.setObjectName("launchTest")
        self.launchCreation = QtGui.QPushButton(Dialogboot)
        self.launchCreation.setGeometry(QtCore.QRect(350, 120, 220, 100))
        icon3 = QtGui.QIcon()
        icon3.addPixmap(QtGui.QPixmap(":/launch-creation.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.launchCreation.setIcon(icon3)
        self.launchCreation.setIconSize(QtCore.QSize(150, 100))
        self.launchCreation.setFlat(True)
        self.launchCreation.setObjectName("launchCreation")
        self.LabelTest = QtGui.QLabel(Dialogboot)
        self.LabelTest.setGeometry(QtCore.QRect(180, 220, 160, 31))
        self.LabelTest.setAlignment(QtCore.Qt.AlignCenter)
        self.LabelTest.setObjectName("LabelTest")
        self.LabeCreation = QtGui.QLabel(Dialogboot)
        self.LabeCreation.setGeometry(QtCore.QRect(350, 220, 220, 31))
        self.LabeCreation.setAlignment(QtCore.Qt.AlignCenter)
        self.LabeCreation.setObjectName("LabeCreation")
        self.labelPlug = QtGui.QLabel(Dialogboot)
        self.labelPlug.setGeometry(QtCore.QRect(350, 150, 220, 40))
        font = QtGui.QFont()
        font.setWeight(75)
        font.setBold(True)
        self.labelPlug.setFont(font)
        self.labelPlug.setAlignment(QtCore.Qt.AlignCenter)
        self.labelPlug.setObjectName("labelPlug")
        self.launchLock = QtGui.QPushButton(Dialogboot)
        self.launchLock.setGeometry(QtCore.QRect(10, 120, 160, 100))
        icon4 = QtGui.QIcon()
        icon4.addPixmap(QtGui.QPixmap(":/launch-lock.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.launchLock.setIcon(icon4)
        self.launchLock.setIconSize(QtCore.QSize(150, 100))
        self.launchLock.setFlat(True)
        self.launchLock.setObjectName("launchLock")
        self.LabelLock = QtGui.QLabel(Dialogboot)
        self.LabelLock.setGeometry(QtCore.QRect(10, 220, 160, 31))
        self.LabelLock.setAlignment(QtCore.Qt.AlignCenter)
        self.LabelLock.setObjectName("LabelLock")

        self.retranslateUi(Dialogboot)
        QtCore.QMetaObject.connectSlotsByName(Dialogboot)

    def retranslateUi(self, Dialogboot):
        Dialogboot.setWindowTitle(QtGui.QApplication.translate("Dialogboot", _("Launcher Live CD U.F.O."), None, QtGui.QApplication.UnicodeUTF8))
        self.pushQuit.setText(QtGui.QApplication.translate("Dialogboot", _("Quit"), None, QtGui.QApplication.UnicodeUTF8))
        self.launchTest.setToolTip(QtGui.QApplication.translate("Dialogboot", _("Launch the Live version of the LiveCD\n"
"to see the system capacities.\n"
"This mode does not require identification.\n"
"Your work and modification will not be save."), None, QtGui.QApplication.UnicodeUTF8))
        self.launchCreation.setToolTip(QtGui.QApplication.translate("Dialogboot", _("Launch the creation of a UFO usb key.\n"
"You need a usb key with a 8Go capacity (preferred)\n"
"plugged before launching this application."), None, QtGui.QApplication.UnicodeUTF8))
        self.LabelTest.setText(QtGui.QApplication.translate("Dialogboot", _("Test the U.F.O. system"), None, QtGui.QApplication.UnicodeUTF8))
        self.LabeCreation.setText(QtGui.QApplication.translate("Dialogboot", _("Create a new U.F.O. USB key"), None, QtGui.QApplication.UnicodeUTF8))
        self.labelPlug.setText(QtGui.QApplication.translate("Dialogboot", _("Plug a usb key\n"
"and reboot the Live CD"), None, QtGui.QApplication.UnicodeUTF8))
        self.launchLock.setToolTip(QtGui.QApplication.translate("Dialogboot", _("Launch the LiveCD U.F.O.\n"
"with your own identifications U.F.O.\n"
"to get your desktop and configurations.\n"
"You need to be connected to the Internet."), None, QtGui.QApplication.UnicodeUTF8))
        self.LabelLock.setText(QtGui.QApplication.translate("Dialogboot", _("Launch my U.F.O. system"), None, QtGui.QApplication.UnicodeUTF8))

import resources_rc
