# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'data/ufo-liveusb-lock.ui'
#
# Created: Tue Mar  9 17:27:00 2010
#      by: PyQt4 UI code generator 4.6.2
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

class Ui_DialogLock(object):
    def setupUi(self, DialogLock):
        DialogLock.setObjectName("DialogLock")
        DialogLock.resize(320, 150)
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(":/ufo.ico"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        DialogLock.setWindowIcon(icon)
        self.boutons = QtGui.QDialogButtonBox(DialogLock)
        self.boutons.setGeometry(QtCore.QRect(10, 110, 300, 30))
        self.boutons.setStandardButtons(QtGui.QDialogButtonBox.Cancel|QtGui.QDialogButtonBox.Ok)
        self.boutons.setCenterButtons(True)
        self.boutons.setObjectName("boutons")
        self.lineId = QtGui.QLineEdit(DialogLock)
        self.lineId.setEnabled(True)
        self.lineId.setGeometry(QtCore.QRect(120, 20, 180, 30))
        self.lineId.setAlignment(QtCore.Qt.AlignCenter)
        self.lineId.setObjectName("lineId")
        self.linePass = QtGui.QLineEdit(DialogLock)
        self.linePass.setGeometry(QtCore.QRect(120, 60, 180, 30))
        self.linePass.setEchoMode(QtGui.QLineEdit.Password)
        self.linePass.setAlignment(QtCore.Qt.AlignCenter)
        self.linePass.setObjectName("linePass")
        self.labelId = QtGui.QLabel(DialogLock)
        self.labelId.setGeometry(QtCore.QRect(20, 30, 100, 20))
        self.labelId.setObjectName("labelId")
        self.labelPass = QtGui.QLabel(DialogLock)
        self.labelPass.setGeometry(QtCore.QRect(20, 70, 100, 20))
        self.labelPass.setObjectName("labelPass")

        self.retranslateUi(DialogLock)
        QtCore.QObject.connect(self.boutons, QtCore.SIGNAL("accepted()"), DialogLock.accept)
        QtCore.QObject.connect(self.boutons, QtCore.SIGNAL("rejected()"), DialogLock.reject)
        QtCore.QMetaObject.connectSlotsByName(DialogLock)

    def retranslateUi(self, DialogLock):
        DialogLock.setWindowTitle(QtGui.QApplication.translate("DialogLock", _("Login"), None, QtGui.QApplication.UnicodeUTF8))
        self.labelId.setText(QtGui.QApplication.translate("DialogLock", _("Login :"), None, QtGui.QApplication.UnicodeUTF8))
        self.labelPass.setText(QtGui.QApplication.translate("DialogLock", _("Password :"), None, QtGui.QApplication.UnicodeUTF8))

import resources_rc
