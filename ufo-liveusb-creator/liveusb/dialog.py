# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'data/ufo-liveusb-creator.ui'
#
# Created: Tue Mar  9 17:27:00 2010
#      by: PyQt4 UI code generator 4.6.2
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

class Ui_Dialog(object):
    def setupUi(self, Dialog):
        Dialog.setObjectName("Dialog")
        Dialog.resize(430, 431)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(Dialog.sizePolicy().hasHeightForWidth())
        Dialog.setSizePolicy(sizePolicy)
        Dialog.setMinimumSize(QtCore.QSize(430, 431))
        Dialog.setMaximumSize(QtCore.QSize(430, 431))
        font = QtGui.QFont()
        font.setFamily("Sans Serif")
        font.setPointSize(8)
        Dialog.setFont(font)
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(":/ufo.ico"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        Dialog.setWindowIcon(icon)
        self.startButton = QtGui.QPushButton(Dialog)
        self.startButton.setGeometry(QtCore.QRect(130, 390, 170, 34))
        font = QtGui.QFont()
        font.setWeight(75)
        font.setBold(True)
        self.startButton.setFont(font)
        self.startButton.setObjectName("startButton")
        self.champText = QtGui.QTextEdit(Dialog)
        self.champText.setGeometry(QtCore.QRect(10, 190, 410, 91))
        self.champText.setReadOnly(True)
        self.champText.setObjectName("champText")
        self.progressBarTotal = QtGui.QProgressBar(Dialog)
        self.progressBarTotal.setGeometry(QtCore.QRect(10, 360, 410, 23))
        self.progressBarTotal.setObjectName("progressBarTotal")
        self.Header = QtGui.QLabel(Dialog)
        self.Header.setGeometry(QtCore.QRect(0, 0, 430, 72))
        self.Header.setPixmap(QtGui.QPixmap(":/liveusb-header.png"))
        self.Header.setTextInteractionFlags(QtCore.Qt.NoTextInteraction)
        self.Header.setObjectName("Header")
        self.refreshListCles = QtGui.QPushButton(Dialog)
        self.refreshListCles.setGeometry(QtCore.QRect(170, 150, 90, 31))
        icon1 = QtGui.QIcon()
        icon1.addPixmap(QtGui.QPixmap(":/refresh.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.refreshListCles.setIcon(icon1)
        self.refreshListCles.setFlat(True)
        self.refreshListCles.setObjectName("refreshListCles")
        self.chemin_usb = QtGui.QLabel(Dialog)
        self.chemin_usb.setGeometry(QtCore.QRect(20, 90, 161, 16))
        font = QtGui.QFont()
        font.setWeight(75)
        font.setBold(True)
        self.chemin_usb.setFont(font)
        self.chemin_usb.setTextInteractionFlags(QtCore.Qt.NoTextInteraction)
        self.chemin_usb.setObjectName("chemin_usb")
        self.listCles = QtGui.QComboBox(Dialog)
        self.listCles.setGeometry(QtCore.QRect(70, 120, 290, 21))
        self.listCles.setObjectName("listCles")
        self.pushQuit = QtGui.QPushButton(Dialog)
        self.pushQuit.setGeometry(QtCore.QRect(330, 400, 85, 31))
        icon2 = QtGui.QIcon()
        icon2.addPixmap(QtGui.QPixmap(":/icone-infos.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.pushQuit.setIcon(icon2)
        self.pushQuit.setFlat(True)
        self.pushQuit.setObjectName("pushQuit")
        self.current_progress_label = QtGui.QLabel(Dialog)
        self.current_progress_label.setGeometry(QtCore.QRect(10, 290, 410, 23))
        self.current_progress_label.setObjectName("current_progress_label")
        self.total_progress_label = QtGui.QLabel(Dialog)
        self.total_progress_label.setGeometry(QtCore.QRect(10, 340, 410, 23))
        self.total_progress_label.setObjectName("total_progress_label")
        self.progressBar_current = QtGui.QProgressBar(Dialog)
        self.progressBar_current.setGeometry(QtCore.QRect(10, 310, 410, 23))
        self.progressBar_current.setObjectName("progressBar_current")

        self.retranslateUi(Dialog)
        QtCore.QMetaObject.connectSlotsByName(Dialog)

    def retranslateUi(self, Dialog):
        Dialog.setWindowTitle(QtGui.QApplication.translate("Dialog", "UFO LiveUSB Creator", None, QtGui.QApplication.UnicodeUTF8))
        self.startButton.setText(QtGui.QApplication.translate("Dialog", _("Launch the operation"), None, QtGui.QApplication.UnicodeUTF8))
        self.champText.setHtml(QtGui.QApplication.translate("Dialog", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:\'Sans Serif\'; font-size:8pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:\'Sans\';\">Sélectionnez les options de réparation et cliquez sur le bouton ci dessous.</span></p></body></html>", None, QtGui.QApplication.UnicodeUTF8))
        self.refreshListCles.setText(QtGui.QApplication.translate("Dialog", _("Refresh"), None, QtGui.QApplication.UnicodeUTF8))
        self.chemin_usb.setText(QtGui.QApplication.translate("Dialog", _("USB key path"), None, QtGui.QApplication.UnicodeUTF8))
        self.pushQuit.setText(QtGui.QApplication.translate("Dialog", _("Quit"), None, QtGui.QApplication.UnicodeUTF8))
        self.current_progress_label.setText(QtGui.QApplication.translate("Dialog", _("Progress of the current stage :"), None, QtGui.QApplication.UnicodeUTF8))
        self.total_progress_label.setText(QtGui.QApplication.translate("Dialog", _("General progress :"), None, QtGui.QApplication.UnicodeUTF8))

import resources_rc
