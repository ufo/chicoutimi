from distutils.core import setup
import sys, os

LOCALE_DIR= '/usr/share/locale'

locales = []

setup(
    name = 'ufo-liveusb-creator',
    version = '1.0.0',
    packages = ['liveusb','liveusb/creation'],
    scripts = ['ufo-liveusb-creator'],
    license = 'GNU General Public License (GPL)',
    url = 'http://ufo.agorabox.fr/',
    description = 'A liveusb creator for UFO',
    long_description = 'A liveusb creator for UFO',
    platforms = ['Linux'],
    maintainer = 'Antonin Fouques',
    maintainer_email = 'antonin.fouques@agorabox.org',
    data_files = [("/usr/share/applications",["data/ufo-liveusb-creator.desktop"]), 
                  ('/usr/share/pixmaps',["data/ufousb.png"]),
                  ] + [(os.path.join(LOCALE_DIR, locale))
                        for locale in locales]
    )

