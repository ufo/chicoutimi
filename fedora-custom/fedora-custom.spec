Name:           fedora-custom
Version:        1.0
BuildArch:      noarch
Release:        1%{?dist}
Summary:        Apply essential settings to Fedora

Group:          System Environment/Base
License:        GPL
URL:            http://www.chicoutimi.org
Source0:        http://www.chicoutimi.org/fedora-custom-1.0.tar.gz
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

Requires:       NetworkManager nautilus GConf2 chkconfig
# Requires:       xmms xmms-mp3 xmms-faad2
Requires:       gstreamer-plugins-ugly gstreamer-plugins-bad libmad libid3tag vlc mplayerplug-in mplayer-codecs
Requires:       unrar
Requires:       pdfedit
Requires:       install-restricted picasafs yum-pool vmware-fixes gnome-converter
Requires:       glipper numlockx
Requires:       adobe-release-i386

%description
Apply essential settings to Fedora

%prep
%setup


%build


%install
rm -rf $RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT/usr/bin
cp setup-online-folders $RPM_BUILD_ROOT/usr/bin
mkdir -p $RPM_BUILD_ROOT/usr/share/applications
cp ftp-client.desktop $RPM_BUILD_ROOT/usr/share/applications
mkdir -p $RPM_BUILD_ROOT/usr/share/pixmaps
cp connect-ftp.png $RPM_BUILD_ROOT/usr/share/pixmaps
mkdir -p $RPM_BUILD_ROOT/usr/share/gnome/autostart
cp online-folders.desktop $RPM_BUILD_ROOT/usr/share/gnome/autostart


%clean
rm -rf $RPM_BUILD_ROOT


%post
%{_sbindir}/chkconfig NetworkManager on
%{_sbindir}/chkconfig network off
%{_bindir}/gconftool-2 --direct --config-source xml:readwrite:/etc/gconf/gconf.xml.defaults --type bool --set /apps/nautilus/preferences/always_use_browser true


%files
%defattr(-,root,root,-)
%doc
%{_bindir}/setup-online-folders
%{_datadir}/applications/ftp-client.desktop
%{_datadir}/pixmaps/connect-ftp.png
%{_datadir}/gnome/autostart/online-folders.desktop

%changelog
* Tue Jul 24 2007 Bob <bob@glumol.com>
  Initial release
