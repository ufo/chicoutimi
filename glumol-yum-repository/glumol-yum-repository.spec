Name:           glumol-yum-repository
Version:        1.0
Release:        1%{?dist}
Summary:        The Glumol yum repository

Group:          System Environment/Base
License:        GPL
URL:            http://www.glumol.com/~bob/yum
Source0:        http://www.glumol.com/~bob/yum/glumol-yum-repository-1.0.tar.gz
BuildArch:      noarch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

Requires:       yum yum-priorities

%description
The Glumol yum repository

%prep
%setup -q


%build


%install
rm -rf $RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT/etc/yum.repos.d
cp glumol.repo $RPM_BUILD_ROOT/etc/yum.repos.d/

%clean
rm -rf $RPM_BUILD_ROOT


%files
%defattr(-,root,root,-)
%doc
/etc/yum.repos.d/glumol.repo


%changelog
