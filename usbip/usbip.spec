Name:           usbip
Version:        0.1.7
Release:        1%{?dist}
Summary:        USB/IP Project aims to develop a general USB device sharing system over IP network.

Group:          Applications/System
License:        GPLv2
URL:            http://usbip.sourceforge.net/
Source0:        http://ignum.dl.sourceforge.net/project/usbip/usbip/0.1.7/usbip-0.1.7.tar.gz
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  libsysfs-devel
Requires:       libsysfs 

%description
The USB/IP Project aims to develop a general USB device sharing system
over IP network. To share USB devices between computers with their full
functionality, USB/IP encapsulates "USB requests" into TCP/IP payloads and
transmits them between computers. Original USB device drivers and
applications can be also used for remote USB devices without any
modification of them.

A computer can use remote USB devices as if they
were directly attached; for example, we can ...

   - USB storage devices: fdisk, mkfs, mount/umount, file operations,
     play a DVD movie and record a DVD-R media.  

   - USB keyboards and USB mice: use with linux console and X Window
     System.  

   - USB webcams and USB speakers: view webcam, capture image data and
     play some music.

   - USB printers, USB scanners, USB serial converters and USB Ethernet
     interfaces: ok, use fine.


%package -n libusbip
Summary: The libusbip library
Group: Applications/System

%description -n libusbip
The libusbip library


%package -n libusbip-devel
Summary: The USB-IP development files
Group: Applications/System

%description -n libusbip-devel
The USB-IP development files


%package server
Summary: The USB-IP server part
Group: Applications/System

%description server
The USB-IP server part


%package client
Summary: The USB-IP client part
Group: Applications/System

%description client
The USB-IP client part


%prep
%setup -n usbip-0.1.7/src


%build
sh autogen.sh
%configure
make %{?_smp_mflags}


%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT


%clean
rm -rf $RPM_BUILD_ROOT


%files
%defattr(-,root,root,-)
%doc README COPYING INSTALL AUTHORS ChangeLog


%files -n libusbip
%{_libdir}/libusbip.so
%{_libdir}/libusbip.so.0
%{_libdir}/libusbip.so.0.0.1
%{_datadir}/usbip/usb.ids


%files -n libusbip-devel
%{_libdir}/libusbip.a
%{_libdir}/libusbip.la
%{_includedir}/usbip/stub_driver.h
%{_includedir}/usbip/usbip.h
%{_includedir}/usbip/usbip_common.h
%{_includedir}/usbip/vhci_driver.h


%files client
%defattr(-,root,root,-)
%{_bindir}/usbip


%files server
%defattr(-,root,root,-)
%{_bindir}/usbipd
%{_bindir}/bind_driver


%changelog
* Wed Jan 20 2010 Sylvain Baubeau <sylvain.baubeau@agorabox.org> - 0.1.7-1
Initial release
