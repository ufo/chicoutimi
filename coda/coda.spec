Name:           coda
Version:        6.9.4
Release:        3%{?dist}
Summary:        Coda distributed file system
Group:          System Environment/Daemons
License:        GPLv2
URL:            http://www.coda.cs.cmu.edu/
Source0:        ftp://ftp.coda.cs.cmu.edu/pub/coda/src/%{name}-%{version}.tar.gz
Source1:        coda-client.init
Source2:        codasrv.init
Source3:        auth2.init
Source4:        update.init
Source5:        sync-ok.png
Source6:        synchronizing.png
Source7:        coda-auth-dialog.desktop
Source8:        gcodacon.desktop
Source9:        prefsync.desktop
Source10:       prefsync
Source11:       prefsync.file-to-exclude
Source12:       prefsync.file-to-include
Patch0:         coda-6.9.3-client-fhs.patch
Patch1:		coda-6.9.4-rc2-no-default-krb.patch
Patch2:		coda-6.9.4-rc2-kernel-alias.patch
Patch3:		coda-6.9.4-gcc44.patch
Patch4:         coda-locales.patch
Patch5:         coda-clog-masquerade.patch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  lwp-devel >= 2.5
BuildRequires:	rpc2-devel >= 2.8
BuildRequires:	rvm-devel
BuildRequires:	rvm-tools readline-devel
BuildRequires:  fltk-devel fltk-fluid flex bison python perl krb5-devel
BuildRequires:	e2fsprogs-devel
BuildRequires:  libgnomeui-devel
Requires:	krb5-libs
Requires:       libgnomeui

# For /etc/rc.d/init.d so that configure can detect we have RH style init
BuildRequires:  chkconfig

%description
Source package for the Coda file system.  Three packages are provided by
this rpm: the client and server and the backup components. Separately
you must install a kernel module, or have a Coda enabled kernel, and
you should get the Coda documentation package.


%package client
Summary:        Client for the Coda distributed file system
Group:          System Environment/Daemons
Requires(post): chkconfig
Requires(preun): chkconfig

%description client
This package contains the main client program, the cachemanager Venus.
Also included are the binaries for the cfs, utilities for logging, ACL
manipulation etc, the hoarding tools for use with laptops and repair
tools for fixing conflicts. Finally there is the cmon and codacon
console utilities to monitor Coda's activities. You need a Coda
kernel-module for your kernel version, or Coda in your kernel, to have
a complete coda client.


%package server
Summary:        Server for the Coda distributed file system
Group:          System Environment/Daemons
Requires:       rvm-tools
Requires(post): chkconfig
Requires(preun): chkconfig

%description server
This package contains the fileserver for the Coda file system, as well
as the volume utilities.


%package backup
Summary:        Backup coordinator for the Coda distributed file system
Group:          System Environment/Daemons

%description backup
This package contains the backup software for the Coda file system, as
well as the volume utilities.


%package vcodacon
Summary:        Graphical console for the Coda distributed file system
Group:          System Environment/Daemons
Requires:       coda-client = %{version}-%{release}

%description vcodacon
This package contains vcodacon, a graphical console for the Coda file
system client.


%prep
%setup -q
%patch0 -p1

# http://www.coda.cs.cmu.edu/trac/ticket/1651
%patch1 -p1

# http://www.coda.cs.cmu.edu/trac/ticket/1653
%patch2 -p1

# 04d3fefdf27127589617cf955b1d3da799ebde5d
%patch3 -p1

%patch4 -p1
%patch5 -p1


%build
# note: remove the -I and -l here when upstream releases fix for krb5 building
./bootstrap.sh
export CFLAGS="$RPM_OPT_FLAGS -I/usr/include/et"
export LIBS="-lkrb5 -lcom_err -lstdc++"
%configure --with-vcodacon --with-krb5
make %{?_smp_mflags}


%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT INSTALL="install -p"

# remove upstream provided init scripts and replace with our own LSB compliant
# ones
rm $RPM_BUILD_ROOT%{_sysconfdir}/rc.d/init.d/*.init
install -p -m 755 %{SOURCE1} $RPM_BUILD_ROOT%{_sysconfdir}/rc.d/init.d/coda-client
install -p -m 755 %{SOURCE2} $RPM_BUILD_ROOT%{_sysconfdir}/rc.d/init.d/codasrv
install -p -m 755 %{SOURCE3} $RPM_BUILD_ROOT%{_sysconfdir}/rc.d/init.d/auth2
install -p -m 755 %{SOURCE4} $RPM_BUILD_ROOT%{_sysconfdir}/rc.d/init.d/update

install -p -m 755 -D %{SOURCE5} $RPM_BUILD_ROOT%{_datadir}/gcodacon/sync-ok.png
install -p -m 755 -D %{SOURCE6} $RPM_BUILD_ROOT%{_datadir}/gcodacon/synchronizing.png
install -p -m 755 -D %{SOURCE7} $RPM_BUILD_ROOT%{_sysconfdir}/xdg/autostart/coda-auth-dialog.desktop
install -p -m 755 -D %{SOURCE8} $RPM_BUILD_ROOT%{_sysconfdir}/xdg/autostart/gcodacon.desktop
install -p -m 755 -D %{SOURCE8} $RPM_BUILD_ROOT%{_datadir}/applications/gcodacon.desktop

install -p -m 755 -D %{SOURCE9} $RPM_BUILD_ROOT%{_sysconfdir}/xdg/autostart/prefsync.desktop
install -p -m 755 -D %{SOURCE10} $RPM_BUILD_ROOT/usr/bin/prefsync
install -p -m 755 %{SOURCE11} $RPM_BUILD_ROOT%{_sysconfdir}/coda/prefsync.file-to-exclude
install -p -m 755 %{SOURCE12} $RPM_BUILD_ROOT%{_sysconfdir}/coda/prefsync.file-to-include

# coda mount point for the client
mkdir -p $RPM_BUILD_ROOT/coda
touch $RPM_BUILD_ROOT/coda/NOT_REALLY_CODA

# coda cache/log dirs for the client
mkdir -p $RPM_BUILD_ROOT/%{_var}/lib/coda
mkdir -p $RPM_BUILD_ROOT/%{_var}/lib/coda/cache
mkdir -p $RPM_BUILD_ROOT/%{_var}/lib/coda/spool
mkdir -p $RPM_BUILD_ROOT/%{_var}/log/coda

# for %%ghost
touch $RPM_BUILD_ROOT%{_sysconfdir}/coda/{venus,server}.conf

#remove parser, it conflicts with grib_api
rm -f $RPM_BUILD_ROOT%{_bindir}/parser

%clean
rm -rf $RPM_BUILD_ROOT


%post client
# not pretty, but we cannot simply put /coda in our files-list because then
# rpm will fail when updating coda-client when coda is mounted
if [ ! -e /coda ]; then
    mkdir /coda
    touch /coda/NOT_REALLY_CODA
fi
/sbin/chkconfig --add coda-client

%preun client
if [ $1 = 0 ]; then
   /sbin/chkconfig --del coda-client
fi

%post server
/sbin/chkconfig --add update
/sbin/chkconfig --add auth2
/sbin/chkconfig --add codasrv

%preun server
if [ $1 = 0 ]; then
   /sbin/chkconfig --del update
   /sbin/chkconfig --del auth2
   /sbin/chkconfig --del codasrv
fi


%files client
%defattr(-,root,root,-)
%doc AUTHORS ChangeLog LICENSE NEWS
%dir %{_sysconfdir}/coda
%ghost %config(noreplace) %{_sysconfdir}/coda/venus.conf
%config(noreplace) %{_sysconfdir}/coda/venus.conf.ex
%config(noreplace) %{_sysconfdir}/coda/realms
%{_sysconfdir}/coda/prefsync.file-to-exclude
%{_sysconfdir}/coda/prefsync.file-to-include
%{_sysconfdir}/rc.d/init.d/coda-client
%{_sysconfdir}/xdg/autostart/coda-auth-dialog.desktop
%{_sysconfdir}/xdg/autostart/gcodacon.desktop
%{_sysconfdir}/xdg/autostart/prefsync.desktop
/usr/sbin/asrlauncher
/usr/sbin/codaconfedit
/usr/sbin/venus
/usr/sbin/venus-setup
/usr/sbin/volmunge
/usr/sbin/vutil
/usr/bin/au
/usr/bin/aup
/usr/bin/cfs
/usr/bin/clog
/usr/bin/cmon
/usr/bin/coda-auth-dialog
/usr/bin/codacon
/usr/bin/cpasswd
/usr/bin/ctokens
/usr/bin/cunlog
/usr/bin/filerepair
/usr/bin/gcodacon
/usr/bin/prefsync
/usr/bin/hoard
/usr/bin/mkcodabf
/usr/bin/mklka
/usr/bin/removeinc
/usr/bin/repair
/usr/bin/coda_replay
/usr/bin/spy
/usr/bin/xaskuser
/usr/bin/xfrepair
/usr/share/coda-auth-dialog/coda-auth-dialog.glade
/usr/share/locale/fr/LC_MESSAGES/coda.mo
/usr/share/applications/gcodacon.desktop
/usr/share/gcodacon/sync-ok.png
/usr/share/gcodacon/synchronizing.png
/usr/share/man/man1/au.1.gz
/usr/share/man/man1/cfs.1.gz
/usr/share/man/man1/clog.1.gz
/usr/share/man/man1/cmon.1.gz
/usr/share/man/man1/coda_replay.1.gz
/usr/share/man/man1/cpasswd.1.gz
/usr/share/man/man1/ctokens.1.gz
/usr/share/man/man1/cunlog.1.gz
/usr/share/man/man1/hoard.1.gz
/usr/share/man/man1/mkcodabf.1.gz
/usr/share/man/man1/repair.1.gz
/usr/share/man/man1/spy.1.gz
/usr/share/man/man8/venus.8.gz
/usr/share/man/man8/venus-setup.8.gz
/usr/share/man/man8/volmunge.8.gz
/usr/share/man/man8/vutil.8.gz
%ghost %dir /coda
%ghost /coda/NOT_REALLY_CODA
%dir %{_var}/lib/coda
%dir %{_var}/lib/coda/cache
%dir %{_var}/lib/coda/spool
%dir %{_var}/log/coda

%files server
%defattr(-,root,root,-)
%doc AUTHORS ChangeLog LICENSE NEWS
%dir %{_sysconfdir}/coda
%ghost %config(noreplace) %{_sysconfdir}/coda/server.conf
%config(noreplace) %{_sysconfdir}/coda/server.conf.ex
%{_sysconfdir}/rc.d/init.d/codasrv
%{_sysconfdir}/rc.d/init.d/auth2
%{_sysconfdir}/rc.d/init.d/update
/usr/sbin/auth2
/usr/sbin/bldvldb.sh
/usr/sbin/coda-server-logrotate
/usr/sbin/codaconfedit
/usr/sbin/codadump2tar
/usr/sbin/codasrv
/usr/sbin/codastart
/usr/sbin/createvol_rep
/usr/sbin/initpw
/usr/sbin/inoder
/usr/sbin/norton
/usr/sbin/norton-reinit
/usr/sbin/parserecdump
/usr/sbin/partial-reinit.sh
/usr/sbin/pdbtool
/usr/sbin/printvrdb
/usr/sbin/purgevol_rep
/usr/sbin/startserver
/usr/sbin/tokentool
/usr/sbin/updatesrv
/usr/sbin/updateclnt
/usr/sbin/updatefetch
/usr/sbin/vice-killvolumes
/usr/sbin/vice-setup
/usr/sbin/vice-setup-rvm
/usr/sbin/vice-setup-srvdir
/usr/sbin/vice-setup-user
/usr/sbin/vice-setup-scm
/usr/sbin/volutil
/usr/bin/getvolinfo
/usr/bin/reinit
/usr/bin/rpc2ping
/usr/bin/rvmsizer
/usr/bin/smon2
/usr/share/man/man5/maxgroupid.5.gz
/usr/share/man/man5/passwd.coda.5.gz
/usr/share/man/man5/servers.5.gz
/usr/share/man/man5/vicetab.5.gz
/usr/share/man/man5/volumelist.5.gz
/usr/share/man/man5/vrdb.5.gz
/usr/share/man/man8/auth2.8.gz
/usr/share/man/man8/bldvldb.sh.8.gz
/usr/share/man/man8/codasrv.8.gz
/usr/share/man/man8/createvol_rep.8.gz
/usr/share/man/man8/initpw.8.gz
/usr/share/man/man8/norton.8.gz
/usr/share/man/man8/pdbtool.8.gz
/usr/share/man/man8/purgevol_rep.8.gz
/usr/share/man/man8/startserver.8.gz
/usr/share/man/man8/updateclnt.8.gz
/usr/share/man/man8/updatesrv.8.gz
/usr/share/man/man8/vice-setup.8.gz
/usr/share/man/man8/volutil.8.gz

%files backup
%defattr(-,root,root,-)
/usr/sbin/auth2
/usr/sbin/backup
/usr/sbin/backup.sh
/usr/sbin/merge
/usr/sbin/readdump
/usr/sbin/tape.pl
/usr/sbin/updateclnt
/usr/sbin/updatefetch
/usr/sbin/volutil
/usr/share/man/man5/backuplogs.5.gz
/usr/share/man/man5/dumpfile.5.gz
/usr/share/man/man5/dumplist.5.gz
/usr/share/man/man8/backup.8.gz
/usr/share/man/man8/merge.8.gz
/usr/share/man/man8/readdump.8.gz

%files vcodacon
%defattr(-,root,root,-)
/usr/bin/vcodacon


%changelog
* Fri Jun 26 2009 Sylvain Baubeau <bob@glumol.com> - 6.9.4-3
- Added gcodacon, translation, auth2 port masquerade

* Tue Mar 31 2009 Neil Horman <nhorman@redhat.com> - 6.9.4-2
- Remove parser from coda-client, due to name conflict (bz 492953)

* Fri Feb 27 2009 Adam Goode <adam@spicenitz.org> - 6.9.4-1
- New upstream release
  + Avoid possible crashes and/or data loss when volumes are removed
    from and re-added to the client cache.
  + Add configuration setting (detect_reintegration_retry) for Coda
    clients running in a VMM which prevents dropping reintegrated
    operations when the virtual machine is reset to an earlier snapshot.
  + Do not assert on various non-fatal conditions (failing chown/chmod)
    that may arise when for instance the client cache is stored on a vfat
    formatted file system.
  + During backups, avoid unlocking locks that may have been taken by
    another thread.
  + Allow changing of the ctime/mtime of symlinks.
  + Avoid a server deadlock by correcting lock ordering between ViceFetch,
    ViceGetAttr and ViceValidataAttrs (problem identified and tracked down
    with lots of help from Rune).
  + Improve tar support and add cpio archive formats for modification log
    checkpoints.
  + Do not invoke server resolution on a single available replica.
  + Add new incremental dump format that maintains full path information
    when converting the resulting volume dump to a tar archive.
- Remove -D_GNU_SOURCE
- Drop sudo patch (upstream)
- Drop SIGTERM patch (upstream)

* Tue Feb 24 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 6.9.4-0.4.rc2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Sun Sep 14 2008 Adam Goode <adam@spicenitz.org> - 6.9.4-0.3.rc2
- Do not change the default behavior of clog when building with krb5
  (rh 462179)

* Tue Sep 09 2008 Neil Horman <nhorman@redhat.com> 6.9.4-0.2.rc2
- Enabling krb5 support (bz 461041)

* Thu May 29 2008 Hans de Goede <j.w.r.degoede@hhs.nl> 6.9.4-0.1.rc2
- Update to 6.9.4~rc2 (bz 448749)

* Tue May 20 2008 Hans de Goede <j.w.r.degoede@hhs.nl> 6.9.3-2
- Make coda-client package put everything in FHS locations like Debian does,
  rename coda-client initscript / service from venus to coda-client (rh 446653)

* Mon May 12 2008 Hans de Goede <j.w.r.degoede@hhs.nl> 6.9.3-1
- Initial Fedora package
