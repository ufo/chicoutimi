# -*- coding: UTF-8 -*-
#
#  DiskInfo.py : Detect and get informations about block devices
#  Copyright (C) 2007 Mertens Florent <flomertens@gmail.com>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#

import os
import sys
import logging
from subprocess import *

from Fstabconfig import *
from FstabError import *

REQUIRED = ["DEV", "DEVICE", "FS_DRIVERS", "FS_TYPE"]


def get_diskinfo_backend(backend = "auto") :
    ''' get_diskinfo_backend([backend]) -> return the best backend available, 
                                           or the backend specify by backend.\n
        ex : info = get_diskinfo_backend()() '''

    backends = []
    path = os.path.dirname(__file__)
    if not path :
        path = os.getcwd()
    sys.path.append(path)
    names = [ os.path.basename(path)[:-3] for path in os.listdir(path)
              if path.endswith("Backend.py") ]
    for name in names:
        module = __import__( name )
        for attr in dir(module):
            obj = getattr(module, attr)
            if hasattr(obj, "_backend_score") :
                backends.append(obj)
    if len(backends) == 0 :
        logging.warning("")
        logging.warning("No DiskInfo backend found. Trying the default one...")
        module = __import__( "ToolsBackend" )
        return getattr(module, "ToolsBackend")
    if not backend == "auto" :
        backend = backend[0].upper() + backend[1:] + "Backend"
        if not backend in [ k.__name__ for k in backends ] :
            logging.warning("%s is not available. Using auto backend." % backend)
        elif getattr(module, backend )()._backend_score() :
            return getattr(module, backend)
        else :
            logging.warning("%s is not currently working. Using auto backend." % backend)
    scores = [ k()._backend_score() for k in backends ]
    return backends[scores.index(max(scores))]


class DiskInfoBase(list) :
    ''' Base class for all DiskInfo backend. This class implememt all the infrastructure
        to manage DiskInfo class, and leave only to backends the works to get sensible
        informations about a device. '''

    def __init__(self) :

        self._loaded = False
        self._loaded_extra = False
        self._loading = False
        
    def _backend_score(self) :
        ''' Return the reliability score of the backend (0-100). You must override
            this method when creating a backend. You might probably make this score
            dependant of the versions of the tools you use. Keep also in mind that
            the score of the default backend (ToolsBackend) is set to 50 '''
             
        return 0
        
    def _get_devices(self) :
        ''' This function try to get all block devices from /proc/partitions. You might or not
            override this method, dependending on how reliable you think it is. '''
    
        fd = open("/proc/partitions")
        devices = fd.readlines()[2:]
        fd.close()
        return devices
        
    def load_database(self, reload = False) :
        ''' x.load_database([reload]) -> load the database.\n
            At DiskInfo creation, the database is not created. You have to call load_database
            to create it. But you should not need to explicitly doing it, since all other methods
            that use the database should do it. In fact this method is keeped public only
            for it's reload option : when calling load_database with reload = True, the
            database is reloaded '''
            
        if (self._loaded or self._loading) and not reload : 
            return
        logging.debug("\n")
        logging.debug("*** Create Disk Database ***")
        self._loading = True
        del self[:]
        devices = self._get_devices()
        for i in range(len(devices)) :
            self.append({})
            self[i] = self._get_device_info(devices[i])
            self[i] = self._get_common_device_info(self[i])
        self._load_reverse_database()
        self._loaded = True
        self._loading = False
        logging.debug("*** Database created ***\n")
        
    def _get_device_info(self, device) :
        ''' This is the method that all backends must override. It should returns a dict of
            information about device. Informations are :
            "DEV"       : simple device name, like sda1 or dm-0. (required)
            "DEVICE"    : the device path, like /dev/sda1, or /dev/mapper/*. (required)
            "FS_UUID"   : the UUID of the device (required)
            "FS_LABEL"  : the label of the device (required if available)
            "FS_TYPE"   : the type of the device, like ext3. (required)
            "FS_USAGE"  : the usage of the fs, like filesystem, or crypto.
                          Devices with FS_USAGE != filesystem are ignored. (required)
            "REMOVABLE" : set to True if the device is from a removable drive.
                          Removable devices are ignored. (required)
            "MINOR"     : the minor number of the device (recommanded)
            "MAJOR"     : the major number of the device (recommaned)
            "SYSFS_PATH": the sysfs path (recommanded)
            "SIZE"      : the size of the device (recommanded)
            "FS_LABEL_SAFE" : the safe label of the device (without special charachter)
                              (recommanded if available)
            "FS_VERSION": the version of the type (recommanded)
            "MODEL", "SERIAL_SHORT", "BUS", "VENDOR", "SERIAL", "TYPE", "REVISION" :
            some informations about the device, and it's drive. (optionnal) '''
    
        #Impement your backend here
        return device
        
    def _get_common_device_info(self, dev) :
        ''' This method complete device informations with common attributes that should not
            be the job of a backend. Also set here what we ignore or not. 
            If you want to to complete FS_DRIVERS for a specific fs_type, you should simply 
            create a get_(fs_type)_drivers method. See get_ntfs_drivers for an exemple '''

        # Set FS_DRIVERS
        if dev.has_key("FS_TYPE") :
            dev["FS_DRIVERS"] = []
            try :
                for driver in getattr(self, "get_%s_drivers" % dev["FS_TYPE"].lower())() :
                    dev["FS_DRIVERS"].append(driver)
                    logging.debug("-> Add   FS_DRIVERS : %s (%s)" \
                            % (driver[0], driver[1] ))
            except AttributeError :
                dev["FS_DRIVERS"].append([dev["FS_TYPE"], "Default driver"])
                logging.debug("-> Add   FS_DRIVERS : " + dev["FS_TYPE"])
            
        # We now set here ignored dev :
        # Ignore every dev if FS_USAGE != filesystem or if REMOVABLE
        # also ignore dev that don't have all REQUIRED attributes
        dev["IGNORE"] = False
        if not dev.has_key("FS_USAGE") :
            dev["IGNORE"] = True
        elif not dev["FS_USAGE"] == "filesystem" :
            dev["IGNORE"] = True
        elif not min(map(dev.has_key, REQUIRED)) :
            logging.warning("DiskInfo backend problem : \n" \
                + "-> %s appears to be a filesystem " % dev["DEV"] 
                + "but is lacking some required information.\n"
                + "-> We ignore it for now, but it's might "
                + "be a bug in the backend, %s." % get_diskinfo_backend())
            dev["IGNORE"] = True
        if dev["REMOVABLE"] :
            dev["IGNORE"] = True

        # Ignore old entry with the same DEVICE
        if dev.has_key("DEVICE") :
            for device in self.search(dev["DEVICE"], keys = ["DEVICE"]) :
                if not self[device]["DEV"] == dev["DEV"] :
                    logging.debug("W: " + "Ignore duplicate entry : " + self[device]["DEV"] \
                        + " -> " + self[device]["DEVICE"])
                    self[device]["IGNORE"] = True

        return dev
        
    def _load_reverse_database(self) :
    
        self._reverse_database = {}
        for i in range(len(self)) :
            if self[i].has_key("DEV") :
                self._reverse_database[self[i]["DEV"]] = i
            if self[i].has_key("DEVICE") :
                self._reverse_database[self[i]["DEVICE"]] = i
            if self[i].has_key("FS_UUID") :
                self._reverse_database[self[i]["FS_UUID"]] = i
                self._reverse_database["UUID=" + self[i]["FS_UUID"]] = i
            if self[i].has_key("FS_LABEL") :
                self._reverse_database[self[i]["FS_LABEL"]] = i
                self._reverse_database["LABEL=" + self[i]["FS_LABEL"]] = i
                
    def get_ntfs_drivers(self, reload = False) :
        ''' x.get_ntfs_drivers([reload]) -> get a list of available NTFS driver in this format :
                                            [driver, description] '''
    
        if hasattr(self, "_ntfs_drivers") and not reload : 
            return self._ntfs_drivers
        drivers = []
        command = MODPROBE + " -l ntfs"
        process = Popen(command, stdout=PIPE, close_fds=True, shell=True)
        process.wait()
        if not process.stdout.read().find("ntfs") == -1 : 
            drivers.append(["ntfs", "Read-only driver"])
        if os.path.isfile("/sbin/mount.ntfs-3g") : 
            drivers.append(["ntfs-3g", "Read-write driver"])
        self._ntfs_drivers = drivers
        return self._ntfs_drivers
                          
    def __getitem__(self, item) :
        
        self.load_database()
        if type(item) == int :
            if item < len(self) :
                return list.__getitem__(self, item)
            else :
                raise NotInDatabase, "Index %i out of range" % item
        else :
            try :
                return list.__getitem__(self, self.search(item)[0])
            except :
                raise NotInDatabase, "Can't find %s in the database" % item
            
    def list(self, col = "DEVICE", ignored = True, keep_index = False) :
        ''' x.list([col], [ignored], [keep_index]) -> List all values of attribute col.
                                                      Default to "DEVICE"\n
            If ignored is set to False, don't list device with IGNORE=True. Default to True.
            If keep_index is set to True and ignored to False, all ignored device result to
            an empty string, to keep the index with the database. Default to False. ''' 

        self.load_database()
        result = []
        for k in self :
            if not ignored and k["IGNORE"] :
                if keep_index :
                    result.append("")
                continue
            if k.has_key(col) :
                result.append(k[col])
            else :
                result.append("")
        return result
        
    def get(self, item, attribute) :
        ''' x.get(item, attribute) -> return attribute of item '''
    
        self.load_database()
        try :
            return self[item][attribute]
        except :
            return "None"
            
    def search_reverse(self, pattern, ignored = True) :
        ''' x.search_reverse(pattern, [ignored]) -> search for pattern in the reverse database\n
            This method is faster than the search method, but not as flexible, since pattern is
            searched in fixed keys : "DEV", "DEVICE", "FS_UUID", "FS_LABEL"
            Set ignored to False, to not return device with IGNORE=True. Default to True '''
        
        self.load_database()
        if self._loaded and self._reverse_database.has_key(pattern) :
            result = self._reverse_database[pattern]
            if ignored or not self[result]["IGNORE"] :
                return result
        return None
                
    def search(self, pattern, keys = ["DEV", "DEVICE"], ignored = True) :
        ''' x.search(pattern, [list], [keys], [ignored]) -> search for pattren in each
                                                            keys of each Entry of x\n
            Default keys are : ["DEV", "DEVICE"]
            Set ignored to False, to not return device with IGNORE=True. Default to True '''

        self.load_database()
        result = []
        for col in keys :
            i = 0
            for value in self.list(col, ignored, keep_index = True) :
                if value == pattern and i not in result :
                    result.append(i)
                if col == "FS_UUID" :
                    if "UUID=" + value == pattern and i not in result :
                        result.append(i)
                if col == "FS_LABEL" :
                    if "LABEL=" + value == pattern and i not in result :
                        result.append(i)
                i = i + 1
        return result

    def export(self, device) :
        ''' x.export(device) -> query database for device, and return a string of printable
                                informations about device.\n
            If device is set to "all", query database for all devices '''
    
        self.load_database()
        result = ""
        if device == "all" :
            result += "Query database for all devices :\n\n"
            for i in range(len(self)) :
                result += "Info for " + self[i]["DEV"] + " :\n"
                result += "\n".join(["-> %s=%s" % (k, v) for k, v in self[i].items()])
                result += "\n\n"
        else :
            result += "Query database for " + device + " :\n\n"
            if self.search(device) :
                result += "Info for " + self[device]["DEV"] + " :\n"
                result += "\n".join(["-> %s=%s" % (k, v) for k, v in self[device].items()])
                result += "\n\n"
            else :
                result += device + " not in the database"
        return result

