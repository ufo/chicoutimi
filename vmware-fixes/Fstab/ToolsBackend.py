# -*- coding: UTF-8 -*-
#
#  ToolsBackend.py : DiskInfo Backend that use various tools
#                    to get device informations
#  Copyright (C) 2007 Mertens Florent <flomertens@gmail.com>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#


import os
import re
import logging
from subprocess import *

from Fstabconfig import *
import FstabData
from DiskInfo import DiskInfoBase

class ToolsBackend(DiskInfoBase) :

    def __init__(self) :
    
        DiskInfoBase.__init__(self)
        
                
    def _backend_score(self) :
        ''' Return the reliability score of the backend (0-100) '''
        
        return 50
        
    def _get_device_info(self, device) :
            
        dev = {}
        # Get dev, size, major and minor directly from /proc/partitions
        dev["DEV"] = device.strip().split()[3]
        logging.debug("Look at : " + dev["DEV"])
        dev["SIZE"] = float(device.strip().split()[2])*1024.
        dev["MAJOR"] = device.strip().split()[0]
        dev["MINOR"] = device.strip().split()[1]
        
        # See if this partition is from a parent already in the database
        try :
            drive = re.search("(\w{1,3})[0-9]", dev["DEV"]).groups()[0]
            dev["PARENT"] = self[drive]["DEV"]
        except :
            dev["PARENT"] = None
        logging.debug("-> Set   PARENT :" + str(dev["PARENT"]))
            
        # Set sysfs_path and /removable path depending of the previous result
        if dev["PARENT"] :
            dev["SYSFS_PATH"] = "/sys/block/" + dev["PARENT"] + "/" + dev["DEV"]
            removable = "/sys/block/" + dev["PARENT"] + "/removable"
        else :
            dev["SYSFS_PATH"] = "/sys/block/" + dev["DEV"]
            removable = "/sys/block/" + dev["DEV"] + "/removable"
            
        # Set removable
        if os.path.exists(removable) :
            fd = open(removable)
            dev["REMOVABLE"] = bool(int(fd.readline()))
        else :
            dev["REMOVABLE"] = False
        
        # Get the more info posible about the device from udevinfo
        logging.debug("Get information from udevinfo")
        cmd = UDEVINFO + " -p " + dev["SYSFS_PATH"].replace("/sys","") + " -q all"
        process = Popen(cmd, stderr=STDOUT, stdout=PIPE, close_fds=True, shell=True)
        sts = process.wait()
        result = process.stdout.readlines()
        if not sts :
            for line in result :
                logging.debug("(udevinfo output) " + line.strip())
                try :
                    device = "/dev/" + re.search("N: (\S+)", line).groups()[0]
                    dev["DEVICE"] = device
                    logging.debug("-> Found DEVICE : " + device)
                except : pass
                # Used for debugging blkid & vol_id :
                #continue
                try :
                    (attr, value) = re.search("E: ID_(\S+)=(\S+)", line).groups()
                    dev[attr] = value
                    logging.debug("-> Found " + attr + " : "+ value)
                except : pass
                
        # Used for debugging dmsetup :
        #if not dev["DEV"].find("dm-") == -1 and dev.has_key("DEVICE") :
        #    del dev["DEVICE"]
                
        # If udevinfo fail or didn't give DEVICE, it's might be a mapped device.
        # Try to get DEVICE from dmsetup
        if not dev.has_key("DEVICE") and not dev["DEV"].find("dm-") == -1 :
            logging.debug("W: " + dev["DEV"] + " is a mapped device, but no DEVICE found, call dmsetup")
            cmd = DMSETUP + " info -j " + dev["MAJOR"] + " -m " + dev["MINOR"] + " | grep Name"
            process = Popen(cmd, stderr=STDOUT, stdout=PIPE, close_fds=True, shell=True)
            sts = process.wait()
            result = process.stdout.read()
            logging.debug("(dmsetup output) " + result.strip())
            if not sts and result :
                device = "/dev/mapper/" + result.split()[-1]
                logging.debug("-> Found DEVICE : " + device)
                if os.path.exists(device) :
                    logging.debug("-> DEVICE exists : ok")
                    dev["DEVICE"] = device
            else :
                logging.debug("W: First attempt fail. Trying something else...")
                cmd = "%s ls |grep '(%s, %s)'" % (DMSETUP, dev["MAJOR"], dev["MINOR"])
                process = Popen(cmd, stderr=STDOUT, stdout=PIPE, close_fds=True, shell=True)
                sts = process.wait()
                result = process.stdout.read()
                logging.debug("(dmsetup output) " + result.strip())
                if not sts and result :
                    device = "/dev/mapper/" + result.split()[0]
                    logging.debug("-> Found DEVICE : " + device)
                    if os.path.exists(device) :
                        logging.debug("-> DEVICE exists : ok")
                        dev["DEVICE"] = device
        # If still no DEVICE, try /dev/ + DEV
        if not dev.has_key("DEVICE") :
            device = "/dev/" + dev["DEV"]
            if os.path.exists(device) :
                logging.debug("W : no DEVICE found, try " + device)
                stat = os.stat(device)
                if stat.st_rdev == os.makedev(int(dev["MAJOR"]), int(dev["MINOR"])) :
                    logging.debug("-> Set   DEVICE : " + device)
                    dev["DEVICE"] = device
                                
        # If device is a mapped device, try to get slaves, ignore them, and
        # set removable depending of the slaves.
        if not dev["DEV"].find("dm-") == -1 and os.path.isdir(dev["SYSFS_PATH"] + "/slaves") :
            dev["SLAVES"] = os.listdir(dev["SYSFS_PATH"] + "/slaves")
            dev["REMOVABLE"] = True
            for slave in dev["SLAVES"] :
                if self.search(slave) :
                    self[slave]["IGNORE"] = True
                    dev["REMOVABLE"] = dev["REMOVABLE"] and self[slave]["REMOVABLE"]   
        
        # If udevinfo don't give FS_USAGE for device with parent (or mapped device)
        # call vol_id to complete informations :    
        if not dev.has_key("FS_USAGE") and dev.has_key("DEVICE") \
                and ( dev["PARENT"] or not dev["DEV"].find("dm-") == -1 ) :
            logging.debug("W: " + dev["DEV"] + " don't have an fs_usage, call vol_id")
            cmd = VOLID + " --export " + dev["DEVICE"]
            process = Popen(cmd, stderr=STDOUT, stdout=PIPE, close_fds=True, shell=True)
            sts = process.wait()
            if not sts :
                result = process.stdout.readlines()
                for line in result :
                    logging.debug("(vol_id output) " + line.strip())
                    try :
                        (attr, value) = re.search("ID_(\S+)=(\S+)", line).groups()
                        dev[attr] = value
                        logging.debug("-> Found " + attr + " : " + value)
                    except : pass
            # If vol_id fail, or if still no fs_usage, call blkid
            if sts or not dev.has_key("FS_USAGE") :
                logging.debug("W: vol_id failled, call blkid")
                cmd = BLKID + " " + dev["DEVICE"]
                process = Popen(cmd, stderr=STDOUT, stdout=PIPE, close_fds=True, shell=True)
                sts = os.waitpid(process.pid, 0)
                result = process.stdout.read()
                logging.debug("(blkid output) " + result.strip())
                for attribute in ("TYPE", "LABEL", "UUID") :
                    try :
                        pattern = "\\b" + attribute + "=\"(\S+)\""
                        dev["FS_" + attribute] = re.search(pattern, result).groups()[0]
                        logging.debug("-> Found " + attribute + " : " + dev["FS_" + attribute])
                    except : pass
                if dev.has_key("FS_TYPE") :
                    if dev["FS_TYPE"] not in FstabData.ignore_fs :
                        dev["FS_USAGE"] = "filesystem"
                    else :
                        dev["FS_USAGE"] = "other"
                    logging.debug("-> Set   FS_USAGE : " + dev["FS_USAGE"])
                    
        for attr in ("UUID", "LABEL") :
            # If no UUID, LABEL try to get them manually
            if dev.has_key("DEVICE") and not dev.has_key("FS_" + attr) :
                if os.path.exists(dev["DEVICE"]) and os.path.isdir("/dev/disk/by-" + attr.lower()) :
                    for file in os.listdir("/dev/disk/by-" + attr.lower()) :
                        if os.path.samefile("/dev/disk/by-" + attr.lower() + "/" + file, dev["DEVICE"]) :
                            logging.debug("-> Set   " + attr + " : " + file)
                            dev["FS_" + attr] = file
                  
        return dev

