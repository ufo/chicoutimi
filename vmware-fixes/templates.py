vmdk = """# Disk DescriptorFile
version=1
CID=c7fc97b4
parentCID=ffffffff
createType="fullDevice"

# Extent description
RW 156301488 FLAT "$device" 0

# The Disk Data Base
#DDB

ddb.virtualHWVersion = "4"
ddb.geometry.cylinders = "9729"
ddb.geometry.heads = "255"
ddb.geometry.sectors = "63"
ddb.geometry.biosCylinders = "9729"
ddb.geometry.biosHeads = "255"
ddb.geometry.biosSectors = "63"
ddb.adapterType = "buslogic"
"""

vmx = """#!/usr/bin/vmware
config.version = "8"
virtualHW.version = "4"
scsi0.present = "TRUE"
memsize = "$mem"
scsi0:0.present = "TRUE"
scsi0:0.fileName = "${name}.vmdk"
scsi0:0.writeThrough = "FALSE"
scsi0:0.deviceType = "rawDisk"
ide1:0.present = "TRUE"
ide1:0.fileName = "/dev/scd0"
ide1:0.deviceType = "cdrom-raw"
floppy0.fileName = "/dev/fd0"
Ethernet0.present = "TRUE"
Ethernet0.connectionType = "nat"
displayName = "$displayname"
guestOS = "redhat"
priority.grabbed = "normal"
priority.ungrabbed = "normal"
powerType.powerOff = "hard"
powerType.powerOn = "hard"
powerType.suspend = "hard"
powerType.reset = "hard"

scsi0:0.redo = ""
ethernet0.addressType = "generated"
uuid.location = "56 4d d5 c6 98 a5 13 2a-ff 63 86 5f a9 4f 2c 77"
uuid.bios = "56 4d d5 c6 98 a5 13 2a-ff 63 86 5f a9 4f 2c 77"
ethernet0.generatedAddress = "00:0c:29:4f:2c:77"
ethernet0.generatedAddressOffset = "0"
"""
