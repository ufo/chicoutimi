Name:           vmware-fixes
Version:        0.1
BuildArch:      noarch
Release:        1%{?dist}
Summary:        Fixes a few VMWare installation bugs for Fedora

Group:          System/Utilities
License:        GPL
URL:            http://www.glumol.com/chicoutimi/vmware-fixes
Source0:        vmware-fixes-0.1.tar.gz
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

Requires:       VMware-server kernel-devel gcc xinetd wxPython

%description


%prep
%setup -q


%build


%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT


%post
mv /usr/bin/vmware /usr/sbin/vmware
cd /usr/bin && ln -s consolehelper vmware
cd /usr/bin && ln -s consolehelper run-vmware
# tmp=`test -f ${XDG_CONFIG_HOME:-~/.config}/user-dirs.dirs && source ${XDG_CONFIG_HOME:-~/.config}/user-dirs.dirs && echo ${XDG_DESKTOP_DIR:-$HOME/Desktop}`
# folder=`basename $tmp`
# tmp=`xdg-user-dir DESKTOP`
tmp=Bureau
folder=`basename $tmp`
for user in `ls /home`
do
  if [ ! -e "/home/$user/$folder/mywindows.desktop" ]
  then
    user_dir=/home/$user/$folder/mywindows.desktop
    echo "[Desktop Entry]" >> $user_dir
    echo "Encoding=UTF-8" >> $user_dir
    echo "Name=My Windows" >> $user_dir
    echo "Comment=Run your Windows" >> $user_dir
    echo "Exec=run-vmware" >> $user_dir
    echo "Terminal=false" >> $user_dir
    echo "Type=Application" >> $user_dir
    echo "Icon=/usr/lib/vmware/share/icons/48x48/apps/vmware-server.png" >> $user_dir
    echo "StartupNotify=true" >> $user_dir
    echo "Categories=Emulator;" >> $user_dir >> $user_dir
    echo "X-Desktop-File-Install-Version=0.13" >> $user_dir
    echo "MimeType=application/x-vmware-vm;" >> $user_dir
    chown $user:$user $user_dir
  fi
done

%postun
tmp=Bureau
folder=`basename $tmp`
for user in `ls /home`
do
  if [ -e "/home/$user/$folder/mywindows.desktop" ]
  then
    rm /home/$user/$folder/mywindows.desktop
  fi
done
unlink /usr/bin/vmware
unlink /usr/bin/run-vmware
mv /usr/sbin/vmware /usr/bin/

%clean
rm -rf $RPM_BUILD_ROOT


%files
%defattr(-,root,root,-)
%doc
/etc/pam.d/vmware
/etc/security/console.apps/vmware
/etc/pam.d/run-vmware
/etc/security/console.apps/run-vmware
/usr/sbin/run-vmware
/usr/share/vmware-fixes/bootdisk.img
/usr/lib/python2.5/site-packages/Fstab/DiskInfo.py
/usr/lib/python2.5/site-packages/Fstab/DiskInfo.pyc
/usr/lib/python2.5/site-packages/Fstab/DiskInfo.pyo
/usr/lib/python2.5/site-packages/Fstab/EventHandler.py
/usr/lib/python2.5/site-packages/Fstab/EventHandler.pyc
/usr/lib/python2.5/site-packages/Fstab/EventHandler.pyo
/usr/lib/python2.5/site-packages/Fstab/Fstab.py
/usr/lib/python2.5/site-packages/Fstab/Fstab.pyc
/usr/lib/python2.5/site-packages/Fstab/Fstab.pyo
/usr/lib/python2.5/site-packages/Fstab/FstabData.py
/usr/lib/python2.5/site-packages/Fstab/FstabData.pyc
/usr/lib/python2.5/site-packages/Fstab/FstabData.pyo
/usr/lib/python2.5/site-packages/Fstab/FstabDialogs.py
/usr/lib/python2.5/site-packages/Fstab/FstabDialogs.pyc
/usr/lib/python2.5/site-packages/Fstab/FstabDialogs.pyo
/usr/lib/python2.5/site-packages/Fstab/FstabError.py
/usr/lib/python2.5/site-packages/Fstab/FstabError.pyc
/usr/lib/python2.5/site-packages/Fstab/FstabError.pyo
/usr/lib/python2.5/site-packages/Fstab/FstabHandler.py
/usr/lib/python2.5/site-packages/Fstab/FstabHandler.pyc
/usr/lib/python2.5/site-packages/Fstab/FstabHandler.pyo
/usr/lib/python2.5/site-packages/Fstab/FstabUtility.py
/usr/lib/python2.5/site-packages/Fstab/FstabUtility.pyc
/usr/lib/python2.5/site-packages/Fstab/FstabUtility.pyo
/usr/lib/python2.5/site-packages/Fstab/Fstabconfig.py
/usr/lib/python2.5/site-packages/Fstab/Fstabconfig.py.in
/usr/lib/python2.5/site-packages/Fstab/Fstabconfig.pyc
/usr/lib/python2.5/site-packages/Fstab/Fstabconfig.pyo
/usr/lib/python2.5/site-packages/Fstab/Makefile.am
/usr/lib/python2.5/site-packages/Fstab/Makefile.in
/usr/lib/python2.5/site-packages/Fstab/Mounter.py
/usr/lib/python2.5/site-packages/Fstab/Mounter.pyc
/usr/lib/python2.5/site-packages/Fstab/Mounter.pyo
/usr/lib/python2.5/site-packages/Fstab/SimpleGladeApp.py
/usr/lib/python2.5/site-packages/Fstab/SimpleGladeApp.pyc
/usr/lib/python2.5/site-packages/Fstab/SimpleGladeApp.pyo
/usr/lib/python2.5/site-packages/Fstab/ToolsBackend.py
/usr/lib/python2.5/site-packages/Fstab/ToolsBackend.pyc
/usr/lib/python2.5/site-packages/Fstab/ToolsBackend.pyo
/usr/lib/python2.5/site-packages/Fstab/__init__.py
/usr/lib/python2.5/site-packages/Fstab/__init__.pyc
/usr/lib/python2.5/site-packages/Fstab/__init__.pyo

%changelog
