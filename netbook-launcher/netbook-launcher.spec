Name:           netbook-launcher
Version:        2.1.12
Release:        1%{?dist}
Summary:        Ubuntu Netbook Remix Launcher

Group:          User Interface/Desktops
License:        GPLv3
URL:            http://launchpad.net/netbook-remix-launcher
Source0:        http://launchpad.net/netbook-remix-launcher/2.0/ubuntu-9.10/+download/netbook-launcher-2.1.12.tar.gz
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  glib2-devel GConf2-devel clutter-gtk-devel clutter-devel gtk2-devel pango-devel
BuildRequires:  gnome-desktop-devel libnotify-devel unique-devel clutk-devel libwnck-devel liblauncher-devel libgnomeui-devel

Requires:       glib2 GConf2 clutter-gtk clutter gtk2 pango gnome-desktop libnotify unique clutk libwnck liblauncher libgnomeui

%description


%prep
%setup -q


%build
%configure
make %{?_smp_mflags}


%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT


%clean
rm -rf $RPM_BUILD_ROOT


%files
%defattr(-,root,root,-)
%doc
%{_sysconfdir}/gconf/schemas/netbook-launcher.schemas
%{_sysconfdir}/xdg/autostart/netbook-launcher.desktop
%{_bindir}/netbook-launcher
%{_includedir}/netbook-launcher/netbook-launcher/netbook-launcher.h
%{_includedir}/netbook-launcher/netbook-launcher/nl-config.h
%{_includedir}/netbook-launcher/netbook-launcher/nl-content-view.h
%{_includedir}/netbook-launcher/netbook-launcher/nl-defines.h
%{_includedir}/netbook-launcher/netbook-launcher/nl-drag-dest.h
%{_includedir}/netbook-launcher/netbook-launcher/nl-drag-server.h
%{_includedir}/netbook-launcher/netbook-launcher/nl-favorite.h
%{_includedir}/netbook-launcher/netbook-launcher/nl-favorites-loader.h
%{_includedir}/netbook-launcher/netbook-launcher/nl-icon-tile.h
%{_includedir}/netbook-launcher/netbook-launcher/nl-notify.h
%{_includedir}/netbook-launcher/netbook-launcher/nl-pixbuf-cache.h
%{_includedir}/netbook-launcher/netbook-launcher/nl-plugin.h
%{_includedir}/netbook-launcher/netbook-launcher/nl-scroll-view.h
%{_includedir}/netbook-launcher/netbook-launcher/nl-shell.h
%{_includedir}/netbook-launcher/netbook-launcher/nl-sidebar-item.h
%{_includedir}/netbook-launcher/netbook-launcher/nl-sidebar.h
%{_includedir}/netbook-launcher/netbook-launcher/nl-texture-frame.h
%{_libdir}/libnetbook-launcher.la
%{_libdir}/libnetbook-launcher.so
%{_libdir}/libnetbook-launcher.so.0
%{_libdir}/libnetbook-launcher.so.0.112.0
%{_libdir}/pkgconfig/netbook-launcher.pc
%{_datadir}/netbook-launcher/add.png
%{_datadir}/netbook-launcher/eject.png
%{_datadir}/netbook-launcher/icon_selected.png
%{_datadir}/netbook-launcher/iconview_normal.png
%{_datadir}/netbook-launcher/iconview_selected.png
%{_datadir}/netbook-launcher/logout-dialog.xml
%{_datadir}/netbook-launcher/scroll_arrow_down.png
%{_datadir}/netbook-launcher/scroll_arrow_up.png
%{_datadir}/netbook-launcher/scroll_view_slider.png
%{_datadir}/netbook-launcher/scroll_view_trough.png
%{_datadir}/netbook-launcher/sidebar_bg.png
%{_datadir}/netbook-launcher/sidebar_item_active.png
%{_datadir}/netbook-launcher/sidebar_item_normal.png
%{_datadir}/netbook-launcher/sidebar_item_prelight.png
%{_datadir}/netbook-launcher/sidebar_normal.png
%{_datadir}/netbook-launcher/unr-gnome-logout.png
/usr/share/locale/*


%changelog
