%define name jockey
%define version 0.5alpha1
%define unmangled_version 0.5alpha1
%define release 1

%{!?python_sitelib: %define python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib()")}
%{!?python_sitearch: %define python_sitearch %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib(1)")}

Summary: UI for managing third-party and non-free drivers
Name: %{name}
Version: %{version}
Release: %{release}
Source0: %{name}-%{unmangled_version}.tar.gz
License: GPL v2 or later
Group: Development/Libraries
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-buildroot
Prefix: %{_prefix}
BuildArch: noarch
Vendor: Martin Pitt <martin.pitt@ubuntu.com>
Url: https://launchpad.net/jockey
Requires: pyxdg dbus-python PolicyKit PolicyKit-gnome PackageKit >= 0.2.2
Requires: notify-python xkit python-distutils-extra redhat-lsb

%description
Jockey provides an user interface and desktop integration for installation 
and upgrade of third-party drivers. It is written in a distribution agnostic 
way, and to be easily portable to different frontends (GNOME, KDE, command 
line).

%prep
%setup -n %{name}-%{unmangled_version}

%build
python setup.py build

%install
python setup.py install --root=$RPM_BUILD_ROOT

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root)
%{_sysconfdir}/dbus-1/system.d/com.ubuntu.DeviceDriver.conf
%{_bindir}/jockey-gtk
%{_bindir}/jockey-kde
%{python_sitelib}/*
%{_datadir}/PolicyKit/policy/com.ubuntu.devicedriver.policy
%{_datadir}/applications/jockey-gtk.desktop
%{_datadir}/applications/jockey-kde.desktop
%{_datadir}/autostart/jockey-gtk.desktop
%{_datadir}/autostart/jockey-kde.desktop
%{_datadir}/dbus-1/services/com.ubuntu.DeviceDriver.service
%{_datadir}/dbus-1/system-services/com.ubuntu.DeviceDriver.service
%{_datadir}/icons/hicolor/16x16/apps/jockey-kde.png
%{_datadir}/icons/hicolor/16x16/apps/jockey.png
%{_datadir}/icons/hicolor/22x22/apps/jockey-kde.png
%{_datadir}/icons/hicolor/22x22/apps/jockey.png
%{_datadir}/icons/hicolor/24x24/apps/jockey-kde.png
%{_datadir}/icons/hicolor/24x24/apps/jockey.png
%{_datadir}/icons/hicolor/32x32/apps/jockey-kde.png
%{_datadir}/icons/hicolor/32x32/apps/jockey.png
%{_datadir}/icons/hicolor/48x48/apps/jockey-kde.png
%{_datadir}/icons/hicolor/scalable/apps/jockey-kde.svg
%{_datadir}/icons/hicolor/scalable/apps/jockey.svg
%{_datadir}/icons/hicolor/scalable/emblems/jockey-certified.svg
%{_datadir}/icons/hicolor/scalable/emblems/jockey-proprietary.svg
%{_datadir}/jockey/ManagerWindowKDE4.ui
%{_datadir}/jockey/ProgressDialog.ui
%{_datadir}/jockey/jockey-backend
%{_datadir}/jockey/main.glade
%{_datadir}/locale/af/LC_MESSAGES/jockey.mo
%{_datadir}/locale/ar/LC_MESSAGES/jockey.mo
%{_datadir}/locale/be/LC_MESSAGES/jockey.mo
%{_datadir}/locale/bg/LC_MESSAGES/jockey.mo
%{_datadir}/locale/ca/LC_MESSAGES/jockey.mo
%{_datadir}/locale/cs/LC_MESSAGES/jockey.mo
%{_datadir}/locale/de/LC_MESSAGES/jockey.mo
%{_datadir}/locale/el/LC_MESSAGES/jockey.mo
%{_datadir}/locale/es/LC_MESSAGES/jockey.mo
%{_datadir}/locale/eu/LC_MESSAGES/jockey.mo
%{_datadir}/locale/fi/LC_MESSAGES/jockey.mo
%{_datadir}/locale/fr/LC_MESSAGES/jockey.mo
%{_datadir}/locale/gl/LC_MESSAGES/jockey.mo
%{_datadir}/locale/hr/LC_MESSAGES/jockey.mo
%{_datadir}/locale/hu/LC_MESSAGES/jockey.mo
%{_datadir}/locale/id/LC_MESSAGES/jockey.mo
%{_datadir}/locale/it/LC_MESSAGES/jockey.mo
%{_datadir}/locale/ja/LC_MESSAGES/jockey.mo
%{_datadir}/locale/ka/LC_MESSAGES/jockey.mo
%{_datadir}/locale/ko/LC_MESSAGES/jockey.mo
%{_datadir}/locale/lt/LC_MESSAGES/jockey.mo
%{_datadir}/locale/lv/LC_MESSAGES/jockey.mo
%{_datadir}/locale/mk/LC_MESSAGES/jockey.mo
%{_datadir}/locale/nb/LC_MESSAGES/jockey.mo
%{_datadir}/locale/nl/LC_MESSAGES/jockey.mo
%{_datadir}/locale/nn/LC_MESSAGES/jockey.mo
%{_datadir}/locale/pl/LC_MESSAGES/jockey.mo
%{_datadir}/locale/pt/LC_MESSAGES/jockey.mo
%{_datadir}/locale/pt_BR/LC_MESSAGES/jockey.mo
%{_datadir}/locale/ro/LC_MESSAGES/jockey.mo
%{_datadir}/locale/ru/LC_MESSAGES/jockey.mo
%{_datadir}/locale/sk/LC_MESSAGES/jockey.mo
%{_datadir}/locale/sl/LC_MESSAGES/jockey.mo
%{_datadir}/locale/sr/LC_MESSAGES/jockey.mo
%{_datadir}/locale/sv/LC_MESSAGES/jockey.mo
%{_datadir}/locale/th/LC_MESSAGES/jockey.mo
%{_datadir}/locale/tr/LC_MESSAGES/jockey.mo
%{_datadir}/locale/uk/LC_MESSAGES/jockey.mo
%{_datadir}/locale/vi/LC_MESSAGES/jockey.mo
%{_datadir}/locale/zh_CN/LC_MESSAGES/jockey.mo
%{_datadir}/locale/zh_HK/LC_MESSAGES/jockey.mo
%{_datadir}/locale/zh_TW/LC_MESSAGES/jockey.mo

%changelog
* Tue Sep 16 2008 Bob <bob@glumol.com>
Initial release
