/*
 * PAM module for user name switches.
 * 
 * Copyright (C) 2009 Kevin Pouget <pouget@agorabox.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  
 *
 *
 */

#include <stdio.h>
#include <unistd.h>
#include <grp.h>
#include <pwd.h>
#include <time.h>
#include <ctype.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <syslog.h>
#include <sys/wait.h>
#include <sys/types.h>

#define MODE_LDAP 0
#define MODE_END 1
#define MODE_NONE 2

#include <security/pam_modules.h>
#include <security/_pam_macros.h>
#include <security/pam_ext.h>

#include <ldap.h>
#include <lber.h>

static int
_get_ldap_value (LDAP * ld, LDAPMessage * e, const char *attr, char *ptr)
{
    char **vals;

    vals = ldap_get_values (ld, e, (char *) attr);
    if (vals == NULL) {
        return PAM_AUTHINFO_UNAVAIL;
    }
    
    snprintf(ptr, 256, "%s", vals[0]);
    ldap_value_free (vals);

    return PAM_SUCCESS;
}

int _switch_user_name(pam_handle_t * pamh, int flags, int argc, const char **argv)
{
    int rc, mode, debug;
    const char * username, * switched_name;
    char filter[256], env[256], ldap_username[256], original_username[256];    
    LDAP * ldap;
    LDAPMessage *res, *msg;

    if (argc < 1) {
        pam_syslog(pamh, LOG_ERR, "Wrong number of parameters");
        return PAM_IGNORE;
    }
    
    /* pasring arguments */
    debug = 0;
    if (strcmp(argv[0], "ldap") == 0) {
        if (argc < 5) {
            pam_syslog(pamh, LOG_ERR, "Wrong number of parameters for ldap mode");
            return PAM_IGNORE;
        }
        if ((argc > 5) && (strcmp(argv[5], "debug") == 0))
            debug = 1;
            
        mode = MODE_LDAP;
    } 
    else if (strcmp(argv[0], "end") == 0) {
        if ((argc > 1) && (strcmp(argv[1], "debug") == 0))
            debug = 1;
        mode = MODE_END;
    }
    else {
        debug = 1;
        mode = MODE_NONE;
    } 
    
    /* firstly retrieve actual user name */
    pam_get_user (pamh, &username, NULL);
    
    switch (mode) {

        /* switch user name with corresponding ldap entry attr */
        case MODE_LDAP:
 	        pam_syslog(pamh, LOG_DEBUG, "Trying to retrieve ldap entry for user %s on %s:%s with dn %s", username, argv[1], argv[2], argv[3]);
            
            if ((ldap = ldap_open(argv[1], atoi(argv[2]))) == NULL) {
                pam_syslog(pamh, LOG_ERR, "unable to open LDAP %s:%s", argv[1], argv[2]);
                return PAM_AUTH_ERR;
            }
            
            snprintf(filter, 256, "(uid=%s)", username);
            rc = ldap_search_s (ldap, argv[3], LDAP_SCOPE_SUBTREE, filter, NULL, 0, &res);
            
            if (rc != LDAP_SUCCESS && rc != LDAP_TIMELIMIT_EXCEEDED && rc != LDAP_SIZELIMIT_EXCEEDED) {
                pam_syslog(pamh, LOG_ERR, "ldap_search_s %s", ldap_err2string (rc));
                return PAM_USER_UNKNOWN;
            }
	        
	        msg = ldap_first_entry (ldap, res);
            if (msg == NULL) {
                ldap_msgfree (res);
                pam_syslog(pamh, LOG_ERR, "msg == NULL");
                return PAM_USER_UNKNOWN;
            }
            
            pam_syslog(pamh, LOG_DEBUG, "Trying to retrieve entry attribute %s", argv[4]);
            if (_get_ldap_value (ldap, msg, argv[4], ldap_username) == PAM_AUTHINFO_UNAVAIL) {
            
                pam_syslog(pamh, LOG_DEBUG, "Unable to retrieve LDAP uid for %s", username);
                return PAM_USER_UNKNOWN;
            }
            
            pam_syslog(pamh, LOG_DEBUG, "LDAP attribute found, %s:%s", argv[4], ldap_username);
            
            snprintf(env, 256, "ORIG_USER=%s", username);
            pam_putenv(pamh, env);
            snprintf(env, 256, "LDAP_USER=%s", ldap_username);
            pam_putenv(pamh, env);
            
            pam_syslog(pamh, LOG_NOTICE, "LDAP authentication for %s (%s)", ldap_username, username);
            
            pam_set_item(pamh, PAM_USER, ldap_username);
            
	        break;
	        
	    /* restore user name */
        case MODE_END:
        
            switched_name = pam_getenv(pamh, "ORIG_USER");
            if (switched_name == NULL) {
                pam_syslog(pamh, LOG_DEBUG, "No user previously switched");
                return PAM_IGNORE;
            }
            
            snprintf(original_username, 256, "%s", switched_name);
            pam_set_item(pamh, PAM_USER, original_username);
            
            pam_syslog(pamh, LOG_DEBUG, "Restore user name to %s", original_username);
            
            break;
        
        default:
        
            pam_syslog(pamh, LOG_DEBUG, "Current user name %s", username);
    }
 
    return PAM_SUCCESS;
}    

PAM_EXTERN
int pam_sm_authenticate(pam_handle_t * pamh, int flags, int argc, const char **argv)
{
    return _switch_user_name(pamh, flags, argc, argv);

}

PAM_EXTERN
int pam_sm_setcred(pam_handle_t * pamh, int flags,
                   int argc, const char **argv)
{
    return PAM_IGNORE;
}

PAM_EXTERN
int pam_sm_open_session(pam_handle_t * pamh, int flags, int argc ,const char **argv)
{
    const char * username, * ldap_username;
    
    /* firstly retrieve actual user name */
    pam_get_user (pamh, &username, NULL);                                      
    
    /* retrieve registrerd ldap user name*/
    ldap_username = pam_getenv(pamh, "LDAP_USER");
    
    pam_syslog(pamh, LOG_NOTICE, "Session opened for %s (%s)", ldap_username, username);
    return PAM_IGNORE;
}

PAM_EXTERN
int pam_sm_close_session(pam_handle_t * pamh, int flags, int argc ,const char **argv)
{
    const char * username, * ldap_username;
    
    /* firstly retrieve actual user name */
    pam_get_user (pamh, &username, NULL);
    
    /* retrieve registrerd ldap user name*/
    ldap_username = pam_getenv(pamh, "LDAP_USER");
    
    pam_syslog(pamh, LOG_NOTICE, "Session closed for %s (%s)", ldap_username, username);
    return PAM_IGNORE;
}


