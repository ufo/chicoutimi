Name:           pam_switchuser
Version:        0.2
Release:        1%{?dist}
Summary:        Pam module to switch user beetween two pam module call 

BuildArch:      i386
Group:          Applications/System
License:        GPLv2
URL:            http://www.glumol.com
Source0:        http://www.glumol.com/chicoutimi/%{name}-%{version}.tar.gz
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)  
Requires:       pam_switchuser
BuildRequires:  openldap-devel

%description
Pam module to switch user beetween two pam module call 

%prep
%setup -q


%build
make build %{?_smp_mflags}


%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT


%post


%preun


%clean
rm -rf $RPM_BUILD_ROOT


%files
%defattr(-,root,root,-)
%doc README COPYING
/lib/security/pam_switchuser.so


%changelog
* Tue Apr 28 2009 Kevin	Pouget <pouget@agorabox.org>
Initial release
