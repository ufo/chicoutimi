%{!?python_sitelib: %define python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib()")}
%{!?python_sitearch: %define python_sitearch %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib(1)")}
%{!?wxpython_dir: %define wxpython_dir %(%{__python} -c "import wx, os.path; print os.path.dirname(wx.__file__)")}

%define 	add_to_doc_files()	\
	mkdir -p %{buildroot}%{_docdir}/%{name}-%{version}; \
	cp -p %1  %{buildroot}%{_docdir}/%{name}-%{version}/$(echo '%1' | sed -e 's!/!.!g') \
	echo %%{_docdir}/%{name}-%{version}/$(echo '%1' | sed -e 's!/!.!g') >> docfiles.list

%define		svn_revision	34522

Name:		WebKit
Version:	1.0.0
Release:	0.8.svn%{svn_revision}%{?dist}
Summary:	Web content engine library

Group:		Development/Libraries
License:	LGPLv2+ and BSD
URL:		http://webkit.org/

Source0:	http://nightly.webkit.org/files/trunk/src/WebKit-r%{svn_revision}.tar.bz2

# Patch0: 	%{name}-gcc43.patch
Patch0:		%{name}-wxpython.patch

BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:	bison
BuildRequires:	curl-devel
BuildRequires:	flex
BuildRequires:	gperf
BuildRequires:	gtk2-devel
BuildRequires:	libicu-devel	
BuildRequires:	libtool
BuildRequires:	libxslt-devel
BuildRequires:	pango-devel
BuildRequires:	pcre-devel
## Necessary for qmake and friends.
BuildRequires:	qt4-devel
BuildRequires:	sqlite-devel

%description 
WebKit is an open source web browser engine.

%package	gtk
Summary:	GTK+ port of WebKit 
Group:		Development/Libraries

%description	gtk
%{name} is an open-source Web content engine library. This package contains
the shared libraries for the WebKit GTK+ port as well as the sample
GtkLauncher tool. 


%package	gtk-devel
Summary:	Development files for %{name}
Group:		Development/Libraries
Requires:	%{name}-gtk = %{version}-%{release}
Requires:	pkgconfig
Requires:	gtk2-devel

%description	gtk-devel
The %{name}-gtk-devel package contains libraries, build data, and header
files for developing applications that use %{name}-gtk.
Please note that the WebKit/GTK+ API is not yet stable. This should
only be used as a "preview" rather than a stable platform library.


%package	qt
Summary:	Qt port of WebKit 
Group:		Development/Libraries

%description	qt
%{name} is an open-source Web content engine library. This package contains
the shared libraries for the WebKit Qt port as well as the sample QtLauncher
tool. 


%package	qt-devel
Summary:	Development files for %{name}
Group:		Development/Libraries
Requires:	%{name}-qt = %{version}-%{release}
Requires:	pkgconfig
Requires:	qt4-devel

%description	qt-devel
The %{name}-qt-devel package contains libraries, build data, and header files
for developing applications that use %{name}-qt, as well as the DumpRenderTree
tool.


%package	wxPython
Summary:	wxPython backend (and frontend) for WebKit
Group:		Development/Libraries
Requires:	%{name}-gtk = %{version}-%{release}
Requires:	wxPython
BuildRequires:  wxPython-devel

%description	wxPython
The goal of the wxWebKit project is to take Apple's WebKit (aka Safari) and
port it to Windows and Linux using the wxWidgets GUI toolkit. This will enable
wxWidgets applications developers to be able to embed a modern,
standards-compliant HTML/CSS/DHTML renderer into their applications. The widget
will also support HTML Editing, so it can be used for rich text editing.


%package	doc
Summary:	Documentation for %{name}
Group:		Documentation

%description	doc
%{name} is an open-source Web content engine library. This package contains
the documentation for %{name}, including various LICENSE, README, and
AUTHORS files.


%prep
%setup -qn "%{name}-r%{svn_revision}"
# %patch0 -b .gcc43
%patch0 -p1


%build
## We don't like pre-built binaries, especially ones for other OSes. =)
rm -r WebKitLibraries/{*.a,win/}

## Fix permissions for the debuginfo generation.
chmod a-x WebCore/dom/Clipboard.cpp

## Build the GTK+ port...
mkdir build-gtk
cp -a $(echo * | sed -e 's/build-gtk //') build-gtk
pushd build-gtk
	./autogen.sh
	%configure --with-font-backend=pango --enable-icon-database
	make %{?_smp_mflags}
popd

## wxPython bindings
WebKitTools/wx/build-wxwebkit wxpython

## ...and the Qt port. 
install -d build-qt
pushd build-qt
	%{_qt4_qmake} -r				\
		OUTPUT_DIR="$PWD"			\
		QMAKE_STRIP=/bin/true			\
		QMAKE_RPATH=				\
		QMAKE_CFLAGS="%{optflags}"		\
		QMAKE_CXXFLAGS="%{optflags}"		\
		VERSION=%{version}			\
		CONFIG-=gtk				\
		CONFIG+=qt-port				\
		WEBKIT_LIB_DIR=%{_libdir}		\
		../WebKit.pro
	make %{?_smp_mflags}
popd


%install
rm -rf %{buildroot}

make -C build-gtk install DESTDIR=%{buildroot}
install -d -m 755 %{buildroot}%{_libexecdir}/%{name}
install -m 755 build-gtk/Programs/GtkLauncher %{buildroot}%{_libexecdir}/%{name}

mkdir -p %{buildroot}/%{wxpython_dir}
cd WebKitBuild/Release
cp webview.py _webview.so %{buildroot}/%{wxpython_dir}
cp libwxwebkit.so %{buildroot}/%{_libdir}
cd ../..

make -C build-qt install INSTALL_ROOT=%{buildroot}
install -m 755 build-qt/bin/QtLauncher %{buildroot}%{_libexecdir}/%{name}
install -m 755 build-qt/bin/DumpRenderTree %{buildroot}%{_libexecdir}/%{name}


## Cleanup the pkgconfig files a bit...
pushd %{buildroot}%{_libdir}/pkgconfig/
	for WEBKIT_LIB in QtWebKit; do
		sed -i -e 's!prefix=.*!prefix=%{_prefix}!' ${WEBKIT_LIB}.pc
		## We don't need linkage that the WebKit DSOs already ensure.
		sed -i -e  "s/^Libs:.*$/Libs: -L\${libdir} -l${WEBKIT_LIB}/" ${WEBKIT_LIB}.pc
	done
popd

## Finally, copy over and rename the various files for %%doc inclusion.
rm -f docfiles.list
%add_to_doc_files JavaScriptCore/COPYING.LIB
%add_to_doc_files JavaScriptCore/icu/LICENSE
%add_to_doc_files JavaScriptGlue/LICENSE
%add_to_doc_files WebKit/LICENSE
%add_to_doc_files WebCore/LICENSE-APPLE
%add_to_doc_files WebCore/LICENSE-LGPL-2.1
%add_to_doc_files WebCore/icu/LICENSE

%add_to_doc_files JavaScriptCore/icu/README
%add_to_doc_files WebCore/icu/README

%add_to_doc_files JavaScriptCore/AUTHORS
%add_to_doc_files JavaScriptCore/pcre/AUTHORS   

%add_to_doc_files JavaScriptCore/THANKS


%clean
rm -rf %{buildroot}


%post	gtk -p /sbin/ldconfig

%postun	gtk -p /sbin/ldconfig


%post	qt -p /sbin/ldconfig

%postun	qt -p /sbin/ldconfig


%files	gtk
%defattr(-,root,root,-)
%doc
%{_libdir}/libwebkit-1.0.so.*
%dir %{_libexecdir}/WebKit/
%{_libexecdir}/WebKit/GtkLauncher

%files	gtk-devel
%defattr(-,root,root,-)
%exclude %{_libdir}/*.la
%{_includedir}/webkit-1.0
%{_libdir}/libwebkit-1.0.so
%{_libdir}/pkgconfig/webkit-1.0.pc

%files	wxPython
%defattr(-,root,root,-)
# %{python_sitelib}/wx-2.8-gtk2-unicode/wx/webview.py
# %{python_sitelib}/wx-2.8-gtk2-unicode/wx/_webview.so
/usr/lib/python2.5/site-packages/wx-2.8-gtk2-unicode/wx/webview.py
/usr/lib/python2.5/site-packages/wx-2.8-gtk2-unicode/wx/webview.pyc
/usr/lib/python2.5/site-packages/wx-2.8-gtk2-unicode/wx/webview.pyo
/usr/lib/python2.5/site-packages/wx-2.8-gtk2-unicode/wx/_webview.so
%{_libdir}/libwxwebkit.so

%files	qt
%defattr(-,root,root,-)
%{_libdir}/libQtWebKit.so.*
%dir %{_libexecdir}/WebKit/
%{_libexecdir}/WebKit/QtLauncher

%files	qt-devel
%defattr(-,root,root,-)
%{_includedir}/QtWebKit/
%{_libdir}/libQtWebKit.prl
%{_libdir}/libQtWebKit.so
%{_libdir}/pkgconfig/QtWebKit.pc
%{_libexecdir}/WebKit/DumpRenderTree
# %{_qt4_plugindir}/imageformats/libqtwebico.so
%{_qt4_prefix}/mkspecs/features/qtwebkit.prf

%files	doc -f docfiles.list
%defattr(-,root,root,-)


%changelog
* Sat Apr 12 2008 Peter Gordon <peter@thecodergeek.com> 1.0.0-0.8.svn31787
- Update to new upstream snapshot (SVN 31787).
- Resolves: CVE-2008-1010 (bug 438532: Arbitrary code execution) and
  CVE-2008-1011 (bug 438531: Cross-Site Scripting).
- Switch to using autotools for building the GTK+ port.

* Wed Mar 05 2008 Peter Gordon <peter@thecodergeek.com> 1.0.0-0.7.svn30667
- Fix the WebKitGtk pkgconfig data (should depend on gtk+-2.0). Resolves
  bug 436073 (Requires: gtk+-2.0 missing from WebKitGtk.pc).
- Thanks to Mamoru Tasaka for helping find and squash these many bugs. 
  
* Sat Mar 01 2008 Peter Gordon <peter@thecodergeek.com> 1.0.0-0.6.svn30667
- Fix include directory naming. Resolves: bug 435561 (Header file <> header
  file location mismatch)
- Remove qt4-devel runtime dependency and .prl file from WebKit-gtk-devel.
  Resolves: bug 433138 (WebKit-gtk-devel has a requirement on qt4-devel) 

* Fri Feb 29 2008 Peter Gordon <peter@thecodergeek.com> 1.0.0-0.5.svn30667
- Update to new upstream snapshot (SVN 30667)
- Add some build fixes for GCC 4.3:
  + gcc43.patch

* Tue Feb 19 2008 Fedora Release Engineering <rel-eng@fedoraproject.org> - 1.0.0-0.5.svn29336
- Autorebuild for GCC 4.3

* Wed Jan 09 2008 Peter Gordon <peter@thecodergeek.com> 1.0.0-0.4.svn29336
- Update to new upstream snapshot (SVN 29336).
- Drop TCSpinLock pthread workaround (fixed upstream):
  - TCSpinLock-use-pthread-stubs.patch

* Thu Dec 06 2007 Peter Gordon <peter@thecodergeek.com> 1.0.0-0.3.svn28482
- Add proper %%defattr line to qt, qt-devel, and doc subpackages.
- Add patch to forcibly build the TCSpinLock code using the pthread
  implementation:
  + TCSpinLock-use-pthread-stubs.patch

* Thu Dec 06 2007 Peter Gordon <peter@thecodergeek.com> 1.0.0-0.2.svn28482
- Package renamed from WebKitGtk.
- Update to SVN 28482.
- Build both the GTK and Qt ports, putting each into their own respective
  subpackages.
- Invoke qmake-qt4 and make directly (with SMP build flags) instead of using
  the build-webkit script from upstream.
- Add various AUTHORS, README, and LICENSE files (via the doc subpackage). 

* Tue Dec 04 2007 Peter Gordon <peter@thecodergeek.com> 1.0.0-0.1.svn28383
- Initial packaging for Fedora.