SUBDIRS=agorabox agorabox-conf agorabox-ui \
        agorabox-yum-repository aufs2-util augeas-ext authconfig cairo-dock coda \
        codamount compcache cryptmount dracut-vbox dracut-ufo firstboot \
        gnome-panel gnome-session grubby hwprofile initrd initscripts kernel kernel-vbox \
        krb5-auth-dialog libufogen libxklavier lwp \
        mtd-utils nautilus PackageKit pam_connected pam_findkey pam_kcoda pam_mount \
        pam_service pam_switchuser pam_vbox penitentiary-setup \
        picasafs plymouth pynfs readahead rpc2 rvm sreadahead slim \
        skype-yum-repository tint2 tsumufs tsumufsmount ufo-liveusb-creator \
        ufo-release vbox-additions vlaunch \
        VirtualBox-OSE VirtualBox-OSE-kmod VirtualBox-OSE-guest-kmod \
        yum-compressed

default: rpm

setup:
	if [ -z ~/rpmbuild ]; then \
		(echo "You must install the 'rpmdevtools' package and run 'rpmdev-setuptree'") \
	fi
	for d in $(SUBDIRS); do \
	(cd $$d; $(MAKE) setup) \
	done

allrpm:
	for d in $(SUBDIRS); do \
	($(MAKE) $$d); \
	# find $$d -name "*.rpm" -exec scp {} $(USER)@kickstart.agorabox.org:repo \; ; \
	done

virtualbox: VirtualBox-OSE VirtualBox-OSE-guest-kmod VirtualBox-OSE-kmod 

allsrpm:
	for d in $(SUBDIRS); do \
	(cd $$d; $(MAKE) srpm) \
	done

FORCE:

$(SUBDIRS): FORCE
	[ -z "$(FEDORA_VERSION)" ] && VERSION=12; \
	if [ "$@" = "kernel" ]; then \
	    cd $@ && make kernel &> make.log; \
	elif [ "$@" = "kernel-vbox" ]; then \
	    cd kernel && make kernel-vbox upload-kickstart &> make.log; \
	else \
	    cd $@ && RPM="rpmbuild --sign" $(MAKE) rpm upload-kickstart &> make.log; \
	fi; \
	if [ $$? -eq 0 ]; then \
	    echo $@ successfully built; \
	else \
	    echo Failed building $@; \
	fi;  \
	# cd $@; \
	nbrpm=`find . -name "*.rpm" | wc -l`; \
	if [ $$nbrpm -lt 1 ]; then \
	    echo No rpm was built...; \
	    exit 1; \
	fi; \

updaterepo:
	ssh $(USER)@kickstart sudo createrepo --groupfile=/var/www/html/yum/$$VERSION/i386/repodata/comps-handy.xml --update /var/www/html/yum/$$VERSION/i386

allclean:
	for d in $(SUBDIRS); do \
	(cd $$d; $(MAKE) clean); \
	find $$d -name "*.rpm" -exec rm {} \; ; \
	done
	rm -rf Makefile~

