Name:           google-yum-repository
Version:        1.0
Release:        1%{?dist}
Summary:        The Google yum repository

Group:          System Environment/Base
License:        GPL
URL:            http://www.glumol.com/~bob/yum
Source0:        http://www.glumol.com/~bob/yum/google-yum-repository-1.0.tar.gz
BuildArch:      i386
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

Requires:       yum

%description
The Google yum repository

%prep
%setup -q


%build


%install
rm -rf $RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT/etc/yum.repos.d
cp google.repo $RPM_BUILD_ROOT/etc/yum.repos.d/

%clean
rm -rf $RPM_BUILD_ROOT


%files
%defattr(-,root,root,-)
%doc
/etc/yum.repos.d/google.repo


%changelog
