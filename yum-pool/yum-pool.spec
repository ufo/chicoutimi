Name:           yum-pool
Version:        1.0
Release:        1%{?dist}
BuildArch:      noarch
Summary:        Installs rpms into a pool

Group:          Applications/Tools
License:        GPL
URL:            http://www.chicoutimi.org/yum-pool
Source0:        http://www.chicoutimi.org/yum-pool-1.0.tar.gz
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

Requires:       python

%description
yum-pool installs softwares into a pool.
A size is assigned to this pool. If the pool is full,
the least used softwares are uninstalled.

%prep
%setup -q


%build


%install
rm -rf $RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT/usr/bin
mkdir -p $RPM_BUILD_ROOT/usr/sbin
cp yum-pool $RPM_BUILD_ROOT/usr/sbin
ln -s consolehelper $RPM_BUILD_ROOT/usr/bin/yum-pool
mkdir -p $RPM_BUILD_ROOT/etc/pam.d
cp yum-pool.pam $RPM_BUILD_ROOT/etc/pam.d/yum-pool
mkdir -p $RPM_BUILD_ROOT/etc/security/console.apps
cp yum-pool.security $RPM_BUILD_ROOT/etc/security/console.apps/yum-pool
mkdir -p $RPM_BUILD_ROOT/var/lib/yum-pool

%clean
rm -rf $RPM_BUILD_ROOT


%files
%defattr(-,root,root,-)
%doc
/usr/bin/yum-pool
/usr/sbin/yum-pool
/etc/pam.d/yum-pool
/etc/security/console.apps/yum-pool
%dir /var/lib/yum-pool

%changelog
* Mon Aug 27 2007 Bob <bob@glumol.com>
  Initial release
