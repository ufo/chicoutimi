Name:           sreadahead
Version:        1.0
Release:        1%{?dist}
Summary:        Makes boot faster

BuildArch:      i386
Group:          Applications/System
License:        GPLv2
URL:            http://code.google.com/p/sreadahead/
Source0:        http://sreadahead.googlecode.com/files/sreadahead-1.0.tar.gz
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)


%description
Makes boot faster by reading files before using them.


%prep
%setup -q
#%patch0 -p1


%build
make %{?_smp_mflags}


%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT/var/lib/sreadahead/debugfs


%post


%preun


%clean
rm -rf $RPM_BUILD_ROOT


%files
%defattr(-,root,root,-)
%doc README COPYING Changelog
/sbin/sreadahead
%dir /var/lib/sreadahead
%dir /var/lib/sreadahead/debugfs


%changelog
* Wed Nov 18 2009 Sylvain Baubeau <sylvain.baubeau@agorabox.org> 
Updated to 1.0

* Tue Oct 30 2008 Kévin Pouget <vienin23@gmail.com>
Initial release
