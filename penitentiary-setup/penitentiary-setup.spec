Name:           penitentiary-setup
Version:        0.2
Release:        4%{?dist}
Summary:        Install penitentiary configurations files

BuildArch:      noarch
Group:          Applications/System
License:        GPLv2
URL:            http://www.glumol.com
Source0:        http://www.glumol.com/chicoutimi/%{name}-%{version}.tar.gz
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)       
Requires:       augeas authconfig openldap-clients openldap nss_ldap pam_findkey >= 0.2 pam_mount pam_service agorabox pam_switchuser >= 0.2


%package utes
Summary: Install specific penitentiary configurations files for UTES
Group: Applications/System
Requires: penitentiary-setup = %{version}-%{release}


%package lip6
Summary: Install specific penitentiary configurations files for LIP6
Group: Applications/System
Requires: penitentiary-setup = %{version}-%{release}


%description
Install penitentiary configurations files


%description utes
Install specific penitentiary configurations files for UTES


%description lip6
Install specific penitentiary configurations files for LIP6


%prep
%setup -q


%build


%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT


%post
# copying files manually 
conf_path="/usr/share/agorabox/penitentiary"

cp -f ${conf_path}/chroot.conf ${conf_path}/findkey.conf \
${conf_path}/pam_mount.conf.xml /etc/security/
cp -f ${conf_path}/ldap.conf /etc/openldap/ldap.conf
cp -f ${conf_path}/nsswitch.conf /etc/nsswitch.conf
cp -f ${conf_path}/ntp.conf /etc/ntp.conf.conf

# configuring ldap authentication
ln -f -s /etc/openldap/ldap.conf /etc/ldap.conf

# rebuild system-auth
cp -f ${conf_path}/penitentiary-system-auth /etc/pam.d/system-auth
cp -f ${conf_path}/penitentiary-system-auth-session /etc/pam.d/penitentiary-system-auth-session

# creating jail mountpoint
mkdir /jail 2>/dev/null

# disable vboxadd-timesync
chkconfig vboxadd-timesync off

# enable penitentiary-gdm-user-list
chkconfig penitentiary-beta-workaround on

# restore default login manager (gdm)
rm -f /etc/sysconfig/desktop


%post utes
conf_path="/usr/share/agorabox/penitentiary"
ln -s -f /etc/ldap-utes.conf /etc/ldap-local.conf 2>/dev/null
cp -f ${conf_path}/rsyslog-utes.conf /etc/rsyslog.conf


%post lip6
conf_path="/usr/share/agorabox/penitentiary"
ln -s -f /etc/ldap-lip6.conf /etc/ldap-local.conf 2>/dev/null
cp -f ${conf_path}/rsyslog-lip6.conf /etc/rsyslog.conf


%preun


%clean
rm -rf $RPM_BUILD_ROOT


%files
%defattr(-,root,root,-)
%doc
/etc/init.d/penitentiary-beta-workaround
%{_datadir}/agorabox/penitentiary/ldap.conf
%{_datadir}/agorabox/penitentiary/nsswitch.conf
%{_datadir}/agorabox/penitentiary/ntp.conf
%{_datadir}/agorabox/penitentiary/chroot.conf
%{_datadir}/agorabox/penitentiary/findkey.conf
%{_datadir}/agorabox/penitentiary/pam_mount.conf.xml
%{_datadir}/agorabox/penitentiary/penitentiary-system-auth
%{_datadir}/agorabox/penitentiary/penitentiary-system-auth-session


%files utes
/etc/ldap-utes.conf
%{_datadir}/agorabox/penitentiary/rsyslog-utes.conf


%files lip6
/etc/ldap-lip6.conf
%{_datadir}/agorabox/penitentiary/rsyslog-lip6.conf


%changelog
* Fri Dec 19 2008 Kevin	Pouget <kevin.pouget@agorabox.org>
Initial release
