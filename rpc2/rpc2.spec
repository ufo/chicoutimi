Name:           rpc2
Version:        2.8
Release:        1%{?dist}
Summary:        RPC2 library
Group:          System Environment/Libraries
License:        LGPLv2
URL:            http://www.coda.cs.cmu.edu/
Source0:        ftp://ftp.coda.cs.cmu.edu/pub/coda/src/%{name}-%{version}.tar.gz
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  lwp-devel lua-devel readline-devel flex bison

%description
The RPC2 library.


%package        devel
Summary:        Development files for %{name}
Group:          Development/Libraries
# headers are LGPLv2, rp2gen is GPLv2
License:        LGPLv2 and GPLv2
Requires:       %{name} = %{version}-%{release}, pkgconfig

%description    devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.


%prep
%setup -q


%build
./bootstrap.sh
%configure --disable-static --with-lua
# Don't use rpath!
sed -i 's|^hardcode_libdir_flag_spec=.*|hardcode_libdir_flag_spec=""|g' libtool
sed -i 's|^runpath_var=LD_RUN_PATH|runpath_var=DIE_RPATH_DIE|g' libtool
make %{?_smp_mflags}


%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT INSTALL="install -p"
find $RPM_BUILD_ROOT -name '*.la' -exec rm -f {} ';'


%clean
rm -rf $RPM_BUILD_ROOT


%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig


%files
%defattr(-,root,root,-)
%doc COPYING NEWS
%{_libdir}/*.so.*
%{_datadir}/%{name}

%files devel
%defattr(-,root,root,-)
%{_bindir}/rp2gen
%{_includedir}/%{name}
%{_libdir}/*.so
%{_libdir}/pkgconfig/%{name}.pc


%changelog
* Wed Feb 4 2009 Marc Schlinger <marc.schlinger@agorabox.fr> 2.8-1
- update to version 2.8 
* Mon May 12 2008 Hans de Goede <j.w.r.degoede@hhs.nl> 2.7-1
- Initial Fedora package
