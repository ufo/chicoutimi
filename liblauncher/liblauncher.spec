Name:           liblauncher
Version:        0.1.8
Release:        1%{?dist}
Summary:        A library to build launchers

Group:          System Environment/Libraries
License:        GPLv3
URL:            https://launchpad.net/liblauncher
Source0:        http://launchpad.net/liblauncher/0.1/0.1.8/+download/liblauncher-0.1.8.tar.gz
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  glib2-devel gtk2-devel GConf2-devel gnome-menus-devel libwnck-devel libX11-devel
Requires:       glib2-devel gtk2 GConf2 gnome-menus libwnck libX11

%description
A library to build launchers


%package        devel
Summary:        Development files for %{name}
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release}

%description    devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.


%prep
%setup -q


%build
autoreconf
%configure --disable-static
make %{?_smp_mflags}


%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT
find $RPM_BUILD_ROOT -name '*.la' -exec rm -f {} ';'


%clean
rm -rf $RPM_BUILD_ROOT


%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig


%files
%defattr(-,root,root,-)
%doc
%{_libdir}/*.so.*

%files devel
%defattr(-,root,root,-)
%doc
%{_includedir}/*
%{_libdir}/*.so
%{_libdir}/pkgconfig/launcher-0.1.pc

%changelog
