# buildforkernels macro hint: when you build a new version or a new release
# that contains bugfixes or other improvements then you must disable the
# "buildforkernels newest" macro for just that build; immediately after
# queuing that build enable the macro again for subsequent builds; that way
# a new akmod package will only get build when a new one is actually needed
#define buildforkernels newest

Name:           VirtualBox-OSE-guest-kmod
Version:        2.2.4
Release:        1%{?dist}.1

Summary:        Kernel module for VirtualBox-OSE Guest Additions
Group:          System Environment/Kernel
License:        GPLv2 or CDDL
URL:            http://www.virtualbox.org/wiki/VirtualBox
# This filters out the XEN kernel, since we don't run on XEN
Source0:        http://www.glumol.com/chicoutimi/%{name}-%{version}.tar.gz
Source1:        VirtualBox-OSE-kmod-1.6.4-kernel-variants.txt
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%global AkmodsBuildRequires %{_bindir}/kmodtool, VirtualBox-OSE-guest-kmodsrc = %{version}
BuildRequires:  %{AkmodsBuildRequires}

Requires:       VirtualBox-OSE-guest = %{version}-%{release}

# needed for plague to make sure it builds for i586 and i686
ExclusiveArch:  i586 i686 x86_64

# get the proper build-sysbuild package from the repo, which
# tracks in all the kernel-devel packages
%{!?kernels:BuildRequires: buildsys-build-rpmfusion-kerneldevpkgs-%{?buildforkernels:%{buildforkernels}}%{!?buildforkernels:current}-%{_target_cpu} }

# kmodtool does its magic here
%{expand:%(kmodtool --target %{_target_cpu} --repo rpmfusion --kmodname %{name} %{?buildforkernels:--%{buildforkernels}} %{?kernels:--for-kernels "%{?kernels}"} --filterfile %{SOURCE1} 2>/dev/null) }


%description
Kernel module for VirtualBox-OSE Guest Additions


%prep
%setup -T -c
tar --use-compress-program lzma -xf %{_datadir}/%{name}-%{version}/%{name}-%{version}.tar.lzma

# error out if there was something wrong with kmodtool
%{?kmodtool_check}

# print kmodtool output for debugging purposes:
kmodtool --target %{_target_cpu}  --repo rpmfusion --kmodname %{name} %{?buildforkernels:--%{buildforkernels}} %{?kernels:--for-kernels "%{?kernels}"} --filterfile %{SOURCE1} 2>/dev/null


for kernel_version in %{?kernel_versions} ; do
    cp -al %{name}-%{version} _kmod_build_${kernel_version%%___*}
done


%build
for kernel_version in %{?kernel_versions}; do
    for module in vbox{add,vfs}; do
        make VBOX_USE_INSERT_PAGE=1 %{?_smp_mflags} -C "${kernel_version##*___}" SUBDIRS="${PWD}/_kmod_build_${kernel_version%%___*}/${module}"  modules
    done
done


%install
rm -rf $RPM_BUILD_ROOT
mkdir $RPM_BUILD_ROOT

for kernel_version in %{?kernel_versions}; do
    install -d ${RPM_BUILD_ROOT}%{kmodinstdir_prefix}/${kernel_version%%___*}/%{kmodinstdir_postfix}
    install _kmod_build_${kernel_version%%___*}/*/*.ko ${RPM_BUILD_ROOT}%{kmodinstdir_prefix}/${kernel_version%%___*}/%{kmodinstdir_postfix}
done

%{?akmod_install}


%clean
rm -rf $RPM_BUILD_ROOT


%changelog
* Wed Jun 24 2009 kevin Pouget <pouget@agorabox.org> - 1.5.4-1
- initial spec, split out from VirtualBox-OSE-kmod spec
