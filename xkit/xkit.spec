%define name xkit
%define version trunk
%define unmangled_version trunk
%define release 1

%{!?python_sitelib: %define python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib()")}
%{!?python_sitearch: %define python_sitearch %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib(1)")}

Summary: library for the manipulation of the xorg.conf
Name: %{name}
Version: %{version}
Release: %{release}
Source0: %{name}-%{unmangled_version}.tar.gz
License: GPL v2 or later
Group: Development/Libraries
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-buildroot
Prefix: %{_prefix}
Requires: python
BuildArch: noarch
Vendor: Alberto Milone <albertomilone@alice.it>
Url: https://launchpad.net/x-kit

%description
A kit to manipulate xorg.conf which is aimed at both developers and users. 
It is a distribution and desktop agnostic project.

X-kit is composed of the following programs:

* XorgParser: a simple, transparent and easy to extend xorg parser.
* XorgValidator: a program which uses X-parser, tries to make sense of xorg.conf and operates accordingly.
* XorgConfig-gtk: a simple GUI to xorg.conf
* XorgConfig-kde: a simple GUI to xorg.conf

%prep
%setup -n %{name}-%{unmangled_version}

%build
python setup.py build

%install
python setup.py install --root=$RPM_BUILD_ROOT

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root)
%{python_sitelib}/*
