getmountarg() {
    local o
    for o in $VBOXCMDLINE; do
	[ "$o" = "$1" ] && return 0
	[ "${o%%=*}" = "${1%=}" ] && { echo ${o#*=}; return 0; }
    done
    getarg $1
    return $?
}

founddev=""
rootdev="$(getmountarg root=)"
if [ "${rootdev%%=*}" == "UUID" ] || [ "${rootdev%%=*}" == "LABEL" ]; then
    tries=$(( $timeout * 5 ))   
    while [ $tries -gt 0 ] && [ "${founddev::7}" != "/dev/sd" ]; do
        founddev=`blkid -c /dev/null -t $rootdev -o device 2>/dev/null`
        result=$?
        tries=$(( $tries - 1 ))  
        sleep 0.2
    done
fi

if [ $result -eq 0 ]; then
    rootdev="$founddev"
    root="$rootdev"
    rootok=1
else
    emergency_shell "Didn't find root device"
    die "Didn't find root device $rootdev"
fi

mkdir /aufs /ro /rw /compress 2> /dev/null

# Mount root as read-only branch
if rootopts="$(getarg rootflags=)"; then
    rootopts="ro,noatime,$rootopts"
else
    rootopts="ro,noatime"
fi

mount -t ${fstype:-auto} -o "$rootopts" "$rootdev" "$NEWROOT" 2> /dev/null

# Mount overlay
failed=1
overlay=1
overlayarg="$(getmountarg overlay=)"
rootfree=`df $NEWROOT | tail -n 1 | awk '{ print $4 }'`
while [ 1 ]; do
    if [ "${overlayarg%%=*}" == "tmpfs" ]; then

        # Mount tmpfs as read-write branch
        mount -t tmpfs -o "noatime,size=${rootfree}k" overlaytmpfs /rw 2> /dev/null && failed=0

        if [ $failed -eq 1 ]; then 
            getmountarg "notryothers" || overlayarg=""; continue
            die "Cannot mount tmpfs overlay device"
        fi
        break
    elif [ ! "${overlayarg%%=*}" == "" ]; then

        # Find overlay device
        overlayfs="${overlayarg%%=*}"
        overlaydev="${overlayarg#*=}"
        if [ "${overlaydev%%=*}" == "UUID" ] || 
           [ "${overlaydev%%=*}" == "LABEL" ]
        then
            overlaydev=`findfs "$overlaydev" 2>/dev/null`
        fi
        if [ -e "$overlaydev" ]; then

            # Resize overlay to the available space left on root
            [ ! -f /etc/mtab ] && busybox touch /etc/mtab
            resize2fs -f $overlaydev ${rootfree}K >/dev/null 2>&1
            if [ $? -ne 0 ]; then
                busybox yes | mkfs.ext4 -O ^large_file,^huge_file,^has_journal -b 1024 $overlaydev ${rootfree} >/dev/null 2>&1
            fi

            # Mount overlay device as read-write branch
            mount -t "$overlayfs" -o "ro,noatime" "$overlaydev" /rw 2> /dev/null && failed=0
            mount -o remount,rw /rw 2> /dev/null

            # Remove obsolete datas
	    find /rw -maxdepth 1 -regex "/rw/.*" | xargs rm -rf
        fi
        if [ $failed -eq 1 ]; then 
            getmountarg "notryothers" || overlayarg="tmpfs"; continue
            die "Cannot use overlay device $overlaydev"
        fi
        break
    else
        overlay=0
        break
    fi
done

# Mount compressed device as read-only branch
compress=1
comprdev="$(getmountarg compress=)"
if [ $? == 0 ]; then
    if [ "${comprdev%%=*}" == "UUID" ] || [ "${comprdev%%=*}" == "LABEL" ]; then
        comprdev=`findfs "$comprdev" 2>/dev/null`
    fi
    if [ $? -eq 0 ] && [ -e "$comprdev" ]; then
        mount -t btrfs -o "ro,compress" "$comprdev" /compress 2> /dev/null
    else
        die "Cannot find compress device $comprdev"
    fi
else
    compress=0
fi

# Build aufs mount command
aufsmnt=1
if [ $overlay -eq 1 ]; then
    mount --move $NEWROOT /ro 2> /dev/null
    aufsmntopts="dirs=/rw:/ro=ro"
    movemntpts="ro rw"
    if [ $compress -eq 1 ]; then
        aufsmntopts="$aufsmntopts+wh:/compress=ro"
        movemntpts="$movemntpts compress"
    fi
elif [ $compress -eq 1 ]; then
    mount -o "rw,remount" $NEWROOT 2> /dev/null
    mount --move $NEWROOT /rw 2> /dev/null
    aufsmntopts="dirs=/rw:/compress=ro"
    movemntpts="compress rw"
else
    aufsmnt=0
fi

if [ $aufsmnt -eq 1 ]; then
    # Finaly mount aufs root file system 
    mount -t aufs -o "udba=inotify,$aufsmntopts" aufs /aufs 2> /dev/null

    for movemntpt in $movemntpts; do
        [ -d /aufs/$movemntpt ] || mkdir /aufs/$movemntpt 
        mount --move /"$movemntpt" /aufs/"$movemntpt" 2> /dev/null
    done
    mount --move /aufs $NEWROOT 2> /dev/null
else
    mount -o remount,rw $NEWROOT
fi

# Adding root to mtab, as it does not appears in fstab
grep "$NEWROOT " /proc/mounts | awk '{ print $1" / "$3" "$4" "$5" "$6 }' > $NEWROOT/etc/mtab

# Mount some cache dirs as tmpfs
mount -t tmpfs -o mode=0755 varcacheyum $NEWROOT/var/cache/yum 2> /dev/null
mount -t tmpfs tmp $NEWROOT/tmp 2> /dev/null
mount -t tmpfs vartmp $NEWROOT/var/tmp 2> /dev/null

# Debug option
if getmountarg "debug"; then
    echo "* overlay  => $overlay"
    echo "* compress => $compress"
    echo "* aufsmnt  => $aufsmnt"
    echo "* movemntpts  => $movemntpts"
    echo "* aufsmntopts => $aufsmntopts"
    emergency_shell "Debug mode"
fi
