Name:           dracut-ufo
Version:        0.1
Release:        4%{?dist}
Summary:        UFO module for Dracut

License:        GPLv2 and GPLv2+
Group:          System Environment/Base
URL:            http://www.glumol.com/chicoutimi/dracut-ufo
Source0:        http://www.glumol.com/chicoutimi/%{name}-%{version}.tar.gz
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch

BuildRequires:  dracut
Requires:       busybox dracut-vbox

%description


%prep
%setup -q


%build


%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT


%clean
rm -rf $RPM_BUILD_ROOT


%files
%defattr(-,root,root,-)
%doc
/usr/share/dracut/modules.d/60ufo


%changelog
