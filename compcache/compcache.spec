# buildforkernels macro hint: when you build a new version or a new release
# that contains bugfixes or other improvements then you must disable the
# "buildforkernels newest" macro for just that build; immediately after
# queuing that build enable the macro again for subsequent builds; that way
# a new akmod package will only get build when a new one is actually needed
#define buildforkernels newest

Name:           compcache
Version:        0.6
Release:        1%{?dist}

Summary:        Compressed in-memory swap device for Linux
Group:          System Environment/Kernel
License:        GPLv2
URL:            http://code.google.com/p/compcache/
# This filters out the XEN kernel, since we don't run on XEN
Source0:        http://compcache.googlecode.com/files/compcache-0.6.tar.gz
Source1:        compcache.init
Patch0:         compcache-enable-swap-free-notify.patch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%global AkmodsBuildRequires %{_bindir}/kmodtool
BuildRequires:  %{AkmodsBuildRequires}

Requires:       compcache = %{version}-%{release}

# needed for plague to make sure it builds for i586 and i686
ExclusiveArch:  i586 i686 x86_64

# get the proper build-sysbuild package from the repo, which
# tracks in all the kernel-devel packages
%{!?kernels:BuildRequires: buildsys-build-rpmfusion-kerneldevpkgs-%{?buildforkernels:%{buildforkernels}}%{!?buildforkernels:current}-%{_target_cpu} }

# kmodtool does its magic here
%{expand:%(kmodtool --target %{_target_cpu} --repo rpmfusion --kmodname %{name} --noakmod %{?buildforkernels:--%{buildforkernels}} %{?kernels:--for-kernels "%{?kernels}"} 2>/dev/null) }


%description
This project creates RAM based block device (named ramzswap) which acts as
swap disk. Pages swapped to this disk are compressed and stored in memory
itself.
Compressing pages and keeping them in RAM virtually increases its capacity.
This allows more applications to fit in given amount of memory. The usual
argument I get is - memory is so cheap so why bother with compression? So I list
here some of the use cases. Rest depends on your imagination :)

- Netbooks: Market is now getting flooded with these "lighweight laptops". These are memory constrained but have CPU enough to drive on compressed memory (e.g. Cloudbook features 1.2 GHz processor!). 
- Virtualization: With compcache at hypervisor level, we can compress any part of guest memory transparently - this is true for any type of Guest OS (Linux, Windows etc.). This should allow running more number of VMs for given amount of total host memory. 
- Embedded Devices: Memory is scarce and adding more memory increases device cost. Also, flash storage suffers from wear-leveling issues, so its useful if we can avoid using them as swap device.


%package -n compcache-utils
Summary: Control ramzswap devices  
Group: Applications/System
Provides:       %{name}-kmod-common = %{version}

%description -n compcache-utils
Userspace utility to setup individual ramzswap devices


%prep
%setup -n compcache-0.6 -c
%patch0

# error out if there was something wrong with kmodtool
%{?kmodtool_check}

# print kmodtool output for debugging purposes:
kmodtool --target %{_target_cpu}  --repo rpmfusion --kmodname %{name} %{?buildforkernels:--%{buildforkernels}} %{?kernels:--for-kernels "%{?kernels}"} 2>/dev/null


for kernel_version in %{?kernel_versions} ; do
    cp -al compcache-%{version} _kmod_build_${kernel_version%%___*}
done


%build
for kernel_version in %{?kernel_versions}; do
    make %{?_smp_mflags} -C "${kernel_version##*___}" M="${PWD}/_kmod_build_${kernel_version%%___*}/sub-projects/allocators/xvmalloc-kmod" modules
    CFLAGS=-DCONFIG_BLK_DEV_RAMZSWAP_STATS make %{?_smp_mflags} -C "${kernel_version##*___}" M="${PWD}/_kmod_build_${kernel_version%%___*}/" modules
done
make -C compcache-%{version}/sub-projects/rzscontrol
make -C compcache-%{version}/sub-projects/rzscontrol doc


%install
rm -rf $RPM_BUILD_ROOT
mkdir $RPM_BUILD_ROOT

for kernel_version in %{?kernel_versions}; do
    find _kmod_build_${kernel_version%%___*}
    install -d ${RPM_BUILD_ROOT}%{kmodinstdir_prefix}/${kernel_version%%___*}/%{kmodinstdir_postfix}
    find _kmod_build_${kernel_version%%___*} -name *.ko -exec install {} ${RPM_BUILD_ROOT}%{kmodinstdir_prefix}/${kernel_version%%___*}/%{kmodinstdir_postfix} \;
done
install -D -m 755 compcache-%{version}/sub-projects/rzscontrol/rzscontrol ${RPM_BUILD_ROOT}/usr/sbin/rzscontrol
gzip --force compcache-%{version}/sub-projects/rzscontrol/man/rzscontrol.1
install -D -m 644 compcache-%{version}/sub-projects/rzscontrol/man/rzscontrol.1.gz ${RPM_BUILD_ROOT}/usr/share/man/man1/rzscontrol.1.gz
install -D -m 755 %{SOURCE1} /${RPM_BUILD_ROOT}%{_initddir}/compcache

%{?akmod_install}


%post
/sbin/chkconfig --add %{name}


%preun 
if [ $1 -eq 0 ] ; then
/sbin/chkconfig --del %{name}
fi


%files -n compcache-utils
%doc compcache-%{version}/README
%{_initddir}/compcache
%{_sbindir}/rzscontrol
%{_mandir}/man1/rzscontrol.1.gz


%clean
rm -rf $RPM_BUILD_ROOT


%changelog
* Wed Oct 7 2009 Sylvain Baubeau <sylvain.baubeau@agorabox.org> - 0.6-1
- Updated to 0.6

* Fri Aug 14 2009 Sylvain Baubeau <sylvain.baubeau@agorabox.org> - 0.6pre3
- Updated to 0.6pre3

* Wed Jul 29 2009 Sylvain Baubeau <sylvain.baubeau@agorabox.org> - 0.6pre2
- Initial release
