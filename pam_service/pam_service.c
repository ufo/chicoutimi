/*
 * PAM module for services management.
 * This module should be used as "optional" in "session",
 * it just start or stop given services.
 *
 * 
 * Copyright (C) 2009 Kevin Pouget <pouget@agorabox.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  
 *
 *
 */
 
#include <stdio.h>
#include <unistd.h>
#include <grp.h>
#include <pwd.h>
#include <time.h>
#include <ctype.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <syslog.h>
#include <sys/wait.h>
#include <sys/types.h>

#define SERVICE_PROGRAM "/sbin/service"
#define SERVICE_START "start"
#define SERVICE_STOP "stop"
#define MAX_SERVICES 5

#include <security/pam_modules.h>
#include <security/_pam_macros.h>

static void _pam_log(int err, const char *format, ...) {
    va_list args;

    va_start(args, format);
    openlog("pam_service", LOG_PID, LOG_AUTHPRIV);
    vsyslog(err, format, args);
    va_end(args);
    closelog();
}

int _pam_service (char *service_program, char *service_name, 
                  char *service_action) 
{
    int code, status;
    char *execargs[4];
    int result = PAM_SUCCESS;
     
    /* set up arguments for service */
    execargs[0] = "service";
    execargs[3] = NULL;
    
    execargs[1] = malloc ((strlen(service_name)+1)*sizeof(char));
    strcpy (execargs[1], service_name);
    execargs[2] = malloc ((strlen(service_action)+1)*sizeof(char));
    strcpy (execargs[2], service_action);

    /* action service */
    code = fork();
    if (code == -1) {
        _pam_log(LOG_ERR, "fork failed");
        result = PAM_SESSION_ERR;
        goto out;
    }
    if (code == 0) {
        execv(service_program, execargs);
        _pam_log(LOG_CRIT, "exec returned");
        exit( 1 );
    }
    waitpid(code, &status, 0);
    _pam_log(LOG_DEBUG, "service %s %s, return %d", 
        service_name, service_action, status);
        
out:
    free( execargs[1] );
    free( execargs[2] );
    return result;
}

PAM_EXTERN
int pam_sm_authenticate(pam_handle_t * pamh, int flags,
                        int argc, const char **argv)
{
    return PAM_IGNORE;
}

PAM_EXTERN
int pam_sm_setcred(pam_handle_t * pamh, int flags,
                   int argc, const char **argv)
{
    return PAM_IGNORE;
}

PAM_EXTERN
int pam_sm_open_session(pam_handle_t * pamh, int flags,
                        int argc ,const char **argv)
{
    int retval, nbservices, ignore_root, flag = 1;
    char *service_program;
    char *services[MAX_SERVICES];
    const char *user = NULL;
    const char *ruser = NULL;
    struct passwd *pw = NULL;
    struct passwd *rpw = NULL;
    
    retval = pam_get_user(pamh, &user, NULL);
    if (retval != PAM_SUCCESS)
        return PAM_USER_UNKNOWN;
    retval = pam_get_item(pamh, PAM_RUSER, (const void **) &ruser);
    if (retval != PAM_SUCCESS)
        return PAM_SESSION_ERR;
	
    /* get user information */
    pw = getpwnam(user);
    
    /* get ruser information */    
    if (ruser != NULL)
    	rpw = getpwnam(ruser);

    /* user does not exist */
    if (pw == NULL) 
        return PAM_USER_UNKNOWN;
    
    service_program = malloc ((strlen(SERVICE_PROGRAM)+1)*sizeof(char));
    strcpy (service_program, SERVICE_PROGRAM);

    ignore_root = nbservices = 0;
    
    /* parse parameters for module */
    for (; argc-- > 0; argv++) {
        if (strcmp(*argv, "ignore_root") == 0)
	        ignore_root = 1;
	    else if (strcmp(*argv, "close") == 0)
	        flag = 0;
	    else if (strcmp(*argv, "open") == 0)
	        continue;
        else if (nbservices < MAX_SERVICES -1) {
            services[nbservices] = malloc ((strlen(*argv)+1)*sizeof(char));
            strcpy (services[nbservices++], *argv);
        }
    }

    /* handle root */
    /* Slim workaround: slim set pw->pw_uid to 0 for each users... 
    
    _pam_log(LOG_NOTICE, "loggin user: %d, ignore_root: %d", pw->pw_uid, 
        ignore_root);
    if (pw->pw_uid == 0 && ignore_root == 1) {
        result = PAM_SUCCESS;
        goto out;
    }
    */

    /* execute actions */
    while (flag && nbservices-- > 0) {
        _pam_service (service_program, services[nbservices], SERVICE_STOP);
        _pam_service (service_program, services[nbservices], SERVICE_START);
        free(services[nbservices]);
    } 
    
    free(service_program);
    return PAM_SUCCESS;
}

PAM_EXTERN
int pam_sm_close_session(pam_handle_t * pamh, int flags,
                         int argc ,const char **argv)
{
    int retval, nbservices, ignore_root, flag = 1;
    char *service_program;
    char *services[MAX_SERVICES];
    const char *user = NULL;
    const char *ruser = NULL;
    struct passwd *pw = NULL;
    struct passwd *rpw = NULL;
    
    retval = pam_get_user(pamh, &user, NULL);
    if (retval != PAM_SUCCESS)
        return PAM_USER_UNKNOWN;
    retval = pam_get_item(pamh, PAM_RUSER, (const void **) &ruser);
    if (retval != PAM_SUCCESS)
        return PAM_SESSION_ERR;
	
    /* get user information */
    pw = getpwnam(user);
    
    /* get ruser information */    
    if (ruser != NULL)
    	rpw = getpwnam(ruser);

    /* user does not exist */
    if (pw == NULL) 
        return PAM_USER_UNKNOWN;
    
    service_program = malloc ((strlen(SERVICE_PROGRAM)+1)*sizeof(char));
    strcpy (service_program, SERVICE_PROGRAM);

    ignore_root = nbservices = 0;
    
    /* parse parameters for module */
    for (; argc-- > 0; argv++) {
        if (strcmp(*argv, "ignore_root") == 0)
	        ignore_root = 1;
	    else if (strcmp(*argv, "close") == 0)
	        continue;
	    else if (strcmp(*argv, "open") == 0)
	        flag = 0;
        else if (nbservices < MAX_SERVICES -1) {
            services[nbservices] = malloc ((strlen(*argv)+1)*sizeof(char));
            strcpy (services[nbservices++], *argv);
        }
    }

    /* handle root */
    /* Slim workaround: slim set pw->pw_uid to 0 for each users... 
    
    _pam_log(LOG_NOTICE, "loggin user: %d, ignore_root: %d", pw->pw_uid, 
        ignore_root);
    if (pw->pw_uid == 0 && ignore_root == 1) {
        result = PAM_SUCCESS;
        goto out;
    }
    */

    /* execute actions */
    while (flag && nbservices-- > 0) {
        _pam_service (service_program, services[nbservices], SERVICE_STOP);
        free(services[nbservices]);
    } 
    
    free(service_program);
    return PAM_SUCCESS;
}

/* end of module definition */
