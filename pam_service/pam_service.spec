Name:           pam_service
Version:        0.1
Release:        1%{?dist}
Summary:        Pam module to start and stop services at open and close session stage.

BuildArch:      i386
Group:          Applications/System
License:        GPLv2
URL:            http://www.glumol.com
Source0:        http://www.glumol.com/chicoutimi/%{name}-%{version}.tar.gz
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)  

%description
Pam module to start and stop services at open and close 
session stage. Sometimes, it can be convenient to start service
after any pam modules...

%prep
%setup -q


%build
make build %{?_smp_mflags}


%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT


%post


%preun


%clean
rm -rf $RPM_BUILD_ROOT


%files
%defattr(-,root,root,-)
%doc README COPYING
/lib/security/pam_service.so


%changelog
* Wed May 13 2009 Kevin	Pouget <pouget@agorabox.org>
Initial release
