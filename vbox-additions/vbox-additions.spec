Name:           vbox-additions
Version:        2.2.0
Release:        1%{?dist}
Summary:        Install virtual box guest addtions

BuildArch:      i386
Group:          Applications/System
License:        GPLv2
URL:            http://www.glumol.com
Source0:        http://www.glumol.com/chicoutimi/vbox-additions-%{version}.tar.gz
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)  
Requires:       make gcc kernel kernel-devel

%description
Install virtual box guest addtions.
Also add creation of shared folder symlink.

%prep
%setup -q
wget http://downloads.agorabox.org/virtualization/VBoxLinuxAdditions-x86-%{version}.run

%build


%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT/var/hostshared


%post

cp -f /usr/share/vbox/xorg.conf.vbox /etc/X11/xorg.conf
useradd -d /var/run/vboxadd -g 1 -r -s /bin/sh vboxadd
/sbin/chkconfig --level 4 vboxadd on
/sbin/chkconfig --level 4 vboxadd-timesync on


%preun


%clean
rm -rf $RPM_BUILD_ROOT


%files
%defattr(-,root,root,-)
%doc ReadMe-OS2.txt  ReadMe-Solaris.txt  VBox-CodingGuidelines.cpp  VBox-doc.c COPYING  COPYING.CDDL
/usr/share/hal/fdi/policy/20thirdparty/90-vboxguest.fdi
/usr/sbin/vboxadd-timesync
/usr/share/vbox/xorg.conf.vbox
/usr/lib/xorg/modules/input/vboxmouse_drv.so
/usr/lib/xorg/modules/drivers/vboxvideo_drv.so
/usr/src/vboxadd-2.2.0
/usr/src/vboxvfs-2.2.0
/usr/src/vboxvideo-2.2.0
/usr/bin/VBoxClient
/usr/bin/VBoxClient-all  
/usr/bin/VBoxClientSymlink
/usr/bin/VBoxControl 
/usr/bin/VBoxRandR
/etc/init.d/vboxadd  
/etc/init.d/vboxadd-timesync
/etc/udev/rules.d/60-vboxadd.rules
/etc/xdg/autostart/vboxclient.desktop
/etc/xdg/autostart/vboxclientsymlink.desktop
/sbin/mount.vboxsf
/usr/lib/VBoxOGLarrayspu.so
/usr/lib/VBoxOGLcrutil.so
/usr/lib/VBoxOGLerrorspu.so
/usr/lib/VBoxOGLfeedbackspu.so
/usr/lib/VBoxOGLpackspu.so
/usr/lib/VBoxOGLpassthroughspu.so
/usr/lib/VBoxOGL.so
/usr/lib/dri/vboxvideo_dri.so

	
%dir /var/hostshared

%changelog
* Mon Apr 20 2009 Kevin	Pouget <pouget@agorabox.org>
Update from 2.1.4 to 2.2.0.
Add OpenGL drivers.

%changelog
* Fri Dec 19 2008 Kevin	Pouget <pouget@agorabox.org>
Initial release
