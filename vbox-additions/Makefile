NAME=vbox-additions
VERSION=2.2.0
SOURCES=VBoxLinuxAdditions-x86.run VBoxClientSymlink vboxclientsymlink.desktop xorg.conf.vbox COPYING COPYING.CDDL doc/ReadMe-OS2.txt  doc/ReadMe-Solaris.txt  doc/VBox-CodingGuidelines.cpp  doc/VBox-doc.c

DIR=$(NAME)-$(VERSION)
ARCHIVE=$(DIR).tar.gz
SPECFILE=$(NAME).spec
URL=http://www.glumol.com/chicoutimi/vbox-additions

ifneq ($(findstring ../Makefile.mk,$(wildcard ../Makefile.mk)), )
	include ../Makefile.mk
endif

all:

install:
	mkdir -p files
	mkdir -p $(DESTDIR)/usr/share/vbox/
	mkdir -p $(DESTDIR)/etc/init.d
	mkdir -p $(DESTDIR)/usr/src
	mkdir -p $(DESTDIR)/sbin
	mkdir -p $(DESTDIR)/usr/sbin
	mkdir -p $(DESTDIR)/usr/bin
	mkdir -p $(DESTDIR)/etc/xdg/autostart
	mkdir -p $(DESTDIR)/etc/udev/rules.d
	mkdir -p $(DESTDIR)/usr/lib/dri
	mkdir -p $(DESTDIR)/usr/lib/xorg/modules/drivers
	mkdir -p $(DESTDIR)/usr/lib/xorg/modules/input
	mkdir -p $(DESTDIR)/usr/share/hal/fdi/policy/20thirdparty

	chmod u+x VBoxLinuxAdditions-x86-$(VERSION).run
	./VBoxLinuxAdditions-x86-$(VERSION).run --noexec --target files

	cp -a files/module/vboxadd $(DESTDIR)/usr/src/vboxadd-$(VERSION)
	cp -a files/module/vboxvfs $(DESTDIR)/usr/src/vboxvfs-$(VERSION)
	cp -a files/module/vboxvideo_drm $(DESTDIR)/usr/src/vboxvideo-$(VERSION)
	cp xorg.conf.vbox $(DESTDIR)/usr/share/vbox/

	# Installing the Guest Additions kernel module system service.
	cp files/vboxadd.sh $(DESTDIR)/etc/init.d/vboxadd
	chmod 755 $(DESTDIR)/etc/init.d/vboxadd

	# Installing the shared folder system service.
	install -o 0 -g 0 -m 04755 files/mount.vboxsf $(DESTDIR)/sbin/mount.vboxsf

	# Installing the time synchronisation system service.
	install -o 0 -g 0 -m 0755 files/vboxadd-timesync $(DESTDIR)/usr/sbin/vboxadd-timesync
	cp files/vboxadd-timesync.sh $(DESTDIR)/etc/init.d/vboxadd-timesync
	chmod 755 $(DESTDIR)/etc/init.d/vboxadd-timesync

	# Installing the VBoxControl command line interface.
	install -o 0 -g 0 -m 0755 files/VBoxControl $(DESTDIR)/usr/bin/VBoxControl

	# Installing the X Window System user service.
	install -o 0 -g 0 -m 0755 files/VBoxClient $(DESTDIR)/usr/bin/VBoxClient
	install -o 0 -g 0 -m 0755 files/VBoxRandR.sh $(DESTDIR)/usr/bin/VBoxRandR
	install -o 0 -g 0 -m 0755 files/98vboxadd-xclient $(DESTDIR)/usr/bin/VBoxClient-all
	install -m 0644 files/vboxclient.desktop $(DESTDIR)/etc/xdg/autostart/vboxclient.desktop

	# Creating udev rule for the Guest Additions kernel module.
	echo "KERNEL==\"vboxadd\", NAME=\"vboxadd\", OWNER=\"vboxadd\", MODE=\"0660\"" > $(DESTDIR)/etc/udev/rules.d/60-vboxadd.rules
	echo "KERNEL==\"vboxuser\", NAME=\"vboxuser\", OWNER=\"vboxadd\", MODE=\"0666\"" >> $(DESTDIR)/etc/udev/rules.d/60-vboxadd.rules

	# Installing Xorg Server 1.5 modules
	install -o 0 -g 0 -m 0644 files/vboxvideo_drv_15.so $(DESTDIR)/usr/lib/xorg/modules/drivers/vboxvideo_drv.so
	install -o 0 -g 0 -m 0644 files/vboxmouse_drv_15.so $(DESTDIR)/usr/lib/xorg/modules/input/vboxmouse_drv.so
	files/x11config15.pl
	install -o 0 -g 0 -m 0644 files/90-vboxguest.fdi $(DESTDIR)/usr/share/hal/fdi/policy/20thirdparty/90-vboxguest.fdi

	# Install the guest OpenGL drivers
	install -o 0 -g 0 -m 0644 files/VBoxOGLarrayspu.so $(DESTDIR)/usr/lib
	install -o 0 -g 0 -m 0644 files/VBoxOGLcrutil.so $(DESTDIR)/usr/lib
	install -o 0 -g 0 -m 0644 files/VBoxOGLerrorspu.so $(DESTDIR)/usr/lib
	install -o 0 -g 0 -m 0644 files/VBoxOGLfeedbackspu.so $(DESTDIR)/usr/lib
	install -o 0 -g 0 -m 0644 files/VBoxOGLpackspu.so $(DESTDIR)/usr/lib
	install -o 0 -g 0 -m 0644 files/VBoxOGLpassthroughspu.so $(DESTDIR)/usr/lib
	install -o 0 -g 0 -m 0644 files/VBoxOGL.so $(DESTDIR)/usr/lib
	ln -s /usr/lib/VBoxOGL.so $(DESTDIR)/usr/lib/dri/vboxvideo_dri.so
	
	# Installing 
	chmod +x VBoxClientSymlink
	cp VBoxClientSymlink $(DESTDIR)/usr/bin
	cp vboxclientsymlink.desktop $(DESTDIR)/etc/xdg/autostart
