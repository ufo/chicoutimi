Name:           rvm
Version:        1.16
Release:        1%{?dist}
Summary:        RVM library
Group:          System Environment/Libraries
License:        LGPLv2
URL:            http://www.coda.cs.cmu.edu/
Source0:        ftp://ftp.coda.cs.cmu.edu/pub/coda/src/%{name}-%{version}.tar.gz
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  lwp-devel

%description
The RVM persistent recoverable memory library. The RVM library is used by
the Coda distributed filesystem.


%package tools
Summary:        RVM tools
Group:          Development/Libraries
License:        GPLv2
Requires:       %{name} = %{version}-%{release}

%description tools
Userspace tools to initialize and manipulate RVM log and data segments.
The RVM library is used by the Coda distributed filesystem.


%package        devel
Summary:        Development files for %{name}
Group:          Development/Libraries
License:        LGPLv2
Requires:       %{name} = %{version}-%{release}, pkgconfig, lwp-devel

%description    devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.


%prep
%setup -q


%build
./bootstrap.sh
%configure --disable-static
# Don't use rpath!
sed -i 's|^hardcode_libdir_flag_spec=.*|hardcode_libdir_flag_spec=""|g' libtool
sed -i 's|^runpath_var=LD_RUN_PATH|runpath_var=DIE_RPATH_DIE|g' libtool
# work around linking failures because of the disabling of rpath above
export LD_LIBRARY_PATH=`pwd`/rvm/.libs:`pwd`/seg/.libs
make %{?_smp_mflags}


%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT INSTALL="install -p"
find $RPM_BUILD_ROOT -name '*.la' -exec rm -f {} ';'


%clean
rm -rf $RPM_BUILD_ROOT


%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig


%files
%defattr(-,root,root,-)
%doc COPYING NEWS
%{_libdir}/*.so.*

%files tools
%defattr(-,root,root,-)
%{_sbindir}/rvmutl
%{_sbindir}/rdsinit

%files devel
%defattr(-,root,root,-)
%{_includedir}/%{name}
%{_libdir}/*.so
%{_libdir}/pkgconfig/%{name}lwp.pc


%changelog
* Wed Feb 4 2009 Sylvain Baubeau <bob@glumol.com> 1.16-1
- Update to 1.16

* Mon May 12 2008 Hans de Goede <j.w.r.degoede@hhs.nl> 1.15-1
- Initial Fedora package
