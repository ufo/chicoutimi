%define name pynfs
%define version 0.3.1
%define unmangled_version 0.3.1
%define release 1

Summary: Python NFS4 tools
Name: %{name}
Version: %{version}
Release: %{release}%{?dist}
# Source0: %{name}-%{unmangled_version}.tar.gz
Source0: pynfs-current.tar.gz
Patch0: pynfs-removed-accents.patch
Patch1: pynfs-install-path.patch
License: GPL
Group: Development/Libraries
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-buildroot
Prefix: %{_prefix}
Vendor: Fred Isaman <iisaman@citi.umich.edu>
Provides: pynfs
Requires: python2 krb5-libs
BuildRequires: python-devel krb5-devel
Url: http://www.citi.umich.edu/projects/nfsv4/pynfs/

%description
pynfs is a collection of tools and libraries for NFS4. It includes
a NFS4 library, a commandline client and a server test application.


%prep
%setup -n pynfs
%patch0 -p1
%patch1 -p1


%build
/usr/bin/python setup.py || true
mkdir pynfs
mv nfs4constants.py nfs4packer.py nfs4types.py rpc.py nfs4lib.py xdrlib.py nfs4state.py pynfs_completer.py pynfs
touch pynfs/__init__.py
env CFLAGS="$RPM_OPT_FLAGS" /usr/bin/python setup.py build


%install
/usr/bin/python setup.py install -O1 --root=$RPM_BUILD_ROOT


%clean
rm -rf $RPM_BUILD_ROOT


%post
python2 - <<EOF
import sys
sitedir = sys.prefix + "/lib/python" + sys.version[:3] + "/site-packages"
filename = sitedir + "/pynfs.pth"
f = open(filename, "w")
f.write("/usr/lib/pynfs\n")
f.close()
print "Created", filename
EOF


%files
%defattr(-,root,root)
%doc README COPYING HACKING nfs4.x
/usr/bin/nfs4client.py
/usr/bin/nfs4server.py
/usr/bin/nfs4stest.py
/usr/bin/test_tree_net.py
/usr/lib/python2.6/site-packages/gssapi.so
/usr/lib/python2.6/site-packages/nfs4st
/usr/lib/python2.6/site-packages/pynfs
/usr/lib/python2.6/site-packages/pynfs-0.3.1-py2.6.egg-info

