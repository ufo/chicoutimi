Name:           agorabox-conf
Version:        0.4
Release:        8%{?dist}
Summary:        Constants needed for Handy

BuildArch:      noarch
Group:          System Environment/Base	
License:        GPLv2
URL:            http://www.glumol.com/chicoutimi/agorabox-conf
Source0:        http://www.glumol.com/chicoutimi/agorabox-conf/%{name}-%{version}.tar.gz
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires:       python-augeas augeas-ext >= 0.2-2 PackageKit >= 0.5.6-3


#==============================================================================
# details_of_package adds some generic details to specific packages. Argument is:
#
# %1 the name of the package
#==============================================================================

%define details_of_package() \
Summary: Install specific configurations files for use for %1 \
Group: Applications/System \
Requires: agorabox-conf \
Obsoletes: agorabox-yum-repository \
Provides: agorabox-yum-repository \
%{nil}


#==============================================================================
# post_of_package do generic post to specific packages. Argument is:
#
# %1 the name of the package in min
# %2 the name of the package in maj
# %3 the alternative name
# %4 the NFS port
#==============================================================================

%define post_of_package() \
echo %1 > /etc/sysconfig/ufo/flavour \
\
python <<EOF \
# -*- coding: utf-8 -*- \
import augeas \
import ConfigParser \
\
aug = augeas.Augeas() \
aug.set("/files/etc/sysconfig/ufo/kerberos5/ADMINSRV", "01.kerberos.agorabox.org") \
aug.set("/files/etc/sysconfig/ufo/kerberos5/REALM", "%2.AGORABOX.ORG") \
aug.set("/files/etc/sysconfig/nfs/SECURE_NFS", '"yes"') \
aug.set("/files/etc/sysconfig/ufo/sync/SYNCPATH", '/var/tsumufs/overlay') \
aug.set("/files/etc/sysconfig/ufo/sync/SYNCPRETTYPATH", '"/home/\\$USER/Bureau/Mes fichiers synchronisés"') \
aug.set("/files/etc/sysconfig/ufo/sync/SYNCNAME", '"Mes fichiers synchronisés"') \
aug.set("/files/etc/sysconfig/ufo/ldap/BASE", 'cn=%2.AGORABOX.ORG,cn=realms,dc=agorabox,dc=org') \
aug.set("/files/etc/sysconfig/ufo/ldap/SERVER", '01.ldap.agorabox.org') \
aug.save() \
\
import ConfigParser \
cf = ConfigParser.ConfigParser() \
cf.read([ "/etc/idmapd.conf" ]) \
\
if not cf.has_section("Translation"): \
    cf.add_section("Translation") \
cf.set("Translation", "Method", "umich_ldap") \
\
if not cf.has_section("UMICH_SCHEMA"): \
    cf.add_section("UMICH_SCHEMA") \
cf.set("UMICH_SCHEMA", "LDAP_server", "01.ldap.agorabox.org") \
cf.set("UMICH_SCHEMA", "LDAP_base", "cn=%2.AGORABOX.ORG,cn=realms,dc=agorabox,dc=org") \
cf.set("UMICH_SCHEMA", "LDAP_group_base", "cn=%2.AGORABOX.ORG,cn=realms,dc=agorabox,dc=org") \
\
cf.set("UMICH_SCHEMA", "GSS_principal_attr", "krbPrincipalName") \
\
cf.set("UMICH_SCHEMA", "NFSv4_name_attr", "krbPrincipalName") \
cf.set("UMICH_SCHEMA", "NFSv4_member_attr", "memberUid") \
cf.set("UMICH_SCHEMA", "NFSv4_acctname_attr", "krbPrincipalName") \
cf.set("UMICH_SCHEMA", "NFSv4_person_objectclass", "posixAccount") \
\
cf.set("UMICH_SCHEMA", "NFSv4_group_attr", "cn") \
cf.set("UMICH_SCHEMA", "NFSv4_group_objectclass", "posixGroup") \
\
cf.write(open("/etc/idmapd.conf", "w")) \
\
cf = ConfigParser.ConfigParser() \
cf.read([ "/etc/tsumufs/tsumufs.conf" ]) \
\
if len(cf.sections()): \
    section = cf.sections()[0] \
    cf.set(section, "ip", "nfs.%3.agorabox.org") \
    cf.set(section, "options", "sec=krb5p,rw,proto=tcp,resvport,user,noauto,port=%4") \
    cf.write(open("/etc/tsumufs/tsumufs.conf", "w")) \
EOF\
\
update-ufo-hosts \
cp -f %{_datadir}/agorabox-conf-%1/krb5.conf.default /etc/krb5.conf \
cp -f %{_datadir}/agorabox-conf-%1/krb5.keytab.default /etc/krb5.keytab \
%{nil} 


#==============================================================================
# files_of_package list some generic files to specific packages.
#
# %1 : the name of the package
#==============================================================================

%define files_of_package() \
%{_sysconfdir}/yum.repos.d/agorabox-%1.repo \
%config(noreplace) /etc/tsumufs/tsumufs.conf \
%config(noreplace) /etc/sysconfig/ufo/kerberos5 \
%config(noreplace) /etc/sysconfig/ufo/sync \
%{_datadir}/agorabox-conf-%1/krb5.conf.default \
%{_datadir}/agorabox-conf-%1/krb5.keytab.default \
%config(noreplace) /etc/sysconfig/ufo/flavour \
%{nil}


%package jussieu
Summary: Install specific configurations files for use at Jussieu
Group: Applications/System
Requires: agorabox-conf

%package polenumerique
%details_of_package "Pole Numerique"


%package descartes
%details_of_package "Descartes University"


%package demo
%details_of_package demo


%package ambassador
%details_of_package ambassador


%package dexxon
%details_of_package dexxon


%description
Constants needed for Handy


%description jussieu
Constants needed for use at Jussieu


%description polenumerique
Constants needed for use at Pole Numerique


%description descartes
Constants needed for use at Descartes university


%description demo
Constants needed for use for demos


%description ambassador
Constants needed for use for ambassadors


%description dexxon
Constants needed for use for Dexxon


%prep
%setup -q

%build


%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT/etc/sysconfig/ufo
touch $RPM_BUILD_ROOT/etc/sysconfig/ufo/coda
touch $RPM_BUILD_ROOT/etc/sysconfig/ufo/kerberos5
touch $RPM_BUILD_ROOT/etc/sysconfig/ufo/vboxclient
touch $RPM_BUILD_ROOT/etc/sysconfig/ufo/sync
install -D -m 644 10-ufo-policy.pkla $RPM_BUILD_ROOT/var/lib/polkit-1/localauthority/50-local.d/10-ufo-policy.pkla
install -D -m 644 common/tsumufs.conf $RPM_BUILD_ROOT/etc/tsumufs/tsumufs.conf

install -D -m 644 descartes/krb5.keytab $RPM_BUILD_ROOT/usr/share/agorabox-conf-descartes/krb5.keytab.default
install -D -m 644 descartes/krb5.conf $RPM_BUILD_ROOT/usr/share/agorabox-conf-descartes/krb5.conf.default
install -D -m 644 descartes/agorabox.repo $RPM_BUILD_ROOT/etc/yum.repos.d/agorabox-descartes.repo

install -D -m 644 demo/krb5.keytab $RPM_BUILD_ROOT/usr/share/agorabox-conf-demo/krb5.keytab.default
install -D -m 644 demo/krb5.conf $RPM_BUILD_ROOT/usr/share/agorabox-conf-demo/krb5.conf.default
install -D -m 644 demo/agorabox.repo $RPM_BUILD_ROOT/etc/yum.repos.d/agorabox-demo.repo

install -D -m 644 polenumerique/krb5.keytab $RPM_BUILD_ROOT/usr/share/agorabox-conf-polenumerique/krb5.keytab.default
install -D -m 644 polenumerique/krb5.conf $RPM_BUILD_ROOT/usr/share/agorabox-conf-polenumerique/krb5.conf.default
install -D -m 644 polenumerique/agorabox.repo $RPM_BUILD_ROOT/etc/yum.repos.d/agorabox-polenumerique.repo

install -D -m 644 ambassador/krb5.keytab $RPM_BUILD_ROOT/usr/share/agorabox-conf-ambassador/krb5.keytab.default
install -D -m 644 ambassador/krb5.conf $RPM_BUILD_ROOT/usr/share/agorabox-conf-ambassador/krb5.conf.default
install -D -m 644 ambassador/agorabox.repo $RPM_BUILD_ROOT/etc/yum.repos.d/agorabox-ambassador.repo

install -D -m 644 dexxon/krb5.keytab $RPM_BUILD_ROOT/usr/share/agorabox-conf-dexxon/krb5.keytab.default
install -D -m 644 dexxon/krb5.conf $RPM_BUILD_ROOT/usr/share/agorabox-conf-dexxon/krb5.conf.default
install -D -m 644 dexxon/agorabox.repo $RPM_BUILD_ROOT/etc/yum.repos.d/agorabox-dexxon.repo

touch $RPM_BUILD_ROOT/etc/sysconfig/ufo/flavour


%post 
python <<EOF
import augeas
aug = augeas.Augeas()
aug.set("/files/etc/sysconfig/ufo/vboxclient/VBOX_DND_SF_PATH", "/tmp/vbox-dnd/")
aug.save()
EOF

/usr/sbin/groupadd ufo >/dev/null 2>&1
python << EOF
import augeas
import os.path

def add_to_sudoers(username, program, tag = "NOPASSWD"):
    aug = augeas.Augeas()
    specs = aug.match("/files/etc/sudoers/spec[*]/user")
    for spec in specs:
        user = aug.get(spec)
        if user == username:
            commands = aug.match(os.path.dirname(spec) + "/host_group/command")
            for command in commands:
                if aug.get(command) == program and aug.get(command + "/tag") == tag:
                    return
            n = len(aug.match(os.path.dirname(spec) + "/host_group/command")) + 1
            aug.set(os.path.dirname(spec) + "/host_group/command[" + str(n) + "]", program)
            aug.set(os.path.dirname(spec) + "/host_group/command[" + str(n) + "]/tag", tag)
            aug.save()
            return
        
    open("/etc/sudoers", "a").write("%s\tALL=\t%s: %s\n" % (username, tag, program))

add_to_sudoers("%ufo", "ALL", "PASSWD")
EOF

# reactivate updates
sed -i '6s/enabled=0/enabled=1/' /etc/yum.repos.d/fedora-updates.repo
sed -i '5s/enabled=0/enabled=1/' /etc/yum.repos.d/rpmfusion-nonfree-updates.repo
sed -i '5s/enabled=0/enabled=1/' /etc/yum.repos.d/rpmfusion-free-updates.repo


%post jussieu
echo jussieu > /etc/sysconfig/ufo/flavour

python <<EOF
import augeas
aug = augeas.Augeas()
aug.set("/files/etc/sysconfig/ufo/kerberos5/ADMINSRV", "01.kerberos.agorabox.org")
aug.set("/files/etc/sysconfig/ufo/kerberos5/REALM", "AGORABOX.ORG")
aug.set("/files/etc/sysconfig/ufo/coda/CODAREALM", "agorabox.org")
aug.set("/files/etc/sysconfig/ufo/coda/KRB5REALM", "AGORABOX.ORG")
aug.set("/files/etc/sysconfig/ufo/coda/CODASRV", "elko.agorabox.org,white.agorabox.org")
aug.set("/files/etc/sysconfig/ufo/coda/CODAFOLDER", '"Ma fac"')
aug.save()
EOF
update-ufo-hosts
touch /etc/coda/auth2.conf.ex
codaconfedit auth2.conf auth2_masquerade_port 2431
authconfig --updateall


%post polenumerique
%post_of_package polenumerique DROME drome 2051


%post descartes
%post_of_package descartes P5 p5 2050


%post demo
%post_of_package demo BETA beta 2052


%post ambassador
%post_of_package ambassador AMBASSADOR ambassador 2055


%post dexxon
%post_of_package dexxon DEXXON dexxon 2057


%clean
rm -rf $RPM_BUILD_ROOT


%files
%defattr(-,root,root,-)
%doc
%{_bindir}/update-ufo-hosts
%config /etc/sysconfig/ufo/vboxclient
%config /var/lib/polkit-1/localauthority/50-local.d/10-ufo-policy.pkla


%files jussieu
%config(noreplace) /etc/sysconfig/ufo/coda
%config(noreplace) /etc/sysconfig/ufo/kerberos5
%config(noreplace) /etc/sysconfig/ufo/flavour


%files polenumerique
%files_of_package polenumerique

%files descartes
%files_of_package descartes


%files demo
%files_of_package demo


%files ambassador
%files_of_package ambassador


%files dexxon
%files_of_package dexxon


%changelog
* Tue Apr 13 2010 Antonin Fouques <antonin.fouques@agorabox.org> - 0.4-7
Added Dexxon package and maked some macros in the spec file.

* Tue Jan 19 2010 Kevin Pouget <pouget@agorabox.org> - 0.4-6
Reactivate updates.

* Tue Jan 19 2010 Kevin Pouget <pouget@agorabox.org> - 0.4-5
Added PolKit-1 policy rule for the ufo group.

* Mon Jan 11 2010 Kevin Pouget <pouget@agorabox.org> - 0.4-4
Modified krb5.keytab configuration Pole Numerique 
and Descartes due to servers configuratuion changes.

* Fri Jun 26 2009 Sylvain Baubeau <bob@glumol.com>
Added configuration for Pole Numerique

* Tue Jun 25 2009 Kevin Pouget <pouget@agorabox.org>
Created configuration file for VirtualBox guest 
additions drag and drop.

* Tue Apr 28 2009 Sylvain Baubeau <bob@glumol.com>
Created configuration file for Coda

* Wed Nov 5 2008 Sylvain Baubeau <bob@glumol.com>
Initial release
