Name:           agorabox
Version:        0.1
Release:        3%{?dist}
Summary:        The full agorabox package

BuildArch:      noarch
Group:          System Environment/Base	
License:        GPLv2
URL:            http://www.glumol.com/chicoutimi/agorabox
Source0:        http://www.glumol.com/chicoutimi/agorabox/%{name}-%{version}.tar.gz
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

Requires:       cryptmount codamount agorabox-ui openldap-clients python-ldap
Requires:       firstboot augeas-ext >= 0.2 python-augeas
# Requires:       hwprofile

%description
Installs all the packages needed for the Agorabox distribution.


%prep
%setup -q


%build
make %{?_smp_mflags}


%install
rm -rf $RPM_BUILD_ROOT
mkdir $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT


%post
echo -e "set /files/etc/sysconfig/selinux/SELINUX disabled\nsave" | augtool
echo 0 > /selinux/enforce


%postun


%clean
rm -rf $RPM_BUILD_ROOT


%files
%defattr(-,root,root,-)
%doc
%{_bindir}/ldap-validate


%changelog
* Thu Feb 19 2009 Sylvain Baubeau <bob@glumol.com>
User creation and firstboot modifications moved to package firstboot

* Tue Jan 27 2009 Sylvain Baubeau <bob@glumol.com>
Set value for field 'home' in /etc/sysconfig/agorabox/uuids

* Thu Oct 9 2008 Sylvain Baubeau <bob@glumol.com>
Initial release
