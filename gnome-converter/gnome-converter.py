#!/usr/bin/python
# -*- coding: iso-8859-1 -*-

import os
import urllib
import os.path as path
import commands
import threading

import gtk
import nautilus
import gconf

class BaseConverter:
    def get_menu_items(self, file, menu_activate_cb):
        item = nautilus.MenuItem('NautilusPython::convert_file_item',
                                     'Convertir...',
                                     'Convertir...')
        item.connect('activate', menu_activate_cb, (self, file, ".toto"))
        return item,
            
        # En attendant que ca marche....
        """
        items = self.get_output_formats(file)
        
        if len(items) == 0:
            item = nautilus.MenuItem('NautilusPython::convert_file_item',
                                     'Convertir en ' + items[0][0],
                                     'Convertir en ' + items[0][0])
            #item.connect('activate', menu_activate_cb, (self, file, items[0][1]))
        else:
            menu = nautilus.Menu()
            item = nautilus.MenuItem('NautilusPython::convert_file_item_menu',
                                     'Convertir...',
                                     'Convertir...')
            #item.connect('activate', menu_activate_cb, (self, file, ".png"))
            
            #self.l = []
            n = 0
            for it in items:
                action = gtk.Action('NautilusPython::convert_file_item_' + str(n),
                                    'Convertir en ' + it[0],
                                    None, None)
                item2 = nautilus.MenuItem('NautilusPython::convert_file_item_' + str(n),
                                          'Convertir en ' + it[0],
                                          'Convertir en ' + it[0])
                #self.l.append(item2)
                #self.l.append(action)
                #action.connect('activate', menu_activate_cb, (self, file, it[1]))
                item2.connect('activate', menu_activate_cb, (self, file, it[1]))
                menu.append_item(item2)
                n = n + 1

            item.set_submenu(menu)
        """
            
        return item,
    
    def get_output_formats(self, file):
        ret_formats = []
        for f in self.formats:
            if 'w' in f[2]:
                ret_formats.append(f)
        return ret_formats

    def get_input_formats(self, file):
        ret_formats = []
        for f in self.formats:
            if 'r' in f[2]:
                ret_formats.append(f)
        return ret_formats

    def require_program(self, program, package):
        if not path.exists(program):
            print "installing package /usr/bin/yum-pool", package 
            os.spawnl(os.P_WAIT, "/usr/bin/yum-pool", "yum-pool", "install", package)

class ImageConverter(BaseConverter):
    formats = [
                ("fichier JPEG", "jpg", "rw"),
                ('Raw alpha samples', 'A', 'rw+'),
                ('PFS: 1st Publisher', 'ART', 'r--'),
                ('Microsoft Audio/Visual Interleaved', 'AVI', 'r--'),
                ('AVS X image', 'AVS', 'rw+'),
                ('Raw blue samples', 'B', 'rw+'),
                ('Microsoft Windows bitmap image', 'BMP', 'rw-'),
                ('Microsoft Windows bitmap image v2', 'BMP2', '-w-'),
                ('Microsoft Windows bitmap image v3', 'BMP3', '-w-'),
                ('Raw cyan samples', 'C', 'rw+'),
                ('Image caption', 'CAPTION', 'r--'),
                ('Cineon Image File', 'CIN', 'rw+'),
                ('Cisco IP phone image format', 'CIP', '-w-'),
                ('Image Clip Mask', 'CLIP', '-w+'),
                ('Raw cyan, magenta, yellow, and black samples', 'CMYK', 'rw+'),
                ('Raw cyan, magenta, yellow, black, and opacity samples', 'CMYKA', 'rw+'),
                ('Microsoft icon', 'CUR', 'rw-'),
                ('DR Halo', 'CUT', 'r--'),
                ('Digital Imaging and Communications in Medicine image', 'DCM', 'r--'),
                ('ZSoft IBM PC multi-page Paintbrush', 'DCX', 'rw+'),
                ('Multi-face font package (Freetype 2.3.2)', 'DFONT', 'r--'),
                ('Graphviz', 'DOT', '---'),
                ('Display Postscript Interpreter', 'DPS', '---'),
                ('SMPTE 268M-2003 (DPX)', 'DPX', 'rw+'),
                ('Encapsulated Portable Document Format', 'EPDF', 'rw-'),
                ('Encapsulated PostScript Interchange format', 'EPI', 'rw-'),
                ('Encapsulated PostScript', 'EPS', 'rw-'),
                ('Level II Encapsulated PostScript', 'EPS2', '-w-'),
                ('Level III Encapsulated PostScript', 'EPS3', '-w+'),
                ('Encapsulated PostScript', 'EPSF', 'rw-'),
                ('Encapsulated PostScript Interchange format', 'EPSI', 'rw-'),
                ('Encapsulated PostScript with TIFF preview', 'EPT', 'rw-'),
                ('Encapsulated PostScript Level II with TIFF preview', 'EPT2', 'rw-'),
                ('Encapsulated PostScript Level III with TIFF preview', 'EPT3', 'rw+'),
                ('Group 3 FAX', 'FAX', 'rw+'),
                ('Flexible Image Transport System', 'FITS', 'rw-'),
                ('Plasma fractal image', 'FRACTAL', 'r--'),
                ('Flexible Image Transport System', 'FTS', 'rw-'),
                ('Raw green samples', 'G', 'rw+'),
                ('Group 3 FAX', 'G3', 'rw-'),
                ('CompuServe graphics interchange format', 'GIF', 'rw+'),
                ('CompuServe graphics interchange format (version 87a)', 'GIF87', 'rw-'),
                ('Gradual passing from one shade to another', 'GRADIENT', 'r--'),
                ('Raw gray samples', 'GRAY', 'rw+'),
                ('Histogram of the image', 'HISTOGRAM', '-w-'),
                ('Hypertext Markup Language and a client-side image map', 'HTM', '-w-'),
                ('Hypertext Markup Language and a client-side image map', 'HTML', '-w-'),
                ('Truevision Targa image', 'ICB', 'rw+'),
                ('Microsoft icon', 'ICO', 'rw-'),
                ('Microsoft icon', 'ICON', 'rw-'),
                ('The image format and characteristics', 'INFO', '-w+'),
                ('JPEG Network Graphics', 'JNG', 'rw-'),
                ('Joint Photographic Experts Group JFIF format (62)', 'JPEG', 'rw-'),
                ('Joint Photographic Experts Group JFIF format', 'JPG', 'rw-'),
                ('Raw black samples', 'K', 'rw+'),
                ('Image label', 'LABEL', 'r--'),
                ('Raw magenta samples', 'M', 'rw+'),
                ('MPEG Video Stream', 'M2V', 'rw+'),
                ('Colormap intensities and indices', 'MAP', 'rw-'),
                ('MATLAB image format', 'MAT', 'rw+'),
                ('MATTE format', 'MATTE', '-w+'),
                ('Magick Image File Format', 'MIFF', 'rw+'),
                ('Multiple-image Network Graphics (libpng 1.2.16)', 'MNG', 'rw+'),
                ('Bi-level bitmap in least-significant-byte first order', 'MONO', 'rw-'),
                ('Magick Persistent Cache image format', 'MPC', 'rw+'),
                ('MPEG Video Stream', 'MPEG', 'rw+'),
                ('MPEG Video Stream', 'MPG', 'rw+'),
                ('Magick Scripting Language', 'MSL', 'rw+'),
                ('MTV Raytracing image format', 'MTV', 'rw+'),
                ('Magick Vector Graphics', 'MVG', 'rw-'),
                ('Constant image of uniform color', 'NULL', 'rw-'),
                ('Raw opacity samples', 'O', 'rw+'),
                ('On-the-air bitmap', 'OTB', 'rw-'),
                ('Open Type font (Freetype 2.3.2)', 'OTF', 'r--'),
                ('16bit/pixel interleaved YUV', 'PAL', 'rw-'),
                ('Palm pixmap', 'PALM', 'rw+'),
                ('Common 2-dimensional bitmap format', 'PAM', 'rw+'),
                ('Predefined pattern', 'PATTERN', 'r--'),
                ('Portable bitmap format (black and white)', 'PBM', 'rw+'),
                ('Photo CD', 'PCD', 'rw-'),
                ('Photo CD', 'PCDS', 'rw-'),
                ('Printer Control Language', 'PCL', 'rw-'),
                ('Apple Macintosh QuickDraw/PICT', 'PCT', 'rw-'),
                ('ZSoft IBM PC Paintbrush', 'PCX', 'rw-'),
                ('Palm Database ImageViewer Format', 'PDB', 'rw+'),
                ('Portable Document Format', 'PDF', 'rw+'),
                ('Postscript Type 1 font (ASCII) (Freetype 2.3.2)', 'PFA', 'r--'),
                ('Postscript Type 1 font (binary) (Freetype 2.3.2)', 'PFB', 'r--'),
                ('Portable graymap format (gray scale)', 'PGM', 'rw+'),
                ('Personal Icon', 'PICON', 'rw-'),
                ('Apple Macintosh QuickDraw/PICT', 'PICT', 'rw-'),
                ('Alias/Wavefront RLE image format', 'PIX', 'r--'),
                ('Progessive Joint Photographic Experts Group JFIF', 'PJPEG', 'rw-'),
                ('Plasma fractal image', 'PLASMA', 'r--'),
                ('Portable Network Graphics (libpng 1.2.16)', 'PNG', 'rw-'),
                ('24-bit RGB PNG, opaque only (zlib 1.2.3)', 'PNG24', 'rw-'),
                ('32-bit RGBA PNG, semitransparency OK', 'PNG32', 'rw-'),
                ('8-bit indexed PNG, binary transparency only', 'PNG8', 'rw-'),
                ('Portable anymap', 'PNM', 'rw+'),
                ('Portable pixmap format (color)', 'PPM', 'rw+'),
                ('Show a preview an image enhancement, effect, or f/x', 'PREVIEW', '-w-'),
                ('PostScript', 'PS', 'rw+'),
                ('Level II PostScript', 'PS2', '-w+'),
                ('Level III PostScript', 'PS3', '-w+'),
                ('Adobe Photoshop bitmap', 'PSD', 'rw+'),
                ('Pyramid encoded TIFF', 'PTIF', 'rw-'),
                ('Seattle Film Works', 'PWP', 'r--'),
                ('Raw red samples', 'R', 'rw+'),
                ('SUN Rasterfile', 'RAS', 'rw+'),
                ('Raw red, green, and blue samples', 'RGB', 'rw+'),
                ('Raw red, green, blue, and alpha samples', 'RGBA', 'rw+'),
                ('Raw red, green, blue, and opacity samples', 'RGBO', 'rw+'),
                ('Alias/Wavefront image', 'RLA', 'r--'),
                ('Utah Run length encoded image', 'RLE', 'r--'),
                ('ZX-Spectrum SCREEN$', 'SCR', 'r--'),
                ('Scitex HandShake', 'SCT', 'r--'),
                ('Seattle Film Works', 'SFW', 'r--'),
                ('Irix RGB image', 'SGI', 'rw+'),
                ('Hypertext Markup Language and a client-side image map', 'SHTML', '-w-'),
                ('Steganographic image', 'STEGANO', 'r--'),
                ('SUN Rasterfile', 'SUN', 'rw+'),
                ('Scalable Vector Graphics (XML 2.6.27)', 'SVG', 'rw+'),
                ('Compressed Scalable Vector Graphics (XML 2.6.27)', 'SVGZ', 'rw+'),
                ('Text', 'TEXT', 'rw+'),
                ('Truevision Targa image', 'TGA', 'rw+'),
                ('EXIF Profile Thumbnail', 'THUMBNAIL', '-w+'),
                ('Tagged Image File Format (LIBTIFF, Version 3.8.2)', 'TIFF', 'rw+'),
                ('Tile image with a texture', 'TILE', 'r--'),
                ('PSX TIM', 'TIM', 'r--'),
                ('TrueType font collection (Freetype 2.3.2)', 'TTC', 'r--'),
                ('TrueType font (Freetype 2.3.2)', 'TTF', 'r--'),
                ('Text', 'TXT', 'rw+'),
                ('X-Motif UIL table', 'UIL', '-w-'),
                ('16bit/pixel interleaved YUV', 'UYVY', 'rw-'),
                ('Truevision Targa image', 'VDA', 'rw+'),
                ('VICAR rasterfile format', 'VICAR', 'rw-'),
                ('Visual Image Directory', 'VID', 'rw+'),
                ('Khoros Visualization image', 'VIFF', 'rw+'),
                ('Truevision Targa image', 'VST', 'rw+'),
                ('Wireless Bitmap (level 0) image', 'WBMP', 'rw-'),
                ('Windows Meta File', 'WMF', 'r--'),
                ('Compressed Windows Meta File', 'WMZ', 'r--'),
                ('Word Perfect Graphics', 'WPG', 'r--'),
                ('X Image', 'X', 'rw+'),
                ('X Windows system bitmap (black and white)', 'XBM', 'rw-'),
                ('Constant image uniform color', 'XC', 'r--'),
                ('GIMP image', 'XCF', 'r--'),
                ('X Windows system pixmap (color)', 'XPM', 'rw-'),
                ('Khoros Visualization image', 'XV', 'rw+'),
                ('X Windows system window dump (color)', 'XWD', 'rw-'),
                ('Raw yellow samples', 'Y', 'rw+'),
                ('Raw Y, Cb, and Cr samples', 'YCbCr', 'rw+'),
                ('Raw Y, Cb, Cr, and opacity samples', 'YCbCrA', 'rw+'),
                ('CCIR 601 4:1:1 or 4:2:2', 'YUV', 'rw-')]
                
    def convert(self, file, format):
        filename = file.get_uri()[7:]
        print "convert " + filename + " " + path.splitext(filename)[0] + '.' + format[1]
        os.system("convert " + filename + " " + path.splitext(filename)[0] + '.' + format[1])
        
class AudioConverter(BaseConverter):
    formats = [
                ("fichier OGG", "ogg", "rw"),
                ("fichier MP3", "mp3", "rw"),
                ("fichier WAV", "wav", "rw")
              ]
              

    def convert(self, file, format):
        filename = file.get_uri()[7:]
        out_file = path.splitext(filename)[0] + '.' + format[1]
        mime = file.get_mime_type()
        if mime == "audio/mpeg":
            if format[1] == "ogg":
                commands.getoutput("lame --quiet --decode \"" + filename + "\" - | oggenc - -Q -b 128 -M 160 -o \"" + out_file + "\"")
            else:
                commands.getoutput("lame --quiet --decode \"" + filename + "\" \"" + out_file + "\"")
        elif mime == "application/ogg" or mime == "audio/x-vorbis+ogg":
            if format[1] == "wav":
                commands.getoutput("ogg123 -q --device=wav \"" + filename + "\" -f \"" + out_file + "\"")
            else:
                commands.getoutput("ogg123 -q --device=wav \"" + filename + "\" -f - | lame --quiet -m auto -v -F -b 128 -B 160 -h - \"" + out_file + "\"")
        else:
            if format[1] == "ogg":
                commands.getoutput("oggenc \"" + filename + "\" -Q -b 128 -M 160 -o \"" + out_file + "\"")
            else:
                commands.getoutput("lame --quiet -m auto -h -v -F -b 128 -B 160 \"" + filename + "\" \"" + out_file + "\"")

class HTMLConverter(BaseConverter):
    formats = [
                ("fichier PDF", "pdf", "rw"),
                ("fichier PostScript", "ps", "rw")
              ]

    def convert(self, file, format):
        filename = file.get_uri()[7:]
        out_file = path.splitext(filename)[0] + '.' + format[1]
        commands.getoutput("htmldoc --outfile \"" + out_file + "\" \"" + filename + "\"")

class OggConverter(BaseConverter):
    formats = [
                ("fichier AVI", "avi", "rw")
              ]

    def convert(self, file, format):
        filename = file.get_uri()[7:]
        out_file = path.splitext(filename)[0] + '.' + format[1]
        print "mencoder \"" + filename + "\" -o \"" + out_file + "\" -ovc lavc"
        commands.getoutput("mencoder \"" + filename + "\" -o \"" + out_file + "\" -ovc lavc")

class VideoConverter(BaseConverter):
    formats = [
                ("fichier MPEG", "mpg", "rw"),
                ("fichier OGG", "ogg", "rw"),
                ("fichier AVI", "avi", "rw"),
                ("fichier SWF", "swf", "rw"),
		("fichier FLV", "flv", "rw")
              ]

    def convert(self, file, format):
        filename = file.get_uri()[7:]
        out_file = path.splitext(filename)[0] + '.' + format[1]
        # self.require_program("/usr/bin/avidemux2", "avidemux")
        print "ffmpeg -i " + filename + " " + out_file
        # commands.getoutput("avidemux2 --load \"" + filename + "\" --video-codec xvid --output-format AVI --save \"" + out_file + "\"")
        commands.getoutput("ffmpeg -i " + filename + " " + out_file)

class PostscriptConverter(BaseConverter):
    formats = [
                ("fichier PDF", "pdf", "rw")
              ]
              
    def convert(self, file, format):
        filename = file.get_uri()[7:]
        out_file = path.splitext(filename)[0] + format[1]
        print "ps2pdf \"" + filename + "\" \"" + out_file + "\""
        commands.getoutput("ps2pdf \"" + filename + "\" \"" + out_file + "\"")

class LatexConverter(BaseConverter):
    formats = [
                ("fichier PS", "ps", "rw", "dvips"),
                ("fichier PDF", "pdf", "rw", "dvipdf"),
                ("fichier GIF", "gif", "rw", "dvigif"),
                ("fichier PNG", "png", "rw", "dvipng")
              ]

    def convert(self, file, format):
        filename = file.get_uri()[7:]
        command = format[3]
        if command in ('dvigif', 'dvipng'):
            out_file = path.splitext(filename)[0] + '-page-%d.' + format[1]
            commands.getoutput(command + " -o \"" + out_file + "\" \"" + filename + "\"")
        else:
            out_file = path.splitext(filename)[0] + '.' + format[1]
            if command == "dvips":
                commands.getoutput(command + " -o \"" + out_file + "\" \"" + filename + "\"")
            else:
                commands.getoutput(command + " \"" + filename + "\"")

class CodeConverter(BaseConverter):
    formats = [
                ("fichier HTML", "html", "rw"),
                ("fichier XML", "xml", "rw"),
                ("fichier RTF", "rtf", "rw"),
                ("fichier LaTeX", "tex", "rw"),
              ]

    def convert(self, file, format):
        filename = file.get_uri()[7:]
        option = { "html" : "", "xml" : "-Z", "rtf" : "-R", "tex" : "-L" }[format[1]]
        out_file = path.splitext(filename)[0] + '.' + format[1]
        commands.getoutput("highlight " + option + " -o \"" + out_file + "\" \"" + filename + "\"")

converters = {
    "image/bmp"   : ImageConverter,
    "image/cgm"   : ImageConverter,
    "image/g3fax" : ImageConverter,
    "image/gif"   : ImageConverter,
    "image/ief"   : ImageConverter,
    "image/jpeg"  : ImageConverter,
    "image/png"   : ImageConverter,
    "image/x-pcx" : ImageConverter,
    "audio/mpeg"  : AudioConverter,
    "audio/x-wav"  : AudioConverter,
    "application/ogg"  : AudioConverter,
    "audio/x-vorbis+ogg" : AudioConverter,
    "text/html" : HTMLConverter,
    "text/x-csrc" : CodeConverter,
    "text/x-chdr" : CodeConverter,
    "text/x-c++src" : CodeConverter,
    "text/x-c++hdr" : CodeConverter,
    "text/x-python" : CodeConverter,
    "text/x-csharp" : CodeConverter,
    "application/x-php": CodeConverter,
    "application/x-dvi" : LatexConverter,
    "application/postscript" : PostscriptConverter,
    "video/x-ms-wmv" : VideoConverter,
    "video/mpeg" : VideoConverter,
    "video/x-msvideo" : VideoConverter,
    "video/x-theora+ogg" : OggConverter
}

class GnomeConverterExtension(nautilus.MenuProvider):
    def __init__(self):
        self.client = gconf.client_get_default()
        
    def menu_activate_cb(self, menu, data):
        conv, file, format = data
        
        dialog = gtk.Dialog("En quel format désirez vous faire la conversion ?", None, 0,
                (gtk.STOCK_OK, gtk.RESPONSE_OK))

        hbox = gtk.HBox(False, 8)
        hbox.set_border_width(8)
        dialog.vbox.pack_start(hbox, False, False, 0)

        stock = gtk.image_new_from_stock(
                gtk.STOCK_DIALOG_QUESTION,
                gtk.ICON_SIZE_DIALOG)
        hbox.pack_start(stock, False, False, 0)

        table = gtk.Table(2, 2)
        table.set_row_spacings(4)
        table.set_col_spacings(4)
        hbox.pack_start(table, True, True, 0)

        label = gtk.Label("Format : ")
        label.set_use_underline(True)
        table.attach(label, 0, 1, 0, 1)
        
        items = conv.get_output_formats(file)
        
        combo_box = gtk.combo_box_new_text()
        # combo_box.set_wrap_width(2)
        for i in items:
            combo_box.append_text(i[0] + " (." + i[1] + ")")
        combo_box.set_active(0)
        table.attach(combo_box, 1, 2, 0, 1)
        label.set_mnemonic_widget(combo_box)

        dialog.show_all()

        response = dialog.run()

        if response == gtk.RESPONSE_OK:
            format = items[combo_box.get_active()]
            thread = threading.Thread(None, conv.convert, args = (file, format))
            thread.setDaemon(True)
            thread.start()
            #thread.run()
            thread.join(0.01)
            
        dialog.destroy()

    def menu_background_activate_cb(self, menu, data): 
        conv, file, format = data
        pass
       
    def guess_available_converters(self, file):
        print file.get_name(), "file -i " + file.get_name(), dir(file), file.get_mime_type()
        mime_type = commands.getoutput("file -i " + file.get_name()).split(':')[1].split(";")[0].strip()
        print "mime_type", mime_type
        try: return converters[file.get_mime_type()]
        except: return None
    
    def get_file_items(self, window, files):
        if len(files) != 1:
            return
        
        file = files[0]

        if file.get_uri_scheme() != 'file':
            return
            
        klass = self.guess_available_converters(file)
        print file.get_mime_type()
        
        if not klass: return tuple()
        conv = klass()
        items = conv.get_menu_items(file, self.menu_activate_cb)
                    
        return items

    def get_background_items(self, window, file):
        return tuple()

