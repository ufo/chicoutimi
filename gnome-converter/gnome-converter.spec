Name:           gnome-converter
Version:        0.1
Release:        1%{?dist}
Summary:        An extension for GNOME that allows to easily convert files to many formats.

BuildArch:      noarch
Group:          Utilities
License:        GPL
URL:            http://www.glumol.com/gnome-converter
Source0:        gnome-converter-0.1.tar.gz
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

Requires:       nautilus-python pygtk2 lame vorbis-tools ImageMagick htmldoc highlight tetex-xdvi tetex-dvips ghostscript tetex

%description
An extension for GNOME that allows to easily convert files to many formats.

%prep
%setup -q


%build


%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT


%clean
rm -rf $RPM_BUILD_ROOT


%files
%defattr(-,root,root,-)
%doc
%{_libdir}/nautilus/extensions-1.0/python/gnome-converter.py
%{_libdir}/nautilus/extensions-1.0/python/gnome-converter.pyc
%{_libdir}/nautilus/extensions-1.0/python/gnome-converter.pyo

%changelog
