ifeq ($(CVSPROGRAM),bzr)
	CMD=bzr branch $(URL)
else ifeq ($(CVSPROGRAM),svn)
	CMD=svn checkout $(URL)
endif

all: tar rpm srpm

rpm: setup tar
	rpmbuild -bb $(SPECFILE)

srpm: setup tar
	rpmbuild -bs $(SPECFILE)

clean:
	rm -rf *.pyc *~ $(ARCHIVE) $(DIR)

tar: $(ARCHIVE)

setup: tar
	if [ ! -e ~/rpmbuild/SOURCES/$(ARCHIVE) ]; then \
		ln -s `pwd`/$(ARCHIVE) ~/rpmbuild/SOURCES; \
	fi

