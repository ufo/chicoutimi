Name:           picasafs
Version:        0.1
BuildArch:      noarch
Release:        1%{?dist}
Summary:        FUSE filesystem to access Google Picasaweb galleries
        
Group:          System Environment/Base
License:        GPL
URL:            http://www.glumol.com/chicoutimi/picasafs
Source0:        picasafs-0.1.tar.gz
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

Requires:       fuse-python pygtk2 seamonkey python-gdata

%description
picasafs shows the content of a Picasaweb gallery as a standard
file tree.

%prep
%setup -q


%build


%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT

%post
/usr/sbin/usermod -G fuse `python -c "import pwd; print pwd.getpwuid(500)[0]"`

%clean
rm -rf $RPM_BUILD_ROOT


%files
%defattr(-,root,root,-)
%{_bindir}/picasafs
%{_bindir}/picasafs-conf
%{_datadir}/applications/picasafs.desktop
%{_datadir}/pixmaps/picasafs.xpm
%{_datadir}/gnome/autostart/picasafs-autostart.desktop
%{_libdir}/nautilus/extensions-1.0/python/emblems
%doc



%changelog
