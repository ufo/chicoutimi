%define name python-distutils-extra
%define version 0.90
%define unmangled_version 0.90
%define unmangled_version 0.90
%define release 1

%{!?python_sitelib: %define python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib()")}
%{!?python_sitearch: %define python_sitearch %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib(1)")}

Summary: Add support for i18n, documentation and icons to distutils
Name: %{name}
Version: %{version}
Release: %{release}
Source0: %{name}-%{unmangled_version}.tar.gz
License: GPLv2
Group: Development/Libraries
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-buildroot
Prefix: %{_prefix}
Requires: python
BuildArch: noarch
Vendor: Sebastian Heinlein <sebi@glatzor.de>

%description
Enables you to easily integrate gettext support, themed icons and scrollkeeper
based documentation into Python's distutils.

%prep
%setup -n %{name}-%{unmangled_version} -n %{name}-%{unmangled_version}

%build
python setup.py build

%install
python setup.py install --single-version-externally-managed --root=$RPM_BUILD_ROOT

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root)
%{python_sitelib}/DistUtilsExtra/__init__.py
%{python_sitelib}/DistUtilsExtra/__init__.pyc
%{python_sitelib}/DistUtilsExtra/__init__.pyo
%{python_sitelib}/DistUtilsExtra/command/__init__.py
%{python_sitelib}/DistUtilsExtra/command/__init__.pyc
%{python_sitelib}/DistUtilsExtra/command/__init__.pyo
%{python_sitelib}/DistUtilsExtra/command/build_extra.py
%{python_sitelib}/DistUtilsExtra/command/build_extra.pyc
%{python_sitelib}/DistUtilsExtra/command/build_extra.pyo
%{python_sitelib}/DistUtilsExtra/command/build_help.py
%{python_sitelib}/DistUtilsExtra/command/build_help.pyc
%{python_sitelib}/DistUtilsExtra/command/build_help.pyo
%{python_sitelib}/DistUtilsExtra/command/build_i18n.py
%{python_sitelib}/DistUtilsExtra/command/build_i18n.pyc
%{python_sitelib}/DistUtilsExtra/command/build_i18n.pyo
%{python_sitelib}/DistUtilsExtra/command/build_icons.py
%{python_sitelib}/DistUtilsExtra/command/build_icons.pyc
%{python_sitelib}/DistUtilsExtra/command/build_icons.pyo
%{python_sitelib}/DistUtilsExtra/command/clean_i18n.py
%{python_sitelib}/DistUtilsExtra/command/clean_i18n.pyc
%{python_sitelib}/DistUtilsExtra/command/clean_i18n.pyo
%{python_sitelib}/python_distutils_extra-0.90-py2.5.egg-info/PKG-INFO
%{python_sitelib}/python_distutils_extra-0.90-py2.5.egg-info/SOURCES.txt
%{python_sitelib}/python_distutils_extra-0.90-py2.5.egg-info/dependency_links.txt
%{python_sitelib}/python_distutils_extra-0.90-py2.5.egg-info/entry_points.txt
%{python_sitelib}/python_distutils_extra-0.90-py2.5.egg-info/top_level.txt

%changelog
* Tue Sep 16 2008 Bob <bob@glumol.com>
Initial release
