import wx
import wxaddons.sized_controls as sc
import wx.wizard as wiz
import os, os.path as path
import templates
import string

class LicensePage(wiz.WizardPageSimple):
    def __init__(self, parent, title):
        wiz.WizardPageSimple.__init__(self, parent)
        sizer = wx.BoxSizer(wx.VERTICAL)
        self.SetSizer(sizer)
        title = wx.StaticText(self, -1, title)
        title.SetFont(wx.Font(14, wx.SWISS, wx.NORMAL, wx.BOLD))
        sizer.Add(title, 0, wx.ALIGN_CENTRE|wx.ALL, 5)
        sizer.Add(wx.StaticLine(self, -1), 0, wx.EXPAND|wx.ALL, 5)
        sizer.Add(wx.TextCtrl(self, -1, open("/usr/share/doc/vmware/VMware_Server_Console_EULA").read(), size = (300, 300), style = wx.TE_MULTILINE), 1, wx.EXPAND|wx.ALL, 5)
        self.agree = wx.CheckBox(self, -1, "I do agree to these terms")
        sizer.Add(self.agree)
        self.Bind(wx.EVT_CHECKBOX, self.OnAgree, self.agree)
        self.sizer = sizer

        wx.FindWindowById(wx.ID_FORWARD).Disable()
        # return sizer

    def OnAgree(self, evt):
        if self.agree.GetValue():
            wx.FindWindowById(wx.ID_FORWARD).Enable()
        else:
            wx.FindWindowById(wx.ID_FORWARD).Disable()

class UpdatePage(wiz.WizardPageSimple):
    def __init__(self, parent, title):
        wiz.WizardPageSimple.__init__(self, parent)

    def SetUp(self):
        sizer = wx.BoxSizer(wx.VERTICAL)
        self.text = wx.StaticText(self, -1, "          Please wait while VMware Server is being updated...          ")
        sizer.Add(self.text, 2, wx.ALL, 5)

        """
        kernel-devel
        gcc
        xinetd
        http://knihovny.cvut.cz/ftp/pub/vmware/vmware-any-any-update114.tar.gz
        """
        import wx.lib.delayedresult as delayedresult
        
        self.jobID = 1
        self.abortEvent = delayedresult.AbortEvent()

        delayedresult.startWorker(self._resultConsumer, self._resultProducer, 
                                  wargs=(self.jobID,self.abortEvent), jobID=self.jobID)

                        
    def _resultProducer(self, jobID, abortEvent):
        """Pretend to be a complex worker function or something that takes 
        long time to run due to network access etc. GUI will freeze if this 
        method is not called in separate thread."""

        if path.exists("/etc/vmware/not_configured"):
            os.chdir("/tmp")
	    os.system("wget http://knihovny.cvut.cz/ftp/pub/vmware/vmware-any-any-update114.tar.gz")
            os.system("tar xvzf vmware-any-any-update114.tar.gz")
            os.chdir("vmware-any-any-update114")
            open('inputs', 'w').write("yes\nyes\nyes\nyes\nyes\nyes\nyes\nyes\nyes\n")
            os.system("./runme.pl < inputs")
            os.chdir("..")
            os.system("rm -rf vmware-any-any-update114 vmware-any-any-update114.tar.gz")

        return jobID


    def handleAbort(self, event): 
        """Abort the result computation."""
        #self.buttonGet.Enable(True)
        #self.buttonAbort.Enable(False)
        self.abortEvent.set()

        
    def _resultConsumer(self, delayedResult):
        jobID = delayedResult.getJobID()
        assert jobID == self.jobID
        try:
            result = delayedResult.get()
        except Exception, exc:
            print "Result for job %s raised exception: %s" % (jobID, exc)
            return
        
        # output result
        self.text.SetLabel("VMware Server has been successfully updated")
        print "Got result for job %s: %s" % (jobID, result)
        # self.textCtrlResult.SetValue(str(result))
        
        # get ready for next job:
        # self.buttonGet.Enable(True)
        # self.buttonAbort.Enable(False)


class DetectionPage(wiz.WizardPageSimple):
    windows_types = ["Windows 95", "Windows 98", "Windows 2000", "Windows XP",
                   "Windows Vista"]

    def DetectWindows(self, path):
        return "Windows XP"

    def SetUp(self):
        sizer = wx.BoxSizer(wx.VERTICAL)
        
        import Fstab.Fstab as fstab
        m = fstab.MntFile("/etc/fstab")
        n = 0
        self.vms = []
        for part in m:
            if part.get_is_mounted():
                if path.exists(path.join(part["FSTAB_PATH"], "WINDOWS")):
                    self.dev = part["DEVICE"]
                    self.path = part["FSTAB_PATH"]
                    print "Found a Windows installation at ", part["FSTAB_PATH"], self.dev
                    while self.dev[-1] in string.digits:
                        self.dev = self.dev[:-1]

                    if n:
                        sizer.Add(wx.StaticLine(self, -1), 0, wx.EXPAND|wx.ALL, 5)

                    win_type = self.DetectWindows(part["FSTAB_PATH"])
                    title = wx.StaticText(self, 1, "Found a Windows installation at " + part["FSTAB_PATH"])
                    sizer.Add(title, 0, wx.ALL, 5)
                    title2 = wx.StaticText(self, 2, "It seems to be " + win_type + ". Please correct it if it's  wrong :")
                    sizer.Add(title2, 0, wx.ALL, 5)
                                        
                    self.choice = wx.Choice(self, 3, choices = self.windows_types)
                    self.choice.SetStringSelection(win_type)
                    sizer.Add(self.choice, 0, wx.ALL, 5)
        
                    self.start = wx.CheckBox(self, -1, "Start it once set up")
                    self.start.SetValue(True)
                    sizer.Add(self.start, 0, wx.ALL, 5)

                    self.vms.append([self.dev, self.path, self.choice, self.start])

                    n = n + 1

        n = 0
        if n == 0:
            title = wx.StaticText(self, 1, "Not existing Windows installation has been found.")
            sizer.Add(title, 0, wx.ALL, 5)
        self.SetSizer(sizer)

class MyApp(wx.App):
    def OnInit(self):
        self.SetAppName("Configure VMWare")
        
        self.pages = []

        return True

    def SetPages(self, pages):
        self.pages = pages

    def CreateVirtualMachine(self, dev, fullpath, name, version, start):
        vmware_path = "/var/lib/vmware/Virtual Machines/"

        sysname = name
        n = 2
        tmp_path = path.join(vmware_path, sysname)
        while path.exists(tmp_path):
            sysname = name + " " + str(n)
            tmp_path = path.join(vmware_path, sysname)
            n = n + 1

        vm_path = path.join(vmware_path, sysname)
        try: os.mkdir(vm_path)
        except: pass
        os.chdir(vm_path)
        vmdk = string.Template(templates.vmdk).substitute({ "device" : dev })
        mem = 1024 # int(open("/proc/meminfo").readlines()[0].split()[1]) / 1024
        vmx = string.Template(templates.vmx).substitute({ "mem" : mem, "displayname" : name, "name" : sysname })
        open(path.join(vm_path, sysname + ".vmdk"), "w").write(vmdk)
        open(path.join(vm_path, sysname + ".vmsd"), "w").write("")
        open(path.join(vm_path, sysname + ".vmx"), "w").write(vmx)        
        open('/etc/vmware/vm-list', "a").write("\nconfig \"/var/lib/vmware/Virtual Machines/" + sysname + "/" + sysname + ".vmx\"")
        if start:
            os.spawnl(os.P_NOWAIT,  "/usr/bin/vmrun", "/usr/bin/vmrun", "start", "/var/lib/vmware/Virtual Machines/" + sysname + "/" + sysname + ".vmx")
            os.spawnl(os.P_NOWAIT,  "/usr/bin/vmware", "/usr/bin/vmware", "-l")

    def OnWizFinished(self, evt):
	"""
        wait = WaitWindow(None, -1, "Updating VMware Server")
        wait.Centre()
        wait.Show(True)
        """
        for vm in self.pages[2].vms:
            self.CreateVirtualMachine(vm[0], vm[1],
                                      "My Windows",
                                      vm[2].GetSelection(),
                                      vm[3].IsChecked())

    def OnWizChanged(self, evt):
        print "WizChanged !!!", evt.GetPage()
        if hasattr(evt.GetPage(), "SetUp"):
            evt.GetPage().SetUp()

def main():
    try:
        path = os.path.dirname(__file__)
        os.chdir(path)
    except:
        pass
    app = MyApp(False)
    wizard = wiz.Wizard(None, -1, "Configure VMWare Server",
                        wx.Bitmap("/usr/share/pixmaps/gnome-xterm.png"))
    page1 = LicensePage(wizard, "Please read and accept the "
                                "VMWare Server license")
    page2 = UpdatePage(wizard, "Patch and configure VMWare Server")
    page3 = DetectionPage(wizard, None, None)
    page2.SetNext(page3)
    page1.SetNext(page2)

    wizard.FitToPage(page1)
    wizard.GetPageAreaSizer().Add(page1)

    app.Bind(wiz.EVT_WIZARD_PAGE_CHANGED, app.OnWizChanged)
    app.Bind(wiz.EVT_WIZARD_FINISHED, app.OnWizFinished)

    app.SetPages([page1, page2, page3])
    wizard.RunWizard(page1)
    wizard.Destroy()
    app.MainLoop()

if __name__ == '__main__':
    __name__ = 'Main'
    main()

