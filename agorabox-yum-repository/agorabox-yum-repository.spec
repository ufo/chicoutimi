Name:           agorabox-yum-repository
Version:        1.0
Release:        1%{?dist}
Summary:        The Agorabox yum repository

Group:          System Environment/Base
License:        GPL
URL:            http://www.agoragox.org
# The source for this package was pulled from upstream's svn.  Use the
# following commands to generate the tarball:
#  svn co http://www.glumol.com/chicoutimi/agorabox-yum-repository agorabox-yum-repository
#  tar -czvf agorabox-yum-repository-1.0 agorabox-yum-repository
Source0:        agorabox-yum-repository-%{version}.tar.gz
BuildArch:      noarch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires:       yum yum-priorities


%package testing
Summary: Agorabox testing repositories
Group: Applications/System
Requires: agorabox-yum-repository


%description
The Agorabox yum repository


%description testing
The Agorabox yum testing repository


%prep
%setup -q


%build


%install
rm -rf $RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT/etc/yum.repos.d
cp agorabox.repo agorabox-testing.repo $RPM_BUILD_ROOT/etc/yum.repos.d/
mkdir -p $RPM_BUILD_ROOT/etc/pki/rpm-gpg
cp RPM-GPG-KEY-agorabox $RPM_BUILD_ROOT/etc/pki/rpm-gpg/


%clean
rm -rf $RPM_BUILD_ROOT


%files
%defattr(-,root,root,-)
%doc
%{_sysconfdir}/yum.repos.d/agorabox.repo
%{_sysconfdir}/pki/rpm-gpg/RPM-GPG-KEY-agorabox

%files testing
%{_sysconfdir}/yum.repos.d/agorabox-testing.repo


%changelog
* Wed Oct 1 2008 Bob <bob@glumol.com>
Initial release

