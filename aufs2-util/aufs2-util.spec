Name:           aufs2-util
Version:        0.1
Release:        1%{?dist}
Summary:        Utilities for AUFSv2

Group:          System/Tools
License:        GPLv2
URL:            http://git.c3sl.ufpr.br/pub/scm/aufs/aufs2-util.git
Source0:        aufs2-util.tar.gz
Patch0:         aufs2-util-nostatic.patch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)


%description


%prep
%setup -n aufs2-util
%patch0 -p1


%build
make %{?_smp_mflags}


%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT


%clean
rm -rf $RPM_BUILD_ROOT


%files
%defattr(-,root,root,-)
%doc README
%{_sysconfdir}/default/aufs
%{_bindir}/aubrsync
%{_mandir}/man5/aufs.5.gz
# %{_bindir}/aufs.shlib
%{_bindir}/auchk
/sbin/mount.aufs
/sbin/auplink
/sbin/umount.aufs


%changelog
* Fri Aug 14 2009 Sylvain Baubeau <sylvain.baubeau@agorabox.org> - 0.6pre3
- Initial release
