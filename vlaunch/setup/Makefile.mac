RESOURCES=dist/UFO.app/Contents/Resources
FRAMEWORKS=dist/UFO.app/Contents/Frameworks
MACOS=dist/UFO.app/Contents/MacOS
VBOXAPP=dist/UFO.app/Contents/Resources/VirtualBox.app
VBOXMACOS=dist/UFO.app/Contents/Resources/VirtualBox.app/Contents/MacOS

py2app:
	if [ ! -f ../src/UFO.py ]; \
	then \
		ln -s launcher.py ../src/UFO.py; \
	fi; \
	rm -rf dist build
	python setup-mac.py py2app # --no-strip --packages=PyQt4

pkg: py2app
	mkdir -p dist/packagemaker/Applications
	mv dist/UFO.app dist/packagemaker/Applications
	/Developer/Tools/packagemaker -build -proj U.F.O\ Launcher.pmproj -p dist/U.F.O\ Launcher.pkg

clean:
	rm -rf dist/packagemaker

mpkg:
	/Developer/Tools/packagemaker -build -proj "U.F.O for Mac Intel hosts.pmproj" -p "dist/U.F.O for Intel Mac hosts.mpkg"

mac: clean py2app pkg mpkg

update-key:
	rm -rf /Volumes/UFO/Mac-Intel/UFO.app
	cp -R dist/UFO.app /Volumes/UFO/Mac-Intel/
	mkdir /Volumes/UFO/Mac-Intel/UFO.app/Contents/Resources/settings
	cp settings.conf.mac /Volumes/UFO/Mac-Intel/UFO.app/Contents/Resources/settings/settings.conf
	cp -R .VirtualBox /Volumes/UFO/Mac-Intel/UFO.app/Contents/Resources/

test:
	mkdir dist/UFO.app/Contents/Resources/settings
	cp settings.conf.mac dist/UFO.app/Contents/Resources/settings/settings.conf
	cp -R .VirtualBox dist/UFO.app/Contents/Resources/

mac-launcher: clean py2app
	if [ -z "$$VBOX_PATH" ]; \
	then \
		exit 1; \
	fi; \
	cp -R $$VBOX_PATH ${VBOXAPP}
	rm -rf ${VBOXMACOS}/tst* ${VBOXMACOS}/testcase
	# cp -R kexts ${VBOXMACOS}/kexts
	mkdir -p ${VBOXMACOS}/kexts/Tiger
	cp -R ${VBOX_PATH}/../*.kext ${VBOXMACOS}/kexts
	mv ${VBOXMACOS}/kexts/VBoxDrvTiger.kext ${VBOXMACOS}/kexts/Tiger
	mkdir ${VBOXAPP}/Contents/Frameworks
	cp -R /Library/Frameworks/QtCore.framework ${VBOXAPP}/Contents/Frameworks/
	cp -f /Library/Frameworks/Python.framework/Python ${FRAMEWORKS}/Python.framework/Versions/2.5/Python
	cp -R /Library/Frameworks/QtGui.framework ${VBOXAPP}/Contents/Frameworks/
	cp -R /Library/Frameworks/QtNetwork.framework ${VBOXAPP}/Contents/Frameworks/
	cp -R /Library/Frameworks/QtOpenGL.framework ${VBOXAPP}/Contents/Frameworks/
	# rm ${VBOXMACOS}/VBoxPython2_{5,6}.so
	# mv ${VBOXMACOS}/VBoxPython{2_3.so,2_5.so}
	cp /opt/local/lib/libiconv.2.dylib /opt/local/lib/libz.1.dylib /opt/local/lib/libcrypto.0.9.8.dylib /opt/local/lib/libcurl.4.dylib ${VBOXMACOS}/
	for file in `ls ${VBOXMACOS}`; do ln -s ../Resources/VirtualBox.app/Contents/MacOS/$$file dist/UFO.app/Contents/MacOS/$$file; done
	strip -S ${VBOXMACOS}/* ${VBOXMACOS}/components/* || true
	rm -rf ${VBOXMACOS}/testcase 
	rm -rf ${VBOXMACOS}/tst* 
	rm -f ${RESOURCES}/site.pyc
	
	for lib in QtGui QtNetwork; \
	do \
		install_name_tool -change QtCore.framework/Versions/4/QtCore @executable_path/../Frameworks/QtCore.framework/Versions/4/QtCore ${VBOXAPP}/Contents/Frameworks/$$lib.framework/Versions/Current/$$lib; \
		install_name_tool -change QtGui.framework/Versions/4/QtGui @executable_path/../Frameworks/QtGui.framework/Versions/4/QtGui ${VBOXAPP}/Contents/Frameworks/$$lib.framework/Versions/Current/$$lib; \
		install_name_tool -change QtNetwork.framework/Versions/4/QtNetwork @executable_path/../Frameworks/QtNetwork.framework/Versions/4/QtNetwork ${VBOXAPP}/Contents/Frameworks/$$lib.framework/Versions/Current/$$lib; \
	done
	
	# install_name_tool -change QtCore.framework/Versions/4/QtCore @executable_path/../Frameworks/QtCore.framework/Versions/4/QtCore ${VBOXAPP}/Contents/Frameworks/QtNetwork.framework/Versions/4/QtNetwork.framework/Versions/Current/QtGui
	install_name_tool -change /Library/Frameworks/Python.framework/Versions/2.5/Python @executable_path/../Frameworks/Python.framework/Versions/2.5/Python ${VBOXMACOS}/VBoxPython2_5.so
	
	install_name_tool -change QtCore.framework/Versions/4/QtCore @executable_path/../Frameworks/QtCore.framework/Versions/4/QtCore ${VBOXMACOS}/VirtualBoxVM
	install_name_tool -change QtGui.framework/Versions/4/QtGui @executable_path/../Frameworks/QtGui.framework/Versions/4/QtGui ${VBOXMACOS}/VirtualBoxVM
	install_name_tool -change QtNetwork.framework/Versions/4/QtNetwork @executable_path/../Frameworks/QtNetwork.framework/Versions/4/QtNetwork ${VBOXMACOS}/VirtualBoxVM

	install_name_tool -change QtCore.framework/Versions/4/QtCore @executable_path/../Frameworks/QtCore.framework/Versions/4/QtCore ${VBOXMACOS}/VirtualBox
	install_name_tool -change QtGui.framework/Versions/4/QtGui @executable_path/../Frameworks/QtGui.framework/Versions/4/QtGui ${VBOXMACOS}/VirtualBox
	install_name_tool -change QtNetwork.framework/Versions/4/QtNetwork @executable_path/../Frameworks/QtNetwork.framework/Versions/4/QtNetwork ${VBOXMACOS}/VirtualBox

	install_name_tool -change /System/Library/Frameworks/Python.framework/Versions/2.5/Python @executable_path/../Frameworks/Python.framework/Versions/2.5/Python ${VBOXMACOS}/VBoxPython2_5.so
	install_name_tool -change /System/Library/Frameworks/Python.framework/Versions/2.3/Python @executable_path/../Frameworks/Python.framework/Versions/2.5/Python ${VBOXMACOS}/VBoxPython2_5.so

	for qt in QtCore.framework QtGui.framework QtNetwork.framework QtOpenGL.framework; \
	do \
	    rm -rf dist/UFO.app/Contents/Frameworks/$$qt; \
	    ln -s ../Resources/VirtualBox.app/Contents/Frameworks/$$qt ${FRAMEWORKS}/$$qt; \
	done

	install_name_tool -change /opt/local/lib/libiconv.2.dylib @executable_path/libiconv.2.dylib ${VBOXMACOS}/VBoxRT.dylib
	install_name_tool -change /opt/local/lib/libz.1.dylib /usr/lib/libz.1.dylib ${VBOXMACOS}/VBoxRT.dylib
	install_name_tool -change /opt/local/lib/libcrypto.0.9.8.dylib @executable_path/libcrypto.0.9.8.dylib ${VBOXMACOS}/VBoxRT.dylib
	install_name_tool -change /opt/local/lib/libcurl.4.dylib @executable_path/libcurl.4.dylib ${VBOXMACOS}/VBoxRT.dylib

	install_name_tool -change /opt/local/lib/libz.1.dylib /usr/lib/libz.1.dylib ${VBOXMACOS}/libcrypto.0.9.8.dylib
	install_name_tool -change /opt/local/lib/libcrypto.0.9.8.dylib @executable_path/libcrypto.0.9.8.dylib ${VBOXMACOS}/libcrypto.0.9.8.dylib

	install_name_tool -change /opt/local/lib/libcurl.4.dylib @executable_path/libcurl.4.dylib ${VBOXMACOS}/libcurl.4.dylib
	install_name_tool -change /opt/local/lib/libz.1.dylib /usr/lib/libz.1.dylib ${VBOXMACOS}/libcurl.4.dylib

	install_name_tool -change /opt/local/lib/libz.1.dylib /usr/lib/libz.1.dylib ${VBOXMACOS}/libz.1.dylib

	install_name_tool -change /opt/local/lib/libiconv.2.dylib @executable_path/libiconv.2.dylib ${VBOXMACOS}/libiconv.2.dylib

tar-mac-launcher:
	cd dist && tar cvzf mac-intel.tgz UFO.app && cd ..

upload-mac-launcher: mac-launcher tar-mac-launcher
	scp dist/mac-intel.tgz bob@kickstart:/var/www/html/private/virtualization/mac-intel.tgz

livecd-testing: mac-launcher
	mkdir -p dist/UFO.app/Contents/Resources/.data/images
	cp ../graphics/*.png ../graphics/*.bmp ../graphics/*.mng dist/UFO.app/Contents/Resources/.data/images
	cp settings.conf.livecd.macos dist/UFO.app/Contents/Resources/settings.conf
	cp bootdisk.vdi ufo_swap.vdi dist/UFO.app/Contents/Resources/.data

upload-livecd-testing: livecd-testing
	cd dist && tar cvzf mac-intel-livecd-testing.tgz UFO.app && cd ..
	scp dist/mac-intel-livecd-testing.tgz bob@kickstart.agorabox.org:/var/www/html/private/virtualization/

testing: mac-launcher
	rm -rf testing
	mkdir -p testing
	mkdir -p testing/.data
	mkdir -p testing/.data/settings
	mkdir -p testing/.data/images
	mkdir -p testing/.data/.VirtualBox/Images
	mkdir -p testing/.data/.VirtualBox/HardDisks
	mkdir -p testing/.data/logs
	cp ../graphics/*.png ../graphics/*.bmp ../graphics/*.mng testing/.data/images
	cp settings.conf testing/.data/settings
	cp UFO-VirtualBox-boot.img testing/.data/.VirtualBox/Images
	cp ufo_swap.vdi ufo_overlay.vdi testing/.data/.VirtualBox/HardDisks
	rm dist/mac-intel.tgz
	mv dist testing/Mac-Intel
	cd testing && tar cvzf mac-intel.tgz *
	scp testing/mac-intel.tgz bob@kickstart:/var/www/html/private/virtualization/mac-intel-runnable.tgz

