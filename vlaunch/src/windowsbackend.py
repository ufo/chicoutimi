# -*- coding: utf-8 -*-

# UFO-launcher - A multi-platform virtual machine launcher for the UFO OS
#
# Copyright (c) 2008-2010 Agorabox, Inc.
#
# This is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA. 


import _winreg
import win32con
import os, os.path as path
import wmi
import sys
import logging
import conf
import tempfile
import platform
import glob
import gui
import time
import utils

from osbackend import OSBackend
from shutil import copyfile

class WindowsBackend(OSBackend):

    VBOXMANAGE_EXECUTABLE = "VBoxManage.exe"
    VIRTUALBOX_EXECUTABLE = "VirtualBox.exe"
    RELATIVE_VMDK_POLICY  = False

    def __init__(self):
        OSBackend.__init__(self, "windows")
        self.WMI = wmi.WMI()

    def get_default_audio_driver(self):
        return self.vbox.constants.AudioDriverType_DirectSound

    def check_process(self):
        logging.debug("Checking UFO process")
        # TODO: Get pid for possible kill
        processes = self.WMI.Win32_Process(Name="ufo.exe")
        logging.debug("ufo process : " + str(processes))
        if len(processes) > 1:
            logging.debug(str([ x.Name for x in processes if x.ProcessId != os.getpid() ]))
            self.error_already_running("\n".join([ x.Name for x in processes if x.ProcessId != os.getpid() ]).strip())

        logging.debug("Checking VBoxXPCOMIPCD process")
        processes = self.WMI.Win32_Process(Name="VBoxSVC.exe")
        if len(processes)>1 :
            logging.debug("VBoxXPCOMIPCD is still running. Exiting")
            self.error_already_running("\n".join([ x.Name for x in processes ]), "VirtualBox")
            sys.exit(0)

    def prepare_update(self):
        updater_path = tempfile.mkdtemp(prefix="ufo-updater")
        logging.debug("Copying updater to " + updater_path)
        exe_path = path.join(updater_path, "ufo.exe")
        patterns = [ "*.exe", "*.dll", "bin\\library.zip", "bin\\Qt*.dll", "bin\\msv*.dll", "bin\\*.pyd", "bin\\py*.dll" ]
        files = []
        for pattern in patterns:
            files += [ utils.relpath(x, conf.SCRIPT_DIR) for x in glob.glob(path.join(conf.SCRIPT_DIR, pattern)) ]
        for file in files:
            dest = path.join(updater_path, file)
            folder = path.dirname(dest)
            if not path.exists(folder):
                os.makedirs(folder)
            copyfile(file, dest)
        return exe_path

    def call(self, cmd, env = None, shell = True, cwd = None, output=False):
        return OSBackend.call(self, cmd, env, shell, cwd, output)

    def start_services(self):
        start_service = True
        if conf.CREATESRVS:
            logging.debug("Checking if service PortableVBoxDrv exists")
            retcode, output = self.call([ "sc", "query", "PortableVBoxDrv" ], shell=True, output=True)
            create_service = True
            if retcode == 0 and not ("FAILED" in output):
                logging.debug("Service PortableVBoxDrv exists")
                lines = output.split("\n")
                for line in lines:
                    splt = line.split()
                    if "STATE" in splt:
                        logging.debug("State " + splt[-1])
                        if splt[-1] == "STOPPED":
                            logging.debug("Removing PortableVBoxDrv")
                            self.call([ "sc", "delete", "PortableVBoxDrv" ], shell=True)
                        elif splt[-1] == "RUNNING":
                            logging.debug("Service PortableVBoxDrv is running")
                            create_service = False
                            start_service = False

            if create_service:
                logging.debug("Creating services :")
                ret, output = self.call([ "sc", "create", "PortableVBoxDrv",
                                           "binpath=", path.join(conf.VBOXDRIVERS, "VBoxDrv.sys"),
                                           "type=", "kernel", "start=", "demand", "error=", "normal", 
                                           "displayname=", "PortableVBoxDrv" ], shell=True, output=True)
                if ret == 5 or "FAILED" in output:
                    return 1

            if self.puel:
                self.call([ "sc", "create", "PortableVBoxUSBMon", "binpath=", 
                             path.join(conf.BIN, "drivers", "USB", "filter", "VBoxUSBMon.sys"),
                             "type=", "kernel", "start=", "demand", "error=", "normal", 
                             "displayname=", "PortableVBoxUSBMon" ], shell=True)
                         

                try:
                    key = _winreg.OpenKey(_winreg.HKEY_LOCAL_MACHINE, 
                                          "SYSTEM\\CurrentControlSet\\Services\\VBoxUSB")
                    if _winreg.QueryValue(key, "DisplayName") != "VirtualBox USB":
                        self.call(["sc", "create", "VBoxUSB", "binpath=", 
                                   path.join(conf.BIN, "drivers", "USB", "device", "VBoxUSB.sys"),
                                   "type=", "kernel", "start=", "demand", "error=", "normal", 
                                   "displayname=", "PortableVBoxUSB" ], shell=True)
                except:
                    logging.debug("The key HKEY_LOCAL_MACHINE\\SYSTEM\\CurrentControlSet\\Services\\VBoxUSB does not exist")
    
        if conf.STARTSRVS and start_service:
            logging.debug("Starting services :")
        
            code, output = self.call([ "sc", "start", "PortableVBoxDRV" ], shell=True, output=True)
            if code in [ 1060, 5 ] or "FAILED" in output:
                logging.debug("Got error: " + str(code) + " " + output)
                return 1

            if self.puel:
                self.call([ "sc", "start", "PortableVBoxUSBMon" ], shell=True)

        logging.debug("Re-registering server:")

        if self.call([ path.join(conf.BIN, "VBoxSVC.exe"), "/reregserver" ], cwd = conf.BIN, shell=True):
            return 1
        
        self.call([ "regsvr32.exe", "/S", path.join(conf.BIN, "VBoxC.dll") ], cwd = conf.BIN, shell=True)
        self.call([ "rundll32.exe", "/S", path.join(conf.BIN, "VBoxRT.dll"), "RTR3Init" ], cwd = conf.BIN, shell=True)
    
        return 0

    def kill_resilient_vbox(self):
        self.call([ 'taskkill', '/F', '/IM', 'VBoxSVC.exe' ], shell=True) 

    def stop_services(self):
        if conf.STARTSRVS:
            # self.call([ "sc", "stop", "PortableVBoxDRV" ], shell=True)
            if self.puel:
                self.call([ "sc", "stop", "PortableVBoxUSBMon" ], shell=True)
            self.call([ path.join(conf.BIN, "VBoxSVC.exe"), "/unregserver" ], shell=True)
            self.call([ "regsvr32.exe", "/S", "/U", path.join(conf.BIN, "VBoxC.dll") ], shell=True)

        if conf.CREATESRVS:
            # Do not delete service because some Windows reports "service mark as deleted"
            # self.call([ "sc", "delete", "PortableVBoxDRV" ], shell=True)
            if self.puel:
                self.call([ "sc", "delete", "PortableVBoxUSBMon" ], shell=True)

        if self.puel:
            try:
                key = _winreg.OpenKey(_winreg.HKEY_LOCAL_MACHINE, "SYSTEM\\CurrentControlSet\\Services\\VBoxUSB")
                if _winreg.QueryValue(key, "DisplayName") != "VirtualBox USB":
                    self.call([ "sc", "stop", "VBoxUSB" ], shell=True)
                    self.call([ "sc", "delete", "VBoxUSB" ], shell=True)
            except:
                logging.debug("The key HKEY_LOCAL_MACHINE\\SYSTEM\\CurrentControlSet\\Services\\VBoxUSB does not exist")

    def get_device_parts(self, device_name):
        disks = self.WMI.Win32_DiskDrive(Name = device_name)
        if not disks:
            return {}

        partitions = disks[0].associators(wmi_association_class="Win32_DiskDriveToDiskPartition")
        if not partitions:
            return {}

        device_parts = {}
        for part in partitions:
            part_info = [ device_name, part.NumberOfBlocks ]
            device_parts.update({ (int(part.Index) + 1) : part_info })
        return device_parts

    def find_device_by_volume(self, dev_volume):
        logical_disks = self.WMI.Win32_LogicalDisk (VolumeName = dev_volume)
        if not logical_disks:
            return ""
        
        partitions = logical_disks[0].associators(wmi_association_class="Win32_LogicalDiskToPartition")
        if not partitions:
            return ""
        
        disks = partitions[0].associators(wmi_result_class="Win32_DiskDrive")
        if len(disks) > 0:
            return disks[0].Name
        return ""

    def find_device_by_model(self, model):
        disks = self.WMI.Win32_DiskDrive(Model = model)
    
        # Probably manage multiple UFO key choice...
        if len(disks) > 0:
            return disks[0].Name
        return ""

    def get_disk_from_partition(self, part):
        partitions = part.associators(wmi_association_class="Win32_LogicalDiskToPartition")
        if not partitions:
            return ""

        disks = partitions[0].associators(wmi_result_class="Win32_DiskDrive")
        if len(disks) > 0:
            return disks[0].Name

    def prepare_device(self, dev):
        pass

    def get_device_size(self, name, partition = 0):
        assert partition == 0

        import win32file
        import win32con
        import winioctlcon
        import struct

        hFile = win32file.CreateFile(name, win32con.GENERIC_READ, 0, None, win32con.OPEN_EXISTING, 0, None)
        buffer = " " * 1024
        data = win32file.DeviceIoControl(hFile, winioctlcon.IOCTL_DISK_GET_DRIVE_GEOMETRY, buffer, 1024)
        tup = struct.unpack("qiiii", data)
        if tup[1] in (winioctlcon.FixedMedia, winioctlcon.RemovableMedia):
            size = reduce(int.__mul__, tup[:1] + tup[2:])
            logging.debug("Found FixedMedia or RemovableMedia of size " + str(size))
        data = win32file.DeviceIoControl(hFile, winioctlcon.IOCTL_DISK_GET_LENGTH_INFO, buffer, 1024)
        if data:
            tup = struct.unpack("q", data)
            size = tup[0]
            logging.debug("Found regular device of size " + str(size))
        return size >> 9

    def list_devices(self):
        for disk in self.WMI.Win32_DiskDrive():
            print disk.Model

    def find_network_device(self):
        if conf.NETTYPE == conf.NET_HOST:
            if conf.HOSTNET != "":
                return conf.NET_HOST, conf.HOSTNET
        
            net = self.WMI.Win32_networkadapter(NetConnectionStatus = 2)
    
            for adapter in net:
                try: logging.debug("Found net adapter " + adapter.Name)
                except: pass
                if '1394' in adapter.Name:
                    continue
                # Probably except VMware connection for example...
                return conf.NET_HOST, adapter.Name

        return conf.NET_NAT, ""

    def get_dvd_device(self):
        cds = self.WMI.Win32_CDROMDrive()
        drive, burner = None, False
        for cd in cds:
            if not burner: # Already found one, should use the faster one...
                drive, burner = cd.Id, 4 in cd.Capabilities
        return drive
    
    def find_resolution(self):
        if gui.backend == "PyQt":
            return str(gui.screenRect.width()) + "x" + str(gui.screenRect.height())
        
        display = self.WMI.Win32_DisplayControllerConfiguration()
    
        if len(display) > 0:
            return str(display[0].HorizontalResolution) + "x" + str(display[0].VerticalResolution)
        return ""

    def get_free_ram (self):
        try:
            ram = self.WMI.Win32_PerfFormattedData_PerfOS_Memory()
            if len(ram) > 0:
                return max((int(ram[0].AvailableBytes) + int(ram[0].CacheBytes)) / (1<<20), 384)
        except:
            logging.debug("Failed creating WMI object Win32_PerfFormattedData_PerfOS_Memory")
            ram = self.WMI.Win32_ComputerSystem()
            if len(ram) > 0:
                return max(int(ram[0].TotalPhysicalMemory) / 1024 / 1024 / 2, 384)
        return 0

    def get_host_shares(self):
        import _winreg
        shares = []
        wanted = { 'Desktop'  : _("My Windows desktop"),
                   'Personal' : _("My Windows documents") }

        key_string = "Software\Microsoft\Windows\CurrentVersion\Explorer\Shell Folders"
        try:
            hive = _winreg.ConnectRegistry(None, _winreg.HKEY_CURRENT_USER)
            key  = _winreg.OpenKey(hive, key_string)
            for i in range(0, _winreg.QueryInfoKey(key)[1]):
                name, value, type = _winreg.EnumValue(key, i)
                if name in wanted:
                    shares.append({ 'sharename' : "windows" + name.lower() + "hosthome",
                                    'sharepath' : value,
                                    'displayed' : wanted[name] })
            _winreg.CloseKey(key)
            _winreg.CloseKey(hive)

        except WindowsError:
            shares.append({ 'sharename' : "windowshosthome",
                            'sharepath' : path.expanduser('~'),
                            'displayed' : "Mes documents Windows" })

        return shares
    
    def get_usb_devices(self):
        logical_disks = self.WMI.Win32_LogicalDisk (DriveType = 2)
        return [[logical_disk.Caption + '\\',
                  str(logical_disk.Caption) + str("_") + str(logical_disk.VolumeName),
                  self.get_disk_from_partition(logical_disk) ] for logical_disk in logical_disks ]

    def get_usb_sticks(self):
        disks = self.WMI.Win32_DiskDrive()
        return [[ disk.Name, disk.Model ] for disk in disks if disk.InterfaceType == "USB" ]

    def rights_error(self):
        msg = _("You don't have enough permissions to run UFO.")
        logging.debug("Using Windows version " + str(platform.win32_ver()))
        if platform.win32_ver()[0].lower() == "vista":
            msg += _("Run UFO as Administrator by right clicking on UFO and select : 'Run as administrator'")
        else:
            msg += _("Run UFO as Administrator by right clicking on UFO and select : 'Run as ...'")
        gui.dialog_info(title=_("Not enough permissions"), msg="\n\n" + msg)
        sys.exit(1)

    def prepare(self):
        # Ajusting paths
        if not conf.HOME: 
            conf.HOME = path.join(conf.APP_PATH, ".VirtualBox")

        try:
            key = _winreg.OpenKey(_winreg.HKEY_LOCAL_MACHINE, "SYSTEM\\CurrentControlSet\\Services\\VBoxUSB")
            conf.VBOX_INSTALLED = True
        except:
            conf.VBOX_INSTALLED = False

        if self.start_services():
            logging.debug("Insufficient rights")
            self.rights_error()

    def cleanup(self):
        self.stop_services()
        if self.eject_at_exit:
            self.eject_key()

    def eject_key(self):
        self.call([ path.join(conf.BIN, "USB_Disk_Eject.exe"), "/REMOVETHIS" ], shell=True)

    def run_vbox(self, command, env):
        self.call(command, env = env, shell=True)

    def wait_for_events(self, interval):
        # Overloaded on Windows because of a VERY STRANGE bug
        # After the first call to VBox's waitForEvents, the
        # characters typed into the balloon windows are uppercase...
        time.sleep(interval)
        gui.app.process_gui_events()

    def get_free_size(self, path):
        logical_disks = self.WMI.Win32_LogicalDisk (Caption = path[0:2])
        if not logical_disks:
            return ""

        return int(logical_disks[0].FreeSpace) / 1000000

    def onExtraDataCanChange(self, key, value):
        # win32com need 3 return values (2 out parameters and return value)
        return "", True, 0

