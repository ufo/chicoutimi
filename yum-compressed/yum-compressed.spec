Name:           yum-compressed
Version:        0.1
Release:        1%{?dist}
Summary:        Installs applications on a compressed filesystem

Group:          System/Tools
License:        GPLv2
URL:            http://www.agorabox.org
Source0:        %{name}-%{version}.tar.gz
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

# BuildRequires:  
Requires:       yum

%description
Installs applications on a compressed filesystem


%prep
%setup -q


%build
python <<EOF
import gdbm

db = gdbm.open("yum-compressed.db", "c")
pkgs = open("packages", "r").read().split()
for pkg in pkgs:
    db[pkg] = "1"
db.close()
EOF
ls


%install
rm -rf $RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT/var/lib/rpm
install -m 644 packages $RPM_BUILD_ROOT/var/lib/rpm/yum-compressed
install -m 644 yum-compressed.db $RPM_BUILD_ROOT/var/lib/rpm/yum-compressed.db
mkdir -p $RPM_BUILD_ROOT/usr/lib/yum-plugins
install -m 644 yum-compressed.py $RPM_BUILD_ROOT/usr/lib/yum-plugins
mkdir -p $RPM_BUILD_ROOT/etc/yum/pluginconf.d
install -m 644 yum-compressed.conf $RPM_BUILD_ROOT/etc/yum/pluginconf.d


%clean
rm -rf $RPM_BUILD_ROOT


%files
%defattr(-,root,root,-)
%doc
%{_sysconfdir}/yum/pluginconf.d/yum-compressed.conf
%{_libdir}/yum-plugins/yum-compressed.py*
%{_sharedstatedir}/rpm/yum-compressed
%{_sharedstatedir}/rpm/yum-compressed.db

%changelog
