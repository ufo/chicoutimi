#
# Copyright (C) 2009
# Agorabox, Inc.  All rights reserved.
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# Author(s): Sylvain Baubeau <sylvain.baubeau@agorabox.org>

from yum.plugins import TYPE_CORE

import os, os.path as path
import logging
import new
import gdbm

requires_api_version = '2.6'
plugin_type = (TYPE_CORE, )

compressed_root = ""
regular_root = ""
install_root = ""
db = None

logger = logging.getLogger("yum.verbose.main")

def switchRoot(root):
    os.system("umount " + install_root)
    os.system("mount -o bind " + root + " " + install_root)

def postconfig_hook(conduit):
    global compressed_root
    global regular_root
    global install_root
    global db
    
    compressed_root = conduit.confString("main", "compressed_root")
    regular_root = conduit.confString("main", "regular_root")
    install_root = conduit.confString("main", "install_root")
    if not path.exists(install_root):
        os.makedirs(install_root)

    logger.debug("Switching installroot from %s to %s",
          (conduit._base.conf.installroot, install_root))
    conduit._base.conf.installroot = install_root
    
    def runTransaction(self, cb):
       self.cb = cb
       return self.runTransactionBackup(cb)
    
    conduit._base.runTransactionBackup = conduit._base.runTransaction
    conduit._base.runTransaction = new.instancemethod(runTransaction, conduit._base, conduit._base.__class__)
    
    db = gdbm.open("/var/lib/rpm/yum-compressed.db", 'r')
    
def to_be_compressed(hdr):
    return db.has_key(hdr["name"])

def pretrans_hook(conduit):
    def _instOpenFile(self, bytes, total, h):
       hdr = h[0]
       if to_be_compressed(hdr):
           print "Compressing", hdr["name"]
           switchRoot(compressed_root)
       else:
           switchRoot(regular_root)
       
       return self._instOpenFileBackup(bytes, total, h)

    conduit._base.cb._instOpenFileBackup = conduit._base.cb._instOpenFile
    conduit._base.cb._instOpenFile = new.instancemethod(_instOpenFile, conduit._base.cb, conduit._base.cb.__class__)

        
