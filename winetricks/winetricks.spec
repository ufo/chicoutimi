Name:           winetricks
Version:        r184
Release:        1%{?dist}
Summary:        Install various redistributable runtime libraries for Wine
BuildArch:      noarch

Group:          Applications/Emulators
License:        LGPL
URL:            http://wiki.winehq.org/winetricks
Source0:        winetricks-trunk.tar.gz
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

# BuildRequires:  
Requires:       wine python

%description
winetricks is a quick and dirty script to download and install various redistributable runtime libraries sometimes needed to run programs in Wine.

%prep
%setup -n winetricks-trunk


%build


%install
rm -rf $RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT%{_bindir}
cp winetricks $RPM_BUILD_ROOT%{_bindir}
chmod +x $RPM_BUILD_ROOT%{_bindir}/winetricks
mkdir -p $RPM_BUILD_ROOT%{_datadir}/applications
cp winetricks.desktop $RPM_BUILD_ROOT%{_datadir}/applications


%clean
rm -rf $RPM_BUILD_ROOT


%files
%defattr(-,root,root,-)
%doc
%{_bindir}/winetricks
%{_datadir}/applications/winetricks.desktop

%changelog
* Tue Sep 16 2008 Bob <bob@glumol.com>
Initial release
