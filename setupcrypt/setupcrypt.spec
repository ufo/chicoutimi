Name:           setupcrypt
Version:        0.1
Release:        1%{?dist}
Summary:        Make symbolic links on security files
BuildArch:      noarch

Group:          System Environment/Base
License:        GPLv2
URL:            http://www.agorabox.org
Source0:        %{name}-%{version}.tar.gz
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires:       augeas-ext agorabox

%description
/!\ WARNING this rpm install needs both agorabox system partitions,
(crypted and non crypted) mounted at /system (non crypted)
and /system2 (crypted).


%prep
%setup -q


%build


%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT


%post
# remove lines from fstab
# torm=`dirname \`augtool match /files/etc/fstab/*/file /system\``
echo -e "rm $torm\nsave" | augtool
torm=`dirname \`augtool match /files/etc/fstab/*/file /system2\``
echo -e "rm $torm\nsave" | augtool

# Disable selinux
echo -e "set /files/etc/sysconfig/selinux/SELINUX disabled\nsave" | augtool
echo 0 > /selinux/enforce

cd %{_datadir}/securitylinks && source build_suid_files_list
cd %{_datadir}/securitylinks && sh make_security_links /system /system2 ln_security_passwd.cfg ln_security_suid.cfg
cd %{_datadir}/securitylinks && rm -f ln_security_suid.cfg

# unpack initrd
cd /tmp
mkdir setupcrypt && cd setupcrypt
gunzip < /boot/initrd-`uname -r`.img | cpio -ivd
grep "/system[\t ]" /etc/fstab > etc/fstab.sys

# save initrd for instance
cp /boot/initrd-`uname -r`.img /boot/initrd-`uname -r`.bak.img

# repack initrd
find . | cpio -H newc -o | gzip -9 > /boot/initrd-`uname -r`.img
cd .. && rm -Rf setupcrypt


%preun
echo "Warning, the uninstall process won't restore linked files !!!"
echo "Your system is probably completely broken"

%clean
rm -rf $RPM_BUILD_ROOT


%files
%defattr(-,root,root,-)
%doc
%dir %{_datadir}/securitylinks
%{_datadir}/securitylinks/ln_security_passwd.cfg
%{_datadir}/securitylinks/make_security_links
%{_datadir}/securitylinks/build_suid_files_list

%changelog
* Tue Oct 8 2008 Vienin <vienin@agorabox.org>
Initial release
