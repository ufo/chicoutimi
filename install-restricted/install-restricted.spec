Name:           install-restricted
Version:        1.0
Release:        1%{?dist}
BuildArch:      noarch
Summary:        Installs restricted softwares

Group:          Applications/Tools
License:        GPL
URL:            http://www.chicoutimi.org/install-restricted
Source0:        http://www.chicoutimi.org/install-restricted-1.0.tar.gz
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

Requires:       python

%description
install-restricted installs softwares that couldn't be included into the distribution due to license incompatibilities.

%prep
%setup -q


%build


%install
rm -rf $RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT/etc/yum.repos.d
cp skype.repo $RPM_BUILD_ROOT/etc/yum.repos.d/
mkdir -p $RPM_BUILD_ROOT/usr/sbin
cp install-restricted $RPM_BUILD_ROOT/usr/sbin
mkdir -p $RPM_BUILD_ROOT/usr/bin
cp launch-restricted $RPM_BUILD_ROOT/usr/bin
ln -s consolehelper $RPM_BUILD_ROOT/usr/bin/install-restricted
mkdir -p $RPM_BUILD_ROOT/usr/share/applications
cp install-skype.desktop $RPM_BUILD_ROOT/usr/share/applications
mkdir -p $RPM_BUILD_ROOT/usr/share/pixmaps
cp SkypeBlue_32x32.png $RPM_BUILD_ROOT/usr/share/pixmaps
mkdir -p $RPM_BUILD_ROOT/etc/pam.d
cp install-restricted.pam $RPM_BUILD_ROOT/etc/pam.d/install-restricted
mkdir -p $RPM_BUILD_ROOT/etc/security/console.apps
cp install-restricted.security $RPM_BUILD_ROOT/etc/security/console.apps/install-restricted

%clean
rm -rf $RPM_BUILD_ROOT


%files
%defattr(-,root,root,-)
%doc
/usr/bin/launch-restricted
/usr/bin/install-restricted
/usr/sbin/install-restricted
/etc/yum.repos.d/skype.repo
/usr/share/applications/install-skype.desktop
/usr/share/pixmaps/SkypeBlue_32x32.png
/etc/pam.d/install-restricted
/etc/security/console.apps/install-restricted

%changelog
* Thu Jul 26 2007 Bob <bob@glumol.com>
  Initial release
