Name:           augeas-ext
Version:        0.3
Release:        2%{?dist}
Summary:        Extends augeas with rules for coda and crypttab
BuildArch:      noarch
Group:          System Environment/Base
License:        GPLv2
URL:            http://www.glumol.com/chicoutimi/augeas-ext
Source0:        http://www.glumol.com/chicoutimi/augeas-ext/%{name}-%{version}.tar.gz
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires:       augeas

%description
Extends augeas with rules for coda and crypttab

%prep
%setup -q


%build


%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT


%clean
rm -rf $RPM_BUILD_ROOT


%files
%defattr(-,root,root,-)
%doc
%{_datadir}/augeas/lenses/dist/crypttab.aug
%{_datadir}/augeas/lenses/dist/coda.aug
%{_datadir}/augeas/lenses/dist/agorabox.aug


%changelog
* Sat Jan 30 2010 Sylvain Baubeau <sylvain.baubeau@agorabox.org> - 0.3-2
Parses all files in /etc/sysconfig/ufo

* Fri Sep 18 2009 Sylvain Baubeau <bob@glumol.com> - 0.3-1
Added support for /etc/sysconfig/ufo/sync

* Wed Jul 8 2009 Kevin Pouget <pouget@agorabox.org> - 0.2-2
Added /etc/sysconfig/ufo/vboxclient at agorabox.aug lense

* Mon Jan 26 2009 Sylvain Baubeau <bob@glumol.com> - 0.2-1
Added lenses for /etc/sysconfig/agorabox/*.conf

* Mon Oct 13 2008 Sylvain Baubeau <bob@glumol.com> - 0.1-1
Initial relase

