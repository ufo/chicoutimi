module Coda =
  autoload xfm

  let sc_incl (n:string) = (incl ("/etc/sysconfig/" . n))

  let filter = Util.stdexcl .
      (incl "/etc/coda/venus.conf")

  let xfm = transform Shellvars.lns filter

(* Local Variables: *)
(* mode: caml       *)
(* End:             *)
