(* Parsing /etc/crypttab *)

module Crypttab =
  autoload xfm

  let sep_tab = Util.del_ws_tab
  let sep_spc = Util.del_ws_spc
  let comma = Util.del_str ","
  let eol = del /[ \t]*\n/ "\n"

  let comment = Util.comment
  let empty   = Util.empty 

  let word = /[^,# \n\t]+/
  let comma_sep_list (l:string) =
    [ label l . store word ] . ([comma . label l . store word])*
  
  let record = [ seq "cryptent" . 
                   [ label "cryptdev" . store  word ] . sep_tab .
                   [ label "dev" . store word ] .
                   ( sep_tab . [ label "passwd" . store word ] .
                    ( sep_tab . comma_sep_list "opt" )? )?
                 . eol ]

  let lns = ( empty | comment | record ) *

  let xfm = transform lns (incl "/etc/crypttab")

  let simple2 = "luks-c81a2201-1894-4207-98a2-a5c67f8faa25 UUID=c81a2201-1894-4207-98a2-a5c67f8faa25 none\n"

  let simple3 = "luks-sda1 /dev/sda1 none none\n"

  let simple = "a	b	c	d"

  let simple_tree =
    { "1"
	{ "cryptdev" = "luks-c81a2201-1894-4207-98a2-a5c67f8faa25" }
        { "dev" = "UUID=c81a2201-1894-4207-98a2-a5c67f8faa25" }
        { "passwd" = "none" } }

  test Crypttab.lns get "/dev/hdc        /media/cdrom0   iso9660 user,noauto\n" =
    { "1"
        { "cryptdev" = "/dev/hdc" }
        { "dev" = "/media/cdrom0" }
        { "passwd" = "iso9660" }
        { "opt" = "user" }
        { "opt" = "noauto" } }

  test Crypttab.lns get simple2 = simple_tree

(* Local Variables: *)
(* mode: caml *)
(* End: *)


