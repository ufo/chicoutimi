module Agorabox =
  autoload xfm

  let sc_incl (n:string) = (incl ("/etc/sysconfig/" . n))

  let filter = Util.stdexcl .
      (incl "/etc/sysconfig/ufo/*")

  let xfm = transform Shellvars.lns filter

(* Local Variables: *)
(* mode: caml       *)
(* End:             *)
