#include <stdio.h>
#include <libnautilus-extension/nautilus-file-info.h>
#include <libnautilus-extension/nautilus-info-provider.h>

#include <unistd.h>
#include <syslog.h> 
#include <string.h>

static void _log(int err, const char *format, ...) {
    va_list args;

    va_start(args, format);
    openlog("ufo-set-emblems", LOG_PID, LOG_AUTHPRIV);
    vsyslog(err, format, args);
    va_end(args);
    closelog();
}

static void xdg_emblems_provider_iface_init (NautilusInfoProviderIface *iface);
static void xdg_emblems_cancel_update(NautilusInfoProvider     *provider, NautilusOperationHandle  *handle);
static NautilusOperationResult xdg_emblems_update_file_info(NautilusInfoProvider *provider, NautilusFileInfo *file, GClosure *update_complete, NautilusOperationHandle **handle);

static GType tpp_type = 0;
static char *sync_folder = NULL;

static void xdg_emblems_provider_iface_init (NautilusInfoProviderIface *iface)
{
        iface->cancel_update = xdg_emblems_cancel_update;
        iface->update_file_info = xdg_emblems_update_file_info;
}

static NautilusOperationResult xdg_emblems_update_file_info(NautilusInfoProvider *provider, NautilusFileInfo *file, GClosure *update_complete, NautilusOperationHandle **handle)
{
        NautilusFileInfoIface *file_iface = NAUTILUS_FILE_INFO_GET_IFACE(file);
           
	    if (g_str_equal(file_iface->get_name(file), "Mes documents Linux")) {
		        file_iface->add_emblem(file, "linux");
	            return NAUTILUS_OPERATION_COMPLETE;
	    }
	    else if (g_str_equal(file_iface->get_name(file), "Mes documents Mac")) {
		        file_iface->add_emblem(file, "mac");
		        return NAUTILUS_OPERATION_COMPLETE;
	    }
	    else if (g_str_equal(file_iface->get_name(file), "Mes documents Windows")) {
		        file_iface->add_emblem(file, "windows");
		        return NAUTILUS_OPERATION_COMPLETE;
	    }
	    else if (sync_folder && g_str_equal(file_iface->get_name(file), sync_folder)) {
		        file_iface->add_emblem(file, "web");
		        return NAUTILUS_OPERATION_COMPLETE;
	    }
        
        char * path = file_iface->get_uri(file);
        char target[256];
        
        if (readlink (path + 7, target, 256) >= 0) {
                if(strlen(target) > strlen("/media/") && strncmp(target, "/media/", 7) == 0) {
                        file_iface->add_emblem(file, "usb");
                }
        }
        
        return NAUTILUS_OPERATION_COMPLETE;
}

static void xdg_emblems_cancel_update(NautilusInfoProvider  *provider, NautilusOperationHandle  *handle)
{
}

void ufo_xdg_emblems_plugin_register_type(GTypeModule  *module)
{
        const GTypeInfo info = {
                sizeof (GObjectClass),
                (GBaseInitFunc) NULL,
                (GBaseFinalizeFunc) NULL,
                (GClassInitFunc) NULL,
                NULL,
                NULL,
                sizeof (GObject),
                0,
                (GInstanceInitFunc) NULL
        };
        const GInterfaceInfo xdg_emblems_provider_iface_info = {
                (GInterfaceInitFunc)xdg_emblems_provider_iface_init,
                NULL,
                NULL
        };

        tpp_type = g_type_module_register_type (module, G_TYPE_OBJECT,
                        "UFOEmblemsInfoPlugin",
                        &info, 0);
        g_type_module_add_interface (module,
                        tpp_type,
                        NAUTILUS_TYPE_INFO_PROVIDER,
                        &xdg_emblems_provider_iface_info);
}

void nautilus_module_initialize (GTypeModule  *module)
{
        /*
        GError *error;
        GKeyFile *key_file = g_key_file_new();
        g_key_file_load_from_file(key_file, "/etc/sysconfig/ufo/coda", G_KEY_FILE_NONE, &error);
        gchar *sync_folder = g_key_file_get_string(key_file, NULL, "CODAFOLDER", &error);
        */

        FILE* in = fopen("/etc/sysconfig/ufo/sync", "r");
	char line[1024];
        char k[1024];
        int n = 0;
	while (in && fgets(line, 1024, in)) {
		if (line[0] != '#') {
                    sscanf(line, "%[^#=]=", &k);
                    if (!strcmp(k, "SYNCNAME")) {
                        char *v = line + strlen(k) + 1;
                        n = strlen(v);
                        sync_folder = malloc(strlen(v) - 2);
                        strncpy(sync_folder, v + 1, strlen(v) - 3);
                        sync_folder[strlen(v) - 3] = '\0';
                    }
                }
        }
        /*
        if (sync_folder) {
            FILE *f = fopen("/tmp/sync_folder", "w");
            fputs(sync_folder, f);
            fputs("\n", f);
            fprintf(f, "%s\n%d\n", sync_folder, n);
            fclose(f);
        }
        else {
            FILE *f = fopen("/tmp/sync_folder", "w");
            if (f) {
                fputs("Arggg", f);
                fclose(f);
            }
        }
        */
        ufo_xdg_emblems_plugin_register_type(module);
}

void nautilus_module_shutdown   (void)
{
        if (sync_folder)
            free(sync_folder);
}

void nautilus_module_list_types (const GType **types, int *num_types)
{
        static GType type_list[1];

        type_list[0] = tpp_type;
        *types = type_list;
        *num_types = G_N_ELEMENTS (type_list);
}

