%define debug_package %{nil}

Name:           agorabox-ui
Version:        0.8
Release:        1%{?dist}
Summary:        Modify the user interface so that it has the agora style. 
Group:          User Interface/Desktops
License:        GNU/GPLv2
URL:            %{name}-%{version}.tar.gz
Source0:        %{name}-%{version}.tar.gz
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      i386
Conflicts:      fedora-logos
Conflicts:      generic-logos
Requires:       grub
# Requires:       google-gadgets google-gadgets-gtk
Requires:       compiz-fusion compiz-fusion-gnome compiz-fusion-extras
Requires:       compiz-fusion-extras-gnome ccsm compizconfig-backend-gconf
Requires:       slim gnome-themes
Requires:       nautilus-extensions
Requires(pre):  GConf2
Requires(post): GConf2
Requires(preun):GConf2
BuildRequires:  nautilus-devel
Obsoletes: redhat-logos
Provides: redhat-logos = 11.0.1-1
Provides: system-logos = 11.0.1-1

%package dock
Summary: Set up the UFO dock (based on caido-dock)
Group: Applications/System
Requires: agorabox-ui cairo-dock cairo-dock-plug-ins tint2


%package ambassador
Summary: Set up the ambassador look
Group: Applications/System
Requires: agorabox-ui


%description
Install the User Interface and apply the UFO theme.


%description dock
Set up the UFO dock (based on caido-dock)


%description ambassador
Set up the ambassador look


%prep
%setup  -q -n %{name}-%{version}


%build
make build


%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT

mkdir -p $RPM_BUILD_ROOT/boot/grub
install -p -m 644 grub/agorabox-splash.xpm.gz $RPM_BUILD_ROOT/boot/grub/splash.xpm.gz

mkdir -p $RPM_BUILD_ROOT/usr/share/slim/themes/UFO/
install -p -m 644 slim/* $RPM_BUILD_ROOT/usr/share/slim/themes/UFO/

mkdir -p $RPM_BUILD_ROOT/usr/share/backgrounds/UFO
install -p -m 644 backgrounds/* $RPM_BUILD_ROOT/usr/share/backgrounds/UFO/

mkdir -p $RPM_BUILD_ROOT/usr/share/pixmaps
install -p -m 644 system-logo-white.png $RPM_BUILD_ROOT/usr/share/pixmaps

mkdir -p $RPM_BUILD_ROOT/usr/share/themes/UFO
find gtk -name .svn -exec rm -rf {} \; || true
cp -R -P gtk/* $RPM_BUILD_ROOT/usr/share/themes/UFO/

mkdir -p $RPM_BUILD_ROOT/usr/bin
install -p -m 755 switch-keyboard-layout auto-proxy $RPM_BUILD_ROOT/usr/bin

mkdir -p $RPM_BUILD_ROOT/usr/bin
install -p -m 755 replace-icons update-ufo-bookmarks $RPM_BUILD_ROOT/usr/bin

mkdir -p $RPM_BUILD_ROOT/etc/X11/xinit/xinitrc.d/
install -p -m 755 01-replace-icons.sh $RPM_BUILD_ROOT/etc/X11/xinit/xinitrc.d/

mkdir -p $RPM_BUILD_ROOT/etc/xdg/autostart/
install -p -m 644 switch-keyboard-layout.desktop auto-proxy.desktop update-ufo-bookmarks.desktop $RPM_BUILD_ROOT/etc/xdg/autostart/

mkdir -p $RPM_BUILD_ROOT/etc/xdg/autostart/
install -p -m 644 switch-keyboard-layout.desktop auto-proxy.desktop $RPM_BUILD_ROOT/etc/xdg/autostart/

mkdir -p $RPM_BUILD_ROOT/usr/share/agorabox-ui
install -p -m 644 slim.conf $RPM_BUILD_ROOT/usr/share/agorabox-ui/slim.conf

mkdir -p $RPM_BUILD_ROOT/%{_sysconfdir}/gconf/schemas
install -p -m 644 *.entries $RPM_BUILD_ROOT/%{_sysconfdir}/gconf/schemas

# install UFO icons
mkdir -p $RPM_BUILD_ROOT/usr/share
find icons -name .svn -exec rm -rf {} \; || true
cp -R icons/ $RPM_BUILD_ROOT/usr/share/icons

# install dock
mkdir -p $RPM_BUILD_ROOT%{_datadir}/cairo-dock/themes/
cp -R theme-dock-UFO $RPM_BUILD_ROOT%{_datadir}/cairo-dock/themes/UFO
cp -R theme-dock-UFO-cartoon $RPM_BUILD_ROOT%{_datadir}/cairo-dock/themes/UFO-cartoon

# setup tray icon for dock
install -D -m 755 tint2rc $RPM_BUILD_ROOT%{_datadir}/agorabox-ui/tint2rc


%clean
rm -rf $RPM_BUILD_ROOT


%post
export GCONF_CONFIG_SOURCE=`gconftool-2 --get-default-source`
gconftool-2 --direct --config-source=$GCONF_CONFIG_SOURCE --load /etc/gconf/schemas/ufo-setup.entries > /dev/null || :
gconftool-2 --direct --config-source=$GCONF_CONFIG_SOURCE --load /etc/gconf/schemas/ufo-setup.entries /apps/panel > /dev/null || :

# sed -i '/splash/s:=.*:='/boot/grub'/agorabox-splash.xpm.gz:' /boot/grub/grub.conf

cp -f /usr/share/agorabox-ui/slim.conf /etc/slim.conf

if [ ! -f /etc/sysconfig/desktop ]; then
     echo "DISPLAYMANAGER=/usr/bin/slim-dynwm" > /etc/sysconfig/desktop
fi

# refresh emblems
/usr/bin/gtk-update-icon-cache --quiet /usr/share/icons/UFO
/usr/bin/gtk-update-icon-cache --quiet /usr/share/icons/Fedora
/usr/bin/gtk-update-icon-cache --quiet /usr/share/icons/gnome

sed -i 's/^Defaults \+requiretty/#Defaults    requiretty/' /etc/sudoers 


%post dock
cp -f /usr/share/agorabox-ui/tint2rc /etc/xdg/tint2/tint2rc

export GCONF_CONFIG_SOURCE=`gconftool-2 --get-default-source`
gconftool-2 --direct --config-source=$GCONF_CONFIG_SOURCE --load /etc/gconf/schemas/ufo-dock.entries > /dev/null || :
gconftool-2 --direct --config-source=$GCONF_CONFIG_SOURCE --load /etc/gconf/schemas/ufo-dock.entries /apps/panel > /dev/null || :

[ -L /usr/share/cairo-dock/themes/_default_ ] && unlink /usr/share/cairo-dock/themes/_default_
ln -s UFO /usr/share/cairo-dock/themes/_default_


%post ambassador
export GCONF_CONFIG_SOURCE=`gconftool-2 --get-default-source`
gconftool-2 --direct --config-source=$GCONF_CONFIG_SOURCE --load /etc/gconf/schemas/ufo-ambassador.entries > /dev/null || :


%files
%defattr(-,root,root,-)
/boot/grub/splash.xpm.gz
%{_sysconfdir}/gconf/schemas/ufo-setup.entries
%{_bindir}/switch-keyboard-layout
%{_bindir}/auto-proxy
%{_bindir}/replace-icons
%{_bindir}/update-ufo-bookmarks
%{_datadir}/agorabox-ui/slim.conf
%{_datadir}/agorabox-ui/tint2rc
%{_datadir}/backgrounds/UFO/ufo-background-pink.png
%{_datadir}/backgrounds/UFO/ufo-default-background.png
%{_datadir}/themes/UFO
%{_datadir}/icons/UFO/index.theme
%{_datadir}/icons/UFO/16x16/places/start-here.png
%{_datadir}/icons/UFO/24x24/places/start-here.png
%{_datadir}/icons/UFO/24x24/emblems/emblem-linux.png
%{_datadir}/icons/UFO/24x24/emblems/emblem-mac.png
%{_datadir}/icons/UFO/24x24/emblems/emblem-windows.png
%{_datadir}/icons/UFO/24x24/emblems/emblem-usb.png
%{_datadir}/icons/UFO/32x32/places/start-here.png
%{_datadir}/icons/UFO/48x48/actions/actions-toggle-fullscreen.png
%{_datadir}/icons/UFO/48x48/emblems/emblem-linux.png
%{_datadir}/icons/UFO/48x48/emblems/emblem-mac.png
%{_datadir}/icons/UFO/48x48/emblems/emblem-windows.png
%{_datadir}/icons/UFO/48x48/emblems/emblem-usb.png
%{_datadir}/icons/UFO/48x48/places/start-here.png
%{_datadir}/icons/UFO/96x96/places/start-here.png
%{_datadir}/pixmaps/system-logo-white.png
%{_datadir}/slim/themes/UFO
%{_sysconfdir}/xdg/autostart/switch-keyboard-layout.desktop
%{_sysconfdir}/xdg/autostart/auto-proxy.desktop
%{_sysconfdir}/xdg/autostart/update-ufo-bookmarks.desktop
%{_sysconfdir}/X11/xinit/xinitrc.d/01-replace-icons.sh
%{_libdir}/nautilus/extensions-2.0/libnautilus-ufo-set-emblems.so


%files dock
%{_sysconfdir}/gconf/schemas/ufo-dock.entries
# Tray icon theme
%config %{_datadir}/agorabox-ui/tint2rc
# Theme UFO
%{_datadir}/cairo-dock/themes/UFO/launchers/01fedora-empathy.desktop
%{_datadir}/cairo-dock/themes/UFO/launchers/01google-chrome.desktop
%{_datadir}/cairo-dock/themes/UFO/launchers/01mozilla-thunderbird.desktop
%{_datadir}/cairo-dock/themes/UFO/launchers/01firefox.desktop
%{_datadir}/cairo-dock/themes/UFO/launchers/01gimp.desktop
%{_datadir}/cairo-dock/themes/UFO/rendering.conf.L9TI5U
%{_datadir}/cairo-dock/themes/UFO/valid.png
%{_datadir}/cairo-dock/themes/UFO/bg.png
%{_datadir}/cairo-dock/themes/UFO/separateur.png
%{_datadir}/cairo-dock/themes/UFO/cadre.png
%{_datadir}/cairo-dock/themes/UFO/plug-ins/GMenu/GMenu.conf
%{_datadir}/cairo-dock/themes/UFO/plug-ins/drop_indicator/drop_indicator.conf
%{_datadir}/cairo-dock/themes/UFO/plug-ins/clock/clock.conf
%{_datadir}/cairo-dock/themes/UFO/plug-ins/illusion/illusion.conf
%{_datadir}/cairo-dock/themes/UFO/plug-ins/Xgamma/Xgamma.conf
%{_datadir}/cairo-dock/themes/UFO/plug-ins/show_mouse/show_mouse.conf
%{_datadir}/cairo-dock/themes/UFO/plug-ins/dustbin/dustbin.conf
%{_datadir}/cairo-dock/themes/UFO/plug-ins/motion_blur/motion_blur.conf
%{_datadir}/cairo-dock/themes/UFO/plug-ins/switcher/switcher.conf
%{_datadir}/cairo-dock/themes/UFO/plug-ins/quick-browser/quick-browser.conf
%{_datadir}/cairo-dock/themes/UFO/plug-ins/GMenu-dock/GMenu-dock.conf
%{_datadir}/cairo-dock/themes/UFO/plug-ins/Clipper/Clipper.conf
%{_datadir}/cairo-dock/themes/UFO/plug-ins/powermanager/powermanager.conf
%{_datadir}/cairo-dock/themes/UFO/plug-ins/logout/logout.conf
%{_datadir}/cairo-dock/themes/UFO/plug-ins/dialog-rendering/dialog-rendering.conf
%{_datadir}/cairo-dock/themes/UFO/plug-ins/Animated-icons/Animated-icons.conf
%{_datadir}/cairo-dock/themes/UFO/plug-ins/dnd2share/dnd2share.conf
%{_datadir}/cairo-dock/themes/UFO/plug-ins/icon-effect/icon-effect.conf
%{_datadir}/cairo-dock/themes/UFO/plug-ins/shortcuts/shortcuts.conf
%{_datadir}/cairo-dock/themes/UFO/plug-ins/rendering/rendering.conf
%{_datadir}/cairo-dock/themes/UFO/plug-ins/rendering/rendering.conf.L9TI5U
%{_datadir}/cairo-dock/themes/UFO/plug-ins/rendering/rendering.conf.40AH5U
%{_datadir}/cairo-dock/themes/UFO/rendering.conf.40AH5U
%{_datadir}/cairo-dock/themes/UFO/invalid.png
%{_datadir}/cairo-dock/themes/UFO/icons/UFO.png
%{_datadir}/cairo-dock/themes/UFO/icons/aide.png
%{_datadir}/cairo-dock/themes/UFO/icons/power.png
%{_datadir}/cairo-dock/themes/UFO/icons/firefox.png
%{_datadir}/cairo-dock/themes/UFO/icons/home.png
%{_datadir}/cairo-dock/themes/UFO/icons/trash.png
%{_datadir}/cairo-dock/themes/UFO/etoile.png 
%{_datadir}/cairo-dock/themes/UFO/cairo-dock.conf
%{_datadir}/cairo-dock/themes/UFO/indicator.png
# Theme UFO-Cartoon
%{_datadir}/cairo-dock/themes/UFO-cartoon/launchers/01fedora-empathy.desktop
%{_datadir}/cairo-dock/themes/UFO-cartoon/launchers/01google-chrome.desktop
%{_datadir}/cairo-dock/themes/UFO-cartoon/launchers/01mozilla-thunderbird.desktop
%{_datadir}/cairo-dock/themes/UFO-cartoon/launchers/01firefox.desktop
%{_datadir}/cairo-dock/themes/UFO-cartoon/launchers/01gimp.desktop
%{_datadir}/cairo-dock/themes/UFO-cartoon/rendering.conf.L9TI5U
%{_datadir}/cairo-dock/themes/UFO-cartoon/valid.png
%{_datadir}/cairo-dock/themes/UFO-cartoon/bg.png
%{_datadir}/cairo-dock/themes/UFO-cartoon/separateur.png
%{_datadir}/cairo-dock/themes/UFO-cartoon/cadre.png
%{_datadir}/cairo-dock/themes/UFO-cartoon/plug-ins/GMenu/GMenu.conf
%{_datadir}/cairo-dock/themes/UFO-cartoon/plug-ins/drop_indicator/drop_indicator.conf
%{_datadir}/cairo-dock/themes/UFO-cartoon/plug-ins/clock/clock.conf
%{_datadir}/cairo-dock/themes/UFO-cartoon/plug-ins/illusion/illusion.conf
%{_datadir}/cairo-dock/themes/UFO-cartoon/plug-ins/Xgamma/Xgamma.conf
%{_datadir}/cairo-dock/themes/UFO-cartoon/plug-ins/show_mouse/show_mouse.conf
%{_datadir}/cairo-dock/themes/UFO-cartoon/plug-ins/dustbin/dustbin.conf
%{_datadir}/cairo-dock/themes/UFO-cartoon/plug-ins/motion_blur/motion_blur.conf
%{_datadir}/cairo-dock/themes/UFO-cartoon/plug-ins/switcher/switcher.conf
%{_datadir}/cairo-dock/themes/UFO-cartoon/plug-ins/quick-browser/quick-browser.conf
%{_datadir}/cairo-dock/themes/UFO-cartoon/plug-ins/GMenu-dock/GMenu-dock.conf
%{_datadir}/cairo-dock/themes/UFO-cartoon/plug-ins/Clipper/Clipper.conf
%{_datadir}/cairo-dock/themes/UFO-cartoon/plug-ins/powermanager/powermanager.conf
%{_datadir}/cairo-dock/themes/UFO-cartoon/plug-ins/logout/logout.conf
%{_datadir}/cairo-dock/themes/UFO-cartoon/plug-ins/dialog-rendering/dialog-rendering.conf
%{_datadir}/cairo-dock/themes/UFO-cartoon/plug-ins/Animated-icons/Animated-icons.conf
%{_datadir}/cairo-dock/themes/UFO-cartoon/plug-ins/stack/stack.conf
%{_datadir}/cairo-dock/themes/UFO-cartoon/plug-ins/dnd2share/dnd2share.conf
%{_datadir}/cairo-dock/themes/UFO-cartoon/plug-ins/icon-effect/icon-effect.conf
%{_datadir}/cairo-dock/themes/UFO-cartoon/plug-ins/shortcuts/shortcuts.conf
%{_datadir}/cairo-dock/themes/UFO-cartoon/plug-ins/rendering/rendering.conf
%{_datadir}/cairo-dock/themes/UFO-cartoon/plug-ins/rendering/rendering.conf.L9TI5U
%{_datadir}/cairo-dock/themes/UFO-cartoon/plug-ins/rendering/rendering.conf.40AH5U
%{_datadir}/cairo-dock/themes/UFO-cartoon/rendering.conf.40AH5U
%{_datadir}/cairo-dock/themes/UFO-cartoon/invalid.png
%{_datadir}/cairo-dock/themes/UFO-cartoon/icons/UFO.png
%{_datadir}/cairo-dock/themes/UFO-cartoon/icons/aide.png
%{_datadir}/cairo-dock/themes/UFO-cartoon/icons/power.png
%{_datadir}/cairo-dock/themes/UFO-cartoon/icons/firefox.png
%{_datadir}/cairo-dock/themes/UFO-cartoon/icons/home.png
%{_datadir}/cairo-dock/themes/UFO-cartoon/icons/trash_empty.png
%{_datadir}/cairo-dock/themes/UFO-cartoon/icons/trash_full.png
%{_datadir}/cairo-dock/themes/UFO-cartoon/etoile.png 
%{_datadir}/cairo-dock/themes/UFO-cartoon/cairo-dock.conf
%{_datadir}/cairo-dock/themes/UFO-cartoon/indicator.png


%files ambassador
%{_datadir}/backgrounds/UFO/ufo-ambassador-background.png
%{_sysconfdir}/gconf/schemas/ufo-ambassador.entries


%changelog
* Mon Jul 27 2009 Antonin Fouques <antonin.fouques@agorabox.org>
Added two bookmarks and a script to keep them in the gtk-bookmarks

* Wed Jul 22 2009 Antonin Fouques <antonin.fouques@agorabox.org>
Added a "replace-icons" script just before session startup

* Mon Jul 13 2009 Sylvain Baubeau <sylvain.baubeau@agorabox.org>
Use UFO icon theme

* Tue Jul 9 2009 Kevin Pouget <pouget@agorabox.org>
Provides distrib icons

* Wed Jul 8 2009 Sylvain Baubeau <sylvain.baubeau@agorabox.org>
Use consolehelper to run program as root

* Mon Jun 15 2009 Antonin Fouques <antonin.fouques@agorabox.org>
Added a shortcut to toggle between fullscreen and windowed mode

* Wed May 27 2009 Kevin Pouget <pouget@agorabox.org>
Add usb emblem on removable media shared folder symlinks

* Wed Apr 01 2009 Kevin Pouget <pouget@agorabox.org>
Add auto-proxy at session startup 

* Wed Apr 01 2009 Sylvain Baubeau <sylvain.baubeau@agorabox.org>
A lot of cleanup

* Thu Feb 26 2009 Marc <marc.schlinger@agorabox.org>
Added Slim theme.

* Tue Feb 24 2009 Marc <marc.schlinger@agorabox.org>
Slim added as login manager.

* Mon Jan 05 2009 Bob <bob@glumol.com>
Shortcut for "Add/Remove software" in applications menu

* Tue Nov 04 2008 Marc <marc.schlinger@agorabox.org>
Deactivation of modification for RHGB.
Fixed agorabox-ui-gdm.xml
Rewrite comments in english.

* Thu Oct 23 2008 Bob <bob@glumol.com>
Added support for composite manager

* Fri Oct 03 2008 Marc <marc.schlinger@agorabox.org>
Initial Release
