#!/bin/sh

OS="None"
[ -d $HOME/Bureau/Mes\ documents\ Linux/ ] && OS="Linux"
[ -d $HOME/Bureau/Mes\ documents\ Windows/ ] && OS="Windows"
[ -d $HOME/Bureau/Mes\ documents\ Mac/ ] && OS="Mac"
[ $OS == "None" ] && OS=" "

file="$HOME/.gtk-bookmarks"

Mes_documents="False"
Mes_fichiers_synchro="False"

while read line
do
    echo $line
    [[ "$line" == file://$HOME/Bureau/Mes%20documents%20$OS* ]] && Mes_documents="True"
    [[ "$line" == file://$HOME/Bureau/Mes%20fichiers%20synchronis%C3%A9s* ]] && Mes_fichiers_synchro="True"
done < $file

[ ${Mes_documents} == "False" ] && echo "file://$HOME/Bureau/Mes%20documents%20$OS" >> $file 
[ ${Mes_fichiers_synchro} == "False" ] && echo "file://$HOME/Bureau/Mes%20fichiers%20synchronis%C3%A9s" >> $file 

