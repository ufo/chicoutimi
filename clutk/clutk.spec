Name:           clutk
Version:        0.2.8
Release:        1%{?dist}
Summary:        A general-purpose toolkit for Clutter used by UNR's netbook-launcher.

Group:          System Environment/Libraries
License:        LGPLv2
URL:            https://code.launchpad.net/clutk
Source0:        http://launchpad.net/clutk/0.2/0.2.8/+download/clutk-0.2.8.tar.gz
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  clutter-gtk-devel clutter-devel gtk2-devel
Requires:       clutter-gtk clutter gtk2

%description
A general-purpose toolkit for Clutter used by UNR's netbook-launcher.


%package        devel
Summary:        Development files for %{name}
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release}

%description    devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.


%prep
%setup -q


%build
%configure --disable-static
make %{?_smp_mflags}


%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT
find $RPM_BUILD_ROOT -name '*.la' -exec rm -f {} ';'


%clean
rm -rf $RPM_BUILD_ROOT


%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig


%files
%defattr(-,root,root,-)
%doc
%{_libdir}/*.so.*

%files devel
%defattr(-,root,root,-)
%doc
%{_includedir}/*
%{_libdir}/*.so
%{_libdir}/pkgconfig/clutk-0.2.pc


%changelog
