Name:           initrd-ufo
Version:        0.1
Release:        1%{?dist}
Summary:        Install a busybox based initrd on /boot.

BuildArch:      noarch
Group:          Applications/System
License:        GPLv2
URL:            http://www.glumol.com
Source0:        http://www.glumol.com/chicoutimi/initrd-ufo-0.1.tar.gz
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

Requires:       augeas

%description
Install a busybox based initrd on /boot. It provides a fast initrd 
boot sequence, by waiting wanted UFO device ready only.
Also update default grub entry.   

%prep
%setup -q


%build
make %{?_smp_mflags}


%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT

%post
default_string=`augtool get /files/etc/grub.conf/default`
default=${default_string#*= }
default=$(( $default + 1 ))
echo "::set /files/etc/grub.conf/titre[${default}]/initrd '/boot/initrd-ufo.img'\nsave"
echo -e "set /files/etc/grub.conf/title[${default}]/initrd '/boot/initrd-ufo.img'\nsave" | augtool


%preun
#rm -rf /media/UFO/Linux


%clean
rm -rf $RPM_BUILD_ROOT


%files
%defattr(-,root,root,-)
%doc
/boot/initrd-ufo.img


%changelog
* Mon Nov 24 2008 Kevin	Pouget <kevin.pouget@agorabox.org>
Initial release
