Name:           tsumufsmount
Version:        0.1
Release:        3%{?dist}
Summary:        Manage the user's TsumuFS folder
BuildArch:      noarch

Group:          System Environment/Base
License:        GPLv2
URL:            http://www.agorabox.org
Source0:        %{name}-%{version}.tar.gz
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

Requires:       TsumuFS pam_mount pam_connected /etc/tsumufs/tsumufs.conf /etc/sysconfig/ufo/sync

%description
Manage the user's synchronized folder.


%prep
%setup -q


%build


%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT


%post
authconfig --enablekrb5 --enablemount --enableconnected --update
python %{_datadir}/tsumufsmount/modify-pam_mount-conf.py --install

%posttrans
chkconfig rpcbind off
chkconfig rpcgssd off
chkconfig rpcidmapd off
chkconfig ntpd off
authconfig --enablekrb5 --enablemount --enableconnected --update
python %{_datadir}/tsumufsmount/modify-pam_mount-conf.py --install


%preun
authconfig --disablekrb5 --disablemount --disableconnected --update
python %{_datadir}/tsumufsmount/modify-pam_mount-conf.py --uninstall


%clean
rm -rf $RPM_BUILD_ROOT


%files
%defattr(-,root,root,-)
%doc
%{_bindir}/tsumufsmount
%{_sysconfdir}/sysconfig/modules/tsumufs.modules
%{_sysconfdir}/xdg/autostart/create-sync-symlink.desktop
%{_sysconfdir}/xdg/autostart/ufo-conf-sync-loop.desktop
%{_sysconfdir}/NetworkManager/dispatcher.d/01-tsumufs
%{_datadir}/tsumufsmount
%{_datadir}/tsumufsmount/excludesync.list
%{_bindir}/create-sync-symlink
%{_bindir}/ufo-conf-sync
%{_bindir}/ufo-conf-sync-loop
%{_bindir}/ufo-restore-sync-conf


%changelog
* Wed Jan 20 2010 Kevin Pouget <pouget@agorabox.org> - 0.1-3
Added user configuration sync script.
Added excluded files configuration file.
Removed auto sync configuration file.

* Wed Sep 16 2009 Kevin Pouget <pouget@agorabox.org> - 0.1-2
Added auto sync configuration file.
Added monting of remote file system by tsumufsmount. 

* Thu Sep 10 2009 Sylvain Baubeau <sylvain.baubeau@agorabox.org> - 0.1-1
Initial release

