#!/usr/bin/python

import string
import commands
import sys
import xml.dom.minidom as dom
import augeas

system_auth = '/etc/security/pam_mount.conf.xml'
usage = """modify-pam_mount-conf --install
                   --uninstall"""

if len(sys.argv) < 2:
    print usage
    sys.exit(1)

p = dom.parse(system_auth)
pammount = p.getElementsByTagName("pam_mount")[0]

aug = augeas.Augeas()
mountpoint = aug.get("/files/etc/sysconfig/ufo/sync/SYNCPATH")
mountpoint = mountpoint.replace("$USER", "%(USER)")

if sys.argv[1] == '--install':
    flag = 0
    volumes = p.getElementsByTagName("volume")
    for volume in volumes:
        attr = volume.getAttribute("path")
        if "tsumufs" in attr:
            flag = 1

    if not flag:
        pammount = p.getElementsByTagName("pam_mount")[0]
        element  = p.createElement("volume")
        element.setAttribute("user", "*")
        element.setAttribute("fstype", "fuse")
        element.setAttribute("path", "tsumufs")
        element.setAttribute("mountpoint", mountpoint)
        pammount.appendChild(element)
    
    tsumufsmount = p.createTextNode("/usr/bin/tsumufsmount")
    fusemount = p.getElementsByTagName("fusemount")
    if not fusemount:
        fusemount = p.createElement("fusemount")
        fusemount.appendChild(tsumufsmount)
        pammount.appendChild(fusemount)
    else:
        fusemount = fusemount[0]
        fusemount.replaceChild(tsumufsmount, fusemount.childNodes[0])

    tsumufsumount = p.createTextNode("fusermount -u %(MNTPT)")
    fuseumount = p.getElementsByTagName("fuseumount")
    if not fuseumount:
        fuseumount = p.createElement("fuseumount")
        fuseumount.appendChild(tsumufsumount)
        pammount.appendChild(fuseumount)
    else:
        fuseumount = fuseumount[0]
        fuseumount.replaceChild(tsumufsumount, fuseumount.childNodes[0])

elif sys.argv[1] == '--uninstall':
    volumes = p.getElementsByTagName("volume")
    for volume in volumes:
        attr = volume.getAttribute("path")
        if "tsumufs" in attr:
            pammount.removeChild(volume)

open(system_auth, 'w').write(p.toxml())

