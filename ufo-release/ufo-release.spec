Name:           ufo-release
Version:        1.1
Release:        1%{?dist}
Summary:        ufo meta package

BuildArch:      noarch
Group:          System Environment/Base	
License:        GPLv2
URL:            http://www.glumol.com/chicoutimi/ufo-release
Source0:        http://www.glumol.com/chicoutimi/ufo-release/%{name}-%{version}.tar.gz
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

Requires:       fedora-release = 12 coreutils

%description
The U.F.O metapackage


%prep
%setup -q


%build
make %{?_smp_mflags}


%install
rm -rf $RPM_BUILD_ROOT
mkdir $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT


%post
echo "U.F.O release 2.0 (Handy)" > /etc/fedora-release
cat <<EOF > /etc/issue
U.F.O release 2.0 (Handy)
Kernel \r on an \m (\l)

EOF
cat <<EOF > /etc/rpm/macros.dist
# dist macros.

%%fedora		12
%%dist		.ufo2
%%fc12		1
%%ufo2		1
EOF


%postun


%clean
rm -rf $RPM_BUILD_ROOT


%files
%defattr(-,root,root,-)
%doc


%changelog
* Tue Dec 09 2009 Sylvain Baubeau <bob@glumol.com>
Update to 2.0
* Mon May 25 2009 Sylvain Baubeau <bob@glumol.com>
Update to 1.1
* Fri Mar 13 2009 Sylvain Baubeau <bob@glumol.com>
Initial release
