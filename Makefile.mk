ifndef FEDORA_VERSION
FEDORA_VERSION=12
endif

ifndef DEFAULT_RPM_RULE
DEFAULT_RPM_RULE=build-rpm
endif

ifndef DIR
DIR=$(NAME)-$(VERSION)
endif

ifndef ARCHIVE
ARCHIVE:=$(DIR).tar.gz
endif
SPECFILE=$(NAME).spec
ARCHES = noarch i386 i586 i686 x86_64 ia64 s390 s390x ppc ppc64 pseries ppc64pseries iseries ppc64iseries athlon alpha alphaev6 sparc sparc64 sparcv9 sparcv9v sparc64v i164 mac sh mips geode

# Define the common dir.
# This needs to happen first.
define find-common-dir
for d in common ../common ../../common ; do if [ -f $$d/Makefile.common ] ; then echo "$$d"; break ; fi ; done
endef
COMMON_DIR := $(shell $(find-common-dir))

# Branch and disttag definitions
# These need to happen second.
ifndef HEAD_BRANCH
HEAD_BRANCH := devel
endif
BRANCH:=$(shell pwd | awk -F '/' '{ print $$NF }' )
# check to see if this is an early branched package; we should make this more
# generic in the future
ifeq ($(BRANCH),devel)
BRANCH:=$(shell cvs rlog rpms/$(NAME)/F-10/$(SPECFILE) >/dev/null 2>&1 && echo "F-11" || echo "devel")
endif
BRANCHINFO = $(shell grep ^$(BRANCH): $(COMMON_DIR)/branches | cut -d: --output-delimiter=" " -f2-)
TARGET := $(word 1, $(BRANCHINFO))
DIST = $(word 2, $(BRANCHINFO))
DIST = .ufo2
DISTVAR = $(word 3, $(BRANCHINFO))
DISTVAL = $(word 4, $(BRANCHINFO))
DISTDEF = $(shell echo  $(DIST) | sed -e s/^\\\.// )
DIST_DEFINES = --define "dist $(DIST)" --define "$(DISTVAR) $(DISTVAL)" --define "$(DISTDEF) 1"

BUILD_FLAGS ?= $(KOJI_FLAGS)

LOCALARCH := $(if $(shell grep -i '^BuildArch:.*noarch' $(SPECFILE)), noarch, $(shell uname -m))


## a base directory where we'll put as much temporary working stuff as we can
ifndef WORKDIR
WORKDIR := $(shell pwd)
endif
## of course all this can also be overridden in your RPM macros file,
## but this way you can separate your normal RPM setup from your CVS
## setup. Override RPM_WITH_DIRS in ~/.cvspkgsrc to avoid the usage of
## these variables.
SRCRPMDIR ?= $(WORKDIR)
BUILDDIR ?= $(WORKDIR)
RPMDIR ?= $(WORKDIR)
MOCKDIR ?= $(WORKDIR) 
ifeq ($(DISTVAR),epel)
DISTVAR := rhel
MOCKCFG ?= fedora-$(DISTVAL)-$(BUILDARCH)-epel
else
MOCKCFG ?= fedora-$(DISTVAL)-$(BUILDARCH)
## 4, 5, 6 need -core
ifeq ($(DISTVAL),4)
MOCKCFG = fedora-$(DISTVAL)-$(BUILDARCH)-core
endif
ifeq ($(DISTVAL),5)
MOCKCFG = fedora-$(DISTVAL)-$(BUILDARCH)-core
endif
ifeq ($(DISTVAL),6)
MOCKCFG = fedora-$(DISTVAL)-$(BUILDARCH)-core
endif
## Devel builds use -devel mock config
ifeq ($(BRANCH),devel)
MOCKCFG = fedora-devel-$(BUILDARCH)
endif
endif

## SOURCEDIR is special; it has to match the CVS checkout directory, 
## because the CVS checkout directory contains the patch files. So it basically 
## can't be overridden without breaking things. But we leave it a variable
## for consistency, and in hopes of convincing it to work sometime.
ifndef SOURCEDIR
SOURCEDIR := $(shell pwd)
endif
ifndef SPECDIR
SPECDIR := $(shell pwd)
endif

ifndef RPM_DEFINES
RPM_DEFINES := --define "_sourcedir $(SOURCEDIR)" \
		--define "_specdir $(SPECDIR)" \
		--define "_builddir $(BUILDDIR)/$(DIR)" \
		--define "_srcrpmdir $(SRCRPMDIR)/$(DIR)" \
		--define "_rpmdir $(RPMDIR)/$(DIR)" \
                $(DIST_DEFINES)
RPM_DEFINES_2 := --define "_sourcedir $(SOURCEDIR)/$(DIR)" \
                --define "_specdir $(SPECDIR)/$(DIR)" \
                --define "_builddir $(BUILDDIR)/$(DIR)" \
                --define "_srcrpmdir $(SRCRPMDIR)/$(DIR)" \
                --define "_rpmdir $(RPMDIR)/$(DIR)" \
                $(DIST_DEFINES)
endif

ifndef RPM
ifdef SIGN
RPM := rpmbuild --sign
else
RPM := rpmbuild
endif
endif
ifndef RPM_WITH_DIRS
RPM_WITH_DIRS = $(RPM) $(RPM_DEFINES)
endif

ifndef RPM_WITH_DIRS_2
RPM_WITH_DIRS_2 = $(RPM) $(RPM_DEFINES_2)
endif

# Initialize the variables that we need, but are not defined
# the version of the package

VER_REL := $(shell rpm $(RPM_DEFINES) -q --qf "%{VERSION} %{RELEASE}\n" --specfile $(SPECFILE)| head -1)

ifndef NAME
$(error "You can not run this Makefile without having NAME defined")
endif
ifndef VERSION
VERSION := $(word 1, $(VER_REL))
endif
# the release of the package
ifndef RELEASE
RELEASE := $(word 2, $(VER_REL))
endif

# this is used in make patch, maybe make clean eventually.
# would be nicer to autodetermine from the spec file...
RPM_BUILD_DIR ?= $(BUILDDIR)/$(NAME)-$(VERSION)

PID := $(word 1, $(shell pidof make))
ARGS := $(shell cat /proc/$(PID)/cmdline | tr "\0" "\n")
RULE := $(word 2, $(ARGS))
RULES := $(shell echo $ARGS)

# default target: just make sure we've got the sources
all: sources

ifeq ($(CVSPROGRAM),bzr)
	CMD=bzr branch $(URL)
else ifeq ($(CVSPROGRAM),svn)
	CMD=svn checkout $(URL)
endif

all: rpm srpm

clean ::
	@echo "Running the %clean script of the rpmbuild..."
	-@$(RPM_WITH_DIRS) --clean --nodeps $(SPECFILE)
	@for F in $(FULLSOURCEFILES); do \
                if test -e $$F ; then \
                        echo "Deleting $$F" ; /bin/rm -f $$F ; \
                fi; \
        done
	@if test -d $(TMPCVS); then \
		echo "Deleting CVS dir $(TMPCVS)" ; \
		/bin/rm -rf $(TMPCVS); \
	fi
	@if test -e $(SRCRPMDIR)/$(NAME)-$(VERSION)-$(RELEASE).src.rpm ; then \
		echo "Deleting $(SRCRPMDIR)/$(NAME)-$(VERSION)-$(RELEASE).src.rpm" ; \
		/bin/rm -f $(SRCRPMDIR)/$(NAME)-$(VERSION)-$(RELEASE).src.rpm ; \
        fi
	@rm -rf $(ARCHIVE) $(DIR) rpms noarch i386 *.pyc
	@rm -fv *~ clog
	@rm -rfv rpms
	@echo "Fully clean!"

sources:
	if [ -z "$(NOCLEAN)" ]; then \
	    rm -rf $(DIR); \
	fi; \
	if [ "$(REVISION)" != "" ]; then \
	    CVSARGS="-r $(REVISION)"; \
	fi; \
	if [ "$(CVSPROGRAM)" == "git" ]; then \
	    git clone $(URL) $(DIR); \
	    if [ "$(CVSBRANCH)" != "" ]; then \
	        cd $(DIR) && git checkout $(CVSBRANCH); \
	    fi; \
	    cp -rL $(SOURCES) $(DIR); \
	    echo `ls *.patch`; \
	    for rustine in `ls *.patch`; \
	    do \
	        echo "Applying patch $$rustine"; \
	        patch -d $(DIR) < $$rustine; \
	    done; \
	elif [ "$(CVSPROGRAM)" == "bzr" ]; then \
	    bzr checkout $$CVSARGS $(URL); \
	elif [ "$(CVSPROGRAM)" == "cvs" ]; then \
	    echo cvs $(CVSARGS) co $(URL); \
	    cvs $(CVSARGS) co -d $(DIR) $(URL); \
	elif [ "$(CVSPROGRAM)" == "svn" ]; then \
	    svn co $$CVSARGS $(URL); \
	elif [ "$(TYPE)" = "fedora-patch" ]; then \
	    rm -rf rpms; \
	    mkdir $(DIR); \
	    if [ "$(FEDORA_VERSION)" != "devel" ]; then \
	        MODULE=rpms/$(NAME)/F-$(FEDORA_VERSION); \
	    else \
	        MODULE=devel/$(NAME); \
	    fi; \
	    if test -z "$(FEDORA_CVS_TAG)"; then \
	      CVSROOT=:pserver:anonymous@cvs.fedoraproject.org:/cvs/pkgs cvs co -d $(DIR) $$MODULE; \
	    else \
	      CVSROOT=:pserver:anonymous@cvs.fedoraproject.org:/cvs/pkgs cvs co -d $(DIR) -r $(FEDORA_CVS_TAG) $$MODULE; \
	    fi; \
	    if [ -n "$(SOURCES)" ] || [ -n "$(SOURCES1)" ]; then \
	        cp -rL $(SOURCES) $(SOURCES1) $(DIR); \
	    fi; \
	    if [ -z "$(NOCLEAN)" ]; then \
	        if [ -z "$(ORIGINAL)" ]; then \
	            patch -d $(DIR) < $(NAME).spec.patch || exit 1; \
	        fi; \
	        if [ ! -z "$(FEDORA_PATCHES)" -a -z "$(ORIGINAL)" ]; then \
	            for rustine in $(FEDORA_PATCHES); \
	            do \
	                echo "Applying patch $$rustine"; \
	                echo patch -d $(DIR) < $$rustine; \
	                patch -d $(DIR) < $$rustine || exit 1; \
	            done; \
	        fi; \
	    fi; \
	elif [ "$(TYPE)" = "rpmfusion-patch" ]; then \
	    mkdir $(DIR); \
	    # echo rpmfusion-free-cvs $(NAME); \
	    # rpmfusion-free-cvs -a $(NAME); \
	    if [ "$(FEDORA_VERSION)" != "devel" ]; then \
	        MODULE=rpms/$(NAME)/F-$(FEDORA_VERSION); \
	    else \
	        MODULE=rpms/$(NAME)/devel; \
	    fi; \
	    if test -z "$(RPMFUSION_CVS_TAG)"; then \
	      CVSROOT=:pserver:anonymous@cvs.rpmfusion.org:/cvs/free/ cvs co -d $(DIR) $$MODULE; \
	    else \
	      CVSROOT=:pserver:anonymous@cvs.rpmfusion.org:/cvs/free/ cvs co -d $(DIR) -r $(RPMFUSION_CVS_TAG) $$MODULE; \
	    fi; \
	    cp -rL $(SOURCES) $(SOURCES1) $(DIR); \
	    if [ -z "$(NOCLEAN)" ]; then \
	        if [ -z "$(ORIGINAL)" ]; then \
	            patch -d $(DIR) < $(NAME).spec.patch || exit 1; \
	        fi; \
	        if [ ! -z "$(RPMFUSION_PATCHES)" -a -z "$(ORIGINAL)" ]; then \
	            for rustine in "$(RPMFUSION_PATCHES)"; \
	            do \
	                echo "Applying patch $$rustine"; \
	                echo patch -d $(DIR) < $$rustine; \
	                patch -d $(DIR) < $$rustine || exit 1; \
	            done; \
	        fi; \
	    fi; \
	elif [ "$(TYPE)" = "3rd-party" ]; then \
	    if [ ! -f `basename $(URL)` ]; then \
	        wget $(URL); \
	    fi; \
	else \
	    mkdir $(DIR); \
	    cp -rL $(SOURCES) $(SOURCES1) $(SPECFILE) Makefile $(DIR); \
	    tar cvzf $(ARCHIVE) $(DIR); \
	fi

patches:
	if [ -z "$(NOCLEAN)" ]; then \
	    if [ ! -z "$(AFTER_FEDORA_PATCHES)" ]; then \
	        for rustine in "$(AFTER_FEDORA_PATCHES)"; \
	        do \
	            echo "Applying patch $$rustine"; \
	            echo patch -d $(DIR) < $$rustine; \
	            patch -d $(DIR)/$(DIR) < $$rustine || exit 1; \
	        done; \
	    fi; \
	fi

tar: sources
	if [ -n "$(SOURCES)" ] || [ -n "$(SOURCES1)" ]; then \
	    cp -r $(SOURCES) $(SOURCES1) $(DIR); \
	    tar cvzf $(ARCHIVE) $(DIR); \
	fi

build-rpm: sources tar
	if [ -z "$(RPMSTAGE)" ]; then \
	    RPMSTAGE=-bb; \
	fi; \
	if [ "$(TYPE)" = "fedora-patch" ] || [ "$(TYPE)" = "rpmfusion-patch" ]; then \
	    cd $(DIR) && make sources; \
	    # cd .. && make patches && cd $(DIR); \
	    $(RPM_WITH_DIRS_2) $(RPMSTAGE) $$RPMSTAGE $(SPECFILE); \
	else \
	    $(RPM_WITH_DIRS) $(RPMSTAGE) $$RPMSTAGE $(SPECFILE); \
	fi

rpm: $(DEFAULT_RPM_RULE)

srpm: sources
	if [ -z "$(RPMSTAGE)" ]; then \
	    RPMSTAGE=-bs; \
	fi; \
	if [ "$(TYPE)" = "fedora-patch" ] || [ "$(TYPE)" = "rpmfusion-patch" ]; then \
	    cd $(DIR) && make sources; \
	    # cd .. && make patches && cd $(DIR); \
	    $(RPM_WITH_DIRS_2) $(RPMSTAGE) $$RPMSTAGE $(SPECFILE); \
	else \
	    cd $(DIR); \
	    $(RPM_WITH_DIRS) $(RPMSTAGE) $$RPMSTAGE $(SPECFILE); \
	fi

koji: srpm
	if [ -z "$(NOSCRATCH)" ]; then \
	    koji build --scratch dist-ufo2-updates-candidate $(DIR)/*ufo2.src.rpm; \
	else \
	    koji build dist-ufo2-updates-candidate $(DIR)/*ufo2.src.rpm; \
	fi

test:
	@if [ -z "$(CLIENT)" ]; then echo "CLIENT!!!"; fi

upload-kickstart:
	find . -name "*.rpm" -exec scp {} $(USER)@kickstart:repo \;
	find . -name "*.rpm" -exec ssh $(USER)@kickstart sudo copyonrepo {} ${FEDORA_VERSION} \;
	ssh $(USER)@kickstart sudo createrepo --groupfile=/var/www/html/yum/${FEDORA_VERSION}/i386/repodata/comps-handy.xml --update /var/www/html/yum/${FEDORA_VERSION}/i386

$(ARCHES) : sources
	if [ -z "$(RPMSTAGE)" ]; then \
	    RPMSTAGE=-ba; \
	fi
	$(RPM_WITH_DIRS) --target $@ $(RPMSTAGE) $(SPECFILE) 2>&1 | tee .build-$(VERSION)-$(RELEASE).log ; exit $${PIPESTATUS[0]}

help:
	@echo "Usage: make <target>"
	@echo "Available targets are:"
	@echo "	rpm			Create a rpm"
	@echo "	srpm			Create a srpm"

