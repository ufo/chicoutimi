Name:           pam_findkey
Version:        0.2
Release:        1%{?dist}
Summary:        Pam module to device based authentification

BuildArch:      i386
Group:          Applications/System
License:        GPLv2
URL:            http://www.glumol.com
Source0:        http://www.glumol.com/chicoutimi/%{name}-%{version}.tar.gz
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)  
Requires:       pam_mount

%description
Pam module to device based authentification.
Users are auhthenticated if given device is found,
custom device nodes can be created to make easier
pam_mount module configuration.	

%prep
%setup -q


%build
make build %{?_smp_mflags}


%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT


%post
chkconfig --add penitentiaryd
chkconfig penitentiaryd on


%preun


%clean
rm -rf $RPM_BUILD_ROOT


%files
%defattr(-,root,root,-)
%doc README COPYING
/lib/security/pam_findkey.so
/etc/security/findkey.conf
/usr/sbin/penitentiary_daemon
/etc/init.d/penitentiaryd


%changelog
* Fri Dec 19 2008 Kevin	Pouget <kevin.pouget@agorabox.org>
Initial release
