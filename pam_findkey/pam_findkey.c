/*
 * Linux-PAM session findkey
 * account, session, authentication
 */

#include <syslog.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <regex.h>
#include <stdio.h>
#include <errno.h>
#include <ctype.h>
#include <stdarg.h>
#include <grp.h>
#include <pwd.h>
#include <sys/stat.h>
#include <sys/mount.h>

#define  PAM_SM_AUTH
#define  PAM_SM_ACCOUNT
#define  PAM_SM_SESSION

#include <security/pam_modules.h>
#include <security/_pam_macros.h>
#include <security/pam_ext.h>

/* default location of the pam_findkey config file */
#define CONFIG "/etc/security/findkey.conf"

/* max length (bytes) of line in config file */
#define LINELEN         1024

/* max nodes to create */
#define INTLEN  4

/* max nodes to create */
#define MAX_NODES      10

/* max scaned devices  */
#define MAX_DEVS       30

/* directory use to temporary mount root partition */
#define TMP_MNT_PT "/media"

/* message sended at logout */
#define LOGOUT_MSG "JAIL:logout\n"

/* defines for /proc/partitions line tokens */
#define PART_INFO_MAJ 1
#define PART_INFO_MIN 2
#define PART_INFO_DEV 3
#define PART_INFO_PART 4
#define PART_INFO_NB_TOKENS 5

/* defines for flags */
#define _PAM_OPTS_NOOPTS        0x0000
#define _PAM_OPTS_DEBUG         0x0001
#define _PAM_OPTS_SILENT        0x0002
#define _PAM_OPTS_NOTFOUNDFAILS 0x0004
#define _PAM_OPTS_NO_CHROOT     0x0008
#define _PAM_OPTS_USE_REGEX     0x0010
#define _PAM_OPTS_USE_EXT_REGEX 0x0030 /* includes _PAM_OPTS_USE_REGEX */
#define _PAM_OPTS_USE_GROUPS    0x0040
#define _PAM_OPTS_SECCHECKS     0x0080

/* defines for (internal) return values */
#define _PAM_FINDKEY_INTERNALERR -2
#define _PAM_FINDKEY_SYSERR      -1
#define _PAM_FINDKEY_OK          0
#define _PAM_FINDKEY_KEYNOTFOUND 1
#define _PAM_FINDKEY_BADCONF     2

typedef struct part_path_tuple {
    char path[LINELEN];
    char  part[INTLEN];
    char major[INTLEN];
    char minor[INTLEN];
} _tuple;
_tuple *last_tuple;

typedef struct device {
    char device[INTLEN];
    char major[INTLEN];
    char minor[INTLEN];
    int  part;
} _dev;
_dev *last_dev;

typedef struct key_config {
    char   root_dev[LINELEN];
    _tuple key_file;               /* key file to find */
    _tuple key_nodes[MAX_NODES];   /* name of pam_findkey config file */
} _keyconf;

typedef struct _pam_opts {
    int16_t  flags;                /* combined option fslags */
    int      debug;                /* debug mode */
    _keyconf key_config;           /* target configuration */
    _dev     dev_config[MAX_DEVS]; /* devices configuration */
    char     pipe_path[LINELEN];   /* penitentiary/jail communication pipe */
    char*    conf;                 /* name of pam_findkey config file */
    char*    module;               /* module currently being processed */
} _opts;

/*
static void pam_syslog(pamh, int err, const char *format, ...) {
    va_list args;

    va_start(args, format);
    openlog("pam_findkey", LOG_PID, LOG_AUTHPRIV);
    vsyslog(err, format, args);
    va_end(args);
    closelog();
}
*/

/* initialize opts to a standard known state */
int _pam_opts_init(pam_handle_t *pamh, _opts* opts) {
    if (NULL == opts) {
        pam_syslog(pamh, LOG_ERR, "%s: NULL opts pointer", __FUNCTION__);
        return _PAM_FINDKEY_INTERNALERR;
    }

    opts->flags = _PAM_OPTS_NOOPTS;

    opts->conf = x_strdup(CONFIG);
    if (NULL == opts->conf) {
        pam_syslog(pamh, LOG_ERR, "strdup: %s", strerror(errno));
        return _PAM_FINDKEY_SYSERR;
    }

    return _PAM_FINDKEY_OK;
}

/* configure opts per the passed flags and cmd line args */
int _pam_opts_config(pam_handle_t *pamh, _opts* opts, int flags, int argc, const char** argv) {
    int i;

    if (NULL == opts) {
        pam_syslog(pamh, LOG_ERR, "%s: NULL opts pointer", __FUNCTION__);
        return _PAM_FINDKEY_INTERNALERR;
    }

    if (flags & PAM_SILENT) {
        opts->flags = opts->flags | _PAM_OPTS_SILENT;
    }
    if ((flags & PAM_DISALLOW_NULL_AUTHTOK) &&
        (!strcmp(opts->module, "auth") || !strcmp(opts->module, "account"))) {
        opts->flags = opts->flags | _PAM_OPTS_NOTFOUNDFAILS;
    }

    /* parse command line args */
    for (i = 0; i < argc; i++) {
    
        if (!strncmp(argv[i], "com_pipe=", 9)) {
            if (*(argv[i] + 9) == '\0')
                pam_syslog(pamh, LOG_ERR, "bad config option: \"%s\": specify a path", argv[i]);
            else
                strcpy(opts->pipe_path, argv[i] + 9);
        }

        if (!strcmp(argv[i], "debug"))
            opts->flags = opts->flags | _PAM_OPTS_DEBUG;
        else
            pam_syslog(pamh, LOG_ERR, "unrecognized config option: \"%s\"", argv[i]);
    }

    return _PAM_FINDKEY_OK;
}

/* free the allocated memory of a struct _pam_opts */
int _pam_opts_free(pam_handle_t *pamh, _opts* opts) {
    if (NULL == opts)
        pam_syslog(pamh, LOG_ERR, "%s: NULL opts pointer", __FUNCTION__);

    _pam_drop(opts->conf);
    return _PAM_FINDKEY_OK;
}

/* associate target nodes with respective major and minor */
int _pam_complete_key_config(pam_handle_t *pamh, _opts* opts) {
    _dev *current_dev;
    _tuple *current_node = opts->key_config.key_nodes;
  
    /* for each node to create, retreive major and minor */
    while (current_node != last_tuple) {

        current_dev = opts->dev_config;
        while (current_dev != last_dev) {
      
            /* if root device and partition match */ 
            if (strcmp(opts->key_config.root_dev, current_dev->device) == 0 &&
                atoi(current_node->part) == current_dev->part) 
            {
                strcpy(current_node->major, current_dev->major);
                strcpy(current_node->minor, current_dev->minor);
            }
            current_dev++;
        }
        current_node++;
    } 

    return _PAM_FINDKEY_OK;
}

/* fill device list struct with infos from /proc/partitions */
int _pam_build_device_tab(pam_handle_t *pamh, _opts *opts) {
    FILE *devs;
    char devs_line[LINELEN];
    regex_t reg, reg_part;
    regmatch_t pmatch[PART_INFO_NB_TOKENS];
    char *expr = "^[\t ]+([0-9]+)[\t ]+([0-9]+)[\t ]+[0-9]+[\t ]+([^0-9]+)$";
    char *expr_part = "^[\t ]+([0-9]+)[\t ]+([0-9]+)[\t ]+[0-9]+[\t ]+([^0-9]+)([0-9]+)$" ;
    _dev *current = opts->dev_config;

    if (regcomp(&reg, expr, REG_EXTENDED) != 0 || 
        regcomp(&reg_part, expr_part, REG_EXTENDED) != 0)
        return _PAM_FINDKEY_SYSERR;

    if (!(devs = fopen("/proc/partitions", "r"))) {
        pam_syslog(pamh, LOG_ERR, "%s: fopen(%s): %s", opts->module, "/proc/partitions",
            strerror(errno));
        regfree(&reg);
        return _PAM_FINDKEY_SYSERR;
    } 

    while (fgets(devs_line, LINELEN, devs)) {

        devs_line[strlen(devs_line)-1] = '\0';
        if (regexec(&reg, devs_line, PART_INFO_NB_TOKENS, pmatch , 0) == 0) 
            current->part = 0;
        else if (regexec(&reg_part, devs_line, PART_INFO_NB_TOKENS, pmatch , 0) == 0) 
            current->part = atoi(devs_line+pmatch[PART_INFO_PART].rm_so);
        else continue;
    
        strncpy(current->device, devs_line+pmatch[PART_INFO_DEV].rm_so, 
            pmatch[PART_INFO_DEV].rm_eo - pmatch[PART_INFO_DEV].rm_so);
        current->device[3] = '\0';

        strncpy(current->major, devs_line+pmatch[PART_INFO_MAJ].rm_so,  
            pmatch[PART_INFO_MAJ].rm_eo - pmatch[PART_INFO_MAJ].rm_so);
        current->major[pmatch[PART_INFO_MAJ].rm_eo - pmatch[PART_INFO_MAJ].rm_so] = '\0';

        strncpy(current->minor, devs_line+pmatch[PART_INFO_MIN].rm_so,  
            pmatch[PART_INFO_MIN].rm_eo - pmatch[PART_INFO_MIN].rm_so);
        current->minor[pmatch[PART_INFO_MIN].rm_eo - pmatch[PART_INFO_MIN].rm_so] = '\0';

        if (opts->debug)
            pam_syslog(pamh, LOG_DEBUG, "%s: add device tab entry : %s, major %s, minor %s, part %d",
                opts->module, current->device, current->major, current->minor, current->part);

        current++;
    }
    last_dev = current;
    regfree(&reg);
    fclose(devs);

    return _PAM_FINDKEY_OK;
}

/* check path file on each existing device */ 
int _pam_check_key(pam_handle_t *pamh, _opts *opts) {
    FILE *key;
    char part[LINELEN], real_path[LINELEN];
    _dev *current = opts->dev_config;

    /* search key on each existing devices */
    while (current != last_dev) {
        if (current->part) {
            current++;
            continue;
        }

        snprintf(part, 7 + strlen(current->device), "/dev/%s%s", current->device, 
            opts->key_config.key_file.part);

        /* mount device to test */
        umount(TMP_MNT_PT);
        if (mount(part, TMP_MNT_PT, "ext3", 0, NULL) == 0) {

            if (opts->debug)
                pam_syslog(pamh, LOG_DEBUG, "%s: mount \"%s\" on \"%s\": ok", 
                    opts->module, part, TMP_MNT_PT);

            /* search key file at expected path on partition */
            snprintf(real_path, 7 + strlen(opts->key_config.key_file.path),
                "/media%s", opts->key_config.key_file.path);

            if (!(key = fopen(real_path, "r"))) {

                if (opts->debug)
                    pam_syslog(pamh, LOG_DEBUG, "%s: no key found on \"%s\"", 
                        opts->module, part);

                current++;
                continue;
            }

            strcpy(opts->key_config.root_dev, current->device);

            if (opts->debug)
                pam_syslog(pamh, LOG_DEBUG, "%s: key found on \"%s\"", 
                    opts->module, part);
          
            /* TODO: match key from file with penitentiary one */

            fclose(key);
            umount(TMP_MNT_PT);
            return _PAM_FINDKEY_OK;

        } else {

            if (opts->debug)
                pam_syslog(pamh, LOG_DEBUG, "%s: mount \"%s\" on \"%s\": failed", 
                    opts->module, part, TMP_MNT_PT);

        }
        current++;
    }

    return _PAM_FINDKEY_KEYNOTFOUND;
}

/* create all target nodes */ 
int _pam_create_node(pam_handle_t *pamh, _opts *opts) {
    _tuple *current;

    current = opts->key_config.key_nodes;
    while (current != last_tuple) {
    
        if (opts->debug)
            pam_syslog(pamh, LOG_DEBUG, "%s: create node \"%s\", minor %s, major %s", 
                opts->module, current->path, current->major, current->minor);
    
        remove (current->path);
        if (mknod(current->path, S_IFBLK|S_IRUSR|S_IWUSR|S_IRGRP|S_IWGRP, 
            makedev(atoi(current->major), atoi(current->minor))) != 0) 
            return _PAM_FINDKEY_SYSERR;

        current++;
    }

    return _PAM_FINDKEY_OK;
}

/* parse configuration file findkey.conf */
int _pam_get_keyconfig(pam_handle_t *pamh, _opts *opts) {
    FILE* conf;
    char conf_line[LINELEN];
    int lineno, err;
    _tuple *current;
    regex_t reg;
    regmatch_t pmatch[3];
    char *expr = "^.*=([0-9])([[:alnum:]_/]+).*";

    if (regcomp(&reg, expr, REG_EXTENDED) !=0)
        return _PAM_FINDKEY_SYSERR;

    if (!(conf = fopen(opts->conf, "r"))) {
        pam_syslog(pamh, LOG_ERR, "%s: fopen(%s): %s", opts->module, opts->conf,
            strerror(errno));
        return _PAM_FINDKEY_SYSERR;
    }

    /* for each conf file lines */
    lineno = 0; err = 0;
    current = opts->key_config.key_nodes;
    opts->key_config.key_file.path[0]='\0';
    while (fgets(conf_line, LINELEN, conf)) {
        ++lineno;
    
        if (regexec(&reg, conf_line, 3, pmatch , 0) == 0) {
      
            /* is it the key path entry ? */
            if (!strncmp(conf_line, "KEY_FILE", 8)) {
	
                strncpy(opts->key_config.key_file.part, conf_line+pmatch[1].rm_so, 
                    pmatch[1].rm_eo - pmatch[1].rm_so);
                opts->key_config.key_file.part[pmatch[1].rm_eo - pmatch[1].rm_so] = '\0';
                strncpy(opts->key_config.key_file.path, conf_line+pmatch[2].rm_so, 
	                pmatch[2].rm_eo - pmatch[2].rm_so);
                opts->key_config.key_file.path[pmatch[2].rm_eo - pmatch[2].rm_so] = '\0';

	            if (opts->debug)
                    pam_syslog(pamh, LOG_DEBUG, "%s: register key path \"%s\" on part \"%s\"",
                        opts->module, opts->key_config.key_file.path, 
                         opts->key_config.key_file.part);
	
            /* or a node entry ? */
            } else if (!strncmp(conf_line, "NODE", 4)) {
		
                /* duplicated code... */
                strncpy(current->part, conf_line+pmatch[1].rm_so, 
	                pmatch[1].rm_eo - pmatch[1].rm_so);
                current->part[pmatch[1].rm_eo - pmatch[1].rm_so] = '\0';
                strncpy(current->path, conf_line+pmatch[2].rm_so, 
	                pmatch[2].rm_eo - pmatch[2].rm_so);
                current->path[pmatch[2].rm_eo - pmatch[2].rm_so] = '\0';
	
                if (opts->debug)
                    pam_syslog(pamh, LOG_DEBUG, "%s: register node \"%s\" on part \"%s\"",
                        opts->module, current->path, 
                        opts->key_config.key_file.part);

                current++;
            }
        }
    }
    fclose(conf);
 
    if (opts->key_config.key_file.path[0] == '\0') {
    
        pam_syslog(pamh, LOG_ERR, "%s: %s %d: validation key path expected", 
            opts->module, opts->conf, lineno);
            
        return _PAM_FINDKEY_BADCONF;
    }

    if (current - opts->key_config.key_nodes < 1) {
        
        pam_syslog(pamh, LOG_ERR, "%s: %s %d: at least one target device node exepected", 
            opts->module, opts->conf, lineno);
        
        return _PAM_FINDKEY_BADCONF;
    }
    last_tuple = current;

    return _PAM_FINDKEY_OK;
}

/* Parse configuration file findkey.conf to find
 * communication pipe path */
int _pam_get_pipepath(pam_handle_t *pamh, _opts *opts) {
    FILE* conf;
    char conf_line[LINELEN];
    regex_t reg;
    regmatch_t pmatch[2];
    char *expr = "^.*=([[:alnum:]_/]+).*";
   
    if (regcomp(&reg, expr, REG_EXTENDED) !=0)
        return _PAM_FINDKEY_SYSERR;

    if (!(conf = fopen(opts->conf, "r"))) {
        pam_syslog(pamh, LOG_ERR, "%s: fopen(%s): %s", opts->module, opts->conf,
            strerror(errno));
        return _PAM_FINDKEY_SYSERR;
    }

    /* for each conf file lines */
    while (fgets(conf_line, LINELEN, conf)) {
    
        /* is it the communication pipe path ? */
        if (regexec(&reg, conf_line, 2, pmatch , 0) == 0 && 
            !strncmp(conf_line, "COM_PIPE", 8)) 
        {
            strncpy(opts->pipe_path, conf_line+pmatch[1].rm_so, 
                pmatch[1].rm_eo - pmatch[1].rm_so);
            opts->pipe_path[pmatch[1].rm_eo - pmatch[1].rm_so] = '\0';

            if (opts->debug)
                pam_syslog(pamh, LOG_DEBUG, "%s: register communication path \"%s\"",
                    opts->module, opts->pipe_path);

            break;
        }
    }
    fclose(conf);

    return _PAM_FINDKEY_BADCONF;
}

/* This is the workhorse function. All of the pam_sm_* functions should
 * initialize a _pam_opts struct with the command line args and flags,
 * then pass it to this function */
int _pam_do_nodes(pam_handle_t *pamh, _opts *opts) {
    int err;

    opts->debug = opts->flags & _PAM_OPTS_DEBUG;
    if (opts->debug) 
        pam_syslog(pamh, LOG_DEBUG, "%s: entering stage 1: build device tab", opts->module);

    if ((err = _pam_build_device_tab(pamh, opts)) != _PAM_FINDKEY_OK) {
        pam_syslog(pamh, LOG_ERR, "%s: error while creating device tab", opts->module);
        return _PAM_FINDKEY_SYSERR;
    }

    if (opts->debug)
        pam_syslog(pamh, LOG_DEBUG, "%s: entering stage 2: parse configuration file (%s)", 
            opts->module, opts->conf);

    if ((err = _pam_get_keyconfig(pamh, opts)) != _PAM_FINDKEY_OK) 
        return err;

    if (opts->debug)
        pam_syslog(pamh, LOG_DEBUG, "%s: entering stage 3: expected key validation",
            opts->module);

    if((err = _pam_check_key(pamh, opts)) != _PAM_FINDKEY_OK) 
        return err;

    if (opts->debug)
        pam_syslog(pamh, LOG_DEBUG, "%s: entering stage 4: retrieve nodes major and minor",
            opts->module);
  
    if((err = _pam_complete_key_config(pamh, opts)) != _PAM_FINDKEY_OK) 
        return err;

    if (opts->debug) 
        pam_syslog(pamh, LOG_DEBUG, "%s: entering stage 5: create target nodes", opts->module);

    if((err = _pam_create_node(pamh, opts)) != _PAM_FINDKEY_OK)
        return err;
 
    return _PAM_FINDKEY_OK;
}

PAM_EXTERN int pam_sm_authenticate(pam_handle_t *pamh, int flags, int argc,
                                   const char **argv) 
{
    int err;
    _opts opts;

    _pam_opts_init(pamh, &opts);
    _pam_opts_config(pamh, &opts, flags, argc, argv);
    opts.module = "auth";

    err = _pam_do_nodes(pamh, &opts);
    switch(err) {
        case _PAM_FINDKEY_OK:
            if (opts.debug) 
                pam_syslog(pamh, LOG_DEBUG, "%s: returning success", opts.module);
            err = PAM_SUCCESS;
            break;

        case _PAM_FINDKEY_KEYNOTFOUND:
            if (opts.debug) 
                pam_syslog(pamh, LOG_DEBUG, "%s: unknown key", opts.module);
            err = PAM_AUTH_ERR;
            break;

        case _PAM_FINDKEY_BADCONF:
            if (opts.debug) 
                pam_syslog(pamh, LOG_DEBUG, "%s: returning incomplete", opts.module);
            err = PAM_INCOMPLETE;
            break;

        case _PAM_FINDKEY_INTERNALERR:
            if (opts.debug) 
                pam_syslog(pamh, LOG_ERR, "%s: internal error encountered", opts.module);
            err = PAM_AUTH_ERR;
            break;

        default:
            if (opts.debug) 
                pam_syslog(pamh, LOG_DEBUG, "%s: returning failure", opts.module);
            err = PAM_AUTH_ERR;
            break;
    }
    _pam_opts_free(pamh, &opts);
    return err;
}

PAM_EXTERN int pam_sm_setcred(pam_handle_t *pamh, int flags, int argc, const
                              char **argv) 
{
    pam_syslog(pamh, LOG_ERR, "not a credentialator");
    return PAM_IGNORE;
}

PAM_EXTERN int pam_sm_acct_mgmt(pam_handle_t *pamh, int flags, int argc, const
                                char **argv) 
{
    /* all staff done at auth stage */
    return PAM_SUCCESS;
}

PAM_EXTERN int pam_sm_open_session(pam_handle_t *pamh, int flags, int argc,
                                   const char **argv) 
{  
    int err;
    _opts opts;
 
    _pam_opts_init(pamh, &opts);
    _pam_opts_config(pamh, &opts, flags, argc, argv);
    opts.module = "auth";

    err = _pam_do_nodes(pamh, &opts);
    switch(err) {
        case _PAM_FINDKEY_OK:
            if (opts.debug) 
                pam_syslog(pamh, LOG_DEBUG, "%s: returning success", opts.module);
            err = PAM_SUCCESS;
            break;

        case _PAM_FINDKEY_KEYNOTFOUND:
            if (opts.debug) 
                pam_syslog(pamh, LOG_DEBUG, "%s: unknown key", opts.module);
            err = PAM_AUTH_ERR;
            break;

        case _PAM_FINDKEY_BADCONF:
            if (opts.debug) 
                pam_syslog(pamh, LOG_DEBUG, "%s: returning incomplete", opts.module);
            err = PAM_INCOMPLETE;
            break;

        case _PAM_FINDKEY_INTERNALERR:
            if (opts.debug) 
                pam_syslog(pamh, LOG_ERR, "%s: internal error encountered", opts.module);
            err = PAM_AUTH_ERR;
            break;

        default:
            if (opts.debug) 
                pam_syslog(pamh, LOG_DEBUG, "%s: returning failure", opts.module);
            err = PAM_AUTH_ERR;
            break;
    }
    _pam_opts_free(pamh, &opts);
    return err;
}


PAM_EXTERN int pam_sm_close_session(pam_handle_t *pamh, int flags, int argc,
                                    const char **argv) {
    FILE* pipe;
    _opts opts;

    _pam_opts_init(pamh, &opts);
    _pam_opts_config(pamh, &opts, flags, argc, argv);
    opts.module = "close session";
  
    if (opts.pipe_path != NULL) 
        _pam_get_pipepath(pamh, &opts);
    
    /* here sending a message on the penitentiary/jail */
    /* communication pipe to signal a logout event     */
    if (!(pipe = fopen(opts.pipe_path, "w"))) {
    
        pam_syslog(pamh, LOG_ERR, "%s: fopen(%s): %s", opts.module, opts.pipe_path,
            strerror(errno));

        return _PAM_FINDKEY_SYSERR;
    }

    if (opts.debug) 
        pam_syslog(pamh, LOG_DEBUG, "%s: sending msg on pipe: %s", opts.module, opts.pipe_path);

    fputs(LOGOUT_MSG, pipe);

    fclose(pipe);
    _pam_opts_free(pamh, &opts);  
    return PAM_SUCCESS;
}

PAM_EXTERN int pam_sm_chauthtok(pam_handle_t *pamh, int flags, int argc, const
                                char **argv) {
    pam_syslog(pamh, LOG_ERR, "password management group is unsupported");
    return PAM_SERVICE_ERR;
}

