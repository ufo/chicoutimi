from videoconf import VideoConfiguration
import logging
import os, os.path as path
import shutil
import commands

class VirtualBoxConfiguration(VideoConfiguration):
    name = 'virtualbox'

    def __init__(self, profile, id):
        super(VirtualBoxConfiguration, self).__init__(profile, id)

    def generate_xorg(self):
        logging.info("Generating xorg.conf for VirtualBox")
        open(self.xorg, "w").write("""# Default xorg.conf for Xorg 1.5+ without PCI_TXT_IDS_PATH enabled.
#
# This file was created by VirtualBox Additions installer as it
# was unable to find any existing configuration file for X.

Section "Device"
        Identifier      "VirtualBox Video Card"
        Driver          "vboxvideo"
EndSection
""")

    def activate_compiz(self, state):
        super(VirtualBoxConfiguration, self).activate_compiz(False)

def configuration(profile, id):
    if 'VirtualBox' in id or 'InnoTek' in profile.manufacturer:
        return VirtualBoxConfiguration(profile, id)

