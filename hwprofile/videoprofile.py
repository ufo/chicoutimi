import commands
import os, os.path as path
from hwprofile import HardwareProfile, HWPROFILE_PATH
from videoconf import VideoConfiguration


class VideoProfile(HardwareProfile):
    klass = "videoprofile"
    XORGCONF = '/etc/X11/xorg.conf'
    CGINFO_PATH = HWPROFILE_PATH + "/cginfo"

    def restore_xorg(self):
        os.unlink(self.XORGCONF)

    def get_signature(self):
        output = commands.getoutput(self.CGINFO_PATH)
        return output

    def get_default_configuration(self, id):
        return VideoConfiguration(self, id)

profile = VideoProfile
