Name:           hwprofile
Version:        0.2
Release:        1%{?dist}
Summary:        Hardware profiles manager

BuildArch:      noarch
Group:          Applications/System
License:        GPLv2
URL:            http://www.glumol.com
Source0:        http://www.glumol.com/chicoutimi/hwprofile-0.2.tar.gz
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

Requires:       lshw livna-config-display
Requires:       compiz xcompmgr kmod-nvidia kmod-fglrx

%description
hwprofile manages hardware profiles. It keeps track of the previous working 
configurations and activate them when the corresponding hardware is detected.


%prep
%setup -q


%build
make %{?_smp_mflags}


%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT


%post
/sbin/chkconfig --add hwprofile


%preun
/sbin/chkconfig --del hwprofile


%clean
rm -rf $RPM_BUILD_ROOT


%files
%defattr(-,root,root,-)
%doc
%{_initrddir}/hwprofile
%dir %{_sysconfdir}/sysconfig/hwprofile
%dir %{_datadir}/hwprofile/configurations
%dir %{_datadir}/hwprofile/profiles
%{_datadir}/hwprofile/hwprofile.py
%{_datadir}/hwprofile/hwprofile.pyc
%{_datadir}/hwprofile/hwprofile.pyo
%{_datadir}/hwprofile/cginfo
%{_datadir}/hwprofile/hwconf.py
%{_datadir}/hwprofile/hwconf.pyc
%{_datadir}/hwprofile/hwconf.pyo
%{_datadir}/hwprofile/videoconf.py
%{_datadir}/hwprofile/videoconf.pyc
%{_datadir}/hwprofile/videoconf.pyo
%{_datadir}/hwprofile/profiles/videoprofile.py
%{_datadir}/hwprofile/profiles/videoprofile.pyc
%{_datadir}/hwprofile/profiles/videoprofile.pyo
%{_datadir}/hwprofile/configurations/livna.py
%{_datadir}/hwprofile/configurations/livna.pyc
%{_datadir}/hwprofile/configurations/livna.pyo
%{_datadir}/hwprofile/configurations/virtualbox.py
%{_datadir}/hwprofile/configurations/virtualbox.pyc
%{_datadir}/hwprofile/configurations/virtualbox.pyo


%changelog
* Sat Sep 20 2008 Sylvain Baubeau <bob@glumol.com>
Initial release
