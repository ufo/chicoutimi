#!/usr/bin/python

HWPROFILE_CONF_PATH = '/etc/sysconfig/hwprofile/'
HWPROFILE_PATH = '/usr/share/hwprofile'
HWPROFILE_CONFIG_FILE = '/etc/sysconfig/hwprofile/global.conf'
HWPROFILE_PROFILES_PATH = '/usr/share/hwprofile/profiles'
HWPROFILE_CONFIGURATIONS_PATH = '/usr/share/hwprofile/configurations'
DEFAULT_LOG_LEVEL = 'debug'
DEFAULT_LOG_FILE = '/var/log/hwprofile'

import sys
import os, os.path as path
import logging
from ConfigParser import SafeConfigParser, NoSectionError, DEFAULTSECT
import glob

class HardwareProfile:
    def find_existing_configuration(self, id):
        klass_path = path.join(HWPROFILE_CONFIGURATIONS_PATH)
        self.conf_path = path.join(HWPROFILE_CONF_PATH, self.klass, id)
        sys.path.append(klass_path)
        logging.info("Adding " + klass_path + " to path")
        if not path.exists(self.conf_path):
            logging.info("Configuration " + id + ' does not exist')
            return None
        logging.info('Found configuration ' + self.conf_path)
        self.kind_path = path.join(self.conf_path, "type")
        if not path.exists(self.kind_path):
            logging.error("Configuration " + id + ' is invalid')
            return None
        module = open(self.kind_path).read().strip()
        if not module: return None
        logging.info("Loading module " + module)
        try:
            exec "import " + module
            conf = eval(module + ".configuration(self, '" + self.conf_path + "')")
        except:
            logging.error("Error while loading configuration " + self.conf_path +
                          " of type " + module)
            conf = None
        return conf

    def find_suitable_configuration(self, sig):
        candidates = glob.glob(path.join(HWPROFILE_CONFIGURATIONS_PATH, "*.py"))
        for conf in candidates:
            module = path.splitext(path.basename(conf))[0]
            exec "import " + module
            conf = eval(module + ".configuration(self, '" + self.conf_path + "')")
            if conf:
                return conf
            else:
                logging.debug(module + ' can not handle ' + sig)


    def detect(self):
        sig = self.get_signature()
        id, self.manufacturer = sig.split('\n')
        id = id.strip().replace('/', '_').replace(' ','')
        conf = self.find_existing_configuration(id)
        if not conf:
            logging.info("Didn't find existing configuration for '" +
                         id + "'. Search for a suitable one.")
            conf = self.find_suitable_configuration(id)
        if not conf:
            logging.info("Didn't find suitable configuration for '" +
                         id + "'. Using default.")
            conf = self.get_default_configuration(id)
        self.conf = conf
        conf.apply()

class HardwareManager:
    def __init__(self):
        self.load_global_conf()
        self.setup_logging()
        profiles = self.load_profiles()
        for profile in profiles:
            profile().detect()

    def load_profiles(self):
        sys.path.append(HWPROFILE_PROFILES_PATH)
        candidates = glob.glob(path.join(HWPROFILE_PROFILES_PATH, "*.py"))
        profiles = []
        for profile in candidates:
            module = path.splitext(path.basename(profile))[0]
            logging.info("Loading profile " + module)
            exec "import " + module
            prof = eval(module + ".profile")
            if prof:
                profiles.append(prof)
        return profiles

    def load_global_conf(self):
        self.config = SafeConfigParser({ "logfile" : DEFAULT_LOG_FILE,
                                         "loglevel" : DEFAULT_LOG_LEVEL })
        self.config.read(HWPROFILE_CONFIG_FILE)

    def setup_logging(self):
        format = "%(asctime)s %(levelname)s %(message)s"
        levels = [logging.getLevelName(x) for x in (0, 10, 20, 30, 40, 50)]
        try:
            logfile = self.config.get(DEFAULTSECT, "logfile")
            loglevel = self.config.get(DEFAULTSECT, "loglevel").upper()
        except NoSectionError:
            logfile = ""
            loglevel = "DEBUG"
        try:
            level = levels.index(loglevel) * 10
        except ValueError:
            logging.error("Unknown log level " + loglevel)
        if logfile:
            logging.basicConfig(format=format, level=level, filename=logfile)
        else:
            logging.basicConfig(format=format, level=level)

if __name__ == '__main__':
    HardwareManager()

