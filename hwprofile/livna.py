from videoconf import VideoConfiguration
import logging
import os, os.path as path
import shutil
import commands

LIVNA_CONFIG_DISPLAY = "/usr/bin/livna-config-display --active on"

NVIDIA = 'nVidia Corporation'
NVIDIA_CONFIG_DISPLAY = "/usr/sbin/nvidia-config-display"
NVIDIA_MODULE = 'nvidia'

ATI = 'ATI Technologies Inc'
FGLRX_CONFIG_DISPLAY = "/usr/sbin/fglrx-config-display"
ATI_MODULE = 'fglrx'

LIVNA_CONF = '/etc/sysconfig/videodrv.conf'

class LivnaConfiguration(VideoConfiguration):
    name = 'livna'

    def __init__(self, profile, id):
        super(LivnaConfiguration, self).__init__(profile, id)

    def generate_xorg(self):
        self.remove_xorg()
        logging.info("Generating xorg.conf with livna-config-display")
        logging.debug(commands.getoutput(LIVNA_CONFIG_DISPLAY))
        if self.profile.manufacturer == NVIDIA:
            cmd = NVIDIA_CONFIG_DISPLAY
            module = NVIDIA_MODULE
        else:
            cmd = FGLRX_CONFIG_DISPLAY
            module = ATI_MODULE
        open(LIVNA_CONF, 'w').write(module)
        cmd = cmd + " enable"
        logging.info(commands.getoutput(cmd))
        shutil.copyfile(self.profile.XORGCONF, path.join(self.conf_path, 'xorg.conf'))
        self.remove_xorg()

    def activate_compiz(self, state):
        super(LivnaConfiguration, self).activate_compiz(True)

def configuration(profile, id):
    if profile.manufacturer in (NVIDIA, ATI):
        return LivnaConfiguration(profile, id)

