import os, os.path as path
import logging

class HardwareConfiguration(object):
    def __init__(self, profile, id):
        self.profile = profile
        self.conf_path = profile.conf_path
        self.id = id

    def get_kind_path(self):
        return path.join(self.conf_path, "type")

    def setup(self):
        if not path.exists(self.conf_path):
            logging.info("Creating configuration " + self.conf_path)
            os.makedirs(self.conf_path)
        kind_path = self.get_kind_path()
        open(kind_path, 'w').write(self.name)

