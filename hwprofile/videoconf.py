import os, os.path as path
import shutil
import logging
from hwconf import HardwareConfiguration

SYSTEM_CONFIG_DISPLAY = '/usr/bin/system-config-display --noui'
WINDOW_MANAGER_KEY = "/apps/gnome-session/rh/window_manager"

class VideoConfiguration(HardwareConfiguration):
    name = 'videoconf'

    def __init__(self, profile, id):
        super(VideoConfiguration, self).__init__(profile, id)
        self.xorg = path.join(self.conf_path, 'xorg.conf')

    def activate_compiz(self, state):
        return
        # import gconf
        # if state: wm = "compiz"
        # else: wm = "metacity"
        # conf = gconf.client_get_default()
        # conf.set_string(WINDOW_MANAGER_KEY, wm)

    def generate_xorg(self):
        self.remove_xorg()
        logging.info("Generating xorg.conf")
        os.system(SYSTEM_CONFIG_DISPLAY + " -o " + self.xorg)

    def link_xorg(self):
        logging.debug("Creating symlink " + self.profile.XORGCONF)
        os.system('ln -s "' + path.join(self.conf_path, 'xorg.conf') + '" "' +
                  self.profile.XORGCONF + '"')

    def remove_xorg(self):
        if path.lexists(self.profile.XORGCONF):
            logging.debug("Removing " + self.profile.XORGCONF)
            os.unlink(self.profile.XORGCONF)

    def restore_xorg(self):
        self.remove_xorg()
        self.link_xorg()

    def apply(self):
        self.setup()
        if path.exists(path.join(self.conf_path, 'xorg.conf')):
            self.restore_xorg()
        else:
            self.generate_xorg()
            self.remove_xorg()
            self.link_xorg()
        self.activate_compiz(False)

configuration = VideoConfiguration
