/*
 * PAM module for remote host reachability test.
 *
 * 2 arguments must be given as pam module arguments:
 *   host: host to connect
 *   port: port to connect
 */

#include <stdio.h>
#include <unistd.h>
#include <ctype.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <syslog.h>
#include <sys/types.h>
#include <netdb.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <sys/wait.h>
#include <signal.h>
#include <arpa/inet.h>
#include <errno.h>
#include <fcntl.h>

/* defines for (internal) return values */
#define _PAM_CONNECTED_SYSERR      -1
#define _PAM_CONNECTED_OK          0
#define _PAM_CONNECTED_UNREACH     -2

/* time interval between retries */
#define INTERVAL 100000 


int _pam_connect(const char *host, int port, int timeout, int debug) {

    struct sockaddr_in addr;
    struct hostent *hp;
    int sockfd, flags;
    
    if((sockfd = socket(AF_INET, SOCK_STREAM, 0)) == -1) {
        printf("socket failed\n");
        return _PAM_CONNECTED_SYSERR;
    }

    flags = fcntl (sockfd, F_GETFL, 0);	
    if (fcntl(sockfd, F_SETFL, flags | O_NONBLOCK) < 0) { 
        printf("fnctl failed\n");
	    return _PAM_CONNECTED_SYSERR;
    } 

    printf("Try to connect to %s:%d...\n", host, port);
    
    if ((hp = gethostbyname(host)) == NULL) {
        printf("Unable to resolve %s\n", host);
	    return _PAM_CONNECTED_UNREACH;
    }

    printf("Resolved host %s -> %d.%d.%d.%d\n", host, 
	     (unsigned char)hp->h_addr[0], (unsigned char)hp->h_addr[1], 
	     (unsigned char)hp->h_addr[2], (unsigned char)hp->h_addr[3]);

    memset(&addr,0,sizeof(addr));
    addr.sin_family = AF_INET;
    addr.sin_port = htons(port);
    bcopy(hp->h_addr, &(addr.sin_addr.s_addr), hp->h_length);

    if(connect(sockfd, (struct sockaddr *)&addr, sizeof(struct sockaddr)) < 0) {
        printf("Non blocking connect spawned (%s)\n", strerror(errno));
    }
    
    for(timeout = 10 * timeout; timeout > 0; timeout--) {
        connect(sockfd, (struct sockaddr *)&addr, sizeof(struct sockaddr));

	    if (debug) 
	        printf("Socket status : %s\n", strerror(errno));

	    /* errno 106 : Transport endpoint is already connected */
	    if (errno == 106) {
	        printf("Socket connected to %s:%d\n", host, port);
	  
	        shutdown(sockfd, 2);
	        return _PAM_CONNECTED_OK;
	    }

	    usleep (INTERVAL);
    }
    printf("%s:%d is unreachable\n", host, port);
    
    shutdown(sockfd, 2);
    return _PAM_CONNECTED_UNREACH;
}

int main (int argc, char ** argv) {

    int port;
    int timeout;
    int debug = 0;

    if (argc < 4) {
	    printf("3 args needed (host, port, timeout). %d given\n", argc);
	    return 2;
	}
	if (argc >= 5)
	    debug = 1;

    port    = atoi(argv[2]);
    timeout = atoi(argv[3]);
    
    if (_pam_connect(argv[1], port, timeout, debug) == _PAM_CONNECTED_OK)
        return 0;
    else 
        return 1;
}
