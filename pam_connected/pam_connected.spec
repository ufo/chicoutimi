Name:           pam_connected
Version:        0.1
Release:        1%{?dist}
Summary:        Pam module to test server reachability

BuildArch:      i386
Group:          Applications/System
License:        GPLv2
URL:            http://www.glumol.com
Source0:        http://www.glumol.com/chicoutimi/pam_connected-0.1.tar.gz
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)  
Requires:       pam_connected
# /etc/sysconfig/ufo/kerberos5 agorabox-conf-specific
BuildRequires:  pam-devel

%description
Pam module to test server reachability.
It should be convenient to known this to avoid
some pam module hang on DNS timeout.	

%prep
%setup -q


%build
make build-pam %{?_smp_mflags}
make build-bin %{?_smp_mflags}


%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT


%post


%preun


%clean
rm -rf $RPM_BUILD_ROOT


%files
%defattr(-,root,root,-)
%doc README COPYING
/lib/security/pam_connected.so
%{_bindir}/connected


%changelog
* Fri Dec 19 2008 Kevin	Pouget <kevin.pouget@agorabox.org>
Add "connected" binary to test any host when needed.

* Fri Dec 19 2008 Kevin	Pouget <kevin.pouget@agorabox.org>
Initial release
