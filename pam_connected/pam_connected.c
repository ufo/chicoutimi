/*
 * PAM module for remote host reachability test.
 *
 * 2 arguments must be given as pam module arguments:
 *   host: host to connect
 *   port: port to connect
 */

#include <stdio.h>
#include <unistd.h>
#include <ctype.h>
#include <stdarg.h>
#include <stdlib.h>
#include <string.h>
#include <syslog.h>
#include <sys/types.h>
#include <netdb.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <errno.h>

#include <security/pam_modules.h>
#include <security/_pam_macros.h>
#include <security/pam_ext.h>

/* defines for (internal) return values */
#define _PAM_CONNECTED_SYSERR      -1
#define _PAM_CONNECTED_OK          0
#define _PAM_CONNECTED_UNREACH     -2

/* time interval between retries */
#define INTERVAL 100000 

static void _pam_log(int err, const char *format, ...) {
    va_list args;

    va_start(args, format);
    openlog("pam_connected", LOG_PID, LOG_AUTHPRIV);
    vsyslog(err, format, args);
    va_end(args);
    closelog();
}

int _pam_connect(const char *host, int port, int timeout, int debug) {

    struct sockaddr_in addr;
    struct hostent *hp;
    int sockfd, flags;
    
    if((sockfd = socket(AF_INET, SOCK_STREAM, 0)) == -1) {
        _pam_log(LOG_CRIT, "socket failed");
        return _PAM_CONNECTED_SYSERR;
    }

    flags = fcntl (sockfd, F_GETFL, 0);	
    if (fcntl(sockfd, F_SETFL, flags | O_NONBLOCK) < 0) { 
        _pam_log(LOG_CRIT, "fnctl failed");
	    return _PAM_CONNECTED_SYSERR;
    } 

    _pam_log(LOG_CRIT, "Try to connect to %s:%d...\n", host, port);
    
    if ((hp = gethostbyname(host)) == NULL) {
        _pam_log(LOG_CRIT, "Unable to resolve %s", host);
	    return _PAM_CONNECTED_UNREACH;
    }

    _pam_log(LOG_CRIT, "Resolved host %s -> %d.%d.%d.%d\n", host, 
	     (unsigned char)hp->h_addr[0], (unsigned char)hp->h_addr[1], 
	     (unsigned char)hp->h_addr[2], (unsigned char)hp->h_addr[3]);

    memset(&addr,0,sizeof(addr));
    addr.sin_family = AF_INET;
    addr.sin_port = htons(port);
    bcopy(hp->h_addr, &(addr.sin_addr.s_addr), hp->h_length);

    if(connect(sockfd, (struct sockaddr *)&addr, sizeof(struct sockaddr)) < 0) {
        _pam_log(LOG_CRIT, "Non blocking connect spawned (%s)\n",
		 strerror(errno));
    }
    
    for(timeout = 10 * timeout; timeout > 0; timeout--) {
        connect(sockfd, (struct sockaddr *)&addr, sizeof(struct sockaddr));

	    if (debug) 
	        _pam_log(LOG_CRIT, "Socket status : %s", strerror(errno));

	    /* errno 106 : Transport endpoint is already connected */
	    if (errno == 106) {
	        _pam_log(LOG_CRIT, "Socket connected to %s:%d", host, port);
	  
	        shutdown(sockfd, 2);
	        return _PAM_CONNECTED_OK;
	    }

	    usleep (INTERVAL);
    }
    _pam_log(LOG_CRIT, "%s:%d is unreachable", host, port);
    
    shutdown(sockfd, 2);
    return _PAM_CONNECTED_UNREACH;
}

PAM_EXTERN
int pam_sm_authenticate(pam_handle_t * pamh, int flags,
                        int argc, const char **argv)
{ 
    int port;
    int timeout;
    int debug = 0;

    const char *env_connected = pam_getenv(pamh, "CONNECTED");

    /* Connection not tested yet */
    if (env_connected == NULL) {

        if (argc < 3) {
	        _pam_log(LOG_CRIT, 
		        "3 args needed (host, port, timeout). %d given", argc);
	        return PAM_AUTH_ERR;
	    }
	    if (argc >= 4)
	        debug = 1;

        port    = atoi(argv[1]);
        timeout = atoi(argv[2]);
    
        if (_pam_connect(argv[0], port, timeout, debug) != _PAM_CONNECTED_OK) {
	        pam_putenv(pamh, "CONNECTED=false");
	        return PAM_AUTH_ERR;
	    }
	    pam_putenv(pamh, "CONNECTED=true");
	    return PAM_SUCCESS;
    }
    /* Connection is already tested */
    if (strcmp(env_connected, "true") == 0) {
        return PAM_SUCCESS;
    }
    return PAM_AUTH_ERR;
}

PAM_EXTERN 
int pam_sm_acct_mgmt(pam_handle_t *pamh, int flags, int argc, const
		     char **argv) 
{
    int port;
    int timeout;
    int debug = 0;

    const char *env_connected = pam_getenv(pamh, "CONNECTED");

    /* Connection not tested yet */
    if (env_connected == NULL) {

        if (argc < 3) {
	        _pam_log(LOG_CRIT, 
		        "3 args needed (host, port, timeout). %d given", argc);
	        return PAM_AUTH_ERR;
	    }
	    if (argc >= 4)
	        debug = 1;

        port    = atoi(argv[1]);
        timeout = atoi(argv[2]);
    
        if (_pam_connect(argv[0], port, timeout, debug) != _PAM_CONNECTED_OK) {
	        pam_putenv(pamh, "CONNECTED=false");
	        return PAM_AUTH_ERR;
	    }
	    pam_putenv(pamh, "CONNECTED=true");
	    return PAM_SUCCESS;
    }
    /* Connection is already tested */
    if (strcmp(env_connected, "true") == 0) {
        return PAM_SUCCESS;
    }
    return PAM_AUTH_ERR;
}

PAM_EXTERN
int pam_sm_setcred(pam_handle_t * pamh, int flags,
                   int argc, const char **argv)
{
    return PAM_IGNORE;
}

PAM_EXTERN
int pam_sm_open_session(pam_handle_t * pamh, int flags,
                        int argc ,const char **argv)
{
    int port;
    int timeout;
    int debug = 0;

    const char *env_connected = pam_getenv(pamh, "CONNECTED");

    /* Connection not tested yet */
    if (env_connected == NULL) {

        if (argc < 3) {
	        _pam_log(LOG_CRIT, 
		        "3 args needed (host, port, timeout). %d given", argc);
	        return PAM_AUTH_ERR;
	    }
	    if (argc >= 4)
	        debug = 1;

        port    = atoi(argv[1]);
        timeout = atoi(argv[2]);
    
        if (_pam_connect(argv[0], port, timeout, debug) != _PAM_CONNECTED_OK) {
	        pam_putenv(pamh, "CONNECTED=false");
	        return PAM_AUTH_ERR;
	    }
	    pam_putenv(pamh, "CONNECTED=true");
	    return PAM_SUCCESS;
    }
    /* Connection is already tested */
    if (strcmp(env_connected, "true") == 0) {
        return PAM_SUCCESS;
    }
    return PAM_AUTH_ERR;
}

PAM_EXTERN 
int pam_sm_close_session(pam_handle_t * pamh, int flags, int argc ,
			 const char **argv)
{
    return PAM_IGNORE;
}

PAM_EXTERN 
int pam_sm_chauthtok(pam_handle_t *pamh, int flags, int argc, 
		     const char **argv) 
{     
    int port;
    int timeout;
    int debug = 0;

    const char *env_connected = pam_getenv(pamh, "CONNECTED");

    /* Connection not tested yet */
    if (env_connected == NULL) {

        if (argc < 3) {
	        _pam_log(LOG_CRIT, 
		        "3 args needed (host, port, timeout). %d given", argc);
	        return PAM_AUTH_ERR;
	    }
	    if (argc >= 4)
	        debug = 1;

        port    = atoi(argv[1]);
        timeout = atoi(argv[2]);
    
        if (_pam_connect(argv[0], port, timeout, debug) != _PAM_CONNECTED_OK) {
	        pam_putenv(pamh, "CONNECTED=false");
	        return PAM_AUTH_ERR;
	    }
	    pam_putenv(pamh, "CONNECTED=true");
	    return PAM_SUCCESS;
    }
    /* Connection is already tested */
    if (strcmp(env_connected, "true") == 0) {
        return PAM_SUCCESS;
    }
    return PAM_AUTH_ERR;
}
