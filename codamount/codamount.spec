Name:           codamount
Version:        0.2
Release:        1%{?dist}
Summary:        Manage the user's coda folder
BuildArch:      noarch

Group:          System Environment/Base
License:        GPLv2
URL:            http://www.agorabox.org
Source0:        %{name}-%{version}.tar.gz
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

Requires:       coda-client pam_service authconfig >= 5.4.4-2 pam_connected /etc/sysconfig/ufo/coda


%description
Manage the user's coda folder.


%prep
%setup -q


%build


%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT


%post
. /etc/sysconfig/ufo/coda

/usr/sbin/codaconfedit /etc/coda/venus.conf realm $CODAREALM
/usr/sbin/codaconfedit /etc/coda/venus.conf kerberos5service coda/%s
/usr/sbin/codaconfedit /etc/coda/venus.conf kerberos5kinit kinit
/usr/sbin/codaconfedit /etc/coda/venus.conf RPC2_timeout 10

cp --preserve -f %{_sysconfdir}/krb5.conf %{_datadir}/codamount/
cp -f %{_datadir}/codamount/krb5.conf.default %{_sysconfdir}/krb5.conf
authconfig --enablekrb5 --enableservice --preservices=coda-client --postservices=coda-client --enableconnected --update
/sbin/chkconfig coda-client off


%preun
authconfig --disablekrb5 --disableservice --update
cp --preserve %{_datadir}/codamount/krb5.conf %{_sysconfdir}/krb5.conf


%clean
rm -rf $RPM_BUILD_ROOT


%files
%defattr(-,root,root,-)
%doc
%dir %{_datadir}/codamount
%{_bindir}/create-coda-folder
%{_bindir}/kclog
%{_datadir}/codamount/krb5.conf.default
%{_sysconfdir}/xdg/autostart/kclog.desktop
%{_sysconfdir}/xdg/autostart/create-coda-folder.desktop


%changelog
* Wed Jun 10 2009 Sylvain Baubeau <sylvain.baubeau@agorabox.org>
Use pam_service to launch venus at startup instead of pam_kcoda

* Tue Mar 24 2009 Sylvain Baubeau <sylvain.baubeau@agorabox.org>
Added an autostart to authenticate to Coda server

* Wed Oct 8 2008 Sylvain Baubeau <sylvain.baubeau@agorabox.org>
Initial release
