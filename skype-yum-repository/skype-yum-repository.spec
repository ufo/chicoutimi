Name:           skype-yum-repository
Version:        1.0
Release:        1%{?dist}
Summary:        The yum repository for Skype

Group:          System Environment/Base
License:        GPL
URL:            http://www.skype.com
Source0:        skype-yum-repository-%{version}.tar.gz
BuildArch:      noarch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)


%description
The yum repository for Skype.


%prep
%setup -q


%build


%install
rm -rf $RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT/etc/yum.repos.d
cp skype.repo $RPM_BUILD_ROOT/etc/yum.repos.d/


%clean
rm -rf $RPM_BUILD_ROOT


%files
%defattr(-,root,root,-)
%doc
%{_sysconfdir}/yum.repos.d/skype.repo


%changelog
* Sat Dec 28 2008 Bob <bob@glumol.com>
Initial release

