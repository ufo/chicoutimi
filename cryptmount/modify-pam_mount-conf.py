#!/usr/bin/python

import string
import commands
import sys
import xml.dom.minidom as dom

system_auth = '/etc/security/pam_mount.conf.xml'
usage = """modify-pam_mount-conf --install
                   --uninstall"""

if len(sys.argv) < 2:
    print usage
    sys.exit(1)

p = dom.parse(system_auth)
pammount = p.getElementsByTagName("pam_mount")[0]

dev = "/dev/crypthome"

if sys.argv[1] == '--install':
    flag = 0
    volumes = p.getElementsByTagName("volume")
    for volume in volumes:
        attr = volume.getAttribute("path")
        if dev in attr:
            flag = 1

    if not flag:
        pammount = p.getElementsByTagName("pam_mount")[0]
        element  = p.createElement("volume")
        element.setAttribute("user", "*")
        element.setAttribute("fstype", "crypt")
        element.setAttribute("path", dev)
        element.setAttribute("mountpoint", "/home/%(USER)")
        pammount.insertBefore(element, pammount.firstChild)
        logout = p.getElementsByTagName("logout")[0]
        logout.setAttribute("kill", "1")
    
    new_text = p.createTextNode("UFO password:")
    rnew_text = p.createTextNode("UFO password:")
    promptmsgs = p.getElementsByTagName("msg-authpw")
    if not promptmsgs:
        promptmsg = p.createElement("msg-authpw")
        promptmsg.appendChild(new_text)
        pammount.appendChild(promptmsg)
    else:
        promptmsg = promptmsgs[0]
        promptmsg.replaceChild(new_text, promptmsg.childNodes[0])
    rpromptmsgs = p.getElementsByTagName("msg-sessionpw")
    if not rpromptmsgs:
        rpromptmsg = p.createElement("msg-sessionpw")
        rpromptmsg.appendChild(rnew_text)
        pammount.appendChild(rpromptmsg)
    else:
        rpromptmsg = rpromptmsgs[0]
    rpromptmsg.replaceChild(rnew_text, rpromptmsg.childNodes[0])
    
elif sys.argv[1] == '--uninstall':
    volumes = p.getElementsByTagName("volume")
    for volume in volumes:
        attr = volume.getAttribute("path")
        if dev in attr:
            pammount.removeChild(volume)

open(system_auth, 'w').write(p.toxml())

