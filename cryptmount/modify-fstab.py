#!/usr/bin/python

import string
import commands
import sys

fstab = '/etc/fstab'
usage = """modify-fstab --install
             --uninstall"""

if len(sys.argv) < 2:
    print usage
    sys.exit(1)

lines = open(fstab).readlines()
for i, line in enumerate(lines):
    if '/home' in [x.strip() for x in line.split()]:
        break

if i == len(lines):
    sys.exit(0)

if sys.argv[1] == '--install':
    if not line.startswith('#'):
        lines[i] = '#' + line;

elif sys.argv[1] == '--uninstall':
    if line.startswith('#'):
        lines[i] = line[1:];

open(fstab, "w").write(string.join(lines, ''))
