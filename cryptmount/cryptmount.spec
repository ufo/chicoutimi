Name:           cryptmount
Version:        0.3
Release:        1%{?dist}
Summary:        Mount crypted home at login
BuildArch:      noarch

Group:          System Environment/Base
License:        GPLv2
URL:            http://www.agorabox.org
Source0:        %{name}-%{version}.tar.gz
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires:	pam_mount >= 0.49-3 authconfig >= 5.4.4-2 augeas-ext


%description
Mount crypted home at login.


%prep
%setup -q


%build


%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT


%post
authconfig --update --enablemount
python %{_datadir}/cryptmount/modify-pam_mount-conf.py --install


%preun
authconfig --update --disablemount
python %{_datadir}/cryptmount/modify-pam_mount-conf.py --uninstall


%clean
rm -rf $RPM_BUILD_ROOT


%files
%defattr(-,root,root,-)
%doc
%dir %{_datadir}/cryptmount
%{_datadir}/cryptmount


%changelog
* Mon Jan 26 2009 Sylvain Baubeau <sylvain.baubeau@agorabox.org>
Added createcryptdev rc.d/init.d script

* Tue Oct 8 2008 Kevin Pouget <kevin.pouget@agorabox.org>
Initial release
