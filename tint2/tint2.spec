Name:           tint2
Version:        0.8
Release:        1%{?dist}
Summary:        A lightweight X11 desktop panel and task manager

Group:          User Interface/Desktops
License:        GPLv2
URL:            http://code.google.com/p/tint2
Source0:        http://%{name}.googlecode.com/files/%{name}-%{version}.tar.gz
Source1:	tint2.desktop
Patch0:		tint2-clock-and-systray.patch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  imlib2-devel pango-devel cairo-devel libXinerama-devel

%description
tint2 is a simple X11 desktop panel and taskbar intentionally made for
openbox3, but that should work with other window managers.  tint2 is intended
to be unintrusive and light in resource utilization, while following
freedesktop specifications.

%prep
%setup -q
%patch0 -p1 -b .clock-and-systray

%build
%configure
make %{?_smp_mflags}

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}
rm -rf %{buildroot}%{_docdir}
mkdir -p $RPM_BUILD_ROOT/%{_sysconfdir}/xdg/autostart
cp %{SOURCE1} $RPM_BUILD_ROOT/%{_sysconfdir}/xdg/autostart/

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc AUTHORS ChangeLog COPYING README
%dir %{_sysconfdir}/xdg/%{name}/
%config(noreplace) %{_sysconfdir}/xdg/%{name}/tint2rc
%{_bindir}/tint2
%{_mandir}/man1/tint2.1.*
%{_datadir}/tint2
%{_sysconfdir}/xdg/autostart/%{name}.desktop

%changelog
* Wed Dec 30 2009 Antonin Fouques <antonin.fouques@agorabox.org> - 0.8-1
Initial release
